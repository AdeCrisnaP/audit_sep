-- USE audit;

IF OBJECT_ID('tb_audit_alert') IS NOT NULL 
BEGIN
    DROP TABLE tb_audit_alert
END ;

CREATE TABLE tb_audit_alert(
	id TINYINT IDENTITY(1,1) NOT NULL,
	alert1 TINYINT NOT NULL DEFAULT 0,	
	alert2 TINYINT NOT NULL DEFAULT 0,	
	user_nik varchar(100) NOT NULL,
	user_name varchar(100) NOT NULL,
	date_updated smalldatetime NULL,
	date_created SMALLDATETIME NOT NULL CONSTRAINT [alert_create_TimeStamp] DEFAULT (GetDate()),
	is_deleted BIT DEFAULT 0,
	PRIMARY KEY (id)
);


INSERT INTO [audit].[dbo].[tb_audit_alert](alert1,alert2,user_nik,user_name) VALUES (1,7,'5646','Ryan Riadi');


