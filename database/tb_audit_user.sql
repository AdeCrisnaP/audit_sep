-- USE audit;

IF OBJECT_ID('tb_audit_user') IS NOT NULL 
BEGIN
    DROP TABLE tb_audit_user
END ;

CREATE TABLE tb_audit_user(
	id INT IDENTITY(1,1) NOT NULL,
	username VARCHAR(100) NOT NULL,	
	nik VARCHAR(100) NOT NULL,
	email VARCHAR(200) UNIQUE NULL DEFAULT '',	
	role_id TINYINT NOT NULL,
  role_name varchar(100) NOT NULL DEFAULT '',	
	position VARCHAR(100) NOT NULL,	
	password_hash VARCHAR(500) NOT NULL DEFAULT '',
	password_reset_token VARCHAR(500) NOT NULL DEFAULT '',
	auth_key VARCHAR(200) NOT NULL DEFAULT '',
	industry_id VARCHAR(200) NOT NULL,
	location_id VARCHAR(200) NOT NULL,
	region_id VARCHAR(200) NOT NULL,
	business_unit_id VARCHAR(200) NOT NULL,	
	division_id VARCHAR(200) NOT NULL,  
	user_nik VARCHAR(100) NOT NULL,
	user_name varchar(100) NOT NULL,	
	created_at INT NOT NULL DEFAULT 0,
	updated_at INT NOT NULL DEFAULT 0,
	status INT NOT NULL DEFAULT 10,
	date_updated SMALLDATETIME NULL,
	date_created SMALLDATETIME NOT NULL CONSTRAINT [user_create_TimeStamp] DEFAULT (GetDate()),
	is_deleted BIT DEFAULT 0,	
	PRIMARY KEY (id),
	CONSTRAINT fk_user_role_id FOREIGN KEY (role_id) REFERENCES tb_audit_role(id)	,
	CONSTRAINT [user_unique_columns] UNIQUE(nik,industry_id)
);

