-- USE audit;

IF OBJECT_ID('tb_audit_pica_finding_temp') IS NOT NULL 
BEGIN
    DROP TABLE tb_audit_pica_finding_temp
END ;

CREATE TABLE tb_audit_pica_finding_temp(	
	id INT IDENTITY(1,1) NOT NULL,
	repeat_finding VARCHAR(100) NULL DEFAULT '',
	finding VARCHAR (max) NOT NULL DEFAULT '',
	finding_file VARCHAR(300) NOT NULL DEFAULT '',		
	auditor_nik INT NOT NULL,
	auditor_name VARCHAR(100) NOT NULL,
	auditor_position VARCHAR(100) NOT NULL,
	rating_id TINYINT NOT NULL,
	rating_name varchar(100) NOT NULL,
	category_id TINYINT NOT NULL,
	category_name VARCHAR(100) NOT NULL,
	industry_id TINYINT NOT NULL,
	industry_name VARCHAR(100) NOT NULL,
	location_id TINYINT NOT NULL,
	location_name VARCHAR(100) NOT NULL,
	region_id TINYINT NOT NULL,
	region_name VARCHAR(100) NOT NULL,
	business_unit_id TINYINT  NOT NULL,
	business_unit_name VARCHAR(100) NOT NULL,
	division_id TINYINT NOT NULL,
	division_name VARCHAR(100) NOT NULL,
	approval_status varchar(100) NOT NULL,
	date_due SMALLDATETIME NOT NULL,
	date_posted SMALLDATETIME NOT NULL,
	PRIMARY KEY (id)
);

