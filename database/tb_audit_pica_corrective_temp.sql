-- USE audit;

IF OBJECT_ID('tb_audit_pica_corrective_temp') IS NOT NULL 
BEGIN
    DROP TABLE tb_audit_pica_corrective_temp
END ;

CREATE TABLE tb_audit_pica_corrective_temp(
  id INT IDENTITY(1,1) NOT NULL,
	finding_id TINYINT NOT NULL,	
	corrective_action VARCHAR (max) NOT NULL DEFAULT '',
  auditor_nik INT NOT NULL,
	auditor_name VARCHAR(100) NOT NULL,
  pic_nik VARCHAR(100)NOT NULL,
	pic_name VARCHAR(100) NOT NULL,	
  pic_position VARCHAR(100) NOT NULL,	
  mandatory_id TINYINT NOT NULL,
  mandatory_name VARCHAR(10) NOT NULL,	
	industry_id TINYINT NOT NULL,
	industry_name VARCHAR(100) NOT NULL,
	location_id TINYINT NOT NULL,
	location_name VARCHAR(100) NOT NULL,
	region_id TINYINT NOT NULL,
	region_name VARCHAR(100) NOT NULL,
	business_unit_id TINYINT  NOT NULL,
	business_unit_name VARCHAR(100) NOT NULL,
	division_id TINYINT NOT NULL,
	division_name VARCHAR(100) NOT NULL,	
	PRIMARY KEY (id)
);

