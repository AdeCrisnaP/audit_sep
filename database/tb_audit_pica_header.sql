-- USE audit;

IF OBJECT_ID('tb_audit_pica_header') IS NOT NULL 
BEGIN
    DROP TABLE tb_audit_pica_header
END ;

CREATE TABLE tb_audit_pica_header(
	id INT IDENTITY(1,1) NOT NULL,
	number VARCHAR(100) NOT NULL,	
	auditor_nik VARCHAR(100) NOT NULL,  
	auditor_name VARCHAR(100) NOT NULL, 
	auditor_position VARCHAR(100) NOT NULL,
	team_leader_nik VARCHAR(100) NOT NULL,  
	team_leader_name VARCHAR(100) NOT NULL, 
	team_leader_position VARCHAR(100) NOT NULL,
	industry_id TINYINT NOT NULL,
	industry_name VARCHAR(100) NOT NULL,	
	region_id TINYINT NOT NULL,
	region_name VARCHAR(100) NOT NULL,
	business_unit_id TINYINT  NOT NULL,
	business_unit_name VARCHAR(100) NOT NULL,
	pica_file VARCHAR(300) NOT NULL DEFAULT '', 
	project_name VARCHAR(300) NOT NULL,
	date_from SMALLDATETIME NULL,  
	date_to SMALLDATETIME NULL,  
	date_posted SMALLDATETIME NULL,  
	date_updated SMALLDATETIME NULL,
	date_created SMALLDATETIME NOT NULL CONSTRAINT [pica_header_TimeStamp] DEFAULT (GetDate()),
	is_deleted BIT DEFAULT 0,
	PRIMARY KEY (id),
	-- CONSTRAINT fk_pica_auditor_nik FOREIGN KEY (auditor_nik) REFERENCES tb_audit_user(nik),
	CONSTRAINT fk_pica_header_industry_id FOREIGN KEY (industry_id) REFERENCES tb_audit_industry(id),
	CONSTRAINT fk_pica_region_id FOREIGN KEY (region_id) REFERENCES tb_audit_region(id),
	CONSTRAINT fk_pica_business_unit_id FOREIGN KEY (business_unit_id) REFERENCES tb_audit_business_unit(id)	
);

