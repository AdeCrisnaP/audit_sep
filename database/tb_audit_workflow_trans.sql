-- USE audit;

IF OBJECT_ID('tb_audit_workflow_trans') IS NOT NULL 
BEGIN
    DROP TABLE tb_audit_workflow_trans
END ;

CREATE TABLE tb_audit_workflow_trans(
	id INT IDENTITY(1,1) NOT NULL,
	pica_number VARCHAR(100) NOT NULL,
  approval_status VARCHAR(100) NOT NULL,
	from_user_nik VARCHAR(100) NOT NULL,
	from_user_name VARCHAR(100) NOT NULL,
  from_user_position VARCHAR(100) NOT NULL,
  order_number TINYINT NOT NULL DEFAULT 0,
  to_user_nik VARCHAR(100) NOT NULL,
	to_user_name varchar(100) NOT NULL,  
  to_user_position varchar(100) NOT NULL, 
  comment varchar(max) NOT NULL DEFAULT '',
	industry_id VARCHAR(100) NOT NULL,
	location_id VARCHAR(100) NOT NULL,
	region_id VARCHAR(100) NOT NULL,
	business_unit_id VARCHAR(100)  NOT NULL,
	division_id VARCHAR(100) NOT NULL,
	date_updated SMALLDATETIME NULL,
	date_created SMALLDATETIME NOT NULL CONSTRAINT [workflow_trans_create_TimeStamp] DEFAULT (GetDate()),
	is_deleted BIT DEFAULT 0,
	PRIMARY KEY (id),
	-- CONSTRAINT fk_workflow_trans_user_nik FOREIGN KEY (user_nik) REFERENCES tb_audit_user(nik),
	-- CONSTRAINT fk_workflow_trans_industry_id FOREIGN KEY (industry_id) REFERENCES tb_audit_industry(id),
	-- CONSTRAINT fk_workflow_trans_location_id FOREIGN KEY (location_id) REFERENCES tb_audit_location(id),	
	-- CONSTRAINT fk_workflow_trans_region_id FOREIGN KEY (region_id) REFERENCES tb_audit_region(id),
	-- CONSTRAINT fk_workflow_trans_business_unit_id FOREIGN KEY (business_unit_id) REFERENCES tb_audit_business_unit(id),
	-- CONSTRAINT fk_workflow_trans_division_id FOREIGN KEY (division_id) REFERENCES tb_audit_division(id)
);

