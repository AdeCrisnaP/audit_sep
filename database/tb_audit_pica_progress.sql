-- USE audit;

IF OBJECT_ID('tb_audit_pica_progress') IS NOT NULL 
BEGIN
    DROP TABLE tb_audit_pica_progress
END ;

CREATE TABLE tb_audit_pica_progress(
	id INT IDENTITY(1,1) NOT NULL,
  number VARCHAR(100) NOT NULL,	
	finding_id VARCHAR(100) NOT NULL,
  auditor_nik VARCHAR(100) NOT NULL,  
	auditor_name VARCHAR(100) NOT NULL,
  auditor_position VARCHAR(100) NOT NULL,
  pic_nik VARCHAR(100) NOT NULL,  
	pic_name VARCHAR(100) NOT NULL,
  pic_position VARCHAR(100) NOT NULL,
	finding VARCHAR (max) NOT NULL DEFAULT '',
	finding_file VARCHAR(300) NOT NULL DEFAULT '',		
  corrective_id INT NOT NULL,
	corrective_action VARCHAR (max) NOT NULL DEFAULT '',	
  latest_progress VARCHAR (max) NOT NULL DEFAULT '',
  progress_file VARCHAR(300) NOT NULL DEFAULT '',
  status VARCHAR(100) NOT NULL DEFAULT '', 
  lead_time_status VARCHAR(100) NOT NULL DEFAULT '', 
  mandatory_id TINYINT NOT NULL,
  mandatory_name VARCHAR(10) NOT NULL,		
	industry_id TINYINT NOT NULL,
	industry_name VARCHAR(100) NOT NULL,
	location_id TINYINT NOT NULL,
	location_name VARCHAR(100) NOT NULL,
	region_id TINYINT NOT NULL,
	region_name VARCHAR(100) NOT NULL,
	business_unit_id TINYINT  NOT NULL,
	business_unit_name VARCHAR(100) NOT NULL,
	division_id TINYINT NOT NULL,
	division_name VARCHAR(100) NOT NULL,
  order_number TINYINT NOT NULL DEFAULT 0,
  next_nik VARCHAR(100) NOT NULL,  
	date_progress SMALLDATETIME NULL,
	date_due SMALLDATETIME NULL,
  comment VARCHAR (max) NOT NULL DEFAULT '',
	date_updated SMALLDATETIME NULL,
	date_created SMALLDATETIME NOT NULL CONSTRAINT [pica_progress_create_TimeStamp] DEFAULT (GetDate()),
	is_deleted BIT DEFAULT 0,
	PRIMARY KEY (id),
	-- CONSTRAINT fk_pica_details_auditor_nik FOREIGN KEY (auditor_nik) REFERENCES tb_audit_user(nik),
	-- CONSTRAINT fk_pica_details_pic_nik FOREIGN KEY (pic_nik) REFERENCES tb_audit_user(nik),
	CONSTRAINT fk_pica_progress_industry_id FOREIGN KEY (industry_id) REFERENCES tb_audit_industry(id),
	CONSTRAINT fk_pica_progress_location_id FOREIGN KEY (location_id) REFERENCES tb_audit_location(id),	
	CONSTRAINT fk_pica_progress_region_id FOREIGN KEY (region_id) REFERENCES tb_audit_region(id),
	CONSTRAINT fk_pica_progress_business_unit_id FOREIGN KEY (business_unit_id) REFERENCES tb_audit_business_unit(id),
	CONSTRAINT fk_pica_progress_division_id FOREIGN KEY (division_id) REFERENCES tb_audit_division(id)
);

