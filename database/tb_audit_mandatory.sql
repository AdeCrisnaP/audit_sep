-- USE audit;

IF OBJECT_ID('tb_audit_mandatory') IS NOT NULL 
BEGIN
    DROP TABLE tb_audit_mandatory
END ;

CREATE TABLE tb_audit_mandatory(
	id TINYINT IDENTITY(1,1) NOT NULL,
	name varchar(100) UNIQUE NOT NULL,	
	user_nik varchar(100) NOT NULL,
	user_name varchar(100) NOT NULL,
	date_updated smalldatetime NULL,
	date_created SMALLDATETIME NOT NULL CONSTRAINT [mandatory_create_TimeStamp] DEFAULT (GetDate()),
	is_deleted BIT DEFAULT 0,
	PRIMARY KEY (id)
);


