-- USE audit;

IF OBJECT_ID('tb_audit_workflow') IS NOT NULL 
BEGIN
    DROP TABLE tb_audit_workflow
END ;

CREATE TABLE tb_audit_workflow(
	id INT IDENTITY(1,1) NOT NULL,
	order_number TINYINT NOT NULL DEFAULT 0,
	actor_nik VARCHAR(100) NOT NULL,
	actor_name VARCHAR(100) NOT NULL,
  actor_position VARCHAR(100) NOT NULL,
	approval_name varchar(100) NOT NULL,
	approval_status varchar(100) NOT NULL,
	industry_id TINYINT NOT NULL,
	location_id TINYINT NOT NULL,
	region_id TINYINT NOT NULL,
	business_unit_id TINYINT  NOT NULL,
	division_id TINYINT NOT NULL,
	user_nik VARCHAR(100) NOT NULL,
	user_name VARCHAR(100) NOT NULL,
  is_approved TINYINT NOT NULL DEFAULT 0,
	date_updated SMALLDATETIME NULL,
	date_created SMALLDATETIME NOT NULL CONSTRAINT [workflow_create_TimeStamp] DEFAULT (GetDate()),
	is_deleted BIT DEFAULT 0,
	PRIMARY KEY (id),	
	CONSTRAINT fk_workflow_industry_id FOREIGN KEY (industry_id) REFERENCES tb_audit_industry(id),
	CONSTRAINT fk_workflow_location_id FOREIGN KEY (location_id) REFERENCES tb_audit_location(id),	
	CONSTRAINT fk_workflow_region_id FOREIGN KEY (region_id) REFERENCES tb_audit_region(id),
	CONSTRAINT fk_workflow_business_unit_id FOREIGN KEY (business_unit_id) REFERENCES tb_audit_business_unit(id),
	CONSTRAINT fk_workflow_division_id FOREIGN KEY (division_id) REFERENCES tb_audit_division(id)
);

