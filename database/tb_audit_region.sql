-- USE audit;

IF OBJECT_ID('tb_audit_region') IS NOT NULL 
BEGIN
    DROP TABLE tb_audit_region
END ;

CREATE TABLE tb_audit_region(
	id TINYINT IDENTITY(1,1) NOT NULL,
	name varchar(100) NOT NULL,
	industry_id tinyint NOT NULL,
	location_id tinyint NOT NULL,
	user_nik varchar(100) NOT NULL,
	user_name varchar(100) NOT NULL,
	date_updated smalldatetime NULL,
	date_created SMALLDATETIME NOT NULL CONSTRAINT [region_create_TimeStamp] DEFAULT (GetDate()),
	is_deleted bit NOT NULL DEFAULT 0,
	PRIMARY KEY(id),
	CONSTRAINT [fk_industry_id] FOREIGN KEY([industry_id]) REFERENCES [dbo].[tb_audit_industry] ([id]),
	CONSTRAINT [fk_location_id] FOREIGN KEY([location_id]) REFERENCES [dbo].[tb_audit_location] ([id]),
	CONSTRAINT [region_unique_columns] UNIQUE(location_id,industry_id,name)
)