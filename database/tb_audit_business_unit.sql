-- USE audit;

IF OBJECT_ID('tb_audit_business_unit') IS NOT NULL 
BEGIN
    DROP TABLE tb_audit_business_unit
END ;

CREATE TABLE tb_audit_business_unit(
	id TINYINT IDENTITY(1,1) NOT NULL,
	name varchar(100) NOT NULL,
	code varchar(100) NOT NULL,		
	industry_id tinyint NOT NULL,
	location_id tinyint NOT NULL,
	region_id tinyint NOT NULL,
	user_nik varchar(100) NOT NULL,
	user_name varchar(100) NOT NULL,
	date_updated smalldatetime NULL,
	date_created SMALLDATETIME NOT NULL CONSTRAINT [bu_create_TimeStamp] DEFAULT (GetDate()),
	is_deleted BIT DEFAULT 0,
	PRIMARY KEY (id),	
	CONSTRAINT fk_bu_location_id FOREIGN KEY(location_id) REFERENCES [dbo].[tb_audit_location] (id),
	CONSTRAINT fk_bu_industry_id FOREIGN KEY(industry_id) REFERENCES [dbo].[tb_audit_industry] (id),
	CONSTRAINT fk_bu_region_id FOREIGN KEY(region_id) REFERENCES [dbo].[tb_audit_region] (id),
	CONSTRAINT unique_bu_columns UNIQUE (region_id,location_id,industry_id,name,code)
	
);


