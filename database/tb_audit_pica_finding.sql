-- ISA ANSHORI, 29 Mei 2017, Penambahan Kolom Approval
ALTER TABLE [audit].[dbo].[tb_audit_pica_finding]
ADD approval VARCHAR(10) DEFAULT 'Approve';

UPDATE [audit].[dbo].[tb_audit_pica_finding] SET approval = 'Approve';

-- ISA ANSHORI, 26 Mei 2017, Penambahan Kolom is_extended
ALTER TABLE [audit].[dbo].[tb_audit_pica_finding]
ADD is_extended BIT DEFAULT(0);

UPDATE [audit].[dbo].[tb_audit_pica_finding] SET is_extended =0;

-- USE audit;

IF OBJECT_ID('tb_audit_pica_finding') IS NOT NULL 
BEGIN
    DROP TABLE tb_audit_pica_finding
END ;

CREATE TABLE tb_audit_pica_finding(
	id INT IDENTITY(1,1) NOT NULL,	
	number VARCHAR(100) NOT NULL,	
	repeat_finding VARCHAR(100) NULL DEFAULT '',
	finding VARCHAR (max) NOT NULL DEFAULT '',
	finding_file VARCHAR(300) NOT NULL DEFAULT '',	
	extension_file VARCHAR(300) NOT NULL DEFAULT '',	
	auditor_nik VARCHAR(100) NOT NULL,
	auditor_name VARCHAR(100) NOT NULL,	
	auditor_position VARCHAR(100) NOT NULL,
	team_leader_nik VARCHAR(100) NOT NULL,
	team_leader_name VARCHAR(100) NOT NULL,	
	team_leader_position VARCHAR(100) NOT NULL,
	rating_id TINYINT NOT NULL,
	rating_name varchar(100) NOT NULL,
	category_id TINYINT NOT NULL,
	category_name VARCHAR(100) NOT NULL,
	industry_id TINYINT NOT NULL,
	industry_name VARCHAR(100) NOT NULL,
	location_id TINYINT NOT NULL,
	location_name VARCHAR(100) NOT NULL,
	region_id TINYINT NOT NULL,
	region_name VARCHAR(100) NOT NULL,
	business_unit_id TINYINT  NOT NULL,
	business_unit_name VARCHAR(100) NOT NULL,
	division_id TINYINT NOT NULL,
	division_name VARCHAR(100) NOT NULL,	
	date_posted SMALLDATETIME NULL,  
	date_due SMALLDATETIME NULL,
	date_extension SMALLDATETIME NULL,
	date_extension_request SMALLDATETIME NULL,
	status VARCHAR(100) NOT NULL DEFAULT '', 
	lead_time_status VARCHAR(100) NOT NULL DEFAULT '', 
	is_printed BIT DEFAULT 0,
	order_number TINYINT NOT NULL DEFAULT 0,
	next_nik VARCHAR(100) NOT NULL DEFAULT '', 
	coordinator_comment VARCHAR (max) NOT NULL DEFAULT '',
	div_head_comment VARCHAR (max) NOT NULL DEFAULT '',
	comment VARCHAR (max) NOT NULL DEFAULT '',
	supervisor_comment VARCHAR (max) NOT NULL DEFAULT '',
	date_closed SMALLDATETIME NULL,
	date_updated SMALLDATETIME NULL,
	date_created SMALLDATETIME NOT NULL CONSTRAINT [pica_finding_create_TimeStamp] DEFAULT (GetDate()),
	is_deleted BIT DEFAULT 0,
	PRIMARY KEY (id),
	-- CONSTRAINT fk_pica_details_auditor_nik FOREIGN KEY (auditor_nik) REFERENCES tb_audit_user(nik),
	-- CONSTRAINT fk_pica_details_pic_nik FOREIGN KEY (pic_nik) REFERENCES tb_audit_user(nik),
	CONSTRAINT fk_pica_finding_category_id FOREIGN KEY (category_id) REFERENCES tb_audit_category(id),
	CONSTRAINT fk_pica_finding_industry_id FOREIGN KEY (industry_id) REFERENCES tb_audit_industry(id),
	CONSTRAINT fk_pica_finding_location_id FOREIGN KEY (location_id) REFERENCES tb_audit_location(id),	
	CONSTRAINT fk_pica_finding_region_id FOREIGN KEY (region_id) REFERENCES tb_audit_region(id),
	CONSTRAINT fk_pica_finding_business_unit_id FOREIGN KEY (business_unit_id) REFERENCES tb_audit_business_unit(id),
	CONSTRAINT fk_pica_finding_division_id FOREIGN KEY (division_id) REFERENCES tb_audit_division(id)
);



