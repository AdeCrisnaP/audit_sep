<?php

$this->title = 'PICA';
$pica_number = explode('/',$pica['number']);
$pica_ = explode('-',$pica_number[1]);
$lha_number = $pica['lha_number'];
//$lha_number = $pica_number[0].'/LHA-'.$pica_[1].'/'.$pica_number[2].'/'.$pica_number[3].'/'.$pica_number[4];

function getMonth($m){

    switch($m){

        case 1 :
            return 'Januari';
        break;

        case 2 :
            return 'Februari';
        break;

        case 3 :
            return 'Maret';
        break;

        case 4 :
            return 'April';
        break;

        case 5 :
            return 'Mei';
        break;

        case 6 :
            return 'Juni';
        break;

        case 7 :
            return 'Juli';
        break;

        case 8 :
            return 'Agustus';
        break;

        case 9 :
            return 'September';
        break;

        case 10 :
            return 'Oktober';
        break;
            
        case 11 :
            return 'November';
        break;
            
        case 12 :
            return 'Desember';
        break;       

        default :
            return ;
        break;
    }

}

?>



<div class="header">
    <p><b>PICA AUDIT <?= strtoupper($pica['business_unit_name']); ?> <br><?= strtoupper($pica['number']); ?></b></p>
</div>

<div class="info">

    <table>

        <tr><td>Tanggal</td><td>:</td><td><?= date('d',strtotime($pica['date_posted'])).' '.getMonth(date('m',strtotime($pica['date_posted']))).' '.date('Y',strtotime($pica['date_posted'])); ?></td></tr>
        <tr><td>Project</td><td>:</td><td><?= $pica['type_audit']//$pica['project_name']; ?></td></tr>
        <tr><td>Tanggal Audit</td><td>:</td><td><?= date('d',strtotime($pica['date_from'])).' '.getMonth(date('m',strtotime($pica['date_from']))); ?> - <?= date('d',strtotime($pica['date_to'])).' '.getMonth(date('m',strtotime($pica['date_to']))).' '.date('Y',strtotime($pica['date_to'])); ?></td></tr>
        <tr><td>Nomor LHA</td><td>:</td><td><?= $lha_number; ?></td></tr>

    </table>

</div>

<div class="container">

<table class="border-line" style="width:100%">

<tr><th class="border-line" style="width:5%">No.</th><th class="border-line" style="width:15%">Department</th><th class="border-line" style="width:30%">Audit Findings</th><th class="border-line" style="width:30%">Corrective Action</th><th class="border-line" style="width:15%">PIC</th><th class="border-line" style="width:10%">Due Date</th></tr>
<?php foreach( $finding as $index => $value){ ?>


<?php foreach( $value['corrective'] as $index2 => $value2){ ?>
<?php if($index2 ===0){?>
<tr>
<td rowspan="<?= count($value['corrective']); ?>" class="border-line" ><?= $value['no']; ?>.</td>
<td rowspan="<?= count($value['corrective']); ?>" class="border-line"><?= $value['division_name']; ?></td>
<td rowspan="<?= count($value['corrective']); ?>" class="border-line" style="text-align:justify"><?= $value['finding']; ?></td>
<td  class="border-line" style="text-align:justify"><?= $value2['corrective_action']?></td>
<td  class="border-line" style="text-align:center"><?= $value2['pic_position']?></td>
<td rowspan="<?= count($value['corrective']); ?>"  class="border-line" style="text-align:center"><?= substr(getMonth(date('m',strtotime($value['date_due']))),0,3).'-'.substr(date('Y',strtotime($value['date_due'])),2,2); ?></td>
</tr>
<?php }else{?>
<tr>
<td  class="border-line" style="text-align:justify"><?= $value2['corrective_action']?></td>
<td  class="border-line" style="text-align:center"><?= $value2['pic_position']?></td>
</tr>
<?php 
}
$lokasi[] = $value2['location_name'];
?>

<?php } ?>  




<?php } ?>
</table>


</div>

<div class="footer">

<ul>
<?php if(!in_array("Head Office", $lokasi)){ ?>
    <li>
        <p>Dibuat Oleh :<br><br><br><br><br><?= $pica['auditor_name']; ?><br>(Internal Audit Administrator)</p>
    </li>

    <li>
        <p>Mengetahui :<br><br><br><br><br><?= $user1['username']; ?><br>(Head of Internal Audit)</p>
    </li>

    <li>
        <p>Menyetujui :<br><br><br><br><br><?= $user2['username']; ?><br>(COO <?= $pica['industry_name']; ?>)</p>
    </li>
<?php } else { ?> 
    <li>
        <p>Dibuat Oleh :<br><br><br><br><br><?= $pica['auditor_name']; ?><br>(Internal Audit Administrator)</p>
    </li>

    <li>
        <p><br><br><br><br><br><br></p>
    </li>

    <li>
        <p>Menyetujui :<br><br><br><br><br><?= $user1['username']; ?><br>(Head of Internal Audit)</p>
    </li>
<?php } ?>    

</ul>

</div>
