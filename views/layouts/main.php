<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\widgets\Alert;
use yii\helpers\BaseUrl;


$picture = 'style ="background-image:url('.BaseUrl::base().'/img/AuditLogoKM.jpg);   
            background-repeat:no-repeat;background-size:cover;"';

// AREA SEBELUM LOGIN
if ( Yii::$app->user->isGuest) {
$dashboard ='';


// AREA SESUDAH LOGIN
 }else{  $dashboard = 'id="salesdashboard"'; }

$footer ='style="background-color: rgba(255, 255, 255, 0.75);-moz-box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    -webkit-box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);"'; 
 
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <?= Html::csrfMetaTags() ?>
    
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- <meta http-equiv="refresh" content = "1200; url=<?= BaseUrl::toRoute(['site/logout']);?>"> -->
    <link href="<?= BaseUrl::base().'/img/favicon.ico';?>" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <title><?= Html::encode($this->title) ?></title>
    <style>
        
    #wait {
        
    display:    none;
    position:   fixed;
    z-index:    5000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('img/FhHRx.gif') 
                50% 50% 
                no-repeat;
    }
           
    </style>
    <!-- AREA SEBELUM LOGIN -->
    <?php if ( Yii::$app->user->isGuest) { ?>
        <?php $this->head() ?>
    <?php } ?>
    
    <!-- AREA SESUDAH LOGIN -->
    <?php if ( ! Yii::$app->user->isGuest) { ?>    
    <!--[if lt IE 9]>
    <script type="text/javascript" src="<?= BaseUrl::base().'/js/jquery-1.11.3.min.js';?>"></script>
    <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?= BaseUrl::base().'/js/jquery-2.1.4.min.js';?>"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="<?= BaseUrl::base().'/js/globalize.min.js';?>"></script>
    
    <script type="text/javascript" src="<?= BaseUrl::base().'/js/dx.chartjs.js';?>"></script>
    
    <script type="text/javascript" src="<?= BaseUrl::base().'/js/dx.module-widgets-base.js';?>"></script>
    <script type="text/javascript" src="<?= BaseUrl::base().'/js/dx.module-widgets-web.js';?>"></script>
                           
    <script src="<?= BaseUrl::base().'/js/knockout-3.4.0.js';?>"></script>        
    <script src="<?= BaseUrl::base().'/js/dx.all.js';?>"></script>
    <script src="<?= BaseUrl::base().'/js/jszip.min.js';?>"></script>
    <script src="<?= BaseUrl::base().'/js/underscore-1.5.1.min.js';?>"></script>
    <script src="<?= BaseUrl::base().'/js/js.js';?>"></script>

    <link rel="stylesheet" type="text/css" href="<?= BaseUrl::base().'/css/reset.css';?>" />
    <link rel="stylesheet" href="<?= BaseUrl::base().'/css/dx.common.css';?>" />
    <link rel="stylesheet" href="<?= BaseUrl::base().'/css/dx.light.css';?>" />
    <link rel="stylesheet" type="text/css" href="<?= BaseUrl::base().'/css/site2.css';?>" media="screen and (min-width: 740px)" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" media="all" href="<?= BaseUrl::base().'/css/site2.css';?>"/>
    <script type="text/javascript" src="<?= BaseUrl::base().'/scripts/ie8html5.js';?>"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?= BaseUrl::base().'/css/phone.css'; ?>" media="screen and (max-width: 739px)"/>
              
    <link href="<?= BaseUrl::base().'/css/styles.css';?>" rel="stylesheet" />
    <?php } ?>
    
    
    
</head>
<body <?= $picture ?>  <?= $dashboard ?> >

<!-- AREA SEBELUM LOGIN -->
<?php if ( Yii::$app->user->isGuest) { ?>    
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Internal Audit Tracking System',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
            'style' => 'color:#ffffff'
        ],
    ]);
//    $menuItems = [
//        ['label' => 'WBS', 'url' => ['/site/contact']],
//    ];
    if (Yii::$app->user->isGuest) {
        // $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
        $menuItems[] = ['label' => ''];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">       
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer" <?= $footer ?>>
    <div class="container">
        <p class="pull-left">&copy; Internal Audit 2017 </p>

        <p class="pull-right">Powered By ICT Department</p>
    </div>
</footer>

<?php $this->endBody() ?>

<?php } ?>

<!-- AREA SESUDAH LOGIN -->
<?php if ( ! Yii::$app->user->isGuest) { ?>    
       
        <!-- HEADER -->
        <header class="dashboard-header">
        <div class="supervisor-info">
            <!--<div class="avatar"></div>-->
            <div class="name"><?= Yii::$app->user->identity->username; ?></div>
        </div>
        <div class="dashboard-title">
            <div class="date" id="currentDate"><?= Html::a('Logout', ['/site/logout']) ?></div>
            <h1>Internal Audit Tracking System</h1>
        </div>
        </header>
        
        <header class="dashboard-mobile-header tablet-hide">
        <h1>Internal Audit Tracking System</h1>
        </header>
        <!-- HEADER END -->
        
        <!-- SIDE BAR -->
        <section class="dashboard-navigation">
        <ul>

            <li>
                <a href="<?= BaseUrl::home();?>">
                    <div id="inbox" class="navigation-item clear-fix">
                        <div class="icon documents"></div>
                        <div>
                            <h2>Inbox</h2>
                            <span><?= Yii::$app->session['inbox'];?> Messages</span>
                        </div>
                    </div>
                </a>
            </li>
            <?php if(Yii::$app->user->identity->role_name === 'Super Admin'){ ?>
            <li>
                <a href="#Admin">
                    <div id="admin" class="navigation-item clear-fix">
                        <div class="icon channels"></div>
                        <div>
                            <h2><span> Admin</span></h2>
                            <span>Administrative</span>
                        </div>
                    </div>
                </a>
                
                <ul>
                    <li><a href="<?= BaseUrl::toRoute(['site/industry']);?>"> >> Manage Industry</a></li>
                    <li><a href="<?= BaseUrl::toRoute(['site/location']);?>"> >> Manage Location</a></li>
                    <li><a href="<?= BaseUrl::toRoute(['site/rating']);?>"> >> Manage Rating</a></li>
                    <li><a href="<?= BaseUrl::toRoute(['site/category']);?>"> >> Manage Category</a></li>
                    <li><a href="<?= BaseUrl::toRoute(['site/lead-time-status']);?>"> >> Manage Lead Time Status</a></li>
                    <li><a href="<?= BaseUrl::toRoute(['site/status']);?>"> >> Manage Status</a></li>                    
                    <li><a href="<?= BaseUrl::toRoute(['site/role']);?>"> >> Manage Role</a></li>                   
                    <li><a href="<?= BaseUrl::toRoute(['site/region']);?>"> >> Manage Region</a></li>
                    <li><a href="<?= BaseUrl::toRoute(['site/division']);?>"> >> Manage Division</a></li>
                    <li><a href="<?= BaseUrl::toRoute(['site/business-unit']);?>"> >> Manage Business Unit</a></li>                 
                    <li><a href="<?= BaseUrl::toRoute(['site/user']);?>"> >> Manage User</a></li>
                    <li><a href="<?= BaseUrl::toRoute(['site/workflow']);?>"> >> Manage Work Flow</a></li>
                    <li><a href="<?= BaseUrl::toRoute(['site/alert']);?>"> >> Manage Alert</a></li>
                    
                </ul>

            </li>
            <?php } ?>
            
            <?php if(Yii::$app->user->identity->role_name === 'Super Admin' || Yii::$app->user->identity->role_name ==='Admin'){ ?>
            <li class="phone-hide">
                <a href="#Transactions">
                    <div id="transactions" class="navigation-item  clear-fix">
                        <div class="icon transactions"></div>
                        <div>
                            <h2>Transactions</h2>
                            <span>Create a transaction</span>
                        </div>
                    </div>
                </a>
                <ul>
                    <li><a href="<?= BaseUrl::toRoute(['site/pica-create']);?>"> >> Create New PICA</a></li>
                    <li><a href="<?= BaseUrl::toRoute(['site/pica-upload']);?>"> >> Upload Old PICA</a></li>
                    <li><a href="<?= BaseUrl::toRoute(['site/pica-extension']);?>"> >> Request PICA Extension Date</a></li>
                    <li><a href="<?= BaseUrl::toRoute(['site/pica-switch']);?>"> >> Switch PIC PICA</a></li>
                     
                </ul>

            </li>
            <?php } ?>
            
            <li>
                <a href="#Reports">
                    <div id="reports" class="navigation-item  clear-fix">
                        <div class="icon sectors"></div>
                        <div>
                            <h2>Reports</h2>
                            <span>See a report</span>
                        </div>
                    </div>
                </a>
                <ul>
                    <li><a href="<?= BaseUrl::toRoute(['site/pica']);?>"> >> PICA </a></li>

                    <?php if(Yii::$app->user->identity->division_id === '37'){ ?>
                        <li><a href="<?= BaseUrl::toRoute(['site/pica-progress']);?>"> >> PICA Progress Log</a></li>
                        <li><a href="<?= BaseUrl::toRoute(['site/pica-finding']);?>"> >> PICA Finding Database</a></li>
                    <?php } ?>
                    <?php 
                        // edit for remove scoring PIC, Progress App, Guest ayy
                        $role = Yii::$app->user->identity->role_id; 
                        if($role != '4' && $role != '5' && $role != '6' ){ /*PIC, Progress App, Guest */
                        ?>
                        <li><a href="<?= BaseUrl::toRoute(['site/pica-scoring']);?>"> >> PICA Scoring </a></li>
                    <?php } ?>

                </ul>
            </li>          

        </ul> 
        <div class="footer-logo"></div>
    </section>
        <!-- SIDE BAR END -->
        
        <section class="main-content">
            <div id="dashboard-content" class="dashboard-content clear-fix" ><?= $content ?></div>        
        </section>
        
        <div class="screen-size"></div>
        
        <script>
        $(document).ready(function(){            
            $('.dashboard-navigation > ul > li:has(ul) > a > div').addClass( "navigation-item-left-arrow" );
            
            $('.dashboard-navigation > ul > li > a').click(function() {
                                            
                var checkElement = $(this).next();
                                
                $('.dashboard-navigation li').removeClass('active');
                $(this).closest('li').addClass('active');	

                if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                    $('.dashboard-navigation > ul > li:has(ul).active > a > div').removeClass( "navigation-item-down-arrow" );
                    $('.dashboard-navigation > ul > li:has(ul).active > a > div').addClass( "navigation-item-left-arrow" );
                    
                    $(this).closest('li').removeClass('active');
                    checkElement.slideUp('normal');
                }

                if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                    $('.dashboard-navigation > ul > li:has(ul).active > a > div').removeClass( "navigation-item-left-arrow" );
                    $('.dashboard-navigation > ul > li:has(ul).active > a > div').addClass( "navigation-item-down-arrow" );
                    
                    $('.dashboard-navigation ul ul:visible').slideUp('normal');
                    checkElement.slideDown('normal');
                }

                if (checkElement.is('ul')) {
                    return false;
                } else {
                    return true;	
                }
            });
        });            
    </script>

<?php } ?>
<div id="wait"></div>
</body>
</html>
<?php $this->endPage() ?>
