<?php 

use app\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\BaseUrl;

    header('Content-type: application/vnd.ms-excel;');
    header('Content-Disposition: attachment; filename='.$this->title);
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Pragma: no-cache');
?>

<!DOCTYPE html>
<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Sheet 1</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->

    <meta charset="utf-8">
    
    <title><?= Html::encode($this->title) ?></title>
    <style>

        table, th, td {
            
            font-size:12px;
            font-family:'Calibri,Times New Roman';
            
            border-collapse: collapse;
        }

        table,td,th {border:0.5px solid black;}
        
    </style>
   
</head>
<body>

<?= $content ?>


</body>
</html>

