<?php 

use app\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\BaseUrl;
?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <?= Html::csrfMetaTags() ?>
    
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link href="<?= BaseUrl::base().'/img/favicon.ico';?>" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <title><?= Html::encode($this->title) ?></title>
    <style>

        p {
            font-size:12px;
            font-family:'Arial';
        }
    
        .header {
            text-align: center;
            /*border: 3px solid green;*/
            border:none;
        }

        .border-line{
            border: 0.5px solid black;
            border-collapse: collapse;
            padding:5px;
        }

        .container {
            text-align: center;
            border: none;
            margin-top:10px;
        }

        table, th, td {
            
            font-size:12px;
            font-family:'Arial';
            
        }

        .top-left{
            position: absolute;
            left: 0px;
            top: 0px;
        }

        .left{
            position: absolute;
            left: 0px;
        }
       
        .footer ul { 
            list-style: none outside none; 
            margin:0; 
            padding: 0; 
            border:none; 
            text-align:center; 
            padding:3px 0; 
            overflow:hidden;
        }
        
        .footer ul li {
            display:block; 
            width:30%; 
            float:left; 
            margin: 0 2%; 
            background:none;
        }

        .footer {
            text-align: center;
            border: none;
        }



    </style>
   
    
    
    
</head>
<body>
<img src="<?= BaseUrl::base()?>/img/logoKM.jpg >" class="top-left"/>

<?= $content ?>


</body>
</html>

