<?php

$this->title = 'PICA_Finding.xls';

function getMonth($m){

    switch($m){

        case 1 :
            return 'Januari';
        break;

        case 2 :
            return 'Februari';
        break;

        case 3 :
            return 'Maret';
        break;

        case 4 :
            return 'April';
        break;

        case 5 :
            return 'Mei';
        break;

        case 6 :
            return 'Juni';
        break;

        case 7 :
            return 'Juli';
        break;

        case 8 :
            return 'Agustus';
        break;

        case 9 :
            return 'September';
        break;

        case 10 :
            return 'Oktober';
        break;
            
        case 11 :
            return 'November';
        break;
            
        case 12 :
            return 'Desember';
        break;       

        default :
            return ;
        break;
    }

}

?>

<table  style="width:100%">

<tr>
    <th  style="width:5%;background:rgb(217,217,217);">No.</th>
    <th  style="width:auto;background:rgb(217,217,217);">Region</th>
    <th  style="width:auto;background:rgb(217,217,217);">Pabrik</th>
    <th  style="width:auto;background:rgb(217,217,217);">Team Leader</th>
    <th  style="width:auto;background:rgb(217,217,217);">Nomor PICA</th>
    <th  style="width:auto;background:rgb(217,217,217);">Bagian</th>
    <th  style="width:auto;background:rgb(217,217,217);">Finding</th>
    <th  style="width:auto;background:rgb(217,217,217);">Rating</th>
    <th  style="width:auto;background:rgb(217,217,217);">Category</th>
    <th  style="width:auto;background:rgb(217,217,217);">Corrective Action Plan</th>
    <th  style="width:auto;background:rgb(217,217,217);">Latest Progress</th>
    <th  style="width:auto;background:rgb(217,217,217);">Entry Date</th>
    <th  style="width:auto;background:rgb(217,217,217);">Auditor's Comment</th>
    <th  style="width:auto;background:rgb(217,217,217);">Due Date</th>
    <th  style="width:auto;background:rgb(217,217,217);">Status</th>
</tr>

<?php foreach( $finding as $index => $value){ ?>

    <?php foreach( $value['corrective'] as $index2 => $value2){ ?>
        <?php if($index2 ===0){?>
            <tr>
                <td rowspan="<?= count($value['corrective']); ?>" ><?= $value['no']; ?>.</td>
                <td rowspan="<?= count($value['corrective']); ?>"  ><?= $value['region_name']; ?>.</td>
                <td rowspan="<?= count($value['corrective']); ?>" ><?= $value['business_unit_name']; ?></td>
                <td rowspan="<?= count($value['corrective']); ?>" ><?= $value['team_leader_name']; ?></td>
                <td rowspan="<?= count($value['corrective']); ?>" ><?= $value['number']; ?></td>
                <td rowspan="<?= count($value['corrective']); ?>" ><?= $value['division_name']; ?></td>
                <td rowspan="<?= count($value['corrective']); ?>"  style="text-align:justify"><?= $value['finding']; ?></td>
                <td rowspan="<?= count($value['corrective']); ?>" ><?= $value['rating_name']; ?></td>
                <td rowspan="<?= count($value['corrective']); ?>" ><?= $value['category_name']; ?></td>
                <td   style="text-align:justify"><?= $value2['corrective_action']?></td>
                <td   style="text-align:center"><?= $value2['latest_progress']?></td>
                <td   style="text-align:center"><?= $value2['date_progress'];?></td>
                <td rowspan="<?= count($value['corrective']); ?>"   style="text-align:center"><?= $value['comment']; ?></td>
                <td rowspan="<?= count($value['corrective']); ?>"   style="text-align:center"><?= substr(getMonth(date('m',strtotime($value['date_due']))),0,3).'-'.substr(date('Y',strtotime($value['date_due'])),2,2); ?></td>
                <td   style="text-align:center"><?= $value2['status']?></td>
            </tr>
        <?php }else{?>
            <tr>
                <td   style="text-align:justify"><?= $value2['corrective_action']?></td>
                <td   style="text-align:center"><?= $value2['latest_progress']?></td>
                <td   style="text-align:center"><?= $value2['date_progress']?></td>
                <td   style="text-align:center"><?= $value2['status']?></td>
            </tr>
        <?php }?>

    <?php } ?>  

<?php } ?>
</table>




