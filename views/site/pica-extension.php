<?php
use yii\helpers\Url;
use yii\helpers\BaseUrl;
use yii\bootstrap\ActiveForm;

$this->title = 'PICA Extension Date';

?>

<style>
    
    
    #form {
        height: auto;
        width: 98%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
    }

    #number-box{
        width:30%;
        float:left;
        margin-right:10px;
    }

    #number-box{
        margin-left:10px;
        float:left;
    }
    
    #gridContainer-field {
        height:600px;         
        width: 98%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
        
    }
    
    #gridContainer {
        height:600px;         
        width: 100%;        
    }
    
    #button2 {
        float: right;
        margin: 20px 20px 0 0;
    }
       
    #form-field{
        height:150px;         
        width: auto;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px;
        background-color:#ffffff;
        margin-top:5px;
    }
    
    #popup2{
        display: none;
        height:100%;
        width:100%;
        position:fixed;
        z-index: 3;
        top:0;
        left:0;
        right:0;
        background-color:rgb(0,0,0);
        background-color: rgba(0,0,0,0.9);
        overflow-y:hidden;
        transition: 0.5s;
    }
    
    .popup2{
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        height:35%;
        z-index: 3;
        width:35%;
        background-color: #ffffff;
        padding:10px;
        margin:auto;
        
    }
    
    #file-uploader{
        display:block;
        float:left;
        width:30%;
    }
    
    #file-list{
        display:block;
        float:left;
        width:100%;
    }
    
    #button5 {
        float: right;
        margin: 20px 0 0 0;
    }
    
    #button6 {
        float: right;
        margin: 20px 10px 0 0 ;
    }
    
    #button7 {
        float: right;
        margin: 20px 40px 0 0;
    }
       
    /* End Preview */
    
</style>

<script id="jsCode">

    $(function (){
        
        var tanggal = new Date();
        var location_ = <?= $location ?>;
        var division = <?= $division ?>;
        var category = <?= $category?>;
        var rating = <?= $rating ?>;
        var pica_finding={};
        var pica_number='';
        var isfile =false;
                
    
    $("#number-box").dxTextBox({
        placeholder:"PICA Number",
        showClearButton: true,
        attr: { name: "number-box" }
    }).dxValidator({
        validationRules: [{ type: "required",message: "PICA Number is required" }]
    });
    
    $("#search").dxButton({
        dataField: "PICA Number",
        text: "Search",
        type: "success",
        onClick: search
    });
    
    $("#gridContainer").dxDataGrid({
        dataSource: {},
        columnAutoWidth: true,
        allowColumnResizing: true, 
        wordWrapEnabled:true,
        showRowLines:true,
        showColumnLines: true,
        rowAlternationEnabled: true,
        selection: {
            mode: "single"
        },
        scrolling: {
            mode: "virtual"
        }, 
        hoverStateEnabled: true,
        editing: {
            mode: "row",
            allowUpdating: true,
            allowDeleting: false,
            allowAdding: false
        }, 
        columns: [  
            {
                dataField: "no.",
                width: 40,
                caption: "No.",
                allowEditing:false,
                allowSearch:false,
                cellTemplate: function(cellElement, cellInfo) {
                    cellElement.text(cellInfo.row.rowIndex+1);
                }
        
            },
            {
                    dataField: "finding",
                    caption: "Finding",
                    allowEditing:false,
                    width:400,
                    editCellTemplate: function(container, cellInfo) {
                        $('<div>').appendTo(container).dxTextArea({
                            text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                            onValueChanged: function(e) {
                                cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                            }
                        });    
                    } 
            },
            {
                dataField: "location_id",
                caption: "Location",
                width: 115,
                allowEditing:false,
                validationRules: [
                    { type: "required" }, 
            
                ],            
                lookup: {
                    dataSource: location_,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },     
            {
                dataField: "division_id",
                caption: "Division",
                width: 230,
                validationRules: [{ type: "required" }],
                allowEditing:false,
                lookup: {
                    valueExpr: 'id',
                    displayExpr: 'name',
                    dataSource: division
                    
                }
            }, 
            {
                dataField: "category_id",
                caption: "Category",
                width: 250,
                allowEditing:false,
                validationRules: [
                    { type: "required" }, 
            
                ],
                lookup: {
                    dataSource:category,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },
            {
                dataField: "rating_id",
                caption: "Rating",
                width: 110,
                allowEditing:false,
                validationRules: [
                    { type: "required" }, 
            
                ],
                lookup: {
                    dataSource: rating,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },  
            {
                dataField: "date_due",
                caption:"Due Date",
                dataType: "date",
                format: "dd/MM/yyyy",
                allowEditing:false,
                editorOptions: { 
                    formatString: "dd/MM/yyyy",
                },
                validationRules: [
                    { type: "required" }, 
            
                ],
                width: 125
            }, 
            {
                dataField: "date_extension",
                caption:"Extension Date",
                dataType: "date",
                format: "dd/MM/yyyy",
                allowEditing:true,
                editorOptions: { 
                    formatString: "dd/MM/yyyy",
                },
                validationRules: [
                    { type: "required" }, 
            
                ],
                width: 125
            }, 
            {
                dataField: "date_extension_request",
                caption:"Requested Date",
                dataType: "date",
                format: "dd/MM/yyyy",
                allowEditing:true,
                editorOptions: { 
                    formatString: "dd/MM/yyyy",
                },
                validationRules: [
                    { type: "required" }, 
            
                ],
                width: 125
            },  
            {
                dataField: "extension_file",
                allowEditing:false,
                width: 115,
                cellTemplate: function(container, options) {
                    console.log(options.value);
                    var uploads = "<?= BaseUrl::base();?>" + "/uploads/"+ pica_number.replace("/", "_")+"/"; 
                    container.append($("<a style='text-decoration: underline;color:blue;'  href='"+uploads + options.value + "' download>" + options.value + "</a>"));
                }
            },          
        ], 
        onRowClick:function(e){
         
            if(e.data !== null){
                
                showFile(e.data);
                
            }
           
        },
        onRowUpdating: function(e) {
            
            if(e.newData !== null){
                
                var date_extension = new Date(e.newData.date_extension ? e.newData.date_extension : e.key.date_extension);
                var date_extension_request = new Date(e.newData.date_extension_request ? e.newData.date_extension_request : e.key.date_extension_request);
                
                $.ajax({
                    type: 'POST',
                    url: '<?= Url::to(['pica-finding/update-extension']);?>',
                    data:{
                        id:e.key.id,
                        date_extension:(date_extension.getMonth()+1)+"-"+date_extension.getDate()+"-"+date_extension.getFullYear(),
                        date_extension_request:(date_extension_request.getMonth()+1)+"-"+date_extension_request.getDate()+"-"+date_extension_request.getFullYear(),
                    },
                    success: function(data){
                    
                        if(data.status==='fail'){
                            alert(data.message); 
                            //location.reload(); 
                        }else{
                            
                            showFile(data.pica);
                            
                        }
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
          
        }
    
    });
    
    
    $("#file-uploader").dxFileUploader({
        selectButtonText: "Select File",
        labelText: "",
        accept: "*.*",   
        name:'file',
        uploadMode: "useForm",
    });

    $("#file-list").dxDataGrid({
        dataSource: {},
        columnAutoWidth: true,
        allowColumnResizing: true, 
        showRowLines:true,
        showColumnLines: true,
        rowAlternationEnabled: true,
        selection: {
            mode: "single"
        },
        scrolling: {
            mode: "virtual"
        }, 
        hoverStateEnabled: true,
        editing: {
            mode: "row",
            allowUpdating: false,
            allowDeleting: true,
            allowAdding: false
        }, 
        columns: [
            {
                dataField: "extension_file",
                caption: "File Name",
                width: 115
            },     
        ],
        onRowRemoving: function(e) {
           
           if(e.data !==null){
               
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica-finding/delete-extension-file']);?>',
                    data: {
                        id:e.key.id, 
                    },
                    success: function(data){
                    
                        if(data.status==='fail'){
                            
                            alert(data.message); 

                        }else{
                            
                            $("#file-list").hide();
                            $("#file-uploader").show();
                            
                            $("#gridContainer").dxDataGrid("instance").option("dataSource",[]);
                            $("#gridContainer").dxDataGrid("instance").option("dataSource",data.findings);
                            
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
          
        },
        onRowClick: function(e){
            
           var uploads = "<?= BaseUrl::base();?>" + "/uploads/"; 
           
           window.open(uploads + e.data.finding_file, 'File', 'width=screen.width,height=screen.height');
        }
    }).dxDataGrid("instance");
    
    $("#button5").dxButton({
        text: "Save",
        type: "success", 
        onClick: saveFile
    });
    
    $("#button6").dxButton({
        text: "Close",
        type: "default", 
        onClick: closeFile
    });
    
    $("#button7").dxButton({
        text: "Send Email",
        type: "success", 
        onClick: validateAndSubmit
    });
    
    function search(){
    
        var result = $("#number-box").dxValidator("instance").validate();

        if(result.isValid) {
        
            var number = $("#number-box").dxTextBox("instance").option("value");
            pica_number = number ;
        
            if(number !==null){
            
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica-finding/search']);?>',
                    data: {
                        
                       number:number
                                
                    },
                    beforeSend: function() { $('#wait').show(); },
                    complete: function() { $('#wait').hide(); },
                    success: function(data){
                        
                        if(data.status==='fail'){
                            
                           alert(data.message);

                        }else{
                        
                            $("#gridContainer").dxDataGrid("instance").option("dataSource",[]);
                            $("#gridContainer").dxDataGrid("instance").option("dataSource",data.pica);
                            
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                    
                    }
                });  
            }
        }
    
    }
    
    function showFile(data) { 

        pica_finding = data; 

        if(data.extension_file.length < 3){
                    
              $("#file-uploader").show();
              $("#file-list").hide();
              isfile= false;
                    
         }else{
                    
             $("#file-uploader").hide();
             $("#file-list").show();
             $("#file-list").dxDataGrid("instance").option("dataSource",[]);
             $("#file-list").dxDataGrid("instance").option("dataSource",[data]);  
             isfile=true;
                    
        }

        $("#popup2").show();
        
    }

    function saveFile(){

        $file = $("#file-uploader").dxFileUploader("instance").option("value");

        if($file !== null){

            var form = document.forms.namedItem("fileinfo");
            oData = new FormData(form);
            oData.append("id", pica_finding.id);
            oData.append("isfile", isfile);

            $.ajax({
                type: 'POST',
                url: '<?= Url::to(['pica-finding/file-extension']);?>',
                data: oData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){

                    if(data.status !=='fail'){

                        $("#file-uploader").hide();
                        $("#file-list").show();

                        $("#gridContainer").dxDataGrid("instance").option("dataSource",[]);
                        $("#gridContainer").dxDataGrid("instance").option("dataSource",data.findings);

                        $("#file-list").dxDataGrid("instance").option("dataSource",[data.finding]);

                    }else{

                        alert(data.message);
                    }

                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);   
                }
            });

        }else{

            alert('Silakan Pilih File.');

        }
        
    }

    function closeFile(){
        
        $("#popup2").hide();
    
    }
               
    function validateAndSubmit () {

        var pica = $("#gridContainer").dxDataGrid("instance").option("dataSource");
        var order_number = 7;
        
        $.ajax({
            type: 'POST',
            url: '<?= Url::to(['pica/approve']);?>',
            dataType:"json",
            data: { 
                pica:pica,
                order_number:order_number
            },
            beforeSend: function() { $('#wait').show(); },
            complete: function() { $('#wait').hide(); },
            success: function(data){
                    
                alert(data.message);
                // location.reload();
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);   
                // location.reload();
            }
        });
              
    }
    
    
});
</script>

<div class="content containerPlaceholder">
    <div class="title "><h1>PICA Extension Date</h1></div>
    <div class="pane dx-theme-desktop"> 
        
        <div id="form">
            <div id="number-box"></div>
            <div id="search"></div>
        </div>
        
        <div id="gridContainer-field">
            <div id="gridContainer"></div>
        </div>
        
        <div class="dx-field">
           <div id="button7"></div> 
        </div>  

        <div id="popup2">
            
            <div class="popup2">
                
                <div id="form-field">
                    <form enctype="multipart/form-data" method="post" name="fileinfo">
                   
                        <div id="file-uploader"></div>
                        <div id="file-list"></div>

                    </form>  
                </div>
                          
                <div id="button5"></div> 
                
                <div id="button6"></div> 
                
            </div>
            
        </div>
         
    </div>
</div>

