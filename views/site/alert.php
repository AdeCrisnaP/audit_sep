<?php
use yii\helpers\Url;

$this->title ='Alert';
?>

<style>    
   
#gridContainer {
    height: 200px;
    width: 100%;
    border-color: #EFEFEF;
    border-width: 2px;
    border-style: solid;
    padding:7px 7px 10px 7px;
    background-color:#ffffff;
    margin-top:5px;
}

#button1 {
    float: right;
    margin-top:20px;
    
}

</style>

<script id="jsCode">
    $(function ()
    {      
        
        var data = <?= $alert ?>;

        $("#button1").dxButton({
            text: "Send Email",
            type: "success", 
            onClick: function(){

                $.ajax({
                    type: 'POST',
                    url: '<?= Url::to(['alert/manual']);?>',
                    beforeSend: function() { $('#wait').show(); },
                    complete: function() { $('#wait').hide(); },
                    success: function(data){
                        alert(data.message);
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        // location.reload();  
                    }
                }); 


            }
        });

        $("#gridContainer").dxDataGrid({
            dataSource: data,
            hoverStateEnabled: true,
            showRowLines:true,
            rowAlternationEnabled:true,
            searchPanel: {
                visible: true
            },
            export: {
                enabled: true,
                fileName: "Industry"
            },  
            allowColumnReordering: true,
            allowColumnResizing: true,
            paging: {
                pageSize: 10
            },
            editing: {
                mode: "row",
                allowUpdating: true,
                allowDeleting: false,
                allowAdding: false
            },    
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 15],
                showInfo: true
            },
            onRowUpdating: function(e) {
    
                $.ajax({
                    type: 'POST',
                    url: '<?= Url::to(['alert/update']);?>',
                    data: {
                        alert1:e.newData.alert1 ? e.newData.alert1 : e.key.alert1,
                        alert2:e.newData.alert2 ? e.newData.alert2 : e.key.alert2,
                        id:e.key.id
                    },
                    beforeSend: function() { $('#wait').show(); },
                    complete: function() { $('#wait').hide(); },
                    success: function(data){
                    
                        alert(data.message); 
                
                        location.reload();
                    },
                    error: function(xhr, textStatus, error){
                        
                        alert(xhr.statusText);
                        
                        location.reload();  
                    
                //DI BAWAH INI JANGAN DIHAPUS UNTUK DEBUG      
                //console.log(xhr.statusText);
                //console.log(textStatus);
                //console.log(error);
                    }
                });

            },
            columns: [
                {
                    dataField: "no",
                    width: 40,
                    caption: "No.",
                    allowEditing:false,
                    allowSearch:false,
                    cellTemplate: function(cellElement, cellInfo) {
                        cellElement.text(cellInfo.row.rowIndex+1);
                    }
        
                },
                {
                    dataField: "alert1",
                    width: 130,
                    caption: "Alert 1 (H-Day)",
                    allowEditing:true,
                },
                {
                    dataField: "alert2",
                    width: 130,
                    caption: "Alert 2 (H-Day)",
                    allowEditing:true,
                },
                {
                    dataField: "user_name",
                    width: 115,
                    caption: "Author",
                    allowEditing:false,
                },
                {
                    dataField: "date_updated",
                    width: 100,
                    dataType: "date",
                    allowEditing:false,
                    caption: "Last Send"
                }
            ]
        });

    });
</script>
                       
        
        
        <div class="content containerPlaceholder">
            <div class="title "><h1>Emaill Alert</h1></div>
            <div class="pane dx-theme-desktop">   
                                          
                        
                <div id="gridContainer"></div>  
                <div id="button1"></div>   
                        
                      
            </div>
        </div>
        
        