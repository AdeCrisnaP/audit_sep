<?php
use yii\helpers\Url;
use yii\helpers\BaseUrl;

$this->title = 'PICA Finding Report';

$user =  Yii::$app->user->identity;

?>

<style>

    <?php if($user->division_id === '37'){ ?>
     #form {
        height: auto;
        width: 100%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
    }

    #button3 {
        float: right;
        
    }
    <?php } ?>
       
    #gridContainer {
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        height:450px;         
        width: 100%;  
        margin-bottom: 24px;
    }

    #button2 {
        float: right;
        margin-left:10px;
    }

    #button1 {
        float: right;
        margin-left:10px;
    }


    
    
</style>
        
<script id="jsCode">
                
    $(function (){
       
        var pica = <?= $data;?>;
        var rating = <?= $rating;?>;
        var industry = <?= $industry ?>;
        var location_ = <?= $location ?>;
        var division = <?= $division ?>;
        var region = <?= $region ?>;
        var business_unit = <?= $business_unit ?>;
        var teamLeader = <?= $teamLeader ?>;  
        var category = <?= $category ?>;    
        
        var pica_number='';

        <?php if($user->division_id === '37'){ ?>

        $("#form").dxForm({
            colCount: 2,
            items: [
                {
                    itemType: "group",
                    items: [
                        {
                            dataField: "PICA_Number",
                            editorType: "dxTextBox"
                        }, 
                        {
                            dataField: "TeamLeader",
                            editorType: "dxSelectBox",  
                            editorOptions:{
                                items: teamLeader,                                
                                displayExpr: "username",
                                valueExpr: "nik",
                                onValueChanged: function (e) {},
                                onOpened: function(e){}
                            }
                        }, 
                        {
                            dataField: "Industry",
                            editorType: "dxSelectBox",                            
                            editorOptions:{ 
                                items: industry,                                
                                displayExpr: "name",
                                valueExpr: "id",
                                onValueChanged: function (e) {},
                                onOpened: function (e){}
                            }
                        },          
                        {
                            dataField: "Region",
                            editorType: "dxSelectBox",
                            editorOptions: { 
                                items: region,
                                displayExpr: "name",
                                valueExpr: "id",
                                onValueChanged: function (e) {
                                
                                    // if(e.value !==null){
                                        
                                    //     var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                    //     businessUnitEditor.option("dataSource", []);
                                    
                                    // }
             
                                },
                                onOpened: function(e){
                                
                                    // var filteredRegion = [];
                                        
                                    // var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                                    // var industry_id = $("#form").dxForm("instance").option("formData").Industry;            
                                
                                    // filteredRegion = DevExpress.data.query(region).filter([["industry_id", "=", industry_id]]).toArray();
                                    
                                    // regionEditor.option("dataSource", filteredRegion);  
                                }
                            }
                        }
                    
                    ]
                },
                {
                    itemType: "group",
                    items: [
                        {
                            dataField: "BusinessUnit",
                            editorType: "dxSelectBox",
                            editorOptions: { 
                                items: business_unit,
                                displayExpr: "name",
                                valueExpr: "id",
                                onValueChanged: function (e) {
                                
                                    // if(e.value !==null){
                                        
                                    //     var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                                    //     divisionEditor.option("dataSource", []);
                                    
                                    // }
             
                                },
                                onOpened: function(e){
                                
                                    // var filteredBusinessUnit = [];
                                       
                                    // var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                    // var industry_id = $("#form").dxForm("instance").option("formData").Industry;                                                
                                    // var region_id = $("#form").dxForm("instance").option("formData").Region;  
    
                                    // filteredBusinessUnit = DevExpress.data.query(business_unit).filter([["industry_id", "=", industry_id],"and",["region_id", "=", region_id]]).toArray();
                                    
                                    // businessUnitEditor.option("dataSource", filteredBusinessUnit);  
                                }
                            }
                        }, 
                        {
                            dataField: "Division",
                            editorType: "dxSelectBox",                            
                            editorOptions:{ 
                                items: division,                                
                                displayExpr: "name",
                                valueExpr: "id",
                                onOpened: function(e){
                                
                                    // var filteredDivision = [];
                                       
                                    // var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                                    // var industry_id = $("#form").dxForm("instance").option("formData").Industry;                                                
                                    // var location_id = $("#form").dxForm("instance").option("formData").Location;  
    
                                    // filteredDivision = DevExpress.data.query(division).filter([["industry_id", "=", industry_id],"and",["location_id", "=", location_id]]).toArray();
                                    
                                    // divisionEditor.option("dataSource", filteredDivision);  
                                }
                            
                            }
                        }, 
                        {
                            dataField: "Category",
                            editorType: "dxSelectBox",                            
                            editorOptions:{ 
                                items: category,                                
                                displayExpr: "name",
                                valueExpr: "id",
                            }
                        },
                        {
                            dataField: "Rating",
                            editorType: "dxSelectBox",                            
                            editorOptions:{ 
                                items: rating,                                
                                displayExpr: "name",
                                valueExpr: "id",
                            }
                        }   
                    ]
                }
            ]
        });

        <?php if( intval($user->role_id) !== 6){ ?>

        $("#button3").dxButton({
            text: "Export",
            type: "success", 
            onClick: exportExcel
        });

        <?php } ?>

        $("#button2").dxButton({
            text: "Search",
            type: "success", 
            onClick: search
        });

         $("#button1").dxButton({
            text: "Reset",
            type: "danger", 
            onClick: function(){

                // $("#form").dxForm("instance").resetValues();
                location.reload();  

            }
        });

        <?php } ?>



        $("#gridContainer").dxDataGrid({
            dataSource: pica,
            hoverStateEnabled: true,
            showRowLines:true,
            rowAlternationEnabled:true,
            searchPanel: {
                visible: true
            },
            export: {
                enabled: false,
                fileName: "PICA"
            },
            wordWrapEnabled:true,
            allowColumnReordering: true,
            allowColumnResizing: true,
            paging: {
                pageSize: 15
            },
            editing: {
                mode: "row",
                allowUpdating: false,
                allowDeleting: false,
                allowAdding: false
            },  
            columns: [  
                {
                    dataField: "no",                                    
                    caption: "No.",
                    width:40
                 },
                 {
                    dataField: "number",
                    caption: "PICA Number", 
                    width:220
                 }, 
                 {
                    dataField: "team_leader_name",
                    caption: "Team Leader", 
                    width:220
                 }, 
                 {
                    dataField: "repeat_finding",
                    caption: "Repeat Finding", 
                    width:220
                 }, 
                 {
                    dataField: "finding",
                    caption: "Finding",
                    allowEditing:false,
                    width:400,
                    editCellTemplate: function(container, cellInfo) {
                        $('<div>').appendTo(container).dxTextArea({
                            text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                            onValueChanged: function(e) {
                                cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                            }
                        });    
                    } 
                },
                {
                    dataField: "comment",
                    caption: "Auditor's Comment",
                    allowEditing:false,
                    width:400,
                    editCellTemplate: function(container, cellInfo) {
                        $('<div>').appendTo(container).dxTextArea({
                            text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                            onValueChanged: function(e) {
                                cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                            }
                        });    
                    } 
                },
                {
                    dataField: "lead_time_status",
                    caption: "Lead Time Status", 
                    width:120
                },
                {
                    dataField: "days",
                    caption: "Over Due (days)", 
                    width:120
                },
                {
                    dataField: "status",
                    caption: "Status", 
                    width:120
                },
                {
                    dataField: "category_name",
                    caption: "Category",
                    width:120,
                },
                {
                    dataField: "finding_file",
                    caption: "LHA File",
                    width:100,
                    cellTemplate: function(container, options) {
                        var uploads = "<?= BaseUrl::base();?>" + "/uploads/"; 
                        container.append($("<a style='text-decoration: underline;color:blue;' href='"+uploads + options.value + "' download>" + options.value + "</a>"));
                    }
                },
                {
                    dataField: "rating_name",
                    caption: "Rating",
                    width:70
                },
                {
                    dataField: "date_posted",
                    dataType: "date",
                    caption: "Posting Date",
                    format: "dd/MM/yyyy",
                    editorOptions: { 
                        formatString: "dd/MM/yyyy",
                    },
                    width:120
                },                            
                {
                    dataField: "date_due",
                    dataType: "date",
                    caption: "Due Date",
                    allowEditing:true,
                    format: "dd/MM/yyyy",
                    editorOptions: { 
                        formatString: "dd/MM/yyyy",
                    },
                    width:120
                },
                {
                    dataField: "industry_name",
                    caption: "Industry", 
                    width:120
                }, 
                {
                    dataField: "region_name",
                    caption: "Region", 
                    width:120
                }, 
                {
                    dataField: "business_unit_name",
                    caption: "Business Unit", 
                    width:120
                }, 
                {
                    dataField: "location_name",
                    caption: "Location", 
                    width:120
                },     
                {
                    dataField: "division_name",
                    caption: "Division",
                    width:120
                }, 
                            
            ], 
            masterDetail: {
                enabled: true,
                template: function(container, options) {                                 
                    container.addClass("internal-grid-container");
                    $("<div>")
                    .addClass("internal-grid")
                    .dxDataGrid({
                        width:'auto',
                        height:"auto",
                        columnAutoWidth: true,
                        allowColumnResizing: true, 
                        showRowLines:true,  
                        wordWrapEnabled:true,
                        dataSource: options.data.corrective_action, 
                        scrolling: {
                            mode: "virtual"
                        }, 
                        scrolling: {
                            mode: "virtual"
                        },
                        editing: {
                            mode: "row",
                            allowUpdating: false,
                            allowDeleting: false,
                            allowAdding: false
                        }, 
                        columns: [
                            {
                                dataField: "no",                                    
                                caption: "No.", 
                                width:40,
                                allowEditing:false,
                                cellTemplate: function(cellElement, cellInfo) {
                                    cellElement.text(cellInfo.row.rowIndex+1);
                                }
                            },
                            {
                                dataField: "location_name",
                                caption: "Location", 
                                width:120,
                                allowEditing:false,
                            },     
                            {
                                dataField: "division_name",
                                caption: "Division",
                                width:120,
                                allowEditing:false,
                            },
                            {
                                dataField: "pic_position",
                                caption: "PIC",   
                                width:100,
                                allowEditing:false,
                            },                                           
                            {
                                dataField: "corrective_action",
                                caption: "Corrective Action Plan",
                                width:240,
                                allowEditing:false,
                                editCellTemplate: function(container, cellInfo) {
                                    $('<div>').appendTo(container).dxTextArea({
                                        text: cellInfo.data.corrective_action.replace(/<br>/g,"\n"),
                                        onValueChanged: function(e) {
                                            cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                        }
                                    });    
                                }
                            },
                            {
                                dataField: "latest_progress",                                
                                caption: "Latest Progress",
                                allowEditing:true,
                                width:240,
                                editCellTemplate: function(container, cellInfo) {
                                    $('<div>').appendTo(container).dxTextArea({
                                        text: cellInfo.data.latest_progress.replace(/<br>/g,"\n"),
                                        onValueChanged: function(e) {
                                            cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                        }
                                    });    
                                }
                            },
                            {
                                dataField: "progress_file",
                                caption: "Progress File",
                                width:170,
                                allowEditing:false,
                                cellTemplate: function(container, options) {
                                    var uploads = "<?= BaseUrl::base();?>" + "/uploads/"; 
                                    //var name = options.value? options.value.split("/")[1]:"";
                                    var name = " ";
                                    if(options.value!=" ") { name = options.value.split("/")[1]; }else { name = " ";}
                                    container.append($("<a style='text-decoration: underline;color:blue;'  href='"+uploads + options.value + "' download>" + name + "</a>"));
                                }
                            },
                            {
                                dataField: "mandatory_name",                                
                                caption: "Mandatory",
                                allowEditing:false,
                                width:70,
                            },
                            {
                                dataField: "status",                                
                                caption: "Status",
                                allowEditing:false,
                                width:60,
                            },
                            {
                                dataField: "lead_time_status",                                
                                caption: "Lead Time Status",
                                allowEditing:false,
                                width:120,
                            },
                            {
                                dataField: "days",                                
                                caption: "Over Due Days",
                                allowEditing:false,
                                width:110,
                            },
                            {
                                dataField: "date_progress",                                
                                caption: "Progress Entry",
                                dataType:"date",
                                allowEditing:false,
                                width:120,
                            }  
                        ],
                        onRowClick:function(e){
                            filePreview();
                        },
                        onRowUpdating: function(e) { 
                                                                                     
                            $.ajax({
                                type: 'POST',
                                url: '<?= Url::to(['pica-corrective/update']);?>',
                                data: {
                                    progress: e.newData.latest_progress ? e.newData.latest_progress : e.key.latest_progress,
                                    id:e.key.id,                                                     
                                },
                                beforeSend: function() { $('#wait').show(); },
                                complete: function() { $('#wait').hide(); },
                                success: function(data){
                    
                                    if(data.status==='fail'){

                                        alert(data.message); 

                                    }else{
                                                        
                                        filePreview();
                                    }
                                                    
                                },
                                error: function(xhr, textStatus, error){
                                    alert(xhr.statusText);
                                    location.reload();  
                    
                                }
                            });
                                                        
                        },
                    }).appendTo(container);
                }
            }
        
        });
    
        function search(){

            var number = $("#form").dxForm("instance").option("formData").PICA_Number;                                                
            var leader = $("#form").dxForm("instance").option("formData").TeamLeader;  
            var industry_id = $("#form").dxForm("instance").option("formData").Industry;                                                
            var region_id = $("#form").dxForm("instance").option("formData").Region;  
            var bu_id = $("#form").dxForm("instance").option("formData").BusinessUnit;
            var division_id = $("#form").dxForm("instance").option("formData").Division;
            var category_id = $("#form").dxForm("instance").option("formData").Category;
            var rating_id = $("#form").dxForm("instance").option("formData").Rating;

            $.ajax({
                type: 'POST',
                url: '<?= Url::to(['pica-finding/get-all']);?>',
                data: {

                    number:number,
                    leader:leader,
                    industry:industry_id,
                    region:region_id,
                    business_unit:bu_id,
                    division:division_id,
                    category:category_id,
                    rating:rating_id

                },
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){
                    
                    if(data.status==='fail'){

                        alert(data.message); 

                    }else{

                        pica = data.pica;
                        $("#gridContainer").dxDataGrid("instance").option("dataSource",[]);
                        $("#gridContainer").dxDataGrid("instance").option("dataSource",data.pica); 

                    }
                                                    
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);
                    // location.reload();  
                    
                }
            });
        }

        function exportExcel(){
       
            var number = $("#form").dxForm("instance").option("formData").PICA_Number; 
            var leader = $("#form").dxForm("instance").option("formData").TeamLeader;  
            var industry_id = $("#form").dxForm("instance").option("formData").Industry;                                                
            var region_id = $("#form").dxForm("instance").option("formData").Region;  
            var bu_id = $("#form").dxForm("instance").option("formData").BusinessUnit;
            var division_id = $("#form").dxForm("instance").option("formData").Division;
            var category_id = $("#form").dxForm("instance").option("formData").Category;
            var rating_id = $("#form").dxForm("instance").option("formData").Rating;
        
            var url = '<?= Url::to(['excel/finding']);?>'+'&industry='+industry_id+'&region='+region_id+'&business_unit='+bu_id+'&number='+number+'&leader='+leader+'&division='+division_id+'&category='+category_id+'&rating='+rating_id;

            $(location).attr('href', url);
        }

           
});

</script>
        
<div class="content containerPlaceholder">
    <div class="title"><h1>PICA Finding Report</h1></div>
             
        <div class="pane dx-theme-desktop">
                
            <div id="form"></div>

            <br>
            <div id="button1"></div><div id="button2"></div><div id="button3"></div>  
            <br><br><br><br>
  
            <div id="gridContainer"></div>
             
             
        </div>
            
</div>
   