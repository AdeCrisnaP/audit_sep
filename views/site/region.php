<?php
use yii\helpers\Url;

$this->title='Region';
?>

<style>    
   
#gridContainer {
    height: 730px;
    width: 100%;
    border-color: #EFEFEF;
    border-width: 2px;
    border-style: solid;
    padding:7px 7px 10px 7px;
    background-color:#ffffff;
    margin-top:5px;
}

</style>

<script id="jsCode">
    $(function ()
    {      
        
        var region = <?= $data ?>;
        var industry = <?= $industry ?>;
        var location_ = <?= $location ?>;

        $("#gridContainer").dxDataGrid({
            dataSource: region,
            hoverStateEnabled: true,
            showRowLines:true,
            rowAlternationEnabled:true,
            searchPanel: {
                visible: true
            },
            export: {
                enabled: true,
                fileName: "Region"
            },  
            allowColumnReordering: true,
            allowColumnResizing: true,
            paging: {
                pageSize: 15
            },
            editing: {
                mode: "row",
                allowUpdating: true,
                allowDeleting: true,
                allowAdding: true
            },    
//            pager: {
//                showPageSizeSelector: true,
//                allowedPageSizes: [10, 15],
//                showInfo: true
//            },
            onInitNewRow: function(e) {
        
                var dataGrid = $('#gridContainer').dxDataGrid('instance');
                    e.data.no = dataGrid.totalCount() + 1;
        
                    e.data.user_name='<?= Yii::$app->user->identity->username; ?>';

                    e.data.date_created= '<?= date('Y-m-d h:i:s');?>';
        
            },
        onRowInserting: function(e) { 
            
            $.ajax({
                type: 'POST',
                url: '<?= Url::to(['region/create']);?>',
                data: {
                   name:e.data.name,
                   industry_id:e.data.industry_id,
                   location_id:e.data.location_id,                                     
                },
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){
                    
//                    if(data.status==='fail'){
                        alert(data.message); 
//                    }
                    location.reload();
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);
                    location.reload();  
                    
                    //DI BAWAH INI JANGAN DIHAPUS UNTUK DEBUG      
                    //console.log(xhr.statusText);
                    //console.log(textStatus);
                    //console.log(error);
                }
            }); 
            
    },
    onRowUpdating: function(e) {
    
        $.ajax({
            type: 'POST',
            url: '<?= Url::to(['region/update']);?>',
            data: {
                name:e.newData.name ? e.newData.name : e.key.name,
                industry_id:e.newData.industry_id ? e.newData.industry_id : e.key.industry_id,
                location_id: e.newData.location_id ? e.newData.location_id : e.key.location_id,
                id:e.key.id
            },
            beforeSend: function() { $('#wait').show(); },
            complete: function() { $('#wait').hide(); },
            success: function(data){
                    
//                if(data.status==='fail'){
                    alert(data.message); 
                    
//                } 
                location.reload(); 
                
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);
                location.reload();  
                    
                //DI BAWAH INI JANGAN DIHAPUS UNTUK DEBUG      
                //console.log(xhr.statusText);
                //console.log(textStatus);
                //console.log(error);
            }
        });

    },
    onRowRemoving: function(e) {
    
        $.ajax({
            type: 'POST',
            url: '<?= Url::to(['region/delete']);?>',
            data: {
                id:e.key.id
            },
            beforeSend: function() { $('#wait').show(); },
            complete: function() { $('#wait').hide(); },
            success: function(data){
                    
//                if(data.status==='fail'){
                    alert(data.message);  
                     
//                } 
                
                location.reload();
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);
                location.reload(); 
                    
                //DI BAWAH INI JANGAN DIHAPUS UNTUK DEBUG      
                //console.log(xhr.statusText);
                //console.log(textStatus);
                //console.log(error);
            }
        });

    },
    columns: [
    {
        dataField: "no",
        width: 40,
        caption: "No.",
        allowEditing:false,
        allowSearch:false,
        cellTemplate: function(cellElement, cellInfo) {
            cellElement.text(cellInfo.row.rowIndex+1);
        }
        
    },
    {
            dataField: "industry_id",
            caption: "Industry",
            width: 125,
            validationRules: [
                { type: "required" }, 
            
            ],
            lookup: {
                dataSource: industry,
                displayExpr: "name",
                valueExpr: "id"
            }
    },
    {
            dataField: "location_id",
            caption: "Location",
            width: 125,
            validationRules: [
                { type: "required" }, 
            
            ],
            lookup: {
                dataSource: location_,
                displayExpr: "name",
                valueExpr: "id"
            }
    },
    {
        dataField: "name",
        width: 210,
        validationRules: [
            { type: "required" }, 
            {
                type: "pattern",
                message: 'Numbers are not allowed',
                pattern:"[a-zA-Z]"
                
            }
        ],
        caption: "Region Name"
    },  
    {
        dataField: "id",
        width: 40,
        caption: "ID",
        allowEditing:false,
        allowSearch:false
        
    },
    {
        dataField: "user_name",
        width: 115,
        caption: "Author",
        allowEditing:false,
    },
    {
            dataField: "date_created",
            width: 100,
            dataType: "date",
            allowEditing:false,
            caption: "Created Date"
        }
    ]}
        
    );

    });
</script>
                       
        
        
        <div class="content containerPlaceholder">
            <div class="title "><h1>Region</h1></div>
            <div class="pane dx-theme-desktop">   
                                          
                        
                        <div id="gridContainer"></div>     
                        
                        
                       
                        
                      
            </div>
        </div>
        
        