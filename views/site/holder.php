<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Holder';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
  
.audit-holder {
    overflow: hidden;
    background-color: rgba(255, 255, 255, 0.75);
    width: 370px;
    float: left;
    height: 450px;
    border: 1px solid #e3e3e3;
    -moz-box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    -webkit-box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    margin:0px;
    padding:12px;
    border-radius: 10px;
}

.title-holder {
    text-align: center;    
    height: 45px;
}

.title-holder p {
    color: #f88e1d;
    font-size: 18px;    
    font-weight: bold;
}
    
</style>   

<div class="audit-holder">
    <div class="title-holder">
        <p><?= Html::encode($this->title) ?></p>
    </div>

    
            <?php $form = ActiveForm::begin(['id' => 'holder-form']); ?>
                
                <?= $form->field($model, 'industry')->dropdownList($items); ?>

                <?= $form->field($model, 'nik')->textInput(['autofocus' => true, 'disabled' => 'disabled']) ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div> 

            <?php ActiveForm::end(); ?>
       
</div>


