<?php
use yii\helpers\Url;

$this->title='Workflow';

?>

<style>    
   
    #gridContainer {
        height: 320px;
        width: 100%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
    }
    
    #form {
        height: auto;
        width: 100%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
    }

    #button {
        float: right;
        margin-top:10px;
    }
    
</style>
        
<script id="jsCode">
    
$(function (){
  
    
    var industry = <?= $industry ?>;
    var location_ = <?= $location ?>;
    var region = <?= $region ?>;  
    var business_unit = <?= $business_unit ?>;
    var division = <?= $division ?>;
    var workflow = <?= $workflow ?>;
    var users = <?= $users ?>; 
    
    $("#form").dxForm({
        colCount: 2,
        items: [
            {
                itemType: "group",
                caption: "Area",
                items: [
                    {
                        dataField: "Industry",
                        editorType: "dxSelectBox",                            
                        editorOptions:{ 
                            items: [],                                
                            displayExpr: "name",
                            valueExpr: "id",
                            onValueChanged: function (e) { 
                                    
                                    if(e.value !== null){
                                        
                                        var locationEditor = $("#form").dxForm("instance").getEditor("Location");
                                        locationEditor.option("dataSource", []);
                                        
                                        var locationEditor = $("#form").dxForm("instance").getEditor("Location");
                                        locationEditor.option("value", null);
                                        
                                        var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                                        regionEditor.option("dataSource", []);
                                    
                                        var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                        businessUnitEditor.option("dataSource", []);
                                    
                                        var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                                        divisionEditor.option("dataSource", []);
                                                                                
                                        var progressSubmitterEditor = $("#form").dxForm("instance").getEditor("ProgressSubmitter");
                                        progressSubmitterEditor.option("dataSource", []);
                                                    
                                        var progressApproverEditor = $("#form").dxForm("instance").getEditor("ProgressApprover");
                                        progressApproverEditor.option("dataSource", []);
                                        
                                        var datax = $("#form").dxForm("instance").option("formData");            
         
                                        $("#form").dxForm("instance").option("value", datax);

                                    }            
                                },
                            onOpened: function (e){
                                    
                                var industryEditor = $("#form").dxForm("instance").getEditor("Industry");
                                industryEditor.option("dataSource", industry);
                                
                            }
                        },
                        validationRules: [{
                                type: "required",
                                message: "Industry is required"
                        }]
                    },                
                    {
                        dataField: "Location",
                        editorType: "dxSelectBox",
                        editorOptions: { 
                            items: [],
                            displayExpr: "name",
                            valueExpr: "id",
                            onValueChanged: function (e) {
                                    
                                if(e.value !== null){  
                                        
                                    var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                                    regionEditor.option("dataSource", []);
                                    
                                    var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                    businessUnitEditor.option("dataSource", []);
                                    
                                    var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                                    divisionEditor.option("dataSource", []);   
                                        
                                    var progressSubmitterEditor = $("#form").dxForm("instance").getEditor("ProgressSubmitter");
                                    progressSubmitterEditor.option("dataSource", []);
                                                    
                                    var progressApproverEditor = $("#form").dxForm("instance").getEditor("ProgressApprover");
                                    progressApproverEditor.option("dataSource", []);
                                    
                                    var datax = $("#form").dxForm("instance").option("formData");            
         
                                    $("#form").dxForm("instance").option("value", datax);
         
                                }
                            },
                            onOpened: function (e){
                                    
                                var locationEditor = $("#form").dxForm("instance").getEditor("Location");
                                locationEditor.option("dataSource", location_);
                            }
                        },
                        validationRules: [{
                            type: "required",
                            message: "Location is required"
                        }]
                    },                
                    {
                        dataField: "Region",
                        editorType: "dxSelectBox",
                        editorOptions: { 
                            items: [],
                            displayExpr: "name",
                            valueExpr: "id",
                            onValueChanged: function (e) {
                                
                                if(e.value !==null){
                                        
                                    var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                    businessUnitEditor.option("dataSource", []);
                                    
                                    var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                                    divisionEditor.option("dataSource", []);
                                        
                                    var progressSubmitterEditor = $("#form").dxForm("instance").getEditor("ProgressSubmitter");
                                    progressSubmitterEditor.option("dataSource", []);
                                                    
                                    var progressApproverEditor = $("#form").dxForm("instance").getEditor("ProgressApprover");
                                    progressApproverEditor.option("dataSource", []);
                                        
                                    var datax = $("#form").dxForm("instance").option("formData");            
         
                                    $("#form").dxForm("instance").option("value", datax);

                                }
             
                            },
                            onOpened: function(e){
                                
                                var filteredRegion = [];
                                        
                                var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                                var industry_id = $("#form").dxForm("instance").option("formData").Industry;            
                                var location_id = $("#form").dxForm("instance").option("formData").Location;
                                
                                filteredRegion = DevExpress.data.query(region).filter([["industry_id", "=", industry_id],"and",["location_id", "=", location_id]]).toArray();
                                    
                                regionEditor.option("dataSource", filteredRegion);  
                            }
                        },                            
                        validationRules: [{
                            type: "required",
                            message: "Region is required"
                        }]
                    },
                    {
                        dataField: "BusinessUnit",
                        editorType: "dxSelectBox",
                        editorOptions: { 
                            items: [],
                            displayExpr: "name",
                            valueExpr: "id",
                            onValueChanged: function (e) {                                                                    
                                                         
                                    if(e.value !== null){                                    
                                        
                                        var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                                        divisionEditor.option("dataSource", []);
                                        
                                        var progressSubmitterEditor = $("#form").dxForm("instance").getEditor("ProgressSubmitter");
                                        progressSubmitterEditor.option("dataSource", []);
                                                    
                                        var progressApproverEditor = $("#form").dxForm("instance").getEditor("ProgressApprover");
                                        progressApproverEditor.option("dataSource", []);
                                        
                                        var datax = $("#form").dxForm("instance").option("formData");            
         
                                        $("#form").dxForm("instance").option("value", datax);

                                    }
             
                                },
                                onOpened: function(e){
                                
                                    var filteredBusinessUnit = [];
                                       
                                    var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                    var industry_id = $("#form").dxForm("instance").option("formData").Industry;            
                                    var location_id = $("#form").dxForm("instance").option("formData").Location;
                                    var region_id = $("#form").dxForm("instance").option("formData").Region;  
    
                                    filteredBusinessUnit = DevExpress.data.query(business_unit).filter([["industry_id", "=", industry_id],"and",["location_id", "=", location_id],"and",["region_id", "=", region_id]]).toArray();
                                    
                                    businessUnitEditor.option("dataSource", filteredBusinessUnit);  
                                }
                            },
                            validationRules: [{
                                type: "required",
                                message: "Business Unit is required"
                            }]
                        },                
                    {
                        dataField: "Division",
                        editorType: "dxSelectBox",
                        editorOptions: { 
                            items: [],
                            displayExpr: "name",
                            valueExpr: "id",
                            onValueChanged: function (e) {
                                    
                                if(e.value !== null){ 
                                     
                                    var business_unit_id = $("#form").dxForm("instance").getEditor("BusinessUnit").option("selectedItem").id;
                                        
                                    $.ajax({
                                        type: 'POST',
                                        url: '<?= Url::to(['work-flow/get-all']);?>',
                                        data: {
                                            division_id:e.selectedItem.id,
                                            business_unit_id:business_unit_id
                                        },
                                        beforeSend: function() { $('#wait').show(); },
                                        complete: function() { $('#wait').hide(); },
                                        success: function(data){
                    
                                            if(data.workflow !==0){setWorkflow(data);}
                                                    
                                        },
                                        error: function(xhr, textStatus, error){
                                            alert(xhr.statusText);
                                        }
                                    }); 
                                }                             
                                    
                            },
                            onOpened: function(e){
                                
                                    var filteredDivision = [];
                                       
                                    var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                                    var industry_id = $("#form").dxForm("instance").option("formData").Industry;            
                                    var location_id = $("#form").dxForm("instance").option("formData").Location;
                                                                                                        
                                    filteredDivision = DevExpress.data.query(division).filter([["industry_id", "=", industry_id],"and",["location_id", "=", location_id]]).toArray();
                                    
                                    divisionEditor.option("dataSource", filteredDivision);  
                            }
                        },
                        validationRules: [{
                                type: "required",
                                message: "Division is required"
                        }]
                    }                    
                ]
            },
            {
                itemType: "group",
                caption: "Work Flow",
                items: [                    
                    {
                        dataField: "PICA_Submitter",                        
                        editorType: "dxSelectBox",
                        editorOptions: { 
                            items: [],
                            displayExpr: "username",
                            placeholder:"IA Administrator",
                            valueExpr: "nik",
                            onValueChanged: function (e) {
                                if(e.value !== null){
                                    
                                }
                            },
                            onOpened: function (e) {
                                    
                                // Set PICA Submitter
                                var picaSubmitterEditor = $("#form").dxForm("instance").getEditor("PICA_Submitter");
                                var filteredPICASubmitter = [];                                        
                                    
                                filteredPICASubmitter = DevExpress.data.query(users).filter([["role_name", "=", "Admin"]]).toArray();
                                      
                                picaSubmitterEditor.option("dataSource", filteredPICASubmitter);
                                    
                            }
                                
                        },
                        validationRules: [{
                            type: "required",
                            message: "PICA Submitter is required"
                        }]
                    },                    
                    {
                        dataField: "PICA_Approver",
                        editorType: "dxSelectBox",
                        editorOptions: { 
                            items: [],
                            displayExpr: "username",                                
                            valueExpr: "nik",    
                            placeholder:"IA Coordinator",
                            onValueChanged: function (e) {
                                if(e.value !== null){
                                    
                                }
                            },
                            onOpened: function (e) {
                                
                                // Set PICA Approver                               
                                var picaApproverEditor = $("#form").dxForm("instance").getEditor("PICA_Approver");                                        
                                var filteredPicaApprover = []; 
                                                                                                               
                                filteredPicaApprover = DevExpress.data.query(users).filter([["role_name", "=", "IA Coordinator"]]).toArray();
                                                            
                                picaApproverEditor.option("dataSource", filteredPicaApprover);                    
                                
                            }
                        },
                        validationRules: [{
                            type: "required",
                            message: "PICA Approve is required"
                        }]
                    },                    
                    {
                        dataField: "PICA_Approver_2",
                        editorType: "dxTagBox",
                        editorOptions: { 
                            items: [],
                            displayExpr: "username",                                
                            valueExpr: "nik",    
                            placeholder:"IA Div Head",
                            onValueChanged: function (e) {
                                if(e.value !== null){
                                    
                                }
                            },
                            onOpened: function (e) {
                                
                                // Set PICA Approver 
                                var pica_approver_nik = $("#form").dxForm("instance").option("formData").PICA_Approver;            
                                                                
                                var picaApproverEditor = $("#form").dxForm("instance").getEditor("PICA_Approver_2");                                        
                                var filteredPicaApprover = []; 
                                                                                                               
                                filteredPicaApprover = DevExpress.data.query(users).filter([["role_name", "=", "IA Division Head"],"and",["nik", "<>", pica_approver_nik]]).toArray();
                                                            
                                picaApproverEditor.option("dataSource", filteredPicaApprover);                    
                                
                            }
                        },
                        validationRules: [{
                            type: "required",
                            message: "PICA Approve is required"
                        }]
                    },                    
                    {
                        dataField: "ProgressSubmitter",
                        editorType: "dxSelectBox",
                        placeholder:"PIC",
                        editorOptions: { 
                            items: [],
                            displayExpr: "username",
                            valueExpr: "nik",
                            placeholder:"PIC",
                            onValueChanged: function (e) {   
                                
                                if(e.value !==null){
                                       
                                        
                                }
                            },
                            onOpened: function (e) {
                                    
                                // Set Progress Submitter
                                var progressSubmitterEditor = $("#form").dxForm("instance").getEditor("ProgressSubmitter");                                        
                                var filteredProgressSubmitter = []; 
                                        
                                var division_id = $("#form").dxForm("instance").option("formData").Division;  
                                var business_unit_id = $("#form").dxForm("instance").option("formData").BusinessUnit;
                                var region_id = $("#form").dxForm("instance").option("formData").Region; 
                                var industry_id = $("#form").dxForm("instance").option("formData").Industry; 
                                  
                                  
                                filteredProgressSubmitter = DevExpress.data.query(users).filter([["industry_id", "=",industry_id],"and",["region_id", "=",region_id],"and",["business_unit_id", "=",business_unit_id],"and",["business_unit_id", "=",business_unit_id],"and",["division_id", "=",division_id],"and",["role_name", "=", "PIC"]]).toArray();
                                                      
                                progressSubmitterEditor.option("dataSource", filteredProgressSubmitter);                                        
                                    
                            }
                        },
                        validationRules: [{
                            type: "required",
                            message: "Progress Submitter is required"
                        }]
                    },                    
                    {
                        dataField: "ProgressApprover",
                        editorType: "dxSelectBox",                            
                        editorOptions: { 
                            items: [],
                            displayExpr: "username",
                            valueExpr: "nik",
                            placeholder:"Div. Head / Dir. Ops",
                            onValueChanged: function (e) {
                                    
                                if(e.value !==null){
                                        
                                }
                            },                                
                            onSelectionChanged: function (e) {
                                    
                                if(e.value !==null){
                                                    
                                }
                            },
                            onOpened: function (e) {
                                
                                // Set Progress Approver                               
                                var progressApproverEditor = $("#form").dxForm("instance").getEditor("ProgressApprover");                                        
                                var filteredProgressApprover = [];
                                
                                var division_id = $("#form").dxForm("instance").option("formData").Division;  
                                var business_unit_id = $("#form").dxForm("instance").option("formData").BusinessUnit;
                                var region_id = $("#form").dxForm("instance").option("formData").Region; 
                                var industry_id = $("#form").dxForm("instance").option("formData").Industry; 
                                var location_id = $("#form").dxForm("instance").option("formData").Location; 
                                
                                if(location_id===1){
                                    
                                    filteredProgressApprover = DevExpress.data.query(users).filter([["industry_id", "=",industry_id],"and",["region_id", "=",region_id],"and",["business_unit_id", "=",business_unit_id],"and",["division_id", "=",division_id],"and",["role_name", "=", "Progress Approver"]]).toArray();
                                
                                }else{
                                    
                                    filteredProgressApprover = DevExpress.data.query(users).filter([["industry_id", "=",industry_id],"and",["region_id", "=",region_id],"and",["business_unit_id", "=",business_unit_id],"and",["division_id", "contains",division_id],"and",["role_name", "=", "Progress Approver"]]).toArray();
                                }                                       
                                progressApproverEditor.option("dataSource", filteredProgressApprover);                    
                                
                            }
                        },
                        validationRules: [{
                            type: "required",
                            message: "Progress Approve is required"
                        }]
                    },
                    {
                        dataField: "FinalApprover",
                        editorType: "dxTagBox",                            
                        editorOptions: { 
                            items: [],
                            displayExpr: "username",
                            valueExpr: "nik",
                            placeholder:"IA Coordinator",
                            onValueChanged: function (e) {
                                    
                                if(e.value !==null){
                                        
                                }
                            },                                
                            onSelectionChanged: function (e) {
                                    
                                if(e.value !==null){
                                        
                                }
                            },
                            onOpened: function (e) {
                                
                                // Set PICA Approver                               
                                
                                var finalApproverEditor = $("#form").dxForm("instance").getEditor("FinalApprover");                                        
                                var filteredFinalApprover = []; 
                                                                                                               
                                filteredFinalApprover = DevExpress.data.query(users).filter([["role_name", "=", "IA Coordinator"],"OR",["role_name", "=", "Super Admin"]]).toArray();
                                                            
                                finalApproverEditor.option("dataSource", filteredFinalApprover);                    
                                    
                            }
                        },
                        validationRules: [{
                            type: "required",
                            message: "Final Approve is required"
                        }]
                    }                        
                ]
            }   
        ]
    });
    
    $("#form").dxForm("instance").validate();    

    $("#gridContainer").dxDataGrid({
        dataSource: workflow,
        showRowLines:true,
        hoverStateEnabled: true,
        allowColumnReordering: true,
        allowColumnResizing: true,
        selection: {
            mode: "single"
        },        
        searchPanel: {
            visible: true
        },   
        paging: {
            pageSize: 5
        },
        columns: [
            {
                dataField: "no",
                width: 40,
                caption: "No.",
                allowEditing:false,
                allowSearch:false
            },
            {
                dataField: "industry_id",
                caption: "Industry",
                width: 125,
                lookup: {
                    dataSource: industry,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },
            {
                dataField: "location_id",
                caption: "Location",
                width: 125,
                lookup: {
                    dataSource: location_,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },
            {
                dataField: "region_id",
                caption: "Region",
                width: 125,
                lookup: {
                    dataSource: region,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },
            {
                dataField: "business_unit_id",
                caption: "Business Unit",
                width: 125,
                lookup: {
                    dataSource: business_unit,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },
            {
                dataField: "division_id",
                caption: "Division",
                width: 125,
                lookup: {
                    dataSource: division,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },
            
        ],
        onSelectionChanged: function (selectedItems) {
            var data = selectedItems.selectedRowsData[0];
            setArea(data);
        }

    });

    $("#button").dxButton({
        text: "Submit",
        type: "success", 
        onClick: validateAndSubmit
    }); 
    
    function validateAndSubmit () {
        var result = $("#form").dxForm("instance").validate();  
        if(result.isValid) {createWorkflow();}else{alert('Silakan Lengkapi Data');}
    }
    
    function createWorkflow(){
        
        var dataApproval = [];
        
        var industryEditor = $("#form").dxForm("instance").getEditor("Industry");
        var industry_id = industryEditor._options.selectedItem.id;
        var industry_name = industryEditor._options.selectedItem.name;
            
        var locationEditor = $("#form").dxForm("instance").getEditor("Location");
        var location_id = locationEditor._options.selectedItem.id;
        var location_name = locationEditor._options.selectedItem.name;
            
        var regionEditor = $("#form").dxForm("instance").getEditor("Region");
        var region_id = regionEditor._options.selectedItem.id;
        var region_name = regionEditor._options.selectedItem.name;
            
        var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
        var business_unit_id = businessUnitEditor._options.selectedItem.id;
        var business_unit_name = businessUnitEditor._options.selectedItem.name;

        var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
        var division_id = divisionEditor._options.selectedItem.id;
        var division_name = divisionEditor._options.selectedItem.name;           
                
        var picaSubmitterEditor = $("#form").dxForm("instance").getEditor("PICA_Submitter");
        var user_nik = picaSubmitterEditor._options.selectedItem.nik;
        var filteredUser = DevExpress.data.query(users).filter(["nik", "=", user_nik]).toArray();        
        var user_position = filteredUser[0]['position']; 
        var user_name = picaSubmitterEditor._options.selectedItem.username;
        var approval_name = 'PICA Submitter';
        var approval_status = 'PICA Submitted';
                
        dataApproval.push({approval_status:approval_status,approval_name:approval_name,user_name:user_name,user_nik:user_nik,user_position:user_position,division_name:division_name, division_id:division_id, business_unit_name:business_unit_name, business_unit_id:business_unit_id, region_name:region_name, region_id:region_id, industry_id: industry_id, industry_name: industry_name, location_id:location_id,location_name:location_name });                                          
                
        var picaApproverEditor = $("#form").dxForm("instance").getEditor("PICA_Approver");
        user_nik = picaApproverEditor._options.selectedItem.nik;
        filteredUser = DevExpress.data.query(users).filter(["nik", "=", user_nik]).toArray();
        user_position = filteredUser[0]['position']; 
        user_name = picaApproverEditor._options.selectedItem.username;
        approval_name = 'PICA Approver';
        approval_status = 'PICA Approved';
            
        dataApproval.push({approval_status:approval_status,approval_name:approval_name,user_name:user_name,user_nik:user_nik,user_position:user_position,division_name:division_name, division_id:division_id, business_unit_name:business_unit_name, business_unit_id:business_unit_id, region_name:region_name, region_id:region_id, industry_id: industry_id, industry_name: industry_name, location_id:location_id,location_name:location_name });                                          
        
        var picaApproverEditor2 = $("#form").dxForm("instance").option('formData').PICA_Approver_2;
        user_nik = picaApproverEditor2.join();
        
        var operands = [];
                                
        picaApproverEditor2.forEach(function (i) {
            operands.push(["nik", "=", i])
        });
        
        var criteria = [];
                                
        operands.forEach(function (i) {
            if (criteria.length === 0) {
                criteria.push(i)
            }
            else {
                criteria.push("OR");
                criteria.push(i)
            }
        });
        
        var usr_name = [];
        var usr_position = [];
        
        var filteredUsers =[];
        
        filteredUsers = DevExpress.data.query(users).filter(criteria).toArray();
        filteredUsers.forEach(function (i) {
            
            usr_name.push(i.username);
            usr_position.push(i.position);
            
        });            
        
        user_name = usr_name.join();
        user_position = usr_position.join();
        approval_name = 'PICA Approver 2';
        approval_status = 'PICA Approved';
        
        dataApproval.push({approval_status:approval_status,approval_name:approval_name,user_name:user_name,user_nik:user_nik,user_position:user_position,division_name:division_name, division_id:division_id, business_unit_name:business_unit_name, business_unit_id:business_unit_id, region_name:region_name, region_id:region_id, industry_id: industry_id, industry_name: industry_name, location_id:location_id,location_name:location_name });                                          
        
        var progressSubmitterEditor = $("#form").dxForm("instance").getEditor("ProgressSubmitter");
        user_nik = progressSubmitterEditor._options.selectedItem.nik;
        filteredUser = DevExpress.data.query(users).filter(["nik", "=", user_nik]).toArray();
        user_position = filteredUser[0]['position']; 
        user_name = progressSubmitterEditor._options.selectedItem.username;
        approval_name = 'Progress Submitter';
        approval_status = 'Progress Submitted';
            
        dataApproval.push({approval_status:approval_status,approval_name:approval_name,user_name:user_name,user_nik:user_nik,user_position:user_position,division_name:division_name, division_id:division_id, business_unit_name:business_unit_name, business_unit_id:business_unit_id, region_name:region_name, region_id:region_id, industry_id: industry_id, industry_name: industry_name, location_id:location_id,location_name:location_name });                                          
            
        var progressApproverEditor = $("#form").dxForm("instance").getEditor("ProgressApprover");
        user_nik = progressApproverEditor._options.selectedItem.nik;
        filteredUser = DevExpress.data.query(users).filter(["nik", "=", user_nik]).toArray();
        user_position = filteredUser[0]['position']; 
        user_name = progressApproverEditor._options.selectedItem.username;
        approval_name = 'Progress Approver';
        approval_status = 'Progress Approved';
            
        dataApproval.push({approval_status:approval_status,approval_name:approval_name,user_name:user_name,user_nik:user_nik,user_position:user_position,division_name:division_name, division_id:division_id, business_unit_name:business_unit_name, business_unit_id:business_unit_id, region_name:region_name, region_id:region_id, industry_id: industry_id, industry_name: industry_name, location_id:location_id,location_name:location_name });                                          
            
        var finalApproverEditor = $("#form").dxForm("instance").option('formData').FinalApprover;
        user_nik = finalApproverEditor.join();
        
        operands = [];
                                
        finalApproverEditor.forEach(function (i) {
            operands.push(["nik", "=", i])
        });
        
        criteria = [];
                                
        operands.forEach(function (i) {
            if (criteria.length === 0) {
                criteria.push(i);
            }
            else {
                criteria.push("OR");
                criteria.push(i);
            }
        });
        
        usr_name = [];
        usr_position = [];
        
        filteredUsers=[];
        
        filteredUsers = DevExpress.data.query(users).filter(criteria).toArray();
        filteredUsers.forEach(function (i) {
            
            usr_name.push(i.username);
            usr_position.push(i.position);
            
        });            
        
        user_name = usr_name.join();
        user_position = usr_position.join();
        approval_name = 'Final Approver';
        approval_status = 'Final Approved';
            
        dataApproval.push({approval_status:approval_status,approval_name:approval_name,user_name:user_name,user_nik:user_nik,user_position:user_position,division_name:division_name, division_id:division_id, business_unit_name:business_unit_name, business_unit_id:business_unit_id, region_name:region_name, region_id:region_id, industry_id: industry_id, industry_name: industry_name, location_id:location_id,location_name:location_name });                                          
                                    
        $.ajax({
                type: 'POST',
                url: '<?= Url::to(['work-flow/create']);?>',
                data: {
                   data:dataApproval
                },
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){
//                    if(data.status==='fail'){
                        alert(data.message);                        
//                    }
                    location.reload(); 
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);
                    location.reload();  
                }
        });
    }
    
    function setArea(data){
                                              
        var industryEditor = $("#form").dxForm("instance").getEditor("Industry");
        industryEditor.option("dataSource", industry);
        industryEditor.option("value", parseInt(data.industry_id));
                                                           
        var locationEditor = $("#form").dxForm("instance").getEditor("Location");
        locationEditor.option("dataSource", location_);
        locationEditor.option("value", parseInt(data.location_id));
                                                    
        var regionEditor = $("#form").dxForm("instance").getEditor("Region");
        regionEditor.option("dataSource", region);
        regionEditor.option("value", parseInt(data.region_id));
        
        
        var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
        businessUnitEditor.option("dataSource", business_unit);
        businessUnitEditor.option("value", parseInt(data.business_unit_id));
        
                                                    
        var regionEditor = $("#form").dxForm("instance").getEditor("Region");
        regionEditor.option("dataSource", region);
        regionEditor.option("value", parseInt(data.region_id));
        
        
        var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
        divisionEditor.option("dataSource", division);
        divisionEditor.option("value", parseInt(data.division_id));
        
    
    }
    
    function setWorkflow(data){
    
        var picaSubmitterEditor = $("#form").dxForm("instance").getEditor("PICA_Submitter");
        picaSubmitterEditor.option("dataSource", users);
        picaSubmitterEditor.option("value", data.workflow[0].actor_nik);
                                                   
        var picaApproverEditor = $("#form").dxForm("instance").getEditor("PICA_Approver");
        picaApproverEditor.option("dataSource", users);
        picaApproverEditor.option("value", data.workflow[1].actor_nik);
        
        var picaApproverEditor2 = $("#form").dxForm("instance").getEditor("PICA_Approver_2");
        picaApproverEditor2.option("dataSource", users);    
        
        var approver2 = data.workflow[2].actor_nik.split(",");
        
        var operands = [];
                                
        approver2.forEach(function (i) {
            operands.push(["nik", "=", i])
            
        });
        
        var criteria = [];
                                
        operands.forEach(function (i) {
            if (criteria.length === 0) {
                criteria.push(i)
            }
            else {
                criteria.push("OR");
                criteria.push(i)
            }
        });
        
        var filteredApprover2 = DevExpress.data.query(users).filter(criteria).toArray();
        var approver2_nik = [];
        
        filteredApprover2.forEach(function(i){
            
            approver2_nik.push(i['nik']);
        });
        
        picaApproverEditor2.option("values",[]);        
        picaApproverEditor2.option("values",approver2_nik);
                                            
        var progressSubmitterEditor = $("#form").dxForm("instance").getEditor("ProgressSubmitter");
        progressSubmitterEditor.option("dataSource", users);
        progressSubmitterEditor.option("value", data.workflow[3].actor_nik);
        
        var progressApproverEditor = $("#form").dxForm("instance").getEditor("ProgressApprover");
        progressApproverEditor.option("dataSource", users);
        progressApproverEditor.option("value", data.workflow[4].actor_nik);
                                                    
        var finalApproverEditor = $("#form").dxForm("instance").getEditor("FinalApprover");
        finalApproverEditor.option("dataSource", users);
        
        var finalApprover = data.workflow[5].actor_nik.split(",");
        
        var operands = [];
                                
        finalApprover.forEach(function (i) {
            operands.push(["nik", "=", i])
            
        });
        
        var criteria = [];
                                
        operands.forEach(function (i) {
            if (criteria.length === 0) {
                criteria.push(i)
            }
            else {
                criteria.push("OR");
                criteria.push(i)
            }
        });
        
        var filteredFinal = DevExpress.data.query(users).filter(criteria).toArray();
        
        var final_nik = [];
        filteredFinal.forEach(function(i){
            
            final_nik.push(i['nik']);
        });
        
        finalApproverEditor.option("values",[]);
        finalApproverEditor.option("values",final_nik);
                
    }
});
</script>
        
        
<div class="content containerPlaceholder">
    <div class="title "><h1>Create Work Flow</h1></div>
    <div class="pane dx-theme-desktop">  
        <div id="form"></div> 
        <div class="dx-field"><div id="button"></div></div>
        <div id="gridContainer"></div>  
    </div>
</div>
        