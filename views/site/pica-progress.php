<?php
use yii\helpers\Url;
use yii\helpers\BaseUrl;

$this->title = 'PICA Progress Log';

$user =  Yii::$app->user->identity;

?>

<style>

    <?php if($user->division_id === '37'){ ?>
    #form {
        height: auto;
        width: 100%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
    }

    <?php if( intval($user->role_id) !== 6){ ?>

    #button3 {
        float: right;
    }

    <?php } ?>

    #button1 {
        float: right;
        margin-left:10px;
    }

    <?php } ?>
       
    #gridContainer {
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        height:450px;         
        width: 100%;  
        margin-bottom: 24px;
    }

    #button2 {
        float: right;
        margin-left:10px;
    }

    
    
</style>
        
<script id="jsCode">
                
    $(function (){
       
        var pica = <?= $data;?>;
        var industry = <?= $industry ?>;
        var region = <?= $region ?>;
        var business_unit = <?= $business_unit ?>;
        var status = <?= $status ?>;
        
        var pica_number='';

        <?php if($user->division_id === '37'){ ?>
        
        $("#form").dxForm({
            colCount: 2,
            items: [
                {
                    itemType: "group",
                    items: [
                        {
                            dataField: "PICA_Number",
                            editorType: "dxTextBox"
                        }, 
                        {
                            dataField: "Industry",
                            editorType: "dxSelectBox",                            
                            editorOptions:{ 
                                items: industry,                                
                                displayExpr: "name",
                                valueExpr: "id",
                                onValueChanged: function (e) { 
                                    
                                    // if(e.value !== null){
                                                                                
                                    //     var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                                    //     regionEditor.option("dataSource", []);
                                    
                                    //     var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                    //     businessUnitEditor.option("dataSource", []);
                                    
                                    // }            
                                },
                                onOpened: function (e){
                                    
                                    // var industryEditor = $("#form").dxForm("instance").getEditor("Industry");
                                    // industryEditor.option("dataSource", industry);
                                
                                }
                            }
                        },          
                        {
                            dataField: "Region",
                            editorType: "dxSelectBox",
                            editorOptions: { 
                                items: region,
                                displayExpr: "name",
                                valueExpr: "id",
                                onValueChanged: function (e) {
                                
                                    // if(e.value !==null){
                                        
                                    //     var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                    //     businessUnitEditor.option("dataSource", []);
                                    
                                    // }
             
                                },
                                onOpened: function(e){
                                
                                    // var filteredRegion = [];
                                        
                                    // var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                                    // var industry_id = $("#form").dxForm("instance").option("formData").Industry;            
                                
                                    // filteredRegion = DevExpress.data.query(region).filter([["industry_id", "=", industry_id]]).toArray();
                                    
                                    // regionEditor.option("dataSource", filteredRegion);  
                                }
                            }
                        },
                        {
                            dataField: "BusinessUnit",
                            editorType: "dxSelectBox",
                            editorOptions: { 
                                items: business_unit,
                                displayExpr: "name",
                                valueExpr: "id",
                                onValueChanged: function (e) {                                                                    
                                                
                                },
                                onOpened: function(e){
                                
                                    // var filteredBusinessUnit = [];
                                       
                                    // var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                    // var industry_id = $("#form").dxForm("instance").option("formData").Industry;                                                
                                    // var region_id = $("#form").dxForm("instance").option("formData").Region;  
    
                                    // filteredBusinessUnit = DevExpress.data.query(business_unit).filter([["industry_id", "=", industry_id],"and",["region_id", "=", region_id]]).toArray();
                                    
                                    // businessUnitEditor.option("dataSource", filteredBusinessUnit);  
                                }
                            }
                        },
                    
                    ]
                },
                {
                    itemType: "group",
                    items: [
                        {
                            dataField: "Status",
                            editorType: "dxSelectBox",  
                            editorOptions:{
                                items: status,                                
                                displayExpr: "name",
                                valueExpr: "name",
                                onValueChanged: function (e) {},
                                onOpened: function(e){}
                            },                        
                            validationRules: [{
                                type: "required",
                                message: "Team Leader is required"
                            }]
                        }, 
                        {
                            dataField: "FromDate",
                            editorType: "dxDateBox",
                            format: "date",
                            editorOptions: { 
                                formatString: "MM/yyyy",
                                displayFormat: 'monthAndYear',
                                maxZoomLevel: 'year', 
                                minZoomLevel: 'century', 
                            }
                        },
                        {
                            dataField: "ToDate",
                            editorType: "dxDateBox",
                            format: "date",
                            editorOptions: { 
                                formatString: "MM/yyyy",
                                displayFormat: 'monthAndYear',
                                maxZoomLevel: 'year', 
                                minZoomLevel: 'century', 
                            }
                        }
                    ]
                }   
            ]
        });

        <?php if( intval($user->role_id) !== 6){ ?>

        $("#button3").dxButton({
            text: "Export",
            type: "success", 
            onClick: exportExcel
        });

        <?php } ?>

        $("#button1").dxButton({
            text: "Reset",
            type: "danger", 
            onClick: function(){

                // $("#form").dxForm("instance").resetValues();
                location.reload();  

            }
        });

        $("#button2").dxButton({
            text: "Search",
            type: "success", 
            onClick: search
        });

        <?php } ?>



        $("#gridContainer").dxDataGrid({
            dataSource: pica,
            hoverStateEnabled: true,
            showRowLines:true,
            rowAlternationEnabled:true,
            searchPanel: {
                visible: true
            },
            export: {
                enabled: false,
                fileName: "PICA"
            },
            wordWrapEnabled:true,
            allowColumnReordering: true,
            allowColumnResizing: true,
            paging: {
                pageSize: 15
            },
            editing: {
                mode: "row",
                allowUpdating: false,
                allowDeleting: false,
                allowAdding: false
            },  
            columns: [  
                {
                    dataField: "no",                                    
                    caption: "No.",
                    width:40
                 },
                 {
                    dataField: "number",
                    caption: "PICA Number", 
                    width:220
                 }, 
                 {
                    dataField: "repeat_finding",
                    caption: "Repeat Finding", 
                    width:220
                 }, 
                 {
                    dataField: "finding",
                    caption: "Finding",
                    allowEditing:false,
                    width:400,
                    editCellTemplate: function(container, cellInfo) {
                        $('<div>').appendTo(container).dxTextArea({
                            text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                            onValueChanged: function(e) {
                                cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                            }
                        });    
                    } 
                },
                {
                    dataField: "comment",
                    caption: "Auditor's Comment",
                    allowEditing:false,
                    width:400,
                    editCellTemplate: function(container, cellInfo) {
                        $('<div>').appendTo(container).dxTextArea({
                            text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                            onValueChanged: function(e) {
                                cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                            }
                        });    
                    } 
                },
                {
                    dataField: "lead_time_status",
                    caption: "Lead Time Status", 
                    width:120
                },
                {
                    dataField: "days",
                    caption: "Over Due (days)", 
                    width:120
                },
                {
                    dataField: "status",
                    caption: "Status", 
                    width:120
                },
                {
                    dataField: "category_name",
                    caption: "Category",
                    width:120,
                },
                {
                    dataField: "finding_file",
                    caption: "LHA File",
                    width:100,
                    cellTemplate: function(container, options) {
                        var uploads = "<?= BaseUrl::base();?>" + "/uploads/"; 
                        container.append($("<a style='text-decoration: underline;color:blue;' href='"+uploads + options.value + "' download>" + options.value + "</a>"));
                    }
                },
                {
                    dataField: "rating_name",
                    caption: "Rating",
                    width:70
                },
                {
                    dataField: "date_posted",
                    dataType: "date",
                    caption: "Posting Date",
                    format: "dd/MM/yyyy",
                    editorOptions: { 
                        formatString: "dd/MM/yyyy",
                    },
                    width:120
                },                            
                {
                    dataField: "date_due",
                    dataType: "date",
                    caption: "Due Date",
                    allowEditing:true,
                    format: "dd/MM/yyyy",
                    editorOptions: { 
                        formatString: "dd/MM/yyyy",
                    },
                    width:120
                },
                {
                    dataField: "industry_name",
                    caption: "Industry", 
                    width:120
                }, 
                {
                    dataField: "region_name",
                    caption: "Region", 
                    width:120
                }, 
                {
                    dataField: "business_unit_name",
                    caption: "Business Unit", 
                    width:120
                }, 
                {
                    dataField: "location_name",
                    caption: "Location", 
                    width:120
                },     
                {
                    dataField: "division_name",
                    caption: "Division",
                    width:120
                }, 
                            
            ], 
            masterDetail: {
                enabled: true,
                template: function(container, options) {                                 
                    container.addClass("internal-grid-container");
                    $("<div>")
                    .addClass("internal-grid")
                    .dxDataGrid({
                        width:'auto',
                        height:"auto",
                        columnAutoWidth: true,
                        allowColumnResizing: true, 
                        showRowLines:true,  
                        wordWrapEnabled:true,
                        dataSource: options.data.corrective_action, 
                        scrolling: {
                            mode: "virtual"
                        }, 
                        scrolling: {
                            mode: "virtual"
                        },
                        editing: {
                            mode: "row",
                            allowUpdating: false,
                            allowDeleting: false,
                            allowAdding: false
                        }, 
                        columns: [
                                            {
                                                dataField: "no",                                    
                                                caption: "No.", 
                                                width:40,
                                                allowEditing:false,
                                                cellTemplate: function(cellElement, cellInfo) {
                                                    cellElement.text(cellInfo.row.rowIndex+1);
                                                }
                                            },
                                            {
                                                dataField: "location_name",
                                                caption: "Location", 
                                                width:120,
                                                allowEditing:false,
                                            },     
                                            {
                                                dataField: "division_name",
                                                caption: "Division",
                                                width:120,
                                                allowEditing:false,
                                            },
                                            {
                                                dataField: "pic_position",
                                                caption: "PIC",   
                                                width:100,
                                                allowEditing:false,
                                                
                                            },                                           
                                            {
                                                dataField: "corrective_action",
                                                caption: "Corrective Action Plan",
                                                width:240,
                                                allowEditing:false,
                                                editCellTemplate: function(container, cellInfo) {
                                                    $('<div>').appendTo(container).dxTextArea({
                                                        text: cellInfo.data.corrective_action.replace(/<br>/g,"\n"),
                                                        onValueChanged: function(e) {
                                                            cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                                        }
                                                    });    
                                                }
                                                
                            
                                            },
                                            {
                                                dataField: "latest_progress",                                
                                                caption: "Latest Progress",
                                                allowEditing:true,
                                                width:240,
                                                editCellTemplate: function(container, cellInfo) {
                                                    $('<div>').appendTo(container).dxTextArea({
                                                        text: cellInfo.data.latest_progress.replace(/<br>/g,"\n"),
                                                        onValueChanged: function(e) {
                                                            cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                                        }
                                                    });    
                                                }
                
                                            },
                                            {
                                                dataField: "progress_file",
                                                caption: "Progress File",
                                                width:170,
                                                allowEditing:false,
                                                cellTemplate: function(container, options) {
                                                    var uploads = "<?= BaseUrl::base();?>" + "/uploads/"; 
                                                    var name = " ";
                                                    if(options.value!=" ") { name = options.value.split("/")[1]; }else { name = " ";}
                                                    container.append($("<a style='text-decoration: underline;color:blue;'  href='"+uploads + options.value + "' download>" + name + "</a>"));
                                                }
                                            },
                                            {
                                                dataField: "mandatory_name",                                
                                                caption: "Mandatory",
                                                allowEditing:false,
                                                width:70,
                
                                            },
                                            {
                                                dataField: "status",                                
                                                caption: "Status",
                                                allowEditing:false,
                                                width:60,
                                            },
                                            {
                                                dataField: "lead_time_status",                                
                                                caption: "Lead Time Status",
                                                allowEditing:false,
                                                width:120,
                
                                            },
                                            {
                                                dataField: "days",                                
                                                caption: "Over Due Days",
                                                allowEditing:false,
                                                width:110,
                                            },
                                            {
                                                dataField: "date_progress",                                
                                                caption: "Progress Entry",
                                                dataType:"date",
                                                allowEditing:false,
                                                width:120,
                                            }  
                                        ],
                                        onRowClick:function(e){
                                            filePreview();
                                        },
                                        onRowUpdating: function(e) { 
                                                                                       
                                            $.ajax({
                                                type: 'POST',
                                                url: '<?= Url::to(['pica-corrective/update']);?>',
                                                data: {
                                                    progress: e.newData.latest_progress ? e.newData.latest_progress : e.key.latest_progress,
                                                    id:e.key.id,                                                     
                                                },
                                                beforeSend: function() { $('#wait').show(); },
                                                complete: function() { $('#wait').hide(); },
                                                success: function(data){
                    
                                                    if(data.status==='fail'){

                                                        alert(data.message); 

                                                    }else{
                                                        
                                                        filePreview();
                                                    }
                                                    
                                                },
                                                error: function(xhr, textStatus, error){
                                                    alert(xhr.statusText);
                                                    location.reload();  
                    
                                                }
                                            });
                                            
                                            
                                                        
                                        },
                                    }).appendTo(container);
                            }
                        }
        
                    });
    
    
        
            function search(){

                var number = $("#form").dxForm("instance").option("formData").PICA_Number;                                                
                var status = $("#form").dxForm("instance").option("formData").Status;  
                var industry_id = $("#form").dxForm("instance").option("formData").Industry;                                                
                var region_id = $("#form").dxForm("instance").option("formData").Region;  
                var bu_id = $("#form").dxForm("instance").option("formData").BusinessUnit;
                var from = $("#form").dxForm("instance").option("formData").FromDate;  
                var to = $("#form").dxForm("instance").option("formData").ToDate;

                $.ajax({
                    type: 'POST',
                    url: '<?= Url::to(['pica-progress/get-all']);?>',
                    data: {

                        number:number,
                        status:status,
                        industry:industry_id,
                        region:region_id,
                        business_unit:bu_id,
                        from:from !== undefined ? (from.getMonth()+1)+"-"+from.getFullYear() : from,
                        to:to !== undefined ? (to.getMonth()+1)+"-"+to.getFullYear() : to

                    },
                    beforeSend: function() { $('#wait').show(); },
                    complete: function() { $('#wait').hide(); },
                    success: function(data){
                    
                    if(data.status==='fail'){

                        alert(data.message); 

                    }else{

                        pica = data.pica;
                        $("#gridContainer").dxDataGrid("instance").option("dataSource",[]);
                        $("#gridContainer").dxDataGrid("instance").option("dataSource",data.pica); 

                    }
                                                    
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);
                    // location.reload();  
                    
                }
            });
        }

        function exportExcel(){

        var number = $("#form").dxForm("instance").option("formData").PICA_Number;                                                
        var status = $("#form").dxForm("instance").option("formData").Status;  
        var industry_id = $("#form").dxForm("instance").option("formData").Industry;                                                
        var region_id = $("#form").dxForm("instance").option("formData").Region;  
        var bu_id = $("#form").dxForm("instance").option("formData").BusinessUnit;
        var from = $("#form").dxForm("instance").option("formData").FromDate;  
        var to = $("#form").dxForm("instance").option("formData").ToDate;

        var from_date = from !== undefined ? (from.getMonth()+1)+"-"+from.getFullYear() : from;
        var to_date = to !== undefined ? (to.getMonth()+1)+"-"+to.getFullYear() : to;
        
        var url = '<?= Url::to(['excel/progress']);?>'+'&number='+number+'&status='+status+'&industry='+industry_id+'&region='+region_id+'&business_unit='+bu_id+'&from='+from_date+'&to='+to_date;

        $(location).attr('href', url);
    }

        
});

</script>
        
<div class="content containerPlaceholder">
    <div class="title"><h1>PICA Progress Log</h1></div>
             
        <div class="pane dx-theme-desktop">
                
            <div id="form"></div>

            <br>
            <div id="button1"></div><div id="button2"></div>
            <?php if( intval($user->role_id) !== 6){ ?>
            <div id="button3"></div>  
            <?php } ?>
            <br><br><br><br>
  
            <div id="gridContainer"></div>
             
             
        </div>
            
</div>
   