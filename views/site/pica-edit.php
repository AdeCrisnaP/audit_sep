<?php
use yii\helpers\Url;
use yii\helpers\BaseUrl;

$this->title = 'Edit PICA';

?>

<style>
    
    /*Finding*/
    #form {
        height: auto;
        width: 100%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
    }

    #gridContainer {
        height:470px;         
        width: 100%;        
    }
    
    #button2 {
        float: right;
        margin: 20px 20px 0 0;
    }
    
    /*END Finding*/
    
    /*Corrective*/
    
    #form-field{
        height:190px;         
        width: auto;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px;
        background-color:#ffffff;
        margin-top:5px;
    }
    
    #popup{
        display: none;
        height:100%;
        width:100%;
        position:fixed;
        z-index: 3;
        top:0;
        left:0;
        right:0;
        background-color:rgb(0,0,0);
        background-color: rgba(0,0,0,0.9);
        overflow-y:hidden;
        transition: 0.5s;
    }
    
    .popup{
        position:relative;
        height:100%;
        z-index: 3;
        width:70%;
        background-color: #ffffff;
        padding:10px;
        margin:auto;
        
    }
    
    #popup2{
        display: none;
        height:100%;
        width:100%;
        position:fixed;
        z-index: 3;
        top:0;
        left:0;
        right:0;
        background-color:rgb(0,0,0);
        background-color: rgba(0,0,0,0.9);
        overflow-y:hidden;
        transition: 0.5s;
    }
    
    .popup2{
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        height:35%;
        z-index: 3;
        width:35%;
        background-color: #ffffff;
        padding:10px;
        margin:auto;
        
    }

    #form2{

        width:60%;
        float:left;
        margin-right: 5px;
    }
    
    #file-uploader{
        display:block;
        float:right;
        width:38%;
    }
    
    #file-list{
        display:none;
        float:right;
        width:38%;
    }
    
    #finding-text{
        float:right;
        margin-right: 5px;
    }
    
    #internal-grid{
        height:400px;         
        width: 100%;  
        
    }
    
    #internal-grid-field{
        height:400px;         
        width: auto;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
        margin-bottom:15px;
        overflow: hidden;
    }
    
    #button3 {
        float: right;
        margin-right: 10px;
    }
    
    #button4 {
        float: right;
        margin-right: 10px;
    }
    
    #button5 {
        float: right;
        margin: 20px 0 0 0;
    }
    
    #button6 {
        float: right;
        margin: 20px 10px 0 0 ;
    }
            
    /*End Corrective */
    
    /* Preview */
       
    #preview{
        display: none;
        height:100%;
        width:100%;
        position:fixed;
        z-index: 3;
        top:0;
        left:0;
        right:0;
        background-color:rgb(0,0,0);
        background-color: rgba(0,0,0,0.9);
        overflow-y:hidden;
        transition: 0.5s;
    }
    
    .preview{
        position:fixed;
        height:100%;
        z-index: 3;
        width:100%;
        background-color: #ffffff;
        padding:10px;
        margin:auto;
        
    }
            
    #form-preview-field{
        height:175px;         
        width: 97%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px;
        background-color:#ffffff;
        margin-top:5px;
    }

    #file-pica-list{
        display:none;
        float:left;
        width:25%;
        background-color:#ffffff;
        margin-left: 5px;
    }
    
    #form-preview{
        width:40%;
        float:left;
        margin-left: 5px;
    }
    
    #file-pica-uploader{
        float:left;
        width:25%;
    }
        
    #comment-text{
        width:auto;
        float:left;
        margin-left: 15px;
        float:left;
    }
    
    #grid-preview {
        height:300px;         
        width: 97%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
        margin-bottom:15px;
        overflow: hidden;
        
    }
    
    #gridContainer-preview {
        height:300px;         
        width: 100%; 
    }
    
    #button7 {
        float: right;
        margin: 20px 40px 0 0;
    }
    
    #button8 {
        float: right;
        margin: 20px 40px 0 0 ;
    }

    
    /* End Preview */
    
</style>

<script id="jsCode">

    $(function (){
        
        var tanggal = new Date();
        var industry = <?= $industry ?>;
        var region = <?= $region ?>;
        var business_unit = <?= $business_unit ?>;
        var location_ = <?= $location ?>;
        var division = <?= $division ?>;
        var category = <?= $category?>;
        var rating = <?= $rating ?>;
        var pic = <?= $pic ?>; 
        var mandatory = <?= $mandatory ?>;    
        var findingContainer= <?= $findings ?>;
        var teamLeader = <?= $teamLeader ?>;   
        var picaHeader=<?= $header ?>;
        var pica_number = picaHeader.number;
        var repeat =<?= $repeat ?>;  
        var pica={};
        var pica_finding=[];
        var pica_corrective={};
        var finding_id=0;
        var isfile =false;
        
        $("#file-list").hide();
    
        /*Finding*/
    
     $("#form").dxForm({
        colCount: 2,
        items: [
            {
                itemType: "group",
                items: [
                    {
                        dataField: "number",
                        editorType: "dxTextBox", 
                        editorOptions:{ 
                            disabled:true,
                            value:picaHeader[0].number
                        },                          
                        validationRules: [{
                                type: "required",
                                message: "Number is required"
                        }]
                    },
                    {
                        dataField: "ProjectName",
                        editorType: "dxTextBox", 
                        editorOptions:{ 
                            disabled:false,
                            value:picaHeader[0].project_name
                        },                          
                        validationRules: [{
                                type: "required",
                                message: "Project Name is required"
                        }]
                    }, 
                    {
                        dataField: "TeamLeader",
                        editorType: "dxSelectBox",  
                        editorOptions:{
                            items: teamLeader,                                
                            displayExpr: "username",
                            valueExpr: "nik",
                            disabled:false,
                            value:picaHeader[0].team_leader_nik
                        },                        
                        validationRules: [{
                                type: "required",
                                message: "Team Leader is required"
                        }]
                    }, 
                    {
                        dataField: "Industry",
                        editorType: "dxSelectBox",                            
                        editorOptions:{ 
                            items: industry,                                
                            displayExpr: "name",
                            valueExpr: "id",
                            disabled:true,
                            value:picaHeader[0].industry_id
                        }
                    },
                    {
                        dataField: "Region",
                        editorType: "dxSelectBox",                            
                        editorOptions:{ 
                            items: region,                                
                            displayExpr: "name",
                            valueExpr: "id",
                            disabled:true,
                            value:picaHeader[0].region_id
                        }
                    },
                    
                ]
            },
            {
                itemType: "group",
                items: [
                    {
                        dataField: "BusinessUnit",
                        editorType: "dxSelectBox",                            
                        editorOptions:{ 
                            items: business_unit,                                
                            displayExpr: "name",
                            valueExpr: "id",
                            disabled:true,
                            value:picaHeader[0].business_unit_id
                        }
                    },
                    {
                        dataField: "PostingDate",
                        editorType: "dxDateBox",
                        format: "date",
                        editorOptions: { 
                                formatString: "dd/MM/yyyy",
                                value: picaHeader[0].date_posted,
                                // max: new Date(),
                                // min: new Date(tanggal.getFullYear(), tanggal.getMonth(),tanggal.getDate()-30)
                        }
                    },
                    {
                        dataField: "FromDate",
                        editorType: "dxDateBox",
                        format: "date",
                        editorOptions: { 
                                formatString: "dd/MM/yyyy",
                                value: picaHeader[0].date_from,
                                // min: new Date()
                        },
                        validationRules: [{
                                type: "required",
                                message: "From Date is required"
                        }]
                    },
                    {
                        dataField: "ToDate",
                        editorType: "dxDateBox",
                        format: "date",
                        editorOptions: { 
                                formatString: "dd/MM/yyyy",
                                value: picaHeader[0].date_to,
                        },
                        validationRules: [{
                                type: "required",
                                message: "To Date is required"
                        }]
                    }
                ]
            }   
        ]
    });
    
    $("#gridContainer").dxDataGrid({
        dataSource: findingContainer,
        columnAutoWidth: true,
        allowColumnResizing: true, 
        showRowLines:true,
        showColumnLines: true,
        rowAlternationEnabled: true,
        selection: {
            mode: "single"
        },
        scrolling: {
            mode: "virtual"
        }, 
        hoverStateEnabled: true,
        editing: {
            mode: "row",
            allowUpdating: true,
            allowDeleting: true,
            allowAdding: true
        }, 
        columns: [  
            {
                dataField: "no.",
                width: 40,
                caption: "No.",
                allowEditing:false,
                allowSearch:false,
                cellTemplate: function(cellElement, cellInfo) {
                    cellElement.text(cellInfo.row.rowIndex+1);
                }
        
            },
            {
                dataField: "location_id",
                caption: "Location",
                width: 115,
                validationRules: [
                    { type: "required" }, 
            
                ],            
                lookup: {
                    dataSource: location_,
                    displayExpr: "name",
                    valueExpr: "id"
                },
                setCellValue: function (rowData, value) {
                    rowData.division_id = null;
                    this.defaultSetCellValue(rowData, value);
                }
            },     
            {
                dataField: "division_id",
                caption: "Division",
                width: 230,
                validationRules: [{ type: "required" }],
                lookup: {
                    valueExpr: 'id',
                    displayExpr: 'name',
                    dataSource: function (options) {
                        var dataSourceConfiguration = {
                            store: division
                        };
                        if (options.data) {
                        
                            var industryEditor = $("#form").dxForm("instance").getEditor("Industry");
                            var industry_id = industryEditor._options.selectedItem.id;
                    
                            dataSourceConfiguration.filter = [['location_id', '=', options.data.location_id],"and",['industry_id', '=', industry_id]];
                    
                        }
                        return dataSourceConfiguration;
                    },
                    setCellValue: function (rowData, value) {
                        rowData.pic_id = null;
                        this.defaultSetCellValue(rowData, value);
                    }
                    
                }
            }, 
            {
                dataField: "category_id",
                caption: "Category",
                width: 250,
                validationRules: [
                    { type: "required" }, 
            
                ],
                lookup: {
                    dataSource:category,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },
            {
                dataField: "rating_id",
                caption: "Rating",
                width: 110,
                validationRules: [
                    { type: "required" }, 
            
                ],
                lookup: {
                    dataSource: rating,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },  
            {
                dataField: "date_due",
                caption:"Due Date",
                dataType: "date",
                format: "dd/MM/yyyy",
                editorOptions: { 
                    formatString: "dd/MM/yyyy",
                },
                validationRules: [
                    { type: "required" }, 
            
                ],
                width: 125
            }           
        ],  
        onInitNewRow: function(e) {
            
            var dataGrid = $('#gridContainer').dxDataGrid('instance');
         
            e.data.sequence = dataGrid.totalCount() + 1;
            
            var result = $("#form").dxForm("instance").validate(); 
            
            if(!result.isValid) { alert('Mohon Lengkapi Data Isian');/*location.reload();*/}           
            
        },
        onRowClick:function(e){
         
            if(e.data !== null){
                
                showCorrective(e.data);
                
            }
           
        },
        onRowUpdating: function(e) {
            
            if(e.newData !== null){
                
                var data = $("#form").dxForm("instance").option('formData');
                var date = new Date(e.newData.date_due ? e.newData.date_due : e.key.date_due);
                var posted = new Date(data.PostingDate);
                
                $.ajax({
                type: 'POST',
                url: '<?= Url::to(['pica-finding/update']);?>',
                data:{
                        id:e.key.id,
                        business_unit_id:data.BusinessUnit,
                        industry_id:data.Industry,
                        region_id:data.Region,
                        category_id:e.newData.category_id ? e.newData.category_id : e.key.category_id,
                        division_id:e.newData.division_id ? e.newData.division_id : e.key.division_id,
                        location_id:e.newData.location_id ? e.newData.location_id : e.key.location_id,                                               
                        rating_id:e.newData.rating_id ? e.newData.rating_id : e.key.rating_id,
                        date_due:(date.getMonth()+1)+"-"+date.getDate()+"-"+date.getFullYear(),
                        date_posted:(posted.getMonth()+1) +"-"+posted.getDate()+"-"+posted.getFullYear()
                   
                    },
                success: function(data){
                    
                        if(data.status==='fail'){
                            alert(data.message); 
                            //location.reload(); 
                        } 
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
          
        },
        onRowRemoving: function(e) {
            
          if(e.data !== null){
                
                $.ajax({
                    type: 'POST',
                    url: '<?= Url::to(['pica-finding/delete']);?>',
                    data: {
                       id:e.data.id,
                       number:pica_number
                    },
                    success: function(data){
                    
                        if(data.status==='fail'){
                            alert(data.message);
                            //location.reload(); 
                        }                    
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        // location.reload();  
                    }
                }); 
            }
          
        },
        onRowInserting: function(e){
            
            if(e.data !== null){

                var data = $("#form").dxForm("instance").option('formData');
                var date = new Date(e.data.date_due);
                var posted = new Date(data.PostingDate);
                
                if(e.data !==null){

                    $.ajax({
                        type: 'POST',
                        url: '<?= Url::to(['pica-finding/create']);?>',
                        data: {
                            leader:data.TeamLeader,
                            number:picaHeader[0].number,
                            business_unit_id:data.BusinessUnit,
                            industry_id:data.Industry,
                            region_id:data.Region,
                            category_id:e.data.category_id,
                            division_id:e.data.division_id,
                            location_id:e.data.location_id,
                            rating_id:e.data.rating_id,
                            date_due:(date.getMonth()+1) +"-"+date.getDate()+"-"+date.getFullYear(),
                            date_posted:(posted.getMonth()+1) +"-"+posted.getDate()+"-"+posted.getFullYear(),
                   
                        },
                        success: function(data){
                    
                            if(data.status==='fail'){
                        
                                alert(data.message);
                        
                            }else{
                                
                                showCorrective(data.finding);
                                
                                $("#gridContainer").dxDataGrid("instance").option("dataSource",[]);
                                $("#gridContainer").dxDataGrid("instance").option("dataSource",data.findings);
                                
                            }                  
                    
                        },
                        error: function(xhr, textStatus, error){
                            alert(xhr.statusText);
                            //location.reload();  
                        }
                    }); 
                }
            }
           
        }

    
    });
    
    $("#button2").dxButton({
        text: "Preview",
        type: "default", 
        onClick: validateAndPreview
    }); 
    
    /*End Finding*/
    
    /*Corrective*/
    
    $("#file-uploader").dxFileUploader({
        selectButtonText: "Select File",
        labelText: "",
        accept: "*.*",   
        name:'file',
        uploadMode: "useForm",
    });

    $("#file-list").dxDataGrid({
        dataSource: {},
        columnAutoWidth: true,
        allowColumnResizing: true, 
        showRowLines:true,
        showColumnLines: true,
        rowAlternationEnabled: true,
        selection: {
            mode: "single"
        },
        scrolling: {
            mode: "virtual"
        }, 
        hoverStateEnabled: true,
        editing: {
            mode: "row",
            allowUpdating: false,
            allowDeleting: true,
            allowAdding: false
        }, 
        columns: [
            {
                dataField: "finding_file",
                caption: "File Name",
                width: 115
            },     
        ],
                onRowRemoving: function(e) {
           
           if(e.data !==null){
               
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica-finding/delete-file']);?>',
                    data: {
                        id:e.key.id, 
                    },
                    success: function(data){
                    
                        if(data.status==='fail'){
                            alert(data.message); 
                            //location.reload(); 
                        }else{
                            
                            $("#file-list").hide();
                            $("#file-uploader").show();
                            
                            $("#gridContainer").dxDataGrid("instance").option("dataSource",[]);
                            $("#gridContainer").dxDataGrid("instance").option("dataSource",data.findings);
                            
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
          
        },
        onRowClick: function(e){
            
           var uploads = "<?= BaseUrl::base();?>" + "/uploads/"; 
           
           window.open(uploads + e.data.finding_file, 'File', 'width=screen.width,height=screen.height');
        }
    }).dxDataGrid("instance");
    
    $("#finding-text").dxTextArea({
        value: "",
        height: 170,
        width:"50%",
        placeholder:"Write Finding Here",
        attr: { name: "finding-text" }
    }).dxValidator({
        validationRules: [{ type: "required",message: "Finding is required" }]
    });

    $("#form2").dxForm({
        colCount: 1,
        items: [
            {
                itemType: "group",
                items: [
                    {
                        dataField: "repeat_findings",
                        editorType: "dxSelectBox",  
                        editorOptions:{
                            items:repeat,                                
                            displayExpr: "number",
                            valueExpr: "number",
                            onValueChanged: function (e) {},
                            onOpened: function(e){}
                        }
                    }, 
                    {
                        dataField: "finding",
                        editorType: "dxTextArea",
                        editorOptions: {
                            height: 90,
                            value: "",
                            height: 130,
                            width:"100%",
                            placeholder:"Write Finding Here",
                        },                        
                        validationRules: [{
                                type: "required",
                                message: "Finding is required"
                        }]
                    }
                ]
            }

        ]
    });

    $("#internal-grid").dxDataGrid({
        dataSource: {},
        columnAutoWidth: true,
        allowColumnResizing: true, 
        showRowLines:true,
        showColumnLines: true,
        rowAlternationEnabled: true,
        selection: {
            mode: "single"
        },
        scrolling: {
            mode: "virtual"
        }, 
        hoverStateEnabled: true,
        editing: {
            mode: "row",
            allowUpdating: true,
            allowDeleting: true,
            allowAdding: true
        }, 
        columns: [
            {
                dataField:"no.",
                width: 40,
                caption:"No.",
                allowEditing:false,                       
                cellTemplate: function(cellElement, cellInfo) {
                    cellElement.text(cellInfo.row.rowIndex+1);
                }
        
            },
            {
                dataField: "location_id",
                caption: "Location",
                width: 115,
                validationRules: [
                    { type: "required" }, 
                ],            
                lookup: {
                    dataSource: location_,
                    displayExpr: "name",
                    valueExpr: "id"
                },
                setCellValue: function (rowData, value) {
                    rowData.division_id = null;
                    rowData.pic_nik = null;
                    this.defaultSetCellValue(rowData, value);
                }
            },     
            {
                dataField: "division_id",
                caption: "Division",
                width: 170,
                validationRules: [{ type: "required" }],
                lookup: {
                    valueExpr: 'id',
                    displayExpr: 'name',
                    dataSource: function (options) {
                        var dataSourceConfiguration = {
                            store: division
                        };
                        if (options.data) {
                        
                            var industryEditor = $("#form").dxForm("instance").getEditor("Industry");
                            var industry_id = industryEditor._options.selectedItem.id;
                    
                            dataSourceConfiguration.filter = [['location_id', '=', options.data.location_id],"and",['industry_id', '=', industry_id]];
                    
                        }
                        return dataSourceConfiguration;
                    },
                    setCellValue: function (rowData, value) {
                        rowData.pic_nik = null;
                        this.defaultSetCellValue(rowData, value);
                    }
                },
                setCellValue: function (rowData, value) {
                    rowData.pic_nik = null;
                    this.defaultSetCellValue(rowData, value);
                }
            }, 
            {
                dataField: "pic_nik",
                caption: "PIC",
                width: 180,
                validationRules: [{ type: "required" }],
                lookup: {
                    dataSource: function (options) {
                                    
                        var dataSourceConfiguration = {
                            store: pic
                        };
                        if (options.data) {
                                        
                            var industryEditor = $("#form").dxForm("instance").getEditor("Industry");
                            var industry_id = industryEditor._options.selectedItem.id;
                            
                            var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                            var region_id = regionEditor._options.selectedItem.id;
                            
                            var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                            var business_unit_id = businessUnitEditor._options.selectedItem.id;
                            
                            if(options.data.location_id === 1){
                                
                                dataSourceConfiguration.filter = [['location_id', '=', options.data.location_id],"and",['industry_id', '=', industry_id],"and",['division_id', '=', options.data.division_id],"and",['role_name', '=','PIC']];
                                
                            }else{
                                
                                dataSourceConfiguration.filter = [['location_id', '=', options.data.location_id],"and",['industry_id', '=', industry_id],"and",['region_id', '=', region_id],"and",['business_unit_id', '=', business_unit_id],"and",['division_id', '=', options.data.division_id],"and",['role_name', '=','PIC']];
                            
                            }
                        }
                        return dataSourceConfiguration;
                    },
                    displayExpr: "username",
                    valueExpr: "nik"
                }
            },
            {
                dataField: "mandatory_id",
                caption: "Mandatory",
                width: 90,           
                lookup: {
                    dataSource: mandatory,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            }
        ],
        onRowClick:function(e){
           
           if(e.data !== null){
                
                showCorrectiveText(e.data);
                
            }
           
        },
        onRowUpdating: function(e) {
            
            if(e.data !==null){
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica-corrective/update-edit']);?>',
                    data: {
                        
                        id:e.key.id, 
                        number:pica_number,
                        division_id:e.newData.division_id ? e.newData.division_id : e.key.division_id,
                        location_id:e.newData.location_id ? e.newData.location_id : e.key.location_id,
                        mandatory_id:e.newData.mandatory_id ? e.newData.mandatory_id : e.key.mandatory_id,
                        pic_nik:e.newData.pic_nik ? e.newData.pic_nik : e.key.pic_nik,
                        corrective_action:'""'
                                
                    },
                    success: function(data){
                    
                        if(data.status==='fail'){
                            alert(data.message); 
                            //location.reload(); 
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
          
        },
        onRowRemoving: function(e) {
            
            if(e.data !== null){
                                              
                $.ajax({
                    type: 'POST',
                    url: '<?= Url::to(['pica-corrective/delete']);?>',
                    data: {                    
                                
                        id:e.data.id
                        
                    },
                    success: function(data){
                    
                        if(data.status==='fail'){
                            alert(data.message);  
                            //location.reload(); 
                        }                    
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                                    //location.reload();  
                    
                    }
                });
            }       
        },
        onRowInserting: function(e){
             
            if(e.data !== null){
                $.ajax({
                    type: 'POST',
                    url: '<?= Url::to(['pica-corrective/create']);?>',
                    data: {
                        number:pica_finding.number,  
                        finding_id:pica_finding.id,
                        industry_id:pica_finding.industry_id,
                        region_id:pica_finding.region_id,
                        business_unit_id:pica_finding.business_unit_id,
                        category_id:pica_finding.category_id,
                        location_id:e.data.location_id,
                        division_id:e.data.division_id,
                        mandatory_id:e.data.mandatory_id,
                        corrective:e.data.corrective_action,
                        pic_nik:e.data.pic_nik     
                    },
                    success: function(data){
                    
                        if(data.status==='fail'){
                            
                            alert(data.message);
                            //location.reload(); 
                            
                        }else{
                            
                            $("#internal-grid").dxDataGrid("instance").option("dataSource",[]);
                            $("#internal-grid").dxDataGrid("instance").option("dataSource",data.correctives);
                            
                            showCorrectiveText(data.corrective);
                        }                       
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });
            }
        }

    });
    
    $("#corrective-text").dxTextArea({
        value: "",
        height:'75%',
        width:"100%",
        placeholder:"Corrective Action Here",
        attr: { name: "corrective-text" }
    }).dxValidator({
        validationRules: [{ type: "required",message: "Corrective Action is required" }]
    });
    
    $("#button3").dxButton({
        text: "Submit Finding",
        type: "success", 
        onClick: validateAndSubmitFinding
    });
    
    $("#button4").dxButton({
        text: "Close",
        type: "default", 
        onClick: closeForm
    });
    
    $("#button5").dxButton({
        text: "Save",
        type: "success", 
        onClick: saveCorrective
    });
    
    $("#button6").dxButton({
        text: "Close",
        type: "default", 
        onClick: closeCorrective
    });
    
    $("#button7").dxButton({
        text: "Submit",
        type: "success", 
        onClick: validateAndSubmit
    });
    
    $("#button8").dxButton({
        text: "Close",
        type: "default", 
        onClick: closePreview
    });
    
    /*End Corrective*/
    
    /* Preview */

    $("#file-pica-list").dxDataGrid({
        dataSource: {},
        columnAutoWidth: true,
        allowColumnResizing: true, 
        showRowLines:true,
        showColumnLines: true,
        rowAlternationEnabled: true,
        selection: {
            mode: "single"
        },
        scrolling: {
            mode: "virtual"
        }, 
        hoverStateEnabled: true,
        editing: {
            mode: "row",
            allowUpdating: false,
            allowDeleting: true,
            allowAdding: false
        }, 
        columns: [
            {
                dataField: "pica_file",
                caption: "File Name",
                width: 115
            },     
        ],
        onRowRemoving: function(e) {
           
           if(e.data !==null){
               
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica/delete-file']);?>',
                    data: {
                        id:e.key.id, 
                    },
                    success: function(data){
                    
                        if(data.status==='fail'){
                            alert(data.message); 
                            //location.reload(); 
                        }else{
                            
                            $("#file-pica-list").hide();
                            $("#file-pica-uploader").show();
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
          
        },
        onRowClick: function(e){
            
           var uploads = "<?= BaseUrl::base();?>" + "/uploads/"; 
           
           window.open(uploads + e.data.pica_file, 'File', 'width=screen.width,height=screen.height');
        }
    }).dxDataGrid("instance");
    
    $("#form-preview").dxForm({
        colCount: 2,
        items: [
            {
                itemType: "group",
                items: [
                    {
                        dataField: "number",
                        editorType: "dxTextBox", 
                        editorOptions:{ 
                            disabled:true,
                            value:picaHeader[0].number
                        },                          
                        validationRules: [{
                                type: "required",
                                message: "Project Name is required"
                        }]
                    },
                    {
                        dataField: "ProjectName",
                        editorType: "dxTextBox", 
                        editorOptions:{ 
                            disabled:true,
                            value:""
                        },                           
                        validationRules: [{
                                type: "required",
                                message: "Project Name is required"
                        }]
                    },
                    {
                        dataField: "TeamLeader",
                        editorType: "dxSelectBox",  
                        editorOptions:{
                            items: teamLeader,   
                            disabled:true,                             
                            displayExpr: "username",
                            valueExpr: "nik",
                            onValueChanged: function (e) {},
                            onOpened: function(e){}
                        },                        
                        validationRules: [{
                                type: "required",
                                message: "Team Leader is required"
                        }]
                    }, 
                    {
                        dataField: "Industry",
                        editorType: "dxSelectBox",                            
                        editorOptions:{ 
                            items: industry,                                
                            displayExpr: "name",
                            valueExpr: "id",
                            disabled:true,
                            value:""
                        }
                    },
                    {
                        dataField: "Region",
                        editorType: "dxSelectBox",                            
                        editorOptions:{ 
                            items: region,                                
                            displayExpr: "name",
                            valueExpr: "id",
                            disabled:true,
                            value:""
                        }
                    }
                    
                ]
            },
            {
                itemType: "group",
                items: [
                    {
                        dataField: "BusinessUnit",
                        editorType: "dxSelectBox",                            
                        editorOptions:{ 
                            items: business_unit,                                
                            displayExpr: "name",
                            valueExpr: "id",
                            disabled:true,
                            value:""
                        }
                    },
                    {
                        dataField: "PostingDate",
                        editorType: "dxDateBox",
                        format: "date",
                        editorOptions: { 
                                formatString: "dd/MM/yyyy",
                                disabled:true,
                                value: new Date(),
                                max: new Date(),
                                min: new Date(tanggal.getFullYear(), tanggal.getMonth(),tanggal.getDate()-30)
                        }
                    },
                    {
                        dataField: "FromDate",
                        editorType: "dxDateBox",
                        format: "date",
                        editorOptions: { 
                                formatString: "dd/MM/yyyy",
                                disabled:true,
                                value: new Date(),
                                min: new Date()
                        },
                        validationRules: [{
                                type: "required",
                                message: "From Date is required"
                        }]
                    },
                    {
                        dataField: "ToDate",
                        editorType: "dxDateBox",
                        format: "date",
                        editorOptions: { 
                                formatString: "dd/MM/yyyy",
                                disabled:true,
                                min: new Date()
                        },
                        validationRules: [{
                                type: "required",
                                message: "To Date is required"
                        }]
                    }
                ]
            }   
        ]
    });
   
    $("#comment-text").dxTextArea({
        value: "",
        height: 130,
        width:"30%",
        placeholder:"Write Comment Here",
        attr: { name: "comment-text" }
    });
    
    $("#file-pica-uploader").dxFileUploader({
        selectButtonText: "Select File",
        labelText: "",
        accept: "*.*",   
        name:'file',
        uploadMode: "useForm"
    });
    
    $("#gridContainer-preview").dxDataGrid({
            dataSource: {},
            columnAutoWidth: true,
            allowColumnResizing: true, 
            showRowLines:true,     
            wordWrapEnabled:true,
            scrolling: {
                mode: "virtual"
            }, 
            columns: [  
                {
                    dataField: "no",                                    
                    caption: "No."
                },
                {
                    dataField: "location_name",
                    caption: "Location"                                                        
                },     
                {
                    dataField: "division_name",
                    caption: "Division"                                            
                }, 
                {
                    dataField: "category_name",
                    caption: "Category"
                },
                {
                    dataField: "repeat_finding",
                    caption: "Repeat Finding"
                },
                {
                    dataField: "finding",
                    caption: "Finding",
                    width:420,
                    editCellTemplate: function(container, cellInfo) {
                        $('<div>').appendTo(container).dxTextArea({
                            text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                            onValueChanged: function(e) {
                                cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                            }
                        });    
                    }
                },
                {
                    dataField: "finding_file",
                    caption: "LHA File",
                    cellTemplate: function(container, options) {
                        var uploads = "<?= BaseUrl::base();?>" + "/uploads/"; 
                        container.append($("<a style='text-decoration: underline;color:blue;'  href='"+uploads + options.value + "' download>" + options.value + "</a>"));
                    }
                },
                {
                    dataField: "rating_name",
                    caption: "Rating",
                            
                },                         
                {
                    dataField: "date_due",
                    dataType: "date",
                    caption: "Due Date",
                    format: "dd/MM/yyyy"
                    
                
                }
                                    
            ], 
            masterDetail: {
                enabled: true,
                template: function(container, options) {                                 
                    container.addClass("internal-grid-container");                                         
                    $("<div>").addClass("internal-grid")
                        .dxDataGrid({
                            width:950,
                            height:"auto",
                            columnAutoWidth: true,
                            allowColumnResizing: true, 
                            showRowLines:true,  
                            wordWrapEnabled:true,
                            scrolling: {
                                mode: "virtual"
                            },                                         
                            columns: [
                                {
                                    dataField: "no",                                    
                                    caption: "No.", 
                                    width:40,
                                    cellTemplate: function(cellElement, cellInfo) {
                                        cellElement.text(cellInfo.row.rowIndex+1);
                                    }
                                },
                                {
                                    dataField: "location_name",
                                    caption: "Location", 
                                    width:100
                                },
                                {
                                    dataField: "division_name",
                                    caption: "Division", 
                                    width:200
                                },
                                {
                                    dataField: "pic_name",
                                    caption: "PIC", 
                                    width:100
                                                
                                },                                           
                                {
                                    dataField: "corrective_action",
                                    caption: "Corrective Action Plan",
                                    width:400,
                                    editCellTemplate: function(container, cellInfo) {
                                    $('<div>').appendTo(container).dxTextArea({
                                        text: cellInfo.data.corrective_action.replace(/<br>/g,"\n"),
                                            onValueChanged: function(e) {
                                                cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                            }
                                        });    
                                    }
                                },
                                {
                                    dataField: "mandatory_name",
                                    caption: "Mandatory", 
                                    width:40
                                                
                                }
                            ],
                            dataSource: options.data.corrective_action
                        }).appendTo(container);
                    }
                }
        
    });
        
    $("#button7").dxButton({
        text: "Submit",
        type: "success", 
        onClick: validateAndSubmit
    });
    
    /* End Preview */
     
    
    /*Functions*/
    
    function closeForm(){
        
        $("#popup").hide();
    
    }
    
    function closeCorrective(){
        
        $("#popup2").hide();
    
    }
    
    function closePreview(){
        
        $("#preview").hide();
    
    }
            
    function validateAndSubmitFinding() {
        
        var corrective = $("#internal-grid").dxDataGrid("instance").option("dataSource");
        
        var result = $("#form2").dxForm("instance").validate(); 
            
        if(result.isValid && !jQuery.isEmptyObject(corrective)) {
            
            var result = $("#form2").dxForm("instance").option('formData');

            var findingText = result.finding.replace(new RegExp('\r?\n','g'), '<br>');

            var repeatText = result.repeat_findings === null ? '' : result.repeat_findings.replace(new RegExp('\r?\n','g'), '<br>');
            
            var form = document.forms.namedItem("fileinfo");
            oData = new FormData(form);
            oData.append("repeat_finding", repeatText);
            oData.append("finding", findingText);
            oData.append("id", finding_id);
            oData.append("isfile", isfile);

            $.ajax({
                type: 'POST',
                url: '<?= Url::to(['pica-finding/update2']);?>',
                data: oData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){
                    
                    alert(data.message);
                    
                    if(data.status==='success'){
                        
                        $("#file-uploader").hide();
                        $("#file-list").show();
                        
                        $("#gridContainer").dxDataGrid("instance").option("dataSource",[]);
                        $("#gridContainer").dxDataGrid("instance").option("dataSource",data.findings);
                       
                        $("#file-list").dxDataGrid("instance").option("dataSource",[data.finding]);
                        
                        $("#popup").hide();
                    }
                    
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);   
                }

            });

        }else{
        
            alert("Silakan Isi Finding, File, dan Corrective Action.");
        }
        
    }
    
    function saveCorrective(){
    
        var result = $("#corrective-text").dxValidator("instance").validate();

        if(result.isValid) {
        
            var corrective_action = $("#corrective-text").dxTextArea("instance").option("value");
        
            if(pica_corrective !==null){
            
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica-corrective/update-comment']);?>',
                    data: {
                        
                        id:pica_corrective.id, 
                        corrective_action:corrective_action.replace(new RegExp('\r?\n','g'), '<br>')
                                
                    },
                    beforeSend: function() { $('#wait').show(); },
                    complete: function() { $('#wait').hide(); },
                    success: function(data){
                    
                        alert(data.message); 
                        
                        if(data.status==='fail'){
                            
                            //location.reload(); 
                        }else{
                        
                            $("#corrective-text").dxTextArea("instance").option("value",corrective_action);
                        
                            $("#internal-grid").dxDataGrid("instance").option("dataSource",[]);
                            $("#internal-grid").dxDataGrid("instance").option("dataSource",data.correctives);
                            
                            $("#popup2").hide();
                        
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
        }
    
    }
    
    function showCorrective(data) {        
        
        pica_finding = data;
        finding_id = data.id;
        
        $.ajax({
            type: 'POST',
            url: '<?= Url::to(['pica-corrective/get-all-edit']);?>',
            dataType:"json",
            data: {
                finding_id: finding_id,
                pica_number:data.number,
            },
            success: function(data){
                
                if(data.corrective !== 0){

                    $("#form2").dxForm("instance").getEditor("finding").option("value",null);

                    $("#form2").dxForm("instance").getEditor("finding").option("value",pica_finding.finding);

                    $("#form2").dxForm("instance").getEditor("repeat_findings").option("value",null);

                    $("#form2").dxForm("instance").getEditor("repeat_findings").option("value",pica_finding.repeat_finding);
                    
                    $("#internal-grid").dxDataGrid("instance").option("dataSource",[]);
                    
                    $("#internal-grid").dxDataGrid("instance").option("dataSource",data.corrective); 
                    
                }else{
                    
                    $("#internal-grid").dxDataGrid("instance").option("dataSource",[]);
                }  
                
                if(data.finding.finding_file.length < 3){
                    
                    $("#file-uploader").show();
                    $("#file-list").hide();
                    isfile= false;
                    
                }else{
                    
                   $("#file-uploader").hide();
                   $("#file-list").show();
                   $("#file-list").dxDataGrid("instance").option("dataSource",[]);
                   $("#file-list").dxDataGrid("instance").option("dataSource",[data.finding]);  
                   isfile=true;
                    
                }
                
                $("#popup").show();
                    
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);
//                location.reload();  
                    
            }
        }); 
       
    }
    
    function showCorrectiveText(data) {        
        
        var corrective_action = data.corrective_action ==='""' ? '' : data.corrective_action ;
        
        pica_corrective = data;
        
        $("#corrective-text").dxTextArea("instance").option("value", corrective_action.replace(/<br>/g,"\n"));
                       
        $("#popup2").show();
        
    }
            
    function preview(){
            
        var header = $("#form").dxForm("instance").option('formData');
        $("#form-preview").dxForm("instance").getEditor("ProjectName").option('value',header.ProjectName);
        $("#form-preview").dxForm("instance").getEditor("TeamLeader").option('value',header.TeamLeader);
        $("#form-preview").dxForm("instance").getEditor("Industry").option('value',header.Industry);
        $("#form-preview").dxForm("instance").getEditor("Region").option('value',header.Region);
        $("#form-preview").dxForm("instance").getEditor("BusinessUnit").option('value',header.BusinessUnit);
        $("#form-preview").dxForm("instance").getEditor("PostingDate").option('value',header.PostingDate);
        $("#form-preview").dxForm("instance").getEditor("FromDate").option('value',header.FromDate);
        $("#form-preview").dxForm("instance").getEditor("ToDate").option('value',header.ToDate);
            
        $.ajax({
                type: 'POST',
                url: '<?= Url::to(['pica/get-all']);?>',
                dataType:"json",
                data: {   
                    pica_number:picaHeader[0].number,     
                    industry_id: header.Industry, 
                    region_id: header.Region, 
                    business_unit_id: header.BusinessUnit,
                },
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){
                    
                    if(data.pica !==0){

                        if(data.pica_header.pica_file.length < 3){
                    
                            $("#file-pica-uploader").show();
                            $("#file-pica-list").hide();
                            isfile= false;
                    
                        }else{
                    
                            $("#file-pica-uploader").hide();
                            $("#file-pica-list").show();
                            $("#file-pica-list").dxDataGrid("instance").option("dataSource",[]);
                            $("#file-pica-list").dxDataGrid("instance").option("dataSource",[data.pica_header]);  
                            isfile=true;
                    
                        }

                        pica = data.pica;
                    
                        $("#gridContainer-preview").dxDataGrid("instance").option("dataSource",[]);
                        $("#gridContainer-preview").dxDataGrid("instance").option("dataSource",data.pica); 

                        $("#form-preview").dxForm("instance").getEditor("number").option('value',data.pica_header.number);
                        $("#form-preview").dxForm("instance").getEditor("Industry").option('value',data.pica_header.industry_id);
                        $("#form-preview").dxForm("instance").getEditor("Region").option('value',data.pica_header.region_id);
                        $("#form-preview").dxForm("instance").getEditor("BusinessUnit").option('value',data.pica_header.business_unit_id);
                        $("#form-preview").dxForm("instance").getEditor("PostingDate").option('value',header.PostingDate); 
                        
                        //jangan dihapus, sengaja dibuat agar data selalu tampil.
                        console.log(data.pica);
                    
                    }else{
                    
                        $("#gridContainer-preview").dxDataGrid("instance").option("dataSource",[]);
                    }                                       
                    
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);
                
                }
            });
            
        $("#preview").show();
    }
        
    function validateAndPreview () {
        var result = $("#form").dxForm("instance").validate();  
        if(result.isValid) {
                
            preview();
                
        }
    }
    
    function validateAndSubmit () {
            
        var result = $("#form").dxForm("instance").validate();
            
        if(result.isValid) {
            
            var data = $("#form-preview").dxForm("instance").option('formData'); 
            var posted = new Date(data.PostingDate);
            var from = new Date(data.FromDate);
            var to = new Date(data.ToDate);
            var comment = $("#comment-text").dxTextArea("instance").option("value");

            var form = document.forms.namedItem("filePica");
            oData = new FormData(form);
            oData.append("comment", data.comment === undefined ? '' : data.comment.replace(new RegExp('\r?\n','g'), '<br>'));
            oData.append("teamleader", data.TeamLeader);
            oData.append("project", data.ProjectName);
            oData.append("order_number", 0);
            oData.append("number", data.number);
            oData.append("posting_date",(posted.getMonth()+1) + "-" + posted.getDate() + "-" + posted.getFullYear());
            oData.append("from_date",(from.getMonth()+1)+"-"+from.getDate()+"-"+from.getFullYear());
            oData.append("to_date",(to.getMonth()+1)+"-"+to.getDate()+"-"+to.getFullYear());
            
            $.ajax({
                type: 'POST',
                url: '<?= Url::to(['pica/approve']);?>',
                data: oData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){
                    
                    alert(data.message);

                    var url = '<?= Url::to(['site/index']);?>';
                   $(location).attr('href', url);
                   
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);   
                    location.reload();
                }
            });
  
        }
    }
    
});
</script>

<div class="content containerPlaceholder">
    <div class="title "><h1>Edit PICA </h1></div>
    <div class="pane dx-theme-desktop"> 
        
        <div id="form"></div>
        
        <!-- <div id="gridContainer-field"> -->
            <div id="gridContainer"></div>
        <!-- </div> -->
        
        <div class="dx-field">
           <div id="button2"></div> 
        </div>
        
        <div id="popup">
            
            <div class="popup">
                
                <div id="form-field">
                    <form enctype="multipart/form-data" method="post" name="fileinfo">
                   
                        <div id="file-uploader"></div>
                        <div id="file-list"></div>
                        <div id="form2"></div>

                    </form>  
                </div>
                
                <div id="internal-grid-field">
                    <div id="internal-grid"></div>
                </div>
            
                <div id="button4"></div> 
                
                <div id="button3"></div> 
                
            </div>
            
        </div>
        
        <div id="popup2">
            
            <div class="popup2">
                
                <div id="corrective-text"></div> 
                          
                <div id="button5"></div> 
                
                <div id="button6"></div> 
                
            </div>
            
        </div>
        
        <div id="preview">
            
            <div class="preview">
                
                <div id="form-preview-field">
                    <form enctype="multipart/form-data" method="post" name="filePica">
                        
                        <div id="form-preview"></div>
                        <div id="comment-text"></div>
                        <div id="file-pica-list"></div>
                        <div id="file-pica-uploader"></div>

                    </form>  
                </div>
                
                <div id="grid-preview">
                    <div id="gridContainer-preview"></div>
                </div>
            
                <div id="button7"></div> 
                
                <div id="button8"></div> 
                
            </div>
            
        </div>
        
    </div>
</div>

