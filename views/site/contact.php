<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Whistle Blower System';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
  
.hotel-search {
    overflow-x: hidden;
    background-color: rgba(255, 255, 255, 0.75);
    width: 425px;
    float: left;
    height: 480px;
    border: 1px solid #e3e3e3;
    -moz-box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    -webkit-box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    padding:12px;
    border-radius: 10px;
}

.title-search {
    text-align: center;    
    height: 35px;
}

.title-search p {
    color: #f88e1d;
    font-size: 18px;    
    font-weight: bold;
}



    
</style>   

<div class="hotel-search">
    <div class="title-search">
        <p><?= Html::encode($this->title) ?></p>
    </div>

            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                
                <?= $form->field($model, 'subject') ?>

                <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>   
    
                <?= $form->field($model, 'file')->fileInput(['rows' => 6]) ?>   

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
       
</div>
