<?php
use yii\helpers\Url;
use yii\helpers\BaseUrl;

$this->title = 'Switch PIC';

?>

<style>

    #form {
        height: auto;
        width: 98%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
    }

    #number-box{
        width:30%;
        margin-left:10px;
        margin-right:10px;
        float:left;        
    }

    #gridContainer {
        height:470px;         
        width: 100%;        
    }

        
    #popup{
        display: none;
        height:100%;
        width:100%;
        position:fixed;
        z-index: 3;
        top:0;
        left:0;
        right:0;
        background-color:rgb(0,0,0);
        background-color: rgba(0,0,0,0.9);
        overflow-y:hidden;
        transition: 0.5s;
    }
    
    .popup{
        position:relative;
        height:100%;
        z-index: 3;
        width:70%;
        background-color: #ffffff;
        padding:10px;
        margin:auto;
        
    }

    #form-field{
        height:190px;         
        width: auto;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px;
        background-color:#ffffff;
        margin-top:5px;
    }

    #file-uploader{
        display:block;
        float:right;
        width:38%;
    }
    
    #file-list{
        display:none;
        float:right;
        width:38%;
    }

    #form2{

        width:60%;
        float:left;
        margin-right: 5px;
    }

    #internal-grid-field{
        height:400px;         
        width: auto;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
        margin-bottom:15px;
        overflow: hidden;
    }

    #internal-grid{
        height:400px;         
        width: 100%;  
    }

    #button3 {
        float: right;
        margin-right: 10px;
    }

    #button4 {
        float: right;
        margin-right: 10px;
    }

    #popup2{
        display: none;
        height:100%;
        width:100%;
        position:fixed;
        z-index: 3;
        top:0;
        left:0;
        right:0;
        background-color:rgb(0,0,0);
        background-color: rgba(0,0,0,0.9);
        overflow-y:hidden;
        transition: 0.5s;
    }
    
    .popup2{
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        height:35%;
        z-index: 3;
        width:35%;
        background-color: #ffffff;
        padding:10px;
        margin:auto;
    }

    #button5 {
        float: right;
        margin: 20px 0 0 0;
    }
    
    #button6 {
        float: right;
        margin: 20px 10px 0 0 ;
    }
    

</style>

<script id="jsCode">

 $(function (){

    var division = <?= $division ?>;
    var category = <?= $category?>;
    var rating = <?= $rating ?>;
    var location_ = <?= $location ?>;
    var mandatory = <?= $mandatory ?>; 
    var pic = <?= $pic ?>;
    var status = <?= $status ?>;
    var pica = {};
    var pica_corrective ={};
    
    $("#number-box").dxTextBox({
        placeholder:"PICA Number",
        showClearButton: true,
        attr: { name: "number-box" }
    }).dxValidator({
        validationRules: [{ type: "required",message: "PICA Number is required" }]
    });
    
    $("#search").dxButton({
        dataField: "PICA Number",
        text: "Search",
        type: "success",
        onClick: search
    });

    $("#gridContainer").dxDataGrid({
        dataSource: {},
        columnAutoWidth: true,
        allowColumnResizing: true, 
        showRowLines:true,
        showColumnLines: true,
        rowAlternationEnabled: true,
        selection: {
            mode: "single"
        },
        scrolling: {
            mode: "virtual"
        }, 
        hoverStateEnabled: true,
        editing: {
            mode: "row",
            allowUpdating: false,
            allowDeleting: false,
            allowAdding: false
        }, 
        columns: [  
            {
                dataField: "no.",
                width: 40,
                caption: "No.",
                allowEditing:false,
                allowSearch:false,
                cellTemplate: function(cellElement, cellInfo) {
                    cellElement.text(cellInfo.row.rowIndex+1);
                }
        
            },
            {
                    dataField: "finding",
                    caption: "Finding",
                    allowEditing:false,
                    width:400,
                    editCellTemplate: function(container, cellInfo) {
                        $('<div>').appendTo(container).dxTextArea({
                            text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                            onValueChanged: function(e) {
                                cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                            }
                        });    
                    } 
            },
            {
                dataField: "location_id",
                caption: "Location",
                width: 115,
                validationRules: [
                    { type: "required" }, 
                ],            
                lookup: {
                    dataSource: location_,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },     
            {
                dataField: "division_id",
                caption: "Division",
                width: 230,
                validationRules: [{ type: "required" }],
                lookup: {
                    valueExpr: 'id',
                    displayExpr: 'name',
                    dataSource: division
                }
                    
            }, 
            {
                dataField: "category_id",
                caption: "Category",
                width: 250,
                validationRules: [
                    { type: "required" }, 
                ],
                lookup: {
                    dataSource:category,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },
            {
                dataField: "rating_id",
                caption: "Rating",
                width: 110,
                validationRules: [
                    { type: "required" }, 
                ],
                lookup: {
                    dataSource: rating,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },  
            {
                dataField: "date_due",
                caption:"Due Date",
                dataType: "date",
                format: "dd/MM/yyyy",
                editorOptions: { 
                    formatString: "dd/MM/yyyy",
                },
                validationRules: [
                    { type: "required" }, 
            
                ],
                width: 125
            },    
            {
                dataField: "status",
                caption: "Status",
                width: 110,
                validationRules: [
                    { type: "required" }, 
                ],
                lookup: {
                    dataSource: status,
                    displayExpr: "name",
                    valueExpr: "name"
                }
            },        
        ],  
        onRowClick:function(e){
         
            if(e.data !== null && e.data.status ==='Closed'){
               
               showCorrective(e.data);
                
            }else{

                alert('Status Belum Closed');
            }
           
        }

    });

    $("#file-uploader").dxFileUploader({
        selectButtonText: "Select File",
        labelText: "",
        accept: "*.*",   
        name:'file',
        uploadMode: "useForm",
    });

    $("#file-list").dxDataGrid({
        dataSource: {},
        columnAutoWidth: true,
        allowColumnResizing: true, 
        showRowLines:true,
        showColumnLines: true,
        rowAlternationEnabled: true,
        selection: {
            mode: "single"
        },
        scrolling: {
            mode: "virtual"
        }, 
        hoverStateEnabled: true,
        editing: {
            mode: "row",
            allowUpdating: false,
            allowDeleting: false,
            allowAdding: false
        }, 
        columns: [
            {
                dataField: "finding_file",
                caption: "File Name",
                width: 115
            },     
        ],
        onRowClick: function(e){
            
           var uploads = "<?= BaseUrl::base();?>" + "/uploads/"; 
           
           window.open(uploads + e.data.finding_file, 'File', 'width=screen.width,height=screen.height');
        }

    }).dxDataGrid("instance");

    $("#form2").dxForm({
        colCount: 1,
        items: [
            {
                itemType: "group",
                items: [
                    {
                        dataField: "repeat_findings",
                        editorType: "dxTextBox",  
                        editorOptions: {
                           
                            disabled:true
                        }
                        
                    }, 
                    {
                        dataField: "finding",
                        editorType: "dxTextArea",
                        editorOptions: {
                            height: 90,
                            value: "",
                            height: 130,
                            width:"100%",
                            disabled:true
                        }
                    }
                ]
            }

        ]
    });

    $("#internal-grid").dxDataGrid({
        dataSource: {},
        columnAutoWidth: true,
        allowColumnResizing: true, 
        showRowLines:true,
        showColumnLines: true,
        rowAlternationEnabled: true,
        selection: {
            mode: "single"
        },
        scrolling: {
            mode: "virtual"
        }, 
        hoverStateEnabled: true,
        editing: {
            mode: "row",
            allowUpdating: true,
            allowDeleting: false,
            allowAdding: false
        }, 
        columns: [
            {
                dataField:"no.",
                width: 40,
                caption:"No.",
                allowEditing:false,                       
                cellTemplate: function(cellElement, cellInfo) {
                    cellElement.text(cellInfo.row.rowIndex+1);
                }
        
            },
            {
                dataField: "location_id",
                caption: "Location",
                width: 115,
                validationRules: [
                    { type: "required" }, 
                ],            
                lookup: {
                    dataSource: location_,
                    displayExpr: "name",
                    valueExpr: "id"
                },
                setCellValue: function (rowData, value) {
                    rowData.division_id = null;
                    rowData.pic_nik = null;
                    this.defaultSetCellValue(rowData, value);
                }
            },     
            {
                dataField: "division_id",
                caption: "Division",
                width: 170,
                validationRules: [{ type: "required" }],
                lookup: {
                    valueExpr: 'id',
                    displayExpr: 'name',
                    dataSource: function (options) {
                        var dataSourceConfiguration = {
                            store: division
                        };

                        if (options.data) {
                            
                            var industry_id = pica.finding.industry_id;
                    
                            dataSourceConfiguration.filter = [['location_id', '=', options.data.location_id],"and",['industry_id', '=', industry_id]];
                    
                        }
                        return dataSourceConfiguration;
                    },
                    setCellValue: function (rowData, value) {
                        rowData.pic_nik = null;
                        this.defaultSetCellValue(rowData, value);
                    }
                },
                setCellValue: function (rowData, value) {
                    rowData.pic_nik = null;
                    this.defaultSetCellValue(rowData, value);
                }
            }, 
            {
                dataField: "pic_nik",
                caption: "PIC",
                width: 180,
                validationRules: [{ type: "required" }],
                lookup: {
                    dataSource: function (options) {
                                    
                        var dataSourceConfiguration = {
                            store: pic
                        };

                        if (options.data) {
                                        
                            
                            var industry_id = pica.finding.industry_id;;
                            
                            var region_id = pica.finding.region_id;
                            
                            var business_unit_id = pica.finding.business_unit_id;
                            
                            if(options.data.location_id === 1){
                                
                                dataSourceConfiguration.filter = [['location_id', '=', options.data.location_id],"and",['industry_id', '=', industry_id],"and",['division_id', '=', options.data.division_id],"and",['role_name', '=','PIC']];
                                
                            }else{
                                
                                dataSourceConfiguration.filter = [['location_id', '=', options.data.location_id],"and",['industry_id', '=', industry_id],"and",['region_id', '=', region_id],"and",['business_unit_id', '=', business_unit_id],"and",['division_id', '=', options.data.division_id],"and",['role_name', '=','PIC']];
                            
                            }
                        }
                        return dataSourceConfiguration;
                    },
                    displayExpr: "username",
                    valueExpr: "nik"
                }
            },
            {
                dataField: "mandatory_id",
                caption: "Mandatory",
                width: 90,           
                lookup: {
                    dataSource: mandatory,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },
            {
                dataField: "status",
                caption: "Status",
                width: 90,           
                lookup: {
                    dataSource: status,
                    displayExpr: "name",
                    valueExpr: "name"
                }
            }
        ],
        onRowClick:function(e){
           
           if(e.data !== null){
                
                showCorrectiveText(e.data);
                
            }
           
        },
        onRowUpdating: function(e) {
            
            $.ajax({
                type: 'POST',
                url: '<?= Url::to(['pica-corrective/update-switch']);?>',
                data: {
                    location_id:e.newData.location_id ? e.newData.location_id : e.key.location_id,
                    division_id:e.newData.division_id ? e.newData.division_id : e.key.division_id,
                    mandatory_id:e.newData.mandatory_id ? e.newData.mandatory_id : e.key.mandatory_id,
                    pic_nik: e.newData.pic_nik ? e.newData.pic_nik : e.key.pic_nik,
                    status:e.newData.status ? e.newData.status : e.key.status,  
                    id:e.key.id
                },
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){
                    if(data.status === 'success'){

                        $("#internal-grid").dxDataGrid("instance").option("dataSource",[]);
                    
                        $("#internal-grid").dxDataGrid("instance").option("dataSource",data.correctives); 
                    }
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);
                    location.reload();  
                }
            });

        },
    });

    $("#button4").dxButton({
        text: "Close",
        type: "default", 
        onClick: closeForm
    });

    $("#button3").dxButton({
        text: "Submit",
        type: "success", 
        onClick: validateAndSubmit
    });

    $("#corrective-text").dxTextArea({
        value: "",
        height:'75%',
        width:"100%",
        placeholder:"Corrective Action Here",
        attr: { name: "corrective-text" }
    }).dxValidator({
        validationRules: [{ type: "required",message: "Corrective Action is required" }]
    });

    $("#button5").dxButton({
        text: "Save",
        type: "success", 
        onClick: saveCorrective
    });
    
    $("#button6").dxButton({
        text: "Close",
        type: "default", 
        onClick: closeCorrective
    });

    function closeForm(){
        
        $("#popup").hide();
    
    }

    function search(){
    
        var result = $("#number-box").dxValidator("instance").validate();

        if(result.isValid) {
        
            var number = $("#number-box").dxTextBox("instance").option("value");
            pica_number = number ;
        
            if(number !==null){
            
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica/search']);?>',
                    data: {
                        
                       number:number
                                
                    },
                    beforeSend: function() { $('#wait').show(); },
                    complete: function() { $('#wait').hide(); },
                    success: function(data){
                        
                        if(data.status==='fail'){
                            
                           alert(data.message);

                        }else{

                            pica = data.pica;
                            $("#gridContainer").dxDataGrid("instance").option("dataSource",[]);
                            $("#gridContainer").dxDataGrid("instance").option("dataSource",data.pica);
                            
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                    
                    }
                });  
            }
        }
    
    }

    function showCorrective(data) {        
        
        pica_finding = data;
        finding_id = data.id;
        
        $.ajax({
            type: 'POST',
            url: '<?= Url::to(['pica-corrective/get-all-switch']);?>',
            dataType:"json",
            data: {finding_id: finding_id},
            success: function(data){
                
                if(data.corrective !== 0){
                    pica = data;
                    pica_corrective = data.corrective;

                    $("#internal-grid").dxDataGrid("instance").option("dataSource",[]);
                    
                    $("#internal-grid").dxDataGrid("instance").option("dataSource",data.corrective); 

                    $("#form2").dxForm("instance").getEditor("finding").option("value", data.finding.finding);
                    
                    $("#form2").dxForm("instance").getEditor("repeat_findings").option("value", data.finding.repeat_finding);
                                   
                }else{
                    
                    $("#internal-grid").dxDataGrid("instance").option("dataSource",[]);
                }  
                
                if(data.finding.finding_file.length < 3){
                    
                    $("#file-uploader").show();
                    $("#file-list").hide();
                    isfile= false;
                    
                }else{
                    
                   $("#file-uploader").hide();
                   $("#file-list").show();
                   $("#file-list").dxDataGrid("instance").option("dataSource",[]);
                   $("#file-list").dxDataGrid("instance").option("dataSource",[data.finding]);  
                   isfile=true;
                    
                }
                
                $("#popup").show();
                    
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);
                //location.reload();  
                    
            }
        }); 
       
    }

    function validateAndSubmit() {
        
        var corrective = $("#internal-grid").dxDataGrid("instance").option("dataSource");
      
        if(!jQuery.isEmptyObject(corrective)) {
            
            $.ajax({
                type: 'POST',
                url: '<?= Url::to(['pica/switch']);?>',
                data: {
                    corrective:corrective
                },
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){
                    
                    alert(data.message);
                    
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);   
                }

            });

        }
        
    }

    function showCorrectiveText(data) {        
        
        var corrective_action = data.corrective_action ==='""' ? '' : data.corrective_action ;
        
        pica_corrective = data;
        
        $("#corrective-text").dxTextArea("instance").option("value", corrective_action.replace(/<br>/g,"\n"));
                       
        $("#popup2").show();
        
    }

    function saveCorrective(){
    
        var result = $("#corrective-text").dxValidator("instance").validate();

        if(result.isValid) {
        
            var corrective_action = $("#corrective-text").dxTextArea("instance").option("value");
        
            if(pica_corrective !==null){
            
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica-corrective/update-corrective-action']);?>',
                    data: {
                        
                        id:pica_corrective.id, 
                        corrective_action:corrective_action.replace(new RegExp('\r?\n','g'), '<br>')
                                
                    },
                    beforeSend: function() { $('#wait').show(); },
                    complete: function() { $('#wait').hide(); },
                    success: function(data){
                    
                        alert(data.message); 
                        
                        if(data.status==='fail'){
                            
                            //location.reload(); 
                        }else{
                        
                            $("#corrective-text").dxTextArea("instance").option("value",corrective_action);
                        
                            $("#internal-grid").dxDataGrid("instance").option("dataSource",[]);
                            $("#internal-grid").dxDataGrid("instance").option("dataSource",data.correctives);
                            
                            $("#popup2").hide();
                        
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
        }
    
    }

    function closeCorrective(){
        
        $("#popup2").hide();
    
    }

});
</script>

<div class="content containerPlaceholder">
    <div class="title "><h1>Switch PIC</h1></div>
    <div class="pane dx-theme-desktop"> 
        
        <div id="form">
            <div id="number-box"></div>
            <div id="search"></div>
        </div>
        
        <div id="gridContainer"></div>

         <div id="popup">
            
            <div class="popup">
                
                <div id="form-field">
                    <form enctype="multipart/form-data" method="post" name="fileinfo">
                   
                        <div id="file-uploader"></div>
                        <div id="file-list"></div>
                        <div id="form2"></div>

                    </form>  
                </div>
                
                <div id="internal-grid-field">
                    <div id="internal-grid"></div>
                </div>
            
                <div id="button4"></div> 
                
                <div id="button3"></div> 
                
            </div>
            
        </div>

         <div id="popup2">
            
            <div class="popup2">
                
                <div id="corrective-text"></div> 
                          
                <div id="button5"></div> 
                
                <div id="button6"></div> 
                
            </div>
            
        </div>

    </div>
</div>
