<?php 

use yii\helpers\Url;
use yii\helpers\BaseUrl;

$this->title = 'Inbox';

?>

<style>
    
    #popup2{
        display: none;
        height:100%;
        width:100%;
        position:fixed;
        z-index: 3000;
        top:0;
        left:0;
        right:0;
        background-color:rgb(0,0,0);
        background-color: rgba(0,0,0,0.9);
        overflow-y:hidden;
        transition: 0.5s;
    }
    
    .popup2{
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        height:35%;
        z-index: 3;
        width:35%;
        background-color: #ffffff;
        padding:10px;
        margin:auto;
        
    }

     #popup2a{
        display: none;
        height:100%;
        width:100%;
        position:fixed;
        z-index: 3000;
        top:0;
        left:0;
        right:0;
        background-color:rgb(0,0,0);
        background-color: rgba(0,0,0,0.9);
        overflow-y:hidden;
        transition: 0.5s;
    }
    
    .popup2a{
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        height:35%;
        z-index: 3;
        width:35%;
        background-color: #ffffff;
        padding:10px;
        margin:auto;
        
    }
    
    #popup3{
        display: none;
        height:100%;
        width:100%;
        position:fixed;
        z-index: 1002;
        top:0;
        left:0;
        right:0;
        background-color:rgb(0,0,0);
        background-color: rgba(0,0,0,0.9);
        overflow-y:hidden;
        transition: 0.5s;
    }
    
    .popup3{
        position:relative;
        height:100%;
        z-index: 1002;
        width:70%;
        background-color: #ffffff;
        padding:10px;
        margin:auto;
        
    }

    #popup3a{
        display: none;
        height:100%;
        width:100%;
        position:fixed;
        z-index: 3000;
        top:0;
        left:0;
        right:0;
        background-color:rgb(0,0,0);
        background-color: rgba(0,0,0,0.9);
        overflow-y:hidden;
        transition: 0.5s;
    }
    
    .popup3a{
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        height:35%;
        z-index: 3;
        width:35%;
        background-color: #ffffff;
        padding:10px;
        margin:auto;
        
    }
    
    #popup5{
        display: none;
        height:100%;
        width:100%;
        position:fixed;
        z-index: 1003;
        top:0;
        left:0;
        right:0;
        background-color:rgb(0,0,0);
        background-color: rgba(0,0,0,0.9);
        overflow-y:hidden;
        transition: 0.5s;
    }
    
    .popup5{
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        height:43%;
        z-index: 3;
        width:50%;
        background-color: #ffffff;
        padding:10px;
        margin:auto;
        
    }

    #popup6{

        display: none;
        height:100%;
        width:100%;
        position:fixed;
        z-index: 6000;
        top:0;
        left:0;
        right:0;
        background-color:rgb(0,0,0);
        background-color: rgba(0,0,0,0.9);
        overflow-y:hidden;
        transition: 0.5s;
    }
    
    .popup6{
        position:relative;
        height:100%;
        z-index: 6000;
        width:70%;
        background-color: #ffffff;
        padding:10px;
        margin:auto;
    }
    
    #form-field{
        height:190px;         
        width: auto;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px;
        background-color:#ffffff;
        margin:5px;
    }
    
    #file-uploader{
        display:block;
        float:right;
        width:49%;
    }
    
    #file-list{
        display:none;
        float:right;
        width:49%;
    }
    
    #progress-text{
        float:right;
        margin-right: 5px;
    }
    
    #button9 {
        float: right;
        margin-right: 10px;
    }
    
    #button10 {
        float: right;
        margin-right: 10px;
    }

    #button13 {
        float: right;
        margin-right: 10px;
    }
    
    #preview{
        
        overflow: auto;
    }
    
    #gridContainer {
        height: auto;
        width: 100%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;    
    }
    
    .form-preview {
        height: auto;
        width: auto;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-bottom:25px;
    }
       
    .internal-grid {
        height: 450px;
        width: 100%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
    }
    
    .internal-grid-corrective{
        
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
               
        height:450px;         
        width: 100%;  
        margin-bottom: 24px;
        
        
    }

    .internal-grid-container {
        padding: 10px 0 10px 0;
        margin-bottom:10px;
    }

    .internal-grid-container > div:first-child {
        padding: 0 0 5px 10px;
        font-size: 14px;
        font-weight: bold;
    }

    .internal-grid-container > .internal-grid {
        border: 1px solid #d2d2d2;
    }
    
    .button {
        float: right;
        margin-top: 20px;
    }
    
    .button2 {
        float: right;
        margin: 20px 20px 0 0;
    }
    
    .button3 {
        float: right;
        margin-top: 20px;
    }
    
    .button4 {
        float: right;
        margin: 20px 20px 0 0;
    }
    
    .button5 {
        float: right;
        margin-top: 20px;
    }
    
    .gridContainer-preview {
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        height:350px;         
        width: 100%;  
        margin-bottom: 24px;
    }
    
    #button7 {
        float: right;
        margin: 20px 0 0 0;
    }
    
    #button8 {
        float: right;
        margin: 20px 10px 0 0 ;
    }

    #button7a {
        float: right;
        margin: 20px 0 0 0;
    }
    
    #button8a {
        float: right;
        margin: 20px 10px 0 0 ;
    }
    
    #button11 {
        float: right;
        margin: 20px 0 0 0;
    }
    
    #button12 {
        float: right;
        margin: 20px 10px 0 0 ;
    }

     #button11a {
        float: right;
        margin: 20px 0 0 0;
    }
    
    #button12a {
        float: right;
        margin: 20px 10px 0 0 ;
    }

    #form2{

        width:60%;
        float:left;
        margin-right: 5px;
    }
    
    
</style>
        
<script id="jsCode">
    
$(function (){

   var no =0; 
   var inbox = <?= $data;?>;
   var status = <?= $status;?>;
   var pica_number ='';
   var is_printed = <?= $print;?>;
   var orderNumber = <?= $orderNumber;?>;
   var pica=[];
   var admin = false;
   var pica_finding={};
   var corrective_data = [];
   var viewButton = <?= $button;?>;
   var role_id = <?= Yii::$app->user->identity->role_id; ?>;
   var division_id = '<?= Yii::$app->user->identity->division_id; ?>';
   var order_number =0;
   var nik ='';
   var corrective_id=0;
   var supervisor = false;
   var pic = false;
   var audit = false;
   var corrective = false;
   var isfile = false;
   var update = false;
   var final = false;

   var repeat = [{number:1234},{number:12346}]
   
   if( division_id === '37' && role_id !== 2 ){update=true;audit = true;}
   
   if(role_id === 4){pic = true;corrective=true;}
   
   if(role_id === 4){pic = true;corrective=true;}
   
   if(role_id === 5){update=true;supervisor=true;}

    $("#form2").dxForm({
        colCount: 1,
        items: [
            {
                itemType: "group",
                items: [
                    {
                        dataField: "repeat_findings",
                        editorType: "dxSelectBox",  
                        editorOptions:{
                            items:status,                                
                            displayExpr: "name",
                            valueExpr: "name",
                            onValueChanged: function (e) {},
                            onOpened: function(e){}
                        }
                    }, 
                    {
                        dataField: "finding",
                        editorType: "dxTextArea",
                        editorOptions: {
                            height: 90,
                            value: "",
                            height: 130,
                            width:"100%",
                            placeholder:"Write Finding Here",
                        },                        
                        validationRules: [{
                                type: "required",
                                message: "Finding is required"
                        }]
                    }
                ]
            }

        ]
    });
   
    $("#gridContainer").dxDataGrid({
        dataSource: inbox, 
        height:"600px",
        showRowLines:true,
        rowAlternationEnabled:true,
        columnAutoWidth:true,
        wordWrapEnabled:true,
        selection: {
            mode: "single"
        },
        hoverStateEnabled: true,
        paging: {
            pageSize: 10
        },
        searchPanel: {
            visible: true
        }, 
        scrolling: {
            mode: "virtual"
        }, 
        allowColumnReordering: true,
        allowColumnResizing: true,
        pager: {
            showPageSizeSelector: true,
            allowedPageSizes: [5, 10],
            showInfo: true
        },
        columns: [
        {
            dataField: "no",        
            caption: "No.",
            width: 40,
            allowEditing:false,
            allowSearch:false        
        },
        {
            dataField: "number",
            caption: "PICA Number",
            width: 210,
        },
        {
            dataField: "finding",
            caption: "Finding",
            width: 70,
        },   
        {
            dataField: "date_created",
            dataType: "date",
            caption: "Date",
            format:"dd/MM/yyyy"
        }
        ],       
        onRowUpdating: function(e) {
            
        },
        onRowClick:function(e){
            console.log(e);
            if(e.data !== null){
                
                pica_number = e.data.number;
                order_number = e.data.order_number;
                no = e.data.no;

                preview(pica_number);
                
            }
        }           
    });

    $("#internal-grid").dxDataGrid({
        width:'100%',
        height:"450",
        columnAutoWidth: true,
        allowColumnResizing: true, 
        showRowLines:true,  
        wordWrapEnabled:true,
        scrolling: {
            mode: "virtual"
        }, 
        dataSource: {},
        columns: [
            {
                dataField: "no",                                    
                caption: "No.",                             
                cellTemplate: function(cellElement, cellInfo) {
                    cellElement.text(cellInfo.row.rowIndex+1);
                }
            },
            {
                dataField: "approval_status",
                caption: "Approval Status",                                                
            },                                           
            {
                dataField: "from_user_name",
                caption: "By",                            
            },                        
            {
                dataField: "comment",
                caption: "Comment",
                editCellTemplate: function(container, cellInfo) {
                    $('<div>').appendTo(container).dxTextArea({
                        text: cellInfo.data.comment.replace(/<br>/g,"\n"),
                        onValueChanged: function(e) {
                            cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                        }
                    });    
                }                            
            },
            {
                dataField: "to_user_name",
                caption: "Next Workflow",                            
            },
            {
                dataField: "date_created",
                caption: "Date", 
                dataType:"date",
                format:"dd/MM/yyyy"
            }
        ],
        onRowClick:function(e){
                            
        }
    });
    
    $("#comment-text").dxTextArea({
        value: "",
        height:'75%',
        width:"100%",
        placeholder:"Auditor's Comment Here",
        attr: { name: "comment-text" }
    }).dxValidator({
        validationRules: [{ type: "required",message: "Auditor's Comment is required" }]
    });

    $("#coordinator-text").dxTextArea({
        value: "",
        height:'75%',
        width:"100%",
        placeholder:"IA Coordinator's Comment Here",
        attr: { name: "coordinator-text" }
    }).dxValidator({
        validationRules: [{ type: "required",message: "IA Coordinator's Comment is required" }]
    });
    
    // $("#supervisor-text").dxTextArea({
    //     value: "",
    //     height:'75%',
    //     width:"100%",
    //     placeholder:"PIC Head's Comment Here",
    //     attr: { name: "supervisor-text" }
    // }).dxValidator({
    //     validationRules: [{ type: "required",message: "PIC Head's Comment is required" }]
    // });

    $("#divHead-text").dxTextArea({
        value: "",
        height:'75%',
        width:"100%",
        placeholder:"IA Div. Head's Comment Here",
        attr: { name: "divHead-text" }
    }).dxValidator({
        validationRules: [{ type: "required",message: "IA Div. Head's Comment is required" }]
    });
    
    $("#button7").dxButton({
        text: "Save",
        type: "success", 
        onClick: saveComment
    });
    
    $("#button8").dxButton({
        text: "Close",
        type: "default", 
        onClick: closeComment
    });

    $("#button7a").dxButton({
        text: "Save",
        type: "success", 
        onClick: saveCoordinatorComment
    });
    
    $("#button8a").dxButton({
        text: "Close",
        type: "default", 
        onClick: closeCoordinatorComment
    });
    
    $("#button11").dxButton({
        text: "Save",
        type: "success", 
        onClick: saveSupervisorComment
    });
    
    $("#button12").dxButton({
        text: "Close",
        type: "default", 
        onClick: closeSupervisorComment
    });

    $("#button11a").dxButton({
        text: "Save",
        type: "success", 
        onClick: saveDivHeadComment
    });
    
    $("#button12a").dxButton({
        text: "Close",
        type: "default", 
        onClick: closeDivHeadComment
    });

    $("#button13").dxButton({
        text: "Close",
        type: "default", 
        onClick: closeWorkflow
    });
    
    $("#file-uploader").dxFileUploader({
        selectButtonText: "Select File",
        labelText: "",
        accept: "*.*",   
        name:'file',
        uploadMode: "useForm",
    });

    $("#file-list").dxDataGrid({
        dataSource: {},
        columnAutoWidth: true,
        allowColumnResizing: true, 
        showRowLines:true,
        showColumnLines: true,
        rowAlternationEnabled: true,
        hoverStateEnabled: true,
        selection: {
            mode: "single"
        },
        scrolling: {
            mode: "virtual"
        }, 
        editing: {
            mode: "row",
            allowUpdating: false,
            allowDeleting: true,
            allowAdding: false
        }, 
        columns: [
            {
                dataField: "progress_file",
                caption: "File Name",
                width: 115,
                cellTemplate: function(container, options) {
                    var uploads = "<?= BaseUrl::base();?>" + "/uploads/"+ pica_number.replace("/", "_")+"/"; 
                    container.append($("<a style='text-decoration: underline;color:blue;'  href='"+ uploads + options.value + "' download>" + options.value + "</a>"));
                }
            },     
        ],
        onRowRemoving: function(e) {
           
           if(e.data !==null){
               
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica-corrective/delete-file']);?>',
                    data: {
                        id:e.key.id, 
                    },
                    success: function(data){
                    
                        if(data.status==='fail'){
                            alert(data.message); 
                            //location.reload(); 
                        }else{
                            
                            $("#file-list").hide();
                            $("#file-uploader").show();
                            
                            $(".internal-grid-corrective").dxDataGrid("instance").option("dataSource",[]);
                            $(".internal-grid-corrective").dxDataGrid("instance").option("dataSource",data.correctives);
                            $(".internal-grid-corrective").dxDataGrid("instance").refresh();
                            
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
          
        },
        onRowClick: function(e){
            
           var uploads = "<?= BaseUrl::base();?>" + "/uploads/"; 
           
           window.open(uploads + e.data.finding_file, 'File', 'width=screen.width,height=screen.height');
        }
    }).dxDataGrid("instance");
    
    $("#progress-text").dxTextArea({
        value: "",
        height: 170,
        width:"50%",
        placeholder:"Write Progress Here",
        attr: { name: "finding-text" }
    }).dxValidator({
        validationRules: [{ type: "required",message: "Progress is required" }]
    });
    
    $("#button9").dxButton({
        text: "Save",
        type: "success", 
        onClick: validateAndSubmitProgress
    });
    
    $("#button10").dxButton({
        text: "Close",
        type: "default", 
        onClick: closeProgress
    });
    
    var popup2 = null,    
    popupOptions2 = {
        height:"100%",
        width:"100%",
        showTitle: true,
        title: "PICA Preview",
        visible: false,
        dragEnabled: false,
        closeOnOutsideClick: false,
        contentTemplate: function(container, options) {
                
        $("<div>")
            .addClass("form-preview")
            .dxForm({
                colCount: 3,
                formData:[],
                items: [
                    {
                        itemType: "group",                            
                        items: [
                            {
                                dataField: "number",
                                editorOptions: {
                                    disabled: true
                                }
                            },
                            {
                                dataField: "project_name",
                                editorOptions: {
                                    disabled: true
                                }
                            },
                            {
                                dataField: "industry_name",
                                editorOptions: {
                                    disabled: true
                                }
                            },
                            {
                                dataField: "region_name",
                                editorOptions: {
                                    disabled: true
                                }
                            }
                            
                        ]
                    },
                    {
                        itemType: "group",                            
                        items: [ 
                            {
                                dataField: "business_unit_name",
                                editorOptions: {
                                    disabled: true
                                }
                            },
                            {
                                dataField: "date_posted",
                                caption:"Posting Date",
                                editorType: "dxDateBox",
                                format: "date",
                                editorOptions: { 
                                    formatString: "dd/MM/yyyy",
                                    disabled:true,
                                }
                            },
                            {
                                dataField: "date_from",
                                caption:"Posting Date",
                                editorType: "dxDateBox",
                                format: "date",
                                editorOptions: { 
                                    formatString: "dd/MM/yyyy",
                                    disabled:true,
                                }
                            },
                            {
                                dataField: "date_to",
                                caption:"Posting Date",
                                editorType: "dxDateBox",
                                format: "date",
                                editorOptions: { 
                                    formatString: "dd/MM/yyyy",
                                    disabled:true,
                                }
                            }                                                    
                        ]
                    },
                    {
                        itemType: "group",                            
                        items: [ 
                            {
                                colSpan: 2,
                                dataField: "Notes",                                   
                                editorType: "dxTextArea",
                                editorOptions: {
                                    height: 120,
                                    placeholder:"Tulis Komentar Anda Di Sini",
                                        
                                }
                            }                                                      
                        ]
                    }
                ]
            }).appendTo(container); 
                if(order_number >=3){

                    $("<div>")
                    .addClass("gridContainer-preview")
                    .dxDataGrid({
                        dataSource: pica,
                        width:"auto",
                        height:"300",
                        columnAutoWidth: true,
                        allowColumnResizing: true,
                        showRowLines:true, 
                        wordWrapEnabled:true,
                        hoverStateEnabled: true,
                        selection: {
                            mode: "single"
                        },
                        scrolling: {
                            mode: "virtual"
                        },
                        editing: {
                            mode: "row",
                            allowUpdating: update,
                            allowDeleting: admin,
                            allowAdding: admin
                        }, 
                        columns: [  
                            {
                                dataField: "no",                                    
                                caption: "No.",
                                width:40,
                                allowEditing:false,
                            },
                            {
                                dataField: "location_name",
                                caption: "Location", 
                                width:120,
                                allowEditing:admin,
                            },     
                            {
                                dataField: "division_name",
                                caption: "Division",
                                width:120,
                                allowEditing:admin,
                            }, 
                            {
                                dataField: "category_name",
                                caption: "Category",
                                width:120,
                                allowEditing:admin,
                                
                            },
                            {
                                dataField: "finding",
                                caption: "Finding",
                                allowEditing:false,
                                editCellTemplate: function(container, cellInfo) {
                                    $('<div>').addClass("finding").appendTo(container).dxTextArea({
                                        text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                                        onValueChanged: function(e) {
                                            cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                        }
                                    });    
                                } 
                            },
                            {
                                dataField: "supervisor_comment",
                                caption: "PIC Head's Comment",
                                allowEditing:supervisor,
                                editCellTemplate: function(container, cellInfo) {
                                    $('<div>').addClass("supervisor_comment").appendTo(container).dxTextArea({
                                        text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                                        onValueChanged: function(e) {
                                            cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                        }
                                    });    
                                } 
                            },
                            {
                                dataField: "comment",
                                caption: "Auditor's Comment",
                                allowEditing:false,
                                editCellTemplate: function(container, cellInfo) {
                                    $('<div>').addClass("comment").appendTo(container).dxTextArea({
                                        text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                                        onValueChanged: function(e) {
                                            cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                        }
                                    });    
                                } 
                            },                            
                            {
                                dataField: "finding_file",
                                caption: "LHA File",
                                width:170,
                                allowEditing:false,
                                cellTemplate: function(container, options) {
                                    var uploads = "<?= BaseUrl::base();?>" + "/uploads/"; 
                                    container.append($("<a style='text-decoration: underline;color:blue;' href='"+uploads + options.value + "' download>" + options.value + "</a>"));
                                }
                            },
                            {
                                dataField: "status",
                                caption: "Status",
                                allowEditing:false
                            },
                            {
                                dataField: "rating_name",
                                caption: "Rating",
                                width:70,
                                allowEditing:false
                            
                            },
                            {
                                dataField: "date_posted",
                                dataType: "date",
                                caption: "Posting Date",
                                format:"dd/MM/yyyy",
                                width:120,
                                allowEditing:false
                
                            },                            
                            {
                                dataField: "date_due",
                                dataType: "date",
                                caption: "Due Date",
                                allowEditing:false,
                                format:"dd/MM/yyyy",
                                width:120
                            }
                        ], 
                        onEditingStart: function(e) {
                            
                            if( role_id === 5){
                                
                                supervisorComment(e.data);
                                
                            }else{
                                
                                showComment(e.data);
                            }
                        
                        },
                        onRowClick: function(e){
                            var data = e.data;
                            showCorrective(data);
                        },
                        onSelectionChanged: function (e) {
                            var data = e.selectedRowsData[0];
                            console.log(data);
                            showCorrective(data);
                        }
                        
                    }).appendTo(container); 

                }else{

                     $("<div>")
                    .addClass("gridContainer-preview")
                    .dxDataGrid({
                        dataSource: pica,
                        width:"auto",
                        height:"360",
                        columnAutoWidth: true,
                        allowColumnResizing: true,
                        showRowLines:true, 
                        wordWrapEnabled:true,
                        hoverStateEnabled: true,
                        selection: {
                            mode: "single"
                        },
                        scrolling: {
                            mode: "virtual"
                        },
                        editing: {
                            mode: "row",
                            allowUpdating: update,
                            allowDeleting: admin,
                            allowAdding: admin
                        }, 
                        columns: [  
                            {
                                dataField: "no",                                    
                                caption: "No.",
                                width:40,
                                allowEditing:false,
                            },
                            {
                                dataField: "location_name",
                                caption: "Location", 
                                width:120,
                                allowEditing:admin,
                            },     
                            {
                                dataField: "division_name",
                                caption: "Division",
                                width:120,
                                allowEditing:admin,
                            }, 
                            {
                                dataField: "category_name",
                                caption: "Category",
                                width:120,
                                allowEditing:admin,
                                
                            },
                            {
                                dataField: "finding",
                                caption: "Finding",
                                allowEditing:false,
                                editCellTemplate: function(container, cellInfo) {
                                    $('<div>').addClass("finding").appendTo(container).dxTextArea({
                                        text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                                        onValueChanged: function(e) {
                                            cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                        }
                                    });    
                                } 
                            },
                            {
                                dataField: "coordinator_comment",
                                caption: "IA Coordinator's Comment",
                                allowEditing:false,
                                editCellTemplate: function(container, cellInfo) {
                                    $('<div>').addClass("comment").appendTo(container).dxTextArea({
                                        text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                                        onValueChanged: function(e) {
                                            cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                        }
                                    });    
                                } 
                            },
                            {
                                dataField: "div_head_comment",
                                caption: "IA Div. Head's Comment",
                                allowEditing:supervisor,
                                editCellTemplate: function(container, cellInfo) {
                                    $('<div>').addClass("supervisor_comment").appendTo(container).dxTextArea({
                                        text: cellInfo.data.finding.replace(/<br>/g,"\n"),
                                        onValueChanged: function(e) {
                                            cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                        }
                                    });    
                                } 
                            },
                            {
                                dataField: "finding_file",
                                caption: "LHA File",
                                width:170,
                                allowEditing:false,
                                cellTemplate: function(container, options) {
                                    var uploads = "<?= BaseUrl::base();?>" + "/uploads/"; 
                                    container.append($("<a style='text-decoration: underline;color:blue;' href='"+uploads + options.value + "' download>" + options.value + "</a>"));
                                }
                            },
                            {
                                dataField: "status",
                                caption: "Status",
                                allowEditing:false
                            },
                            {
                                dataField: "rating_name",
                                caption: "Rating",
                                width:70,
                                allowEditing:false
                            
                            },
                            {
                                dataField: "date_posted",
                                dataType: "date",
                                caption: "Posting Date",
                                format:"dd/MM/yyyy",
                                width:120,
                                allowEditing:false
                
                            },                            
                            {
                                dataField: "date_due",
                                dataType: "date",
                                caption: "Due Date",
                                allowEditing:false,
                                format:"dd/MM/yyyy",
                                width:120
                            }
                        ], 
                        onEditingStart: function(e) {
                            
                            if( order_number === 2){
                                
                                divHeadComment(e.data);
                                
                            }else if(order_number === 1){
                                
                                coordinatorComment(e.data);
                            }
                        
                        },
                        onRowClick: function(e){
                            var data = e.data;
                            showCorrective(data);
                        },
                        onSelectionChanged: function (e) {
                            var data = e.selectedRowsData[0];
                            console.log(data);
                            showCorrective(data);
                        }
                        
                    }).appendTo(container); 


                }

                if(viewButton[no]){
                                                                                
                    if(order_number===4){
                            
                        $("<div>")
                            .addClass("button").dxButton({
                            text: "Approve",
                            type: "success", 
                            onClick: validateAndApprove
                        }).appendTo(container);
                    
                            
                        $("<div>")
                            .addClass("button2").dxButton({
                            text: "Reject",
                            type: "danger", 
                            onClick: validateAndReject
                        }).appendTo(container);
                                
                    }
                        
                    if(order_number ===1){
                            
                        $("<div>")
                            .addClass("button").dxButton({
                            text: "Approve",
                            type: "success", 
                            onClick: validateAndApprove
                        }).appendTo(container);
                            
                        $("<div>")
                            .addClass("button2").dxButton({
                            text: "Reject",
                            type: "danger", 
                            onClick: validateAndReject
                        }).appendTo(container);
                                
                    }
                        
                    if(order_number===2 && is_printed[pica_number] ===0 ){
                        
                        $("<div>")
                        .addClass("button2").dxButton({
                            text: "Reject",
                            type: "danger", 
                            onClick: validateAndReject
                        }).appendTo(container);
                            
                            
                        $("<div>")
                        .addClass("button4").dxButton({
                            text: "Print",
                            type: "default", 
                            onClick: validateAndPrint
                        }).appendTo(container);
                            
                    }
                            
                    if(order_number===2 && is_printed[pica_number] ===1 ){
                                
                                $("<div>")
                                .addClass("button").dxButton({
                                    text: "Approve",
                                    type: "success", 
                                    onClick: validateAndApprove
                                }).appendTo(container);
                    
                            
                                $("<div>")
                                .addClass("button2").dxButton({
                                    text: "Reject",
                                    type: "danger", 
                                    onClick: validateAndReject
                                }).appendTo(container);
                            
                                $("<div>")
                                .addClass("button4").dxButton({
                                    text: "Print",
                                    type: "default", 
                                    onClick: validateAndPrint
                                }).appendTo(container);
                        
                    }
                    
                    if(order_number===0){
                        
                                $("<div>")
                                .addClass("button5").dxButton({
                                    text: "Edit PICA",
                                    type: "success", 
                                    onClick: validateAndEdit
                                }).appendTo(container);
                    }
                        
                    if(order_number ===5 ){
                        
                                $("<div>")
                                .addClass("button").dxButton({
                                    text: "Submit",
                                    type: "success", 
                                    onClick: validateAndFinalApprove
                                }).appendTo(container);
                    }

                    $("<div>")
                        .addClass("button").dxButton({
                        text: "View Workflow",
                        type: "success", 
                        onClick: viewWorkflow
                    }).appendTo(container);
                }
                        
            }
    };
    
    var popup4 = null,    
    popupOptions4 = {
        height:"100%",
        width:"100%",
        showTitle: true,
        title: "Corrective Action",
        visible: false,
        dragEnabled: false,
        closeOnOutsideClick: false,
        contentTemplate: function(container, options) {
            
            $("<div>")
                .addClass("internal-grid-corrective")
                .dxDataGrid({
                    width:'auto',
                    height:"500",
                    columnAutoWidth: true,
                    allowColumnResizing: true,
                    showRowLines:true, 
                    wordWrapEnabled:true,
                    hoverStateEnabled: true,
                    selection: {
                        mode: "single"
                    },
                    scrolling: {
                        mode: "virtual"
                    },
                    dataSource: {}, 
                    editing: {
                        mode: "row",
                        allowUpdating: corrective,
                        allowDeleting: false,
                        allowAdding: false
                    }, 
                    columns: [
                        {
                            dataField: "no",                                    
                            caption: "No.", 
                            width:40,
                            allowEditing:false,
                            cellTemplate: function(cellElement, cellInfo) {
                                cellElement.text(cellInfo.row.rowIndex+1);
                            }
                        },
                        {
                            dataField: "location_name",
                            caption: "Location", 
                            width:120,
                            allowEditing:false,
                        },     
                        {
                            dataField: "division_name",
                            caption: "Division",
                            width:120,
                            allowEditing:false,
                        },
                        {
                            dataField: "pic_name",
                            caption: "PIC",   
                            width:100,
                            allowEditing:false,
                        },                                           
                        {
                            dataField: "corrective_action",
                            caption: "Corrective Action Plan",
                            width:240,
                            allowEditing:false,
                            editCellTemplate: function(container, cellInfo) {
                                $('<div>').appendTo(container).dxTextArea({
                                    text: cellInfo.data.corrective_action.replace(/<br>/g,"\n"),
                                    onValueChanged: function(e) {
                                        cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                    }
                                });    
                            }
                                                
                            
                        },
                        {
                            dataField: "latest_progress",                                
                            caption: "Latest Progress",
                            allowEditing:false,
                            width:240,
                            editCellTemplate: function(container, cellInfo) {
                                $('<div>').appendTo(container).dxTextArea({
                                    text: cellInfo.data.latest_progress.replace(/<br>/g,"\n"),
                                    onValueChanged: function(e) {
                                        cellInfo.setValue(e.value.replace(/<br>/g,"\n"));
                                    }
                                });    
                            }
                
                        }, 
                        {
                            dataField: "progress_file",
                            caption: "Progress File",
                            width:170,
                            allowEditing:false,
                            cellTemplate: function(container, options) {
                                var uploads = "<?= BaseUrl::base();?>" + "/uploads/"+ pica_number.replace("/", "_")+"/"; 
                                container.append($("<a style='text-decoration: underline;color:blue;'  href='"+uploads + options.value + "' download>" + options.value + "</a>"));
                            }
                        },
                        {
                            dataField: "mandatory_name",                                
                            caption: "Mandatory",
                            allowEditing:false,
                            width:70,
                        },
                        {
                            dataField: "status",
                            caption: "Status",
                            width: 115,
                            allowEditing:final,
                            validationRules: [
                                { type: "required" }, 
                            ],            
                            lookup: {
                                dataSource: status,
                                displayExpr: "name",
                                valueExpr: "name"
                            }
                        }, 
                        {
                            dataField: "lead_time_status",                                
                            caption: "Lead Time Status",
                            allowEditing:false,
                            width:120,
                        },
                        {
                            dataField: "days",                                
                            caption: "Over Due Days",
                            allowEditing:false,
                            format:"dd/MM/yyyy",
                            width:110,
                        },
                        {
                            dataField: "date_progress",                                
                            caption: "Progress Entry",
                            dataType:"date",
                            allowEditing:false,
                            format:"dd/MM/yyyy",
                            width:120,
                        },
                         
                    ],
                    onRowUpdating: function(e) { 
                                              
                        if(order_number ===5 ){validateAndSave(e)};                                       
                    },
                    onEditingStart: function(e) {
                        
                        corrective_id = e.data.id;
                        
                        if(role_id == 4){progressPreview(e.data)};
                        
                    }
                    
            }).appendTo(container);
            
            if(order_number===3 && role_id === 4){
                        
                $("<div>")
                .addClass("button5").dxButton({
                    text: "Submit Progress",
                    type: "success", 
                    onClick: validateAndApprovePIC
                }).appendTo(container);
            }
            
            if(order_number ===5 ){
                        
                // $("<div>")
                // .addClass("button").dxButton({
                //     text: "Save",
                //     type: "success", 
                //     onClick: validateAndSave
                // }).appendTo(container);
            }
        }        
    };
    
    function progressPreview(data){

        $("#progress-text").dxTextArea("instance").option("value", data.latest_progress.replace(/<br>/g,"\n"));
        $("#file-list").dxDataGrid("instance").option("dataSource",[]);
        $("#file-list").dxDataGrid("instance").option("dataSource",[data]);  

        if( data.progress_file.length < 3){
                    
            $("#file-uploader").show();
            $("#file-list").hide();
            isfile= false;
                    
        }else{
                    
            $("#file-uploader").hide();
            $("#file-list").show();
            isfile= true;
                    
        }
        
        $("#popup5").show();
    }

    function viewWorkflow(){

          var number =  $(".form-preview").dxForm("instance").option('formData').number; 

          $.ajax({
            type: 'POST',
            url: '<?= Url::to(['work-flow/view']);?>',
            dataType:"json",
            data: {                
                number: number, 
            },
            beforeSend: function() { $('#wait').show(); },
            complete: function() { $('#wait').hide(); },
            success: function(data){
                    
                $("#internal-grid").dxDataGrid("instance").option("dataSource",[]);    
                $("#internal-grid").dxDataGrid("instance").option("dataSource",data.workflow);
                    
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);
                
            }
        }); 
        $("#popup6").show();
    }
    
    function preview(number){
        
        if(orderNumber[number] === 5){final=true;corrective=true;}

        $.ajax({
            type: 'POST',
            url: '<?= Url::to(['pica/get-all']);?>',
            dataType:"json",
            data: {                
                pica_number: number, 
                order_number: order_number,
            },
            beforeSend: function() { $('#wait').show(); },
            complete: function() { $('#wait').hide(); },
            success: function(data){
                    
                if(data.pica !==0){
                    
                    $(".gridContainer-preview").dxDataGrid("instance").option("dataSource",[]);
                    
                    $(".form-preview").dxForm("instance").option("formData",[]);
                    
                    $(".gridContainer-preview").dxDataGrid("instance").option("dataSource",data.pica); 
                    
                    $(".form-preview").dxForm("instance").option("formData",data.pica_header);
                    
                    pica = data.pica;
                    
                    nik = data.pica[0]['corrective_action'][0]['next_nik'];
                    
                }else{
                    
                    $(".gridContainer-preview").dxDataGrid("instance").option("dataSource",[]);
                }                                     
                    
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);
                
            }
        }); 
            
        popup2 && $(".popup2").remove();
        
        $popupContainer2 = $("<div />").addClass("popup2").appendTo($("#preview"));
        
        popup2 = $popupContainer2.dxPopup(popupOptions2).dxPopup("instance");
        
        popup2.show();
    };
    
    function showCorrective(data){                    
                    
         $.ajax({
            type: 'POST',
            url: '<?= Url::to(['pica-corrective/get-all2']);?>',
            dataType:"json",
            data: {                
                pica_number: data.number, 
                order_number: order_number,
                finding_id: data.id, 
            },
            beforeSend: function() { $('#wait').show(); },
            complete: function() { $('#wait').hide(); },
            success: function(data){
                    
                if(data.corrective !==0){
                    
                    corrective_data = data.corrective;
                    pica_number = data.pica_number;
                    
                    $(".internal-grid-corrective").dxDataGrid("instance").option("dataSource",[]);
                    
                    $(".internal-grid-corrective").dxDataGrid("instance").option("dataSource",data.corrective); 
                    
                }else{
                    
                    $(".internal-grid-corrective").dxDataGrid("instance").option("dataSource",[]);
                }                                     
                    
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);
                
            }
        });             
                    
        popup4 && $(".popup4").remove();
        
        $popupContainer4 = $("<div />").addClass("popup4").appendTo($("#preview"));
        
        popup4 = $popupContainer4.dxPopup(popupOptions4).dxPopup("instance");
        
        popup4.show();
    }
    
    function validateAndReject(){
        
        var comment = $(".form-preview").dxForm("instance").option('formData').Notes;        
        
        $.ajax({
            type: 'POST',
            url: '<?= Url::to(['pica/reject']);?>',
            dataType:"json",
            data: { 
                pica:pica,
                comment: comment === undefined ? '' : comment,
                order_number:order_number
            },
            beforeSend: function() { $('#wait').show(); },
            complete: function() { $('#wait').hide(); },
            success: function(data){
                    
                alert(data.message);
                location.reload();
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);   
                location.reload();
            }
        });
    
    }
    
    function validateAndFinalApprove(){
        
        var comment = $(".form-preview").dxForm("instance").option('formData').Notes; 
        
        var pica = $(".gridContainer-preview").dxDataGrid("instance").option("dataSource");
        
        $.ajax({
            type: 'POST',
            url: '<?= Url::to(['pica/approve']);?>',
            dataType:"json",
            data: { 
                pica:pica,
                comment: comment === undefined ? '' : comment,
                order_number:order_number
            },
            beforeSend: function() { $('#wait').show(); },
            complete: function() { $('#wait').hide(); },
            success: function(data){
                    
                alert(data.message);
                location.reload();
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);   
                location.reload();
            }
        });
    }
    
    function validateAndSave(data){
        
        $.ajax({
            type: 'POST',
            url: '<?= Url::to(['pica-corrective/update2']);?>',
            dataType:"json",
            data: { 
                pica:data.key,
                status:data.newData.status
            },
            beforeSend: function() { $('#wait').show(); },
            complete: function() { $('#wait').hide(); },
            success: function(data){
                if(data.status==='fail'){   
                    alert(data.message);
                    //location.reload();
                }
                // $(".popup4").hide();
                
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);   
                //location.reload();
            }
        });
        
    }
    
    function validateAndApprove(){
        
        var comment = $(".form-preview").dxForm("instance").option('formData').Notes;        
        
        $.ajax({
            type: 'POST',
            url: '<?= Url::to(['pica/approve']);?>',
            dataType:"json",
            data: { 
                pica:pica,
                comment: comment === undefined ? '' : comment,
                order_number:order_number
            },
            beforeSend: function() { $('#wait').show(); },
            complete: function() { $('#wait').hide(); },
            success: function(data){
                    
                alert(data.message);
                location.reload();
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);   
                location.reload();
            }
        });
    }
    
    function validateAndApprovePIC(){
        
        var comment = $(".form-preview").dxForm("instance").option('formData').Notes; 
        var corrective_data =  $(".internal-grid-corrective").dxDataGrid("instance").option('dataSource'); 
        
        $.ajax({
            type: 'POST',
            url: '<?= Url::to(['pica/approve']);?>',
            dataType:"json",
            data: { 
                pica:corrective_data,
                comment: comment === undefined ? '' : comment,
                order_number:order_number
            },
            beforeSend: function() { $('#wait').show(); },
            complete: function() { $('#wait').hide(); },
            success: function(data){
                    
                alert(data.message);
                location.reload();
            },
            error: function(xhr, textStatus, error){
                alert(xhr.statusText);   
                location.reload();
            }
        });
    }
    
    function validateAndPrint(){

        $.ajax({
                type: 'POST',
                url: '<?= Url::to(['pica-finding/print']);?>',
                dataType:"json",
                data: {   
                    pica:pica[0],
                },
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){

                    

                    var url = '<?= Url::to(['pdf/pica']);?>'+'&number='+data.pica_number;

                    $(location).attr('href', url);
                    
                        // alert(data.message);
                        // location.reload();
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);                    
                }
            });
    }
    
    function validateAndSubmit () {
            
        var result = $("#form").dxForm("instance").validate();
            
        if(result.isValid) {
                
            var data = $(".form-preview").dxForm("instance").option('formData'); 
                                
            $.ajax({
                type: 'POST',
                url: '<?= Url::to(['pica/create']);?>',
                dataType:"json",
                data: {                
                        business_unit_id:data.BusinessUnit,
                        industry_id:data.Industry,
                        region_id:data.Region ,
                        comment:data.Notes === undefined ? '' : data.Notes,
                        file:data.File === undefined ? '' : data.File,
                        pica:pica
                },
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){
                    
                        alert(data.message);
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);                    
                }
            });
                
        }
    }
    
    function validateAndEdit() {

        var url = '<?= Url::to(['site/pica-edit']);?>'+'&id='+pica_number;

        $(location).attr('href', url);
                                               
    }
            
    function showComment(data){
     
        var comment = data.comment ==='""' ? '' : data.comment ;
        
        pica_finding = data;
        
        $("#comment-text").dxTextArea("instance").option("value", comment.replace(/<br>/g,"\n"));
                       
        $("#popup2").show();
    
    }

    function coordinatorComment(data){
     
        var comment = data.coordinator_comment ==='""' || data.coordinator_comment === null ? '' : data.coordinator_comment ;
        
        pica_finding = data;
        
        $("#coordinator-text").dxTextArea("instance").option("value", comment.replace(/<br>/g,"\n"));
                       
        $("#popup2a").show();
    
    }
        
    function closeComment(){
        
        $("#popup2").hide();
        $(".gridContainer-preview").dxDataGrid("instance").refresh()
    
    }

    function closeWorkflow(){

        $("#popup6").hide();

    }
        
    function closeCoordinatorComment(){
        
        $("#popup2a").hide();
        $(".gridContainer-preview").dxDataGrid("instance").refresh()
    
    }
    
    function saveComment(){
    
        var result = $("#comment-text").dxValidator("instance").validate();
        
        if(result.isValid) {
        
            var comment_action = $("#comment-text").dxTextArea("instance").option("value");
            
            if(pica_finding !==null){
            
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica-finding/update-comment']);?>',
                    data: {
                        
                        id:pica_finding.id, 
                        comment:comment_action.replace(new RegExp('\r?\n','g'), '<br>'),
                        number:pica_finding.number,
                        order_number:pica_finding.order_number
                                
                    },
                    beforeSend: function() { $('#wait').show(); },
                    complete: function() { $('#wait').hide(); },
                    success: function(data){
                        
                        alert(data.message); 
                        
                        if(data.status==='fail'){
                            
                            //location.reload(); 
                        }else{
                        
                            $("#comment-text").dxTextArea("instance").option("value",comment_action);
                        
                            $(".gridContainer-preview").dxDataGrid("instance").option("dataSource",[]);
                            $(".gridContainer-preview").dxDataGrid("instance").option("dataSource",data.findings);
                            $(".gridContainer-preview").dxDataGrid("instance").refresh();
                            
                            $("#popup2").hide();
                        
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
        }
    
    }

    function saveCoordinatorComment(){
    
        var result = $("#coordinator-text").dxValidator("instance").validate();
        
        if(result.isValid) {
        
            var comment_action = $("#coordinator-text").dxTextArea("instance").option("value");
            
            if(pica_finding !==null){
            
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica-finding/update-coordinator-comment']);?>',
                    data: {
                        
                        id:pica_finding.id, 
                        comment:comment_action.replace(new RegExp('\r?\n','g'), '<br>'),
                        number:pica_finding.number,
                        order_number:pica_finding.order_number
                                
                    },
                    beforeSend: function() { $('#wait').show(); },
                    complete: function() { $('#wait').hide(); },
                    success: function(data){
                        console.log(data);
                        alert(data.message); 
                        
                        if(data.status==='fail'){
                            
                            //location.reload(); 
                        }else{
                        
                            $("#coordinator-text").dxTextArea("instance").option("value",comment_action);
                        
                            $(".gridContainer-preview").dxDataGrid("instance").option("dataSource",[]);
                            $(".gridContainer-preview").dxDataGrid("instance").option("dataSource",data.findings);
                            $(".gridContainer-preview").dxDataGrid("instance").refresh();
                            
                            $("#popup2a").hide();
                        
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
        }
    
    }
    
    function supervisorComment(data){
        
        var comment = data.supervisor_comment ==='""' ? '' : data.supervisor_comment ;
        
        pica_finding = data;

        
        // $("#supervisor-text").dxTextArea("instance").option("value", comment.replace(/<br>/g,"\n"));
                       
        $("#popup3").show();
    
    }

    function divHeadComment(data){
        
        var comment = data.div_head_comment ==='""' || data.div_head_comment === null ? '' : data.div_head_comment ;
        
        pica_finding = data;
        
        $("#divHead-text").dxTextArea("instance").option("value", comment.replace(/<br>/g,"\n"));
                       
        $("#popup3a").show();
    
    }
    
    function saveSupervisorComment(){
    
        var result_supervisor = $("#supervisor-text").dxValidator("instance").validate();
        
        if(result_supervisor.isValid) {
        
            var supervisor_comment = $("#supervisor-text").dxTextArea("instance").option("value");
            
            if(pica_finding !==null){
            
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica-finding/update-supervisor-comment']);?>',
                    data: {
                        
                        id:pica_finding.id, 
                        comment:supervisor_comment.replace(new RegExp('\r?\n','g'), '<br>'),
                        number:pica_finding.number,
                        order_number:pica_finding.order_number
                                
                    },
                    beforeSend: function() { $('#wait').show(); },
                    complete: function() { $('#wait').hide(); },
                    success: function(data){
                    
                        alert(data.message); 
                        
                        if(data.status==='fail'){
                            
                            //location.reload(); 
                        }else{
                        
                            $("#supervisor-text").dxTextArea("instance").option("value",supervisor_comment);
                        
                            $(".gridContainer-preview").dxDataGrid("instance").option("dataSource",[]);
                            $(".gridContainer-preview").dxDataGrid("instance").option("dataSource",data.findings);
                            $(".gridContainer-preview").dxDataGrid("instance").refresh();
                            
                            $("#popup3").hide();
                        
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
        }
    
    }

    function saveDivHeadComment(){
    
        var result_divHead = $("#divHead-text").dxValidator("instance").validate();
        
        if(result_divHead.isValid) {
        
            var divHead_comment = $("#divHead-text").dxTextArea("instance").option("value");
            
            if(pica_finding !==null){
            
                $.ajax({
                    type: 'POST',                
                    url: '<?= Url::to(['pica-finding/update-divhead-comment']);?>',
                    data: {
                        
                        id:pica_finding.id, 
                        comment:divHead_comment.replace(new RegExp('\r?\n','g'), '<br>'),
                        number:pica_finding.number,
                        order_number:pica_finding.order_number
                                
                    },
                    beforeSend: function() { $('#wait').show(); },
                    complete: function() { $('#wait').hide(); },
                    success: function(data){
                    
                        alert(data.message); 
                        
                        if(data.status==='fail'){
                            
                            //location.reload(); 
                        }else{
                        
                            $("#divHead-text").dxTextArea("instance").option("value",divHead_comment);
                        
                            $(".gridContainer-preview").dxDataGrid("instance").option("dataSource",[]);
                            $(".gridContainer-preview").dxDataGrid("instance").option("dataSource",data.findings);
                            $(".gridContainer-preview").dxDataGrid("instance").refresh();
                            
                            $("#popup3a").hide();
                        
                        }                  
                    
                    },
                    error: function(xhr, textStatus, error){
                        alert(xhr.statusText);
                        //location.reload();  
                    
                    }
                });  
            }
        }
    
    }
    
    function closeSupervisorComment(){
        
        $("#popup3").hide();
        $(".gridContainer-preview").dxDataGrid("instance").refresh()
    
    }
    
    function closeDivHeadComment(){
        
        $("#popup3a").hide();
        $(".gridContainer-preview").dxDataGrid("instance").refresh()
    
    }

    function closeProgress(){
        
        $("#popup5").hide();
        $(".internal-grid-corrective").dxDataGrid("instance").refresh()
    
    }
    
    function validateAndSubmitProgress() {
        
        var result = $("#progress-text").dxValidator("instance").validate();

        if(result.isValid) {
            
            var result = $("#progress-text").dxTextArea("instance").option("value");
        
            var progressText = result.replace(new RegExp('\r?\n','g'), '<br>');
            
            var form = document.forms.namedItem("fileinfo");
            oData = new FormData(form);
            oData.append("progress", progressText);
            oData.append("id", corrective_id);
            oData.append("isfile", isfile);

            $.ajax({
                type: 'POST',
                url: '<?= Url::to(['pica-corrective/update']);?>',
                data: oData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){
                    
                    alert(data.message);
                    
                    if(data.status==='success'){
                        
                        $(".internal-grid-corrective").dxDataGrid("instance").option("dataSource",[]);
                        $(".internal-grid-corrective").dxDataGrid("instance").option("dataSource",data.correctives);
                        $(".internal-grid-corrective").dxDataGrid("instance").refresh();
                        $("#popup5").hide();
                        
                    }
                    
//                  location.reload();
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);   
//                  location.reload();
                }
            });
            
        }else{
        
            alert("Please Fill Progress.");
        }
        
    }
    
});
</script>
<div class="content containerPlaceholder">
    <div class="title"><h1>Inbox</h1></div>
             
    <div class="pane dx-theme-desktop">
                
        <div id="gridContainer"></div>
                  
        <div id="preview"></div>
        
        <div id="popup2">
            
            <div class="popup2">
                
                <div id="comment-text"></div> 
                          
                <div id="button7"></div> 
                
                <div id="button8"></div> 
                
            </div>
            
        </div>

        <div id="popup2a">
            
            <div class="popup2a">
                
                <div id="coordinator-text"></div> 
                          
                <div id="button7a"></div> 
                
                <div id="button8a"></div> 
                
            </div>
            
        </div>
        
        <div id="popup3">
            
            <div class="popup3">

                <div id="form-field">
                    <form enctype="multipart/form-data" method="post" name="fileinfo">
                   
                       
                        <div id="form2"></div>

                    </form>  
                </div>
                <!--
                <div id="supervisor-text"></div> 
                -->
                          
                <div id="button11"></div> 
                
                <div id="button12"></div> 
                
            </div>
            
        </div>

         <div id="popup3a">
            
            <div class="popup3a">
                
                <div id="divHead-text"></div> 
                          
                <div id="button11a"></div> 
                
                <div id="button12a"></div> 
                
            </div>
            
        </div>
        
        <div id="popup5">
            
            <div class="popup5">
                
                <div id="form-field">
                    <form enctype="multipart/form-data" method="post" name="fileinfo">
                   
                        <div id="file-uploader"></div>
                        <div id="file-list"></div>
                        <div id="progress-text"></div>

                    </form>  
                </div>
                                
                <div id="button9"></div> 
                
                <div id="button10"></div> 
                
            </div>
            
        </div>

        <div id="popup6">
            
            <div class="popup6">

                <div id="internal-grid"></div>
                <br>
                <div id="button13"></div>
                                
            </div>
            
        </div>
                   
    </div>
            
</div>
