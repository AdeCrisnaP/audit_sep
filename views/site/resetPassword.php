<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
  
.reset-password {
    overflow: hidden;
    background-color: rgba(255, 255, 255, 0.75);
    width: 370px;
    float: left;
    height: 300px;
    border: 1px solid #e3e3e3;
    -moz-box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    -webkit-box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    margin-top:40px;
    padding:12px;
    border-radius: 10px;
}

.title-reset {
    text-align: center;    
    height: 45px;
}

.title-reset p {
    color: #f88e1d;
    font-size: 18px;    
    font-weight: bold;
}    
</style>

<div class="reset-password">
    <div class="title-reset">
        <p><?= Html::encode($this->title) ?></p>
    </div>

            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'confirm')->passwordInput(['autofocus' => true]) ?>
                
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>

</div>
