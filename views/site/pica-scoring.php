<?php

use yii\helpers\Url;
use yii\helpers\BaseJson;
use yii\helpers\BaseUrl;
use app\models\TbAuditIndustry;
use app\models\TbAuditRegion;
use app\models\TbAuditBusinessUnit;
use app\models\TbAuditPicaFinding; 


$this->title = 'PICA Scoring';

$user =  Yii::$app->user->identity;

$query1='';
$query2='';
$onprogress=0;
$overdue=0;
$closed=0;
$percentage=0;
$kpi = 0;
$month='';
$year='';
$date='';


if(Yii::$app->request->get('date')){

    $date = Yii::$app->request->get('date');
    $date2 = explode('-',$date);
    $day = $date2[0];
    $month = $date2[1];
    $year = $date2[2];

}else{

    $day = 1;
    $year= date('Y');
    $month= date('m');

}

$start = date('Y-m-d',strtotime($year.'-1-1'));

$end = date('Y-m-t',strtotime($year.'-'.$month.'-'.$day));

// $query3 = " AND date_posted <='$end' AND date_due <'$end' AND ( date_closed >'$end' OR date_closed IS NULL) ";
// $query4 = " AND date_closed BETWEEN '$start' AND '$end' ";
// $query5 = " AND date_posted <='$end' AND date_due >='$end' AND ( date_closed >'$end' OR date_closed IS NULL) ";

//29-08-2017
$query3 = " AND is_deleted='0' AND order_number>2 AND date_posted <='$end' AND date_due <'$end' AND (date_closed >'$end' OR date_closed IS NULL)"; 
$query4 = " AND is_deleted='0' AND order_number>2 AND date_closed BETWEEN '$start' AND '$end' AND status='Closed'";
$query5 = " AND is_deleted='0' AND order_number>2 AND date_posted <='$end' AND date_due >='$end' AND (date_closed >'$end' OR date_closed IS NULL)";

//20062017

// //overdue
// $query3 = " AND date_posted <='$end' AND date_due <'$end' AND (date_closed >'$end' OR date_closed IS NULL)"; //AND ( date_closed >'$end' OR date_closed IS NULL)
// //closed
// $query4 = " AND date_closed BETWEEN '$start' AND '$end' AND status='Closed'";
// //on schedule
// $query5 = " AND date_posted <='$end' AND date_due >='$end' AND (date_closed >'$end' OR date_closed IS NULL)"; //AND ( date_closed >'$end' OR da

if($user->division_id != 37){

    $industry = TbAuditIndustry::find($user->industry_id)->all();
  
    if($user->role_name !== 'COO' && $user->role_id != 10){
        $query1 = " AND id=$user->region_id";
        $query2 = " AND id IN ($user->business_unit_id) ";            
    } 

    if($user->role_name == 'Management Development - HO'){ //not IA and show all factory
        $industry = TbAuditIndustry::find()->all();
    } 
}else{
    $industry = TbAuditIndustry::find()->all();
}



function buKPIscore($kpi){

    if($kpi <= 65){return 1;}

    if($kpi <= 80){return 2;}

    if($kpi <= 90){return 3;}

    if($kpi <= 94.99){return 4;}else{return 5;}

}

?>

<!-- DataTables -->
<link rel="stylesheet" href="/plugins/datatables/dataTables.bootstrap.css">


<style>  

<?php if($user->division_id === '37' || $user->role_id === 5 || $user->role_id === 10){ ?>
    #form-field {
        height: auto;
        width: auto;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
    } 
<?php } ?>

table {
    font-family: arial, sans-serif;
     border: 0.5px solid #dddddd;
     width: 100%;
     margin-bottom:20px;
    
}

th {
    border: 0.5px solid #dddddd;
    text-align: center;
    padding: 8px;
}

td {
    border:0.5px solid #dddddd;
    text-align: left;
    padding: 8px;
}


#gridContainer{
    overflow:auto;
    font-size: 15px;
    width: auto;
    min-height:200px;
    padding:7px 7px 10px 7px;
    background-color:#ffffff;
    margin-top:5px;

}

</style>

<script id="jsCode">
                
    $(function (){

        
        var tanggal = new Date(<?= $year; ?>,<?= $month-1;?>);

        <?php if($user->division_id === '37' || $user->role_id === 5 || $user->role_id === 10){ ?>
        $("#form").dxForm({
            colCount: 2,
            width:"20%",
            items: [
                {
                    itemType: "group",
                    items: [
                        {
                            dataField: "ToDate",
                            editorType: "dxDateBox",
                            format: "date",
                            editorOptions: { 
                                formatString: "MM/yyyy",
                                displayFormat: 'monthAndYear',
                                maxZoomLevel: 'year', 
                                minZoomLevel: 'century', 
                                value:tanggal
                            }
                        }
                                            
                    ]
                },
                {
                    itemType: "group",
                    items: [
                        {
                            
                            editorType: "dxButton",
                            editorOptions: { 
                                text: "Submit",
                                type: "success", 
                                onClick: search
                            }
                        }
                        
                    ]
                }   
            ]
        });
        <?php } ?>

        function search(){
            
            var to = $("#form").dxForm("instance").option("formData").ToDate;

            
            var date = to !== undefined ? to.getDate()+"-"+(to.getMonth()+1)+"-"+to.getFullYear() : to ;

            var url = "<?= Url::to(['site/pica-scoring']);?>"+'&date='+date;

            $(location).attr('href', url);
            
        }

    });

</script>

<div class="content containerPlaceholder">
    <div class="title "><h1>PICA Scoring</h1></div>
    <div class="pane dx-theme-desktop"> 

        <div id="form-field">
        <div id="form"></div>
        </div>  
        <div id="gridContainer">
            <table id="example1">
                <thead>
                <tr style="background-color:#2CA8C2;">
                  <th rowspan="2" style="font-weight:bold;">Industry</th>
                  <th rowspan="2" style="font-weight:bold;">Region</th>
                  <th rowspan="2" style="font-weight:bold;">Business Unit</th>
                  <th colspan="2" style="font-weight:bold;">On Progress</th>
                  <th rowspan="2" style="font-weight:bold;">Closed</th>
                  <th rowspan="2" style="font-weight:bold;">Percentage</th>
                  <th rowspan="2" style="font-weight:bold;">KPI Achievement</th>
                  <th rowspan="2" style="font-weight:bold;">BU KPI Score</th>
                  
                </tr>
                <tr style="background-color:#2CA8C2;">
                  <th style="font-weight:bold;">On Schedule</th>
                  <th style="font-weight:bold;">Overdue</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach($industry as $index => $value){ ?>
                                   
                <?php $jumlah_industry = TbAuditBusinessUnit::find()->where("industry_id= $value->id   AND name<>'Head Office' ")->all(); ?>

                <?php $region = TbAuditRegion::find()->where("industry_id=$value->id   AND name<>'Head Office'  $query1 ")->all(); ?>

                <?php foreach($region as $index2 => $value2) { ?>

                <?php $bu = TbAuditBusinessUnit::find()->where("region_id=$value2->id $query2 ")->all(); $totalClosed = 0;  $totalOC=0;  ?>

                <?php foreach($bu as $index3 => $value3) { ?>

                <tr>
                
                  <?php if($index2 ===0 && $index3 ===0){?><td rowspan="<?= count($jumlah_industry)+count($region); ?>"><?= $value['name']; ?></td><?php } ?>
                  <?php if($index3 ===0){?><td rowspan="<?= count($bu); ?>"><?= $value2['name']; ?></td><?php } ?>
                  <td><?= $value3['name']; ?></td>
                  <td style="text-align:center;"><?= $onprogress=TbAuditPicaFinding::find()->where("business_unit_id=$value3->id $query5 ")->count(); ?></td>
                  <td style="text-align:center;"><?= $overdue=TbAuditPicaFinding::find()->where("business_unit_id=$value3->id $query3 ")->count(); ?></td>
                  <td style="text-align:center;"><?= $closed=TbAuditPicaFinding::find()->where("business_unit_id=$value3->id $query4 ")->count(); ?></td>
                  <td style="text-align:center;"><?= ($closed+$overdue) === 0 ? $percentage=100 : round($percentage = ($closed/($overdue+$closed))*100); ?> %</td>
                  <td style="text-align:center;"><?= ($percentage/90) >= 1 ? $kpi = 100 : round($kpi = ($percentage/90)*100) ;  ?> %</td>
                  <td style="text-align:center;"><?= buKPIscore($kpi); ?></td>
                </tr>

                <?php $totalClosed += $closed; $totalOC += ($overdue+$closed);  ?>

                <?php } ?>
                <?php $totalRegion = $totalOC === 0 ? 100 : round(($totalClosed/($totalOC))*100) ; ?>
                <tr style="background-color:#2CA8C2;color:white;"><td colspan="7" style="text-align:center;">Region <?= $value2['name']; ?> KPI Score</td><td style="text-align:center;"><?= buKPIscore(round(($totalRegion/90)*100)); ?></td></tr>

                <?php } ?>

                <?php } ?>

                </tbody>
            </table>
        </div>      
    </div>
</div>
<!-- SlimScroll -->
<script src="<?= BaseUrl::base()?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>    


<!-- DataTables 
<script src="/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>
-->


 <script type="text/javascript">
      $(function(){
        // $('#example1').DataTable({
        //     "paging": false,
        //     "lengthChange": false,
        //     "searching": true,
        //     "ordering": false,
        //     "info": false,
        //     "autoWidth": false
        // });
        $("#gridContainer").slimScroll({
          size: '8px', 
          width: '100%', 
          height: '680px', 
          color: 'gray', 
          allowPageScroll: true, 
          alwaysVisible: true     
        });
      });
    </script>