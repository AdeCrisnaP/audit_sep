<?php
use yii\helpers\Url;
use yii\helpers\BaseUrl;

$this->title = 'Upload Old PICA';

?>


<style>

    #form-field{
        height:140px;         
        width: auto;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px;
        background-color:#ffffff;
        margin-top:5px;
    }

    #button1 {
        float: right;
        
    }
    
    #button2 {
        float: right;
        
    }

    #file-uploader{
        display:block;
        float:left;
        width:49%;
    }
    
    
    
</style>
        
<script id="jsCode">
    $(function (){

        $("#button1").dxButton({
            text: "PICA Template",
            type: "default", 
            onClick: template
        });

        $("#button2").dxButton({
            text: "Upload File",
            type: "success", 
            onClick: validateAndSubmit
        });

        $("#file-uploader").dxFileUploader({
            selectButtonText: "Select File",
            labelText: "",
            accept: "*.*",   
            name:'file',
            uploadMode: "useForm",
        });

        function validateAndSubmit() {
        
            var form = document.forms.namedItem("fileinfo");
            oData = new FormData(form);
            
            $.ajax({
                type: 'POST',
                url: '<?= Url::to(['pica/upload']);?>',
                data: oData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){

                    // if(data.status ==='success'){
                        alert(data.message);
                    // }
                    
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);   
                }

            });

        
        }

        function template(){

             window.location.href = "<?= BaseUrl::base();?>"+"/uploads/template/pica_template.xls";

        }

    });               
   
</script>
        
<div class="content containerPlaceholder">
    <div class="title"><h1>Upload Old PICA</h1></div>
             
    <div class="pane dx-theme-desktop">
        <div id="form-field">
            <form enctype="multipart/form-data" method="post" name="fileinfo">
                   
                <div id="file-uploader"></div>

                <div id="button1"></div> 

            </form>  
        </div>
        <br>
        <div id="button2"></div>
        <br><br><br><br>
           
        
                   
    </div>
            
</div>
   