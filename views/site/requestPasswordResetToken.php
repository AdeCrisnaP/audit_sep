<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
  
.request-reset {
    overflow-x: hidden;
    background-color: rgba(255, 255, 255, 0.75);
    width: 370px;
    float: left;
    height: 280px;
    border: 1px solid #e3e3e3;
    -moz-box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    -webkit-box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    margin-top:40px;
    padding:12px;
    border-radius: 10px;
}

.title-request {
    text-align: center;    
    height: 45px;
}

.title-request p {
    color: #f88e1d;
    font-size: 18px;    
    font-weight: bold;
}    
</style>

<div class="request-reset">
    <div class="title-request">
        <p><?= Html::encode($this->title) ?></p>
    </div>


    <p>Please fill out your email. A link to reset password will be sent there.</p>

 
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
      
</div>
