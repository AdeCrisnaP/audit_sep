<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
  
.audit-login {
    overflow: hidden;
    background-color: rgba(255, 255, 255, 0.75);
    width: 370px;
    float: left;
    height: 450px;
    border: 1px solid #e3e3e3;
    -moz-box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    -webkit-box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    box-shadow: 0 13px 16px rgba(0, 0, 0, 0.5);
    margin:0px;
    padding:12px;
    border-radius: 10px;
}

.title-login {
    text-align: center;    
    height: 45px;
}

.title-login p {
    color: #f88e1d;
    font-size: 18px;    
    font-weight: bold;
}
    
</style>   

<div class="audit-login">
    <div class="title-login">
        <p><?= Html::encode($this->title) ?></p>
    </div>

    
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                
                <?= $form->field($model, 'industry')->dropdownList($items); ?>

                <?= $form->field($model, 'nik')->textInput(['autofocus' => true]) ?>

                /* <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?> */
    
                <div style="color:#222222;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                </div>
    
                /* <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div> */

            <?php ActiveForm::end(); ?>
       
</div>


