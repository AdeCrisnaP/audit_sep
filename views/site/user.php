<?php
use yii\helpers\Url;

$this->title='User';
?>

<style>  
    
    #form {
        height: auto;
        width: 100%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
    }

    #gridContainer {
        height: 350px;
        width: 100%;
        border-color: #EFEFEF;
        border-width: 2px;
        border-style: solid;
        padding:7px 7px 10px 7px;
        background-color:#ffffff;
        margin-top:5px;
    }

    #button {
        float: right;
        margin-top:10px;
    }

</style>

<script id="jsCode">
$(function ()
{      
        
    var users = <?= $data ?>;
    var business_unit = <?= $business_unit ?>;
    var industry = <?= $industry ?>;
    var region = <?= $region ?>;
    var location_ = <?= $location ?>;
    var division = <?= $division ?>;
    var role = <?= $role ?>;
       
    $("#form").dxForm({
        colCount: 2,
        items: [
            {
                itemType: "group",
                caption: "User Profile",
                items: [  
                    {
                        dataField: "id",
                        editorType: "dxTextBox",
                        visible:false,
                        editorOptions: {  
                            
                        }
                    },
                    {
                        dataField: "NIK", 
                        editorType: "dxTextBox", 
                        // editorOptions: {                            
                        //     onFocusOut: function (e) {
                        //        var nik = $("#form").dxForm("instance").option("formData").NIK;
                        //         if(e !== null){
                        //             $.ajax({
                        //                 type: 'POST',
                        //                 url: '<?= Url::to(['user/kiranaku']);?>',
                        //                 data: {
                        //                     nik:nik
                        //                 },
                        //                 beforeSend: function() { $('#wait').show(); },
                        //                 complete: function() { $('#wait').hide(); },
                        //                 success: function(data){
                    
                        //                     var emailEditor = $("#form").dxForm("instance").getEditor("email");
                        //                     emailEditor.option("value",data.email.toLowerCase());
                                        
                        //                     var nameEditor = $("#form").dxForm("instance").getEditor("name");
                        //                     nameEditor.option("value", convert_case(data.nama));
                                        
                        //                     var positionEditor = $("#form").dxForm("instance").getEditor("position");
                        //                     positionEditor.option("value", convert_case(data.posst));
                                        
                        //                 },
                        //                 error: function(xhr, textStatus, error){
                                      
                    
                        //                 }
                        //             });
                        //         }
                                   
                        //     }
                        // },
						
						editorOptions: {                            
                            onFocusOut: function (e) {
                               var nik = $("#form").dxForm("instance").option("formData").NIK;
                                if(e !== null){
                                    $.ajax({
                                        type: 'POST',
                                        url: '<?= Url::to(['user/kiranaku']);?>',
                                        data: {
                                            nik:nik
                                        },
                                        beforeSend: function() { $('#wait').show(); },
                                        complete: function() { $('#wait').hide(); },
                                        success: function(data){
											
											//alert("MASUK SUKSES");
                    
                                            var emailEditor = $("#form").dxForm("instance").getEditor("email");
                                            emailEditor.option("value",data.email.toLowerCase());
                                        
                                            var nameEditor = $("#form").dxForm("instance").getEditor("name");
											nameEditor.option("value", convert_case(data.nama));
                                        
                                            var positionEditor = $("#form").dxForm("instance").getEditor("position");
                                            positionEditor.option("value", convert_case(data.posst));
                                        
                                        },
                                        error: function(xhr, textStatus, error){
											
                                        }
                                    });
                                }
                                   
                            }
                        },
						
                        validationRules: [{
                            type: "required",
                            message: "NIK is required"
                        }]
                    },
                    {
                        dataField: "name",
                        editorType: "dxTextBox",
						editorOptions: { 
							readOnly: true
						},
                        validationRules: [{
                            type: "required",
                            message: "Name is required"
                        }]
                    },
                    {
                        dataField: "position",
                        editorType: "dxTextBox",
						editorOptions: { 
							readOnly: true
						},
                        validationRules: [{
                            type: "required",
                            message: "Position is required"
                        }]
                    },
                    {
                        dataField: "email",
                        editorType: "dxTextBox",
						editorOptions: { 
							readOnly: true
						},
                        validationRules: [{
                            type: "required",
                            message: "Email is required"
                        }]
                    },                    
                    {
                        dataField: "role",                        
                        editorType: "dxSelectBox",
                        editorOptions: { 
                            items: role,
                            displayExpr: "name",
                            valueExpr: "id"                            
                        },
                        validationRules: [{
                            type: "required",
                            message: "Role is required"
                        }]
                    }
                ]
            },  
            {
                itemType: "group",
                caption: "Area Authorization",
                items: [
                    // {
                    //     dataField: "Industry",
                    //     editorType: "dxSelectBox",                            
                    //     editorOptions:{ 
                    //         items: [],                                
                    //         displayExpr: "name",
                    //         valueExpr: "id",
                    //         onValueChanged: function (e) {},
                    //         onOpened: function (e){
                                
                    //             if(e !== null){
                                        
                    //                 var locationEditor = $("#form").dxForm("instance").getEditor("Location");
                    //                 locationEditor.option("dataSource", []); 
                    //                 locationEditor.option("value", null);
                                        
                    //                 var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                    //                 regionEditor.option("dataSource", []);
                    //                 regionEditor.option("value", null);
                                    
                    //                 var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                    //                 businessUnitEditor.option("dataSource", []);
                    //                 businessUnitEditor.option("value", null);
                                    
                    //                 var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                    //                 divisionEditor.option("dataSource", []);
                    //                 divisionEditor.option("value", null);
                                    
                    //                 var industryEditor = $("#form").dxForm("instance").getEditor("Industry");
                    //                 industryEditor.option("dataSource", industry);
                                        
                    //             }            
                    //         }
                    //     },
                    //     validationRules: [{
                    //             type: "required",
                    //             message: "Industry is required"
                    //     }]
                    // },                
                    // {
                    //     dataField: "Location",
                    //     editorType: "dxSelectBox",
                    //     editorOptions: { 
                    //         items: [],
                    //         displayExpr: "name",
                    //         valueExpr: "id",
                    //         onValueChanged: function (e) {},
                    //         onOpened: function (e){
                    //             if(e !== null){  
                                        
                    //                 var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                    //                 regionEditor.option("dataSource", []);
                    //                 regionEditor.option("value", null);
                                    
                    //                 var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                    //                 businessUnitEditor.option("dataSource", []);
                    //                 businessUnitEditor.option("value", null);
                                    
                    //                 var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                    //                 divisionEditor.option("dataSource", []);   
                    //                 divisionEditor.option("value", null);
                                    
                    //                 var locationEditor = $("#form").dxForm("instance").getEditor("Location");
                    //                 locationEditor.option("dataSource", location_);
         
                    //             }
                    //         }
                    //     },
                    //     validationRules: [{
                    //         type: "required",
                    //         message: "Location is required"
                    //     }]
                    // },                
                    // {
                    //     dataField: "Region",
                    //     editorType: "dxSelectBox",
                    //     editorOptions: { 
                    //         dataSource: [],
                    //         displayExpr: "name",
                    //         valueExpr: "id",
                    //         onValueChanged: function (e) {},
                    //         onOpened: function(e){
                                
                    //             if(e !==null){
                                        
                    //                 var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                    //                 businessUnitEditor.option("dataSource", []);
                    //                 businessUnitEditor.option("value", null);
                                    
                    //                 var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                    //                 divisionEditor.option("dataSource", []);
                    //                 divisionEditor.option("value", null);
                                    
                    //                 var filteredRegion = [];
                                        
                    //                 var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                    //                 var industry_id = $("#form").dxForm("instance").option("formData").Industry;            
                    //                 var location_id = $("#form").dxForm("instance").option("formData").Location;
                                
                    //                 filteredRegion = DevExpress.data.query(region).filter([["industry_id", "=", industry_id],"and",["location_id", "=", location_id]]).toArray();
                                    
                    //                 regionEditor.option("dataSource", filteredRegion);
                    //             }
                    //         }
                    //     },                            
                    //     validationRules: [{
                    //         type: "required",
                    //         message: "Region is required"
                    //     }]
                    // },
                    {
                        dataField: "Industry",
                        editorType: "dxTagBox",                            
                        editorOptions:{ 
                            items: [],                                
                            displayExpr: "name",
                            valueExpr: "id",
                            onValueChanged: function (e) {
                                if(e !== null){
                                    var locationEditor = $("#form").dxForm("instance").getEditor("Location");
                                    locationEditor.option("dataSource", []);
                                    locationEditor.option("value", null);

                                    var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                                    regionEditor.option("dataSource", []);
                                    regionEditor.option("value", null);

                                    var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                    businessUnitEditor.option("dataSource", []);
                                    businessUnitEditor.option("value", null);

                                    var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                                    divisionEditor.option("dataSource", []);
                                    divisionEditor.option("value", null);

                                    // var industryEditor = $("#form").dxForm("instance").getEditor("Industry");
                                    // industryEditor.option("dataSource", industry);

                                }
                            },
                            onOpened: function (e){
                                var industryEditor = $("#form").dxForm("instance").getEditor("Industry");
                                industryEditor.option("dataSource", industry);
                            }
                        },
                        validationRules: [{
                                type: "required",
                                message: "Industry is required"
                        }]
                    },
                    {
                        dataField: "Location",
                        editorType: "dxTagBox",
                        editorOptions: { 
                            items: [],
                            displayExpr: "name",
                            valueExpr: "id",
                            onValueChanged: function (e) {
                                if(e !== null){
                                    var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                                    regionEditor.option("dataSource", []);
                                    regionEditor.option("value", null);
                                    
                                    var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                    businessUnitEditor.option("dataSource", []);
                                    businessUnitEditor.option("value", null);
                                    
                                    var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                                    divisionEditor.option("dataSource", []);   
                                    divisionEditor.option("value", null);
         
                                }
                            },
                            onOpened: function (e){
                                var locationEditor = $("#form").dxForm("instance").getEditor("Location");
                                locationEditor.option("dataSource", location_);
                            }
                        },
                        validationRules: [{
                            type: "required",
                            message: "Location is required"
                        }]
                    },                
                    {
                        dataField: "Region",
                        editorType: "dxTagBox",
                        editorOptions: { 
                            items: [],
                            displayExpr: "name",
                            valueExpr: "id",
                            onValueChanged: function (e) {},
                            onOpened: function(e){
                                
                                if(e !==null){
                                        
                                    var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                    businessUnitEditor.option("dataSource", []);
                                    businessUnitEditor.option("value", null);
                                    
                                    var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                                    divisionEditor.option("dataSource", []);
                                    divisionEditor.option("value", null);

                                    var regionEditor = $("#form").dxForm("instance").getEditor("Region");
                                    var industry_id = $("#form").dxForm("instance").option("formData").Industry;
                                    var location_id = $("#form").dxForm("instance").option("formData").Location;
                                    var filteredRegion = DevExpress.data.query(region).filter(function(dataItem){
                                                                return ($.inArray(dataItem.industry_id, industry_id) != -1 && $.inArray(dataItem.location_id, location_id) != -1);
                                                            }).toArray();

                                    regionEditor.option("dataSource", filteredRegion);
                                }
                            }
                        },                            
                        validationRules: [{
                            type: "required",
                            message: "Region is required"
                        }]
                    },
                    {
                        dataField: "BusinessUnit",
                        editorType: "dxTagBox",
                        editorOptions: { 
                            items: [],
                            displayExpr: "name",
                            valueExpr: "id",
                            onValueChanged: function (e) {},
                            onOpened: function(e){
                                
                                if(e !== null){                                    
                                        
                                    var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                                    divisionEditor.option("dataSource", []);
                                    divisionEditor.option("value", null);
                                       
                                    var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
                                    var region_id = $("#form").dxForm("instance").option("formData").Region;
                                    var filteredBusinessUnit = DevExpress.data.query(business_unit).filter(function(dataItem){
                                                                return ($.inArray(dataItem.region_id, region_id) != -1);
                                                            }).toArray();
                                    
                                    businessUnitEditor.option("dataSource", filteredBusinessUnit);  

                                }
                                
                            }
                        },
                        validationRules: [{
                            type: "required",
                            message: "Business Unit is required"
                        }]
                    },                
                    {
                        dataField: "Division",
                        editorType: "dxTagBox",
                        editorOptions: { 
                            items: [],
                            displayExpr: "name",
                            valueExpr: "id",
                            onValueChanged: function (e) {},
                            onOpened: function(e){
                                
                                    if(e !== null){
                                        var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
                                        var industry_id = $("#form").dxForm("instance").option("formData").Industry;            
                                        var location_id = $("#form").dxForm("instance").option("formData").Location;

                                        var filteredDivision = DevExpress.data.query(division).filter(function(dataItem){
                                                                return ($.inArray(dataItem.industry_id, industry_id) != -1 && $.inArray(dataItem.location_id, location_id) != -1);
                                                            }).toArray();
                                    
                                        divisionEditor.option("dataSource", filteredDivision);  
                                    }
                            }
                        },
                        validationRules: [{
                                type: "required",
                                message: "Division is required"
                        }]
                    }                    
                ]
            }             
        ]
    });
    
    $("#form").dxForm("instance").validate(); 

    $("#gridContainer").dxDataGrid({
        dataSource: users,
        hoverStateEnabled: true,
        showRowLines:true,
        rowAlternationEnabled:true,
        columnAutoWidth: true,
        searchPanel: {
            visible: true
        },
        selection: {
            mode: "single"
        },
        export: {
            enabled: true,
            fileName: "Users"
        },
        editing: {
            mode: "row",            
            allowDeleting: true,
        },
        allowColumnReordering: true,
        allowColumnResizing: true,
        columnFixing: { 
            enabled: true
        },
        paging: {
            pageSize: 5
        },
        onRowRemoving: function(e) {
    
            $.ajax({
                type: 'POST',
                url: '<?= Url::to(['user/delete']);?>',
                data: {
                    id:e.key.id
                },
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                success: function(data){
                    
//                if(data.status==='fail'){
                    alert(data.message);  
//                }   
                
                    location.reload();
                },
                error: function(xhr, textStatus, error){
                    alert(xhr.statusText);
                    location.reload(); 
                    
                    //DI BAWAH INI JANGAN DIHAPUS UNTUK DEBUG      
                    //console.log(xhr.statusText);
                    //console.log(textStatus);
                    //console.log(error);
                }
            });

        },
        columns: [
            {
                dataField: "no",                
                caption: "No.",                
                allowSearch:false,
                fixed:true,
            },
            {
                dataField: "nik",                
                fixed:true,
                caption: "NIK"
            },
            {
                dataField: "username",
                fixed:true,
                caption: "Name"
            },            
            {
                dataField: "email",
                fixed:true,                
                caption: "Email"
            },
            {
                dataField: "position", 
                caption: "Position"
            },
            {
                dataField: "role_name",   
                caption: "Role"                
            },
            {
                dataField: "industry_id",
                caption: "Industry",
                validationRules: [
                    { type: "required" }, 
            
                ],
                lookup: {
                    dataSource: industry,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },
            {
                dataField: "location_id",
                caption: "Location",
                validationRules: [{ type: "required" }],            
                lookup: {
                    dataSource: location_,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },            
            {
                dataField: "region_id",
                caption: "Region",
                validationRules: [{ type: "required" }],            
                lookup: {
                    dataSource: region,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            }, 
            {
                dataField: "business_unit_id",
                caption: "Business Unit",
                validationRules: [{ type: "required" }],            
                lookup: {
                    dataSource: business_unit,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },
            {
                dataField: "division_id",
                caption: "Division",
                validationRules: [{ type: "required" }],            
                lookup: {
                    dataSource: division,
                    displayExpr: "name",
                    valueExpr: "id"
                }
            },           
            {
                dataField: "user_name",
                width: 115,
                caption: "Author",
                allowEditing:false,
            },
            {
                dataField: "date_created",
                width: 130,
                dataType: "date",
                allowEditing:false,
                caption: "Created Date"
            }
        ],
        onSelectionChanged: function (selectedItems) {
            var data = selectedItems.selectedRowsData[0]; 
            setProfile(data);
            
            setArea(data);
        }
    });
    
    $("#button").dxButton({
        text: "Submit",
        type: "success", 
        onClick: validateAndSubmit
    });
    
    function validateAndSubmit () {
        var result = $("#form").dxForm("instance").validate();  
        if(result.isValid) {
            
            var data = $("#form").dxForm("instance").option('formData');
            createProfile(data);
            
        }else{
            alert('Silakan Lengkapi Data');
        }
    }
    
    function convert_case(str) {
        var lower = str.toLowerCase();
        return lower.replace(/(^| )(\w)/g, function(x) {
            return x.toUpperCase();
        });
    }
    
    function createProfile(user){
        
        $.ajax({
            type: 'POST',
            url: '<?= Url::to(['user/create']);?>',
            data: {                
                id : user.id === undefined ? 0 : user.id,
                data: user                
            },
            beforeSend: function() { $('#wait').show(); },
            complete: function() { $('#wait').hide(); },
            success: function(data){
                
//                if(data.status==='fail'){    
                    alert(data.message);                    
//                }
                location.reload();
            },
            error: function(xhr, textStatus, error){
                
                alert(xhr.statusText);
                location.reload();
                    
            }
        });
        
    }
    
    function setProfile(data){
        
        var datax = $("#form").dxForm("instance").option("formData"); 
        
        datax.id = data.id;
        
        datax.NIK = data.nik;
        
        datax.name = data.username;
        
        datax.position = data.position;
        
        datax.email = data.email ;
        
        $("#form").dxForm("instance").option("formData", datax);
        
        var roleEditor = $("#form").dxForm("instance").getEditor("role");
        roleEditor.option("dataSource", role);
        roleEditor.option("value", data.role_id);
    }
    
    function setArea(data){
        //==================================Industry===============================//
        var industryEditor = $("#form").dxForm("instance").getEditor("Industry");
        var industry_id = data.industry_id.split(",");
        var operands = [];
        industry_id.forEach(function (i) {
            operands.push(["id", "=", i])

        });
        var criteria = [];
        operands.forEach(function (i) {
            if (criteria.length === 0) {
                criteria.push(i)
            }
            else {
                criteria.push("OR");
                criteria.push(i)
            }
        });
        var industry_id = [];
        var filteredIndustry = DevExpress.data.query(industry).filter(criteria).toArray();
        filteredIndustry.forEach(function(i){
            industry_id.push(i['id']);
        });
        industryEditor.option("dataSource", industry);
        industryEditor.option("values", []);
        industryEditor.option("values", industry_id);

        //==================================Location===============================//
        var locationEditor = $("#form").dxForm("instance").getEditor("Location");
        var location_id = data.location_id.split(",");
        var operands = [];
        location_id.forEach(function (i) {
            operands.push(["id", "=", i])

        });
        var criteria = [];
        operands.forEach(function (i) {
            if (criteria.length === 0) {
                criteria.push(i)
            }
            else {
                criteria.push("OR");
                criteria.push(i)
            }
        });
        var location_id = [];
        var filteredLocation = DevExpress.data.query(location_).filter(criteria).toArray();
        filteredLocation.forEach(function(i){
            location_id.push(i['id']);
        });
        locationEditor.option("dataSource", location_);
        locationEditor.option("values", []);
        locationEditor.option("values", location_id);

        //==================================Region===============================//
        var regionEditor = $("#form").dxForm("instance").getEditor("Region");
        var region_id = data.region_id.split(",");
        var operands = [];
        region_id.forEach(function (i) {
            operands.push(["id", "=", i])

        });
        var criteria = [];
        operands.forEach(function (i) {
            if (criteria.length === 0) {
                criteria.push(i)
            }
            else {
                criteria.push("OR");
                criteria.push(i)
            }
        });
        var region_id = [];
        var filteredRegion = DevExpress.data.query(region).filter(criteria).toArray();
        filteredRegion.forEach(function(i){
            region_id.push(i['id']);
        });
        regionEditor.option("dataSource", region);
        regionEditor.option("values", []);
        regionEditor.option("values", region_id);

        //==================================Business Unit===============================//
        var businessUnitEditor = $("#form").dxForm("instance").getEditor("BusinessUnit");
        var business_unit_id = data.business_unit_id.split(",");
        var operands = [];
        business_unit_id.forEach(function (i) {
            operands.push(["id", "=", i])
            
        });
        var criteria = [];
        operands.forEach(function (i) {
            if (criteria.length === 0) {
                criteria.push(i)
            }
            else {
                criteria.push("OR");
                criteria.push(i)
            }
        });
        var business_unit_id = [];
        var filteredBusinessUnit = DevExpress.data.query(business_unit).filter(criteria).toArray();
        filteredBusinessUnit.forEach(function(i){
            business_unit_id.push(i['id']);
        }); 
        businessUnitEditor.option("dataSource", business_unit);
        businessUnitEditor.option("values", []);
        businessUnitEditor.option("values", business_unit_id);
        
        //==================================Division===============================//
        var divisionEditor = $("#form").dxForm("instance").getEditor("Division");
        var division_id = data.division_id.split(",");
        var operands = [];
        division_id.forEach(function (i) {
            operands.push(["id", "=", i])
            
        });
        var criteria = [];
        operands.forEach(function (i) {
            if (criteria.length === 0) {
                criteria.push(i)
            }
            else {
                criteria.push("OR");
                criteria.push(i)
            }
        });
        var division_id = [];
        var filteredDivision = DevExpress.data.query(division).filter(criteria).toArray();
        filteredDivision.forEach(function(i){
            division_id.push(i['id']);
        }); 
        divisionEditor.option("dataSource",division);
        divisionEditor.option("values", []);
        divisionEditor.option("values", division_id);
       
    }

});
</script>
<div class="content containerPlaceholder">
    <div class="title "><h1>Users</h1></div>
    <div class="pane dx-theme-desktop">   
        <div id="form"></div> 
        <div class="dx-field"><div id="button"></div></div>
        <div id="gridContainer"></div>                   
    </div>
</div>
        
        