<?php

date_default_timezone_set('Asia/Jakarta');

include "PHPMailer/PHPMailerAutoload.php";

// $link = mssql_connect('10.0.0.32', 'audit', 'Audit@21')  or die("Connection Fault");
// $db = mssql_select_db('audit', $link) or die("Database not found");

$link = mssql_connect('10.0.0.35', 'audit', 'audit212')  or die("Connection Fault");
$db = mssql_select_db('uat_audit', $link) or die("Database not found");

function send($user,$query){
    
    $mail = new PHPMailer;
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'mail.kiranamegatara.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'no-reply@kiranamegatara.com';                 // SMTP username
    $mail->Password = '1234567890';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;

    $mail->setFrom('no-reply@kiranamegatara.com', 'Interal Audit');
    // $mail->addAddress($user[3], $user[1]);
    //$mail->addAddress('airiza@kiranamegatara.com', $user[1]);
    //$mail->addAddress('isa@kiranamegatara.com', $user[1]);
    $mail->isHTML(true);  
    $mail->Subject = 'Outstanding PICA'; 

    // $mail_p = new PHPMailer;
    // $mail_p->isSMTP();                                      // Set mailer to use SMTP
    // //$mail_p->Host = '174.127.88.10';  // Specify main and backup SMTP servers
    // //$mail_p->Host = '103.53.198.65';  // Specify main and backup SMTP servers //modified by AFH 14.07.2015
    // $mail_p->Host = '103.24.13.190';  // Specify main and backup SMTP servers //modified by AFH 10.08.2015
    // $mail_p->SMTPAuth = true;                               // Enable SMTP authentication
    // $mail_p->Username = 'no-reply@kiranamegatara.com'; 
    // $mail_p->Password = '1234567890'; 
    // //$mail_p->Port = 587;                                    // TCP port to connect to
    // $mail_p->Port = 25;                                    // TCP port to connect to //modified by AFH 10.08.2015
    // $mail_p->From = 'no-reply@kiranamegatara.com';
    // $mail_p->FromName = 'Kiranalytics-AMS';
    // $recepient_name = getafield('fullname','tb_users','id_user',$recepient_d['id_user']);
    // $recepient_email = getafield('email','tb_users','id_user',$recepient_d['id_user']);
   
    $message ="";

    $message .= "
    <html>
    <head>
    
    <style type='text/css'>

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            text-align: center;
            padding: 8px;
            font-size:12px;
            font-family:'Calibri,Times New Roman';
            text-align:center;
            border-bottom: 1px solid #ddd;
        }

        tr:nth-child(even){background-color: #f2f2f2}

        th {
            background-color: #4CAF50;
            color: white;
        }
      
        
    </style>

    </head>
    <body>

    <div class='password-reset'>
        <p>Dear Bapak / Ibu ".$user[1]." </p>

        <p>Due Date penyelesaian temuan pada PICA : </p>

        <table>

        <tr><th>No. PICA<th><th>jumlah Finding</th></tr>";

        while ($row2 = mssql_fetch_array($query, MSSQL_NUM)) {

            $message .= '<tr><td>'.$row2[0].'<td><td>'.$row2[1].'</td></tr>';

        }

        $message .='</table>

        <p>Sudah/akan jatuh tempo.</p>

        <p>Dimohon agar Bapak/Ibu dapat segera menyelesaikan temuan tersebut.</p>

        <p>Atas perhatian dan kerjasama Bapak/Ibu kami ucapkan terima kasih.</p>

    </div>

    </body>
    </html>';
    
    $mail->Body = $message;

    echo $message;
    echo "<br /><hr /><br />";

    if(!$mail->send()) {        
        //echo 'Message could not be sent.';
        //echo 'Mailer Error: ' . $mail->ErrorInfo;   
    } else {
        echo 'Message has been sent';
    }

}

$sql0 = "SELECT alert1,alert2 FROM tb_audit_alert";
$query0 = mssql_query($sql0,$link);

while ($row0 = mssql_fetch_array($query0, MSSQL_NUM)) {
    //echo (date('t')-date('d'))." || ".$row0[0]." OR ".$row0[1]."--".date('t')." & ".date('d');  
    if( (date('t')-date('d')) === $row0[0] || (date('t')-date('d')) === $row0[1]){

        /* ========================================================= Send to PIC =============================================================== */
        //loop data karyawan dengan role sebagai PIC
        $sql = "SELECT * FROM tb_audit_user WHERE role_id=4";
        $query = mssql_query($sql,$link);
        $x=0;   $y=0;   $z=0;
        while ($row = mssql_fetch_array($query, MSSQL_NUM)) {

            //cek apakah ada yang masih outstanding    
            $sql2 = "SELECT number, COUNT(*) AS jumlah FROM (SELECT number,finding_id FROM tb_audit_pica_corrective WHERE next_nik='{$row[2]}' AND status <>'Closed' GROUP BY number,finding_id) A GROUP BY number";
            $query2 = mssql_query($sql2,$link);
            $query2x = mssql_query($sql2,$link);
            $cekx=0;
            //Jika ada, Send Email
            if($query2){
                while ($rowx = mssql_fetch_array($query2x, MSSQL_NUM)) {
                    $cekx   = $rowx[0];
                }
                if($cekx!=0){
                    send($row,$query2);
                }               
                $x++;            
            }
            mssql_free_result($query2);       
        }
        mssql_free_result($query);    


        /* ============================================================== Send to PIC's Head  ========================================================= */
        //loop data karyawan dengan role sebagai Head

        $sql3 = "SELECT * FROM tb_audit_user WHERE role_id=5";
        $query3 = mssql_query($sql3,$link);

        while ($row3 = mssql_fetch_array($query3, MSSQL_NUM)) {

            //cek apakah ada yang masih outstanding  
            if(intval($row3[11]) === 1){                    
                $sql4 = "SELECT number, COUNT(*) AS jumlah FROM (SELECT number,finding_id FROM tb_audit_pica_corrective WHERE (order_number BETWEEN 3 AND 4) AND status <>'Closed' AND industry_id={$row3[10]} AND division_id IN ({$row3[14]}) GROUP BY number,finding_id) A GROUP BY number"; 
            }else{
                $sql4 = "SELECT number, COUNT(*) AS jumlah FROM (SELECT number,finding_id FROM tb_audit_pica_corrective WHERE (order_number BETWEEN 3 AND 4) AND status <>'Closed' AND industry_id={$row3[10]} AND region_id={$row3[12]} AND business_unit_id in ({$row3[13]})  AND division_id IN ({$row3[14]}) GROUP BY number,finding_id) A GROUP BY number"; 
            }
           
            $query4 = mssql_query($sql4,$link);
            $query4y = mssql_query($sql4,$link);
            $ceky=0;
            //Jika ada, Send Email
            if($query4){
                while ($rowy = mssql_fetch_array($query4y, MSSQL_NUM)) {
                    $ceky   = $rowy[0];
                }
                if($ceky!=0){
                    send($row3,$query4);
                }
                $y++;
            }
            
            mssql_free_result($query4);

        }
        //update date on tb_audit_alert
        $date = date('Y-m-d h:i:s');
        $sql5 = "UPDATE tb_audit_alert SET date_updated='{$date}'";       
        $query5 = mssql_query($sql5,$link);
        mssql_free_result($query3);

         /* ==============================================================Send to IA Coordinator ======================================================*/
        //loop data karyawan dengan role sebagai div IA 
        $sqla = "SELECT * FROM tb_audit_user WHERE division_id='37'";
        $querya = mssql_query($sqla,$link);
        $z=0;
        while ($rowa = mssql_fetch_array($querya, MSSQL_NUM)) {

            //cek apakah ada yang masih outstanding    
            $sql2a = "SELECT number, COUNT(*) AS jumlah FROM (SELECT number,finding_id FROM tb_audit_pica_corrective WHERE CHARINDEX('$rowa[2]', next_nik)>0 AND status <>'Closed' GROUP BY number,finding_id) A GROUP BY number";
            $query2a = mssql_query($sql2a,$link);
            $query2ax = mssql_query($sql2a,$link);
            $cekax=0;
            //Jika ada, Send Email
            if($query2a){
                while ($rowax = mssql_fetch_array($query2ax, MSSQL_NUM)) {
                    $cekax   = $rowax[0];                   
                }
                if($cekax!=0){
                    send($rowa,$query2a);
                }
                $z++;
            }
            mssql_free_result($query2a);        
        }
        mssql_free_result($querya);


         /* ============================================================== Send to Super User ========================================================== */
        //loop data karyawan dengan role sebagai Super USer
        $sqlb = "SELECT * FROM tb_audit_user WHERE role_id=1";
        $queryb = mssql_query($sqlb,$link);
        while ($rowb = mssql_fetch_array($queryb, MSSQL_NUM)) {

            //cek apakah ada yang masih outstanding    
            $sql2b = "SELECT number, COUNT(*) AS jumlah FROM (SELECT number,finding_id FROM tb_audit_pica_corrective WHERE next_nik <> ' ' AND status <>'Closed' GROUP BY number,finding_id) A GROUP BY number";
            $query2b = mssql_query($sql2b,$link);
            $query2bx = mssql_query($sql2b,$link);
            $cekbx=0;
            //Jika ada, Send Email
            if($query2b){
                while ($rowbx = mssql_fetch_array($query2bx, MSSQL_NUM)) {
                    $cekbx   = $rowbx[0];                   
                }
                if($cekbx!=0){
                    send($rowb,$query2b);
                }               
                $z++;            
            }
            mssql_free_result($query2b);        
        }
        mssql_free_result($queryb);

    }

}

mssql_free_result($query0);

mssql_close($link); 

?> 