<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Dear <?= Html::encode($user->username) ?>,</p>

    <p>Anda didaftarkan sebagai pengguna aplikasi <b>Internal Audit Tracking System</b>. </p>
    
    <p>Silakan klik tautan berikut untuk membuat/merubah password :</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
