<?php
use yii\helpers\Html;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/request-password-reset']);

?>

<div class="password-reset">

    <p>Dear Bapak / Ibu <?= Html::encode($user->username) ?>,</p>
 
    <p>Anda terdaftar dalam aplikasi <b>Internal Audit Tracking System </b>.</p>
     
    <p>Silakan masukan email Anda <b> <?= Html::encode($user->email) ?> </b> pada tautan berikut untuk membuat password :</p>
 
    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>

    <p>Atas perhatian dan kerjasama Bapak/Ibu kami ucapkan terima kasih.</p>
    
 </div>