<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user app\models\User */


?>

<div class="password-reset">

<p>Dear Bapak / Ibu <?= $pica['username']; ?>,</p>

<p>Temuan dengan rincian sebagai berikut :</p>

<p>"<?= $pica['finding']; ?>"</p>

<p>Dengan nomor PICA [<?= $pica['pica_number']; ?>],</p>

<p>Status temuan tersebut telah dinyatakan <b><i>CLOSED</i>.</b></p>

<p>Atas perhatian dan kerjasama Bapak/Ibu kami ucapkan terima kasih.</p>

</div>