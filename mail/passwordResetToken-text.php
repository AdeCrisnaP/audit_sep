<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
Dear <?= $user->username ?>,

Silkan klik tautan berikut untuk merubah password Anda :

<?= $resetLink ?>
