<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\User */

?>
<div class="password-reset">
    <p>Dear Bapak / Ibu <?= Html::encode($user->username) ?>,</p>

    <p>Anda terdaftar dalam aplikasi <b>Internal Audit Tracking System </b>.</p>
    
    <p>Silakan akses melalui portal <?= Html::a(Html::encode('http://kiranaku.kiranamegatara.com/home/'), 'http://kiranaku.kiranamegatara.com/home/') ?> pada menu <b>Internal Audit Tracking System</b>.</p>

    <p>Atas perhatian dan kerjasama Bapak/Ibu kami ucapkan terima kasih.</p>
    
</div>
