<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user app\models\User */


?>

    <style>


        table {
    border-collapse: collapse;
    width: 100%;
}

    th, td {
        text-align: center;
        padding: 8px;
        font-size:12px;
        font-family:'Calibri,Times New Roman';
        text-align:center;
        border-bottom: 1px solid #ddd;
    }

    tr:nth-child(even){background-color: #f2f2f2}

    th {
        background-color: #4CAF50;
        color: white;
    }

       
        
    </style>

<div class="password-reset">
<p>Dear Bapak / Ibu <?= $pica['username']; ?>,</p>

<p>Due Date penyelesaian temuan pada PICA :</p>

<table>

<tr><th>No. PICA<th><th>jumlah Finding</th></tr>

<?php foreach($pica['data'] as $index => $value){ ?>

<tr><td><?= $value['number']; ?><td><td><?= $value['jumlah']; ?></td></tr>

<?php } ?>

</table>

<p>Sudah/akan jatuh tempo.</p>

<p>Dimohon agar Bapak/Ibu dapat segera menyelesaikan temuan tersebut.</p>

<p>Atas perhatian dan kerjasama Bapak/Ibu kami ucapkan terima kasih.</p>

</div>