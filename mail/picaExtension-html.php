<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user app\models\User */


?>

<div class="password-reset">

<p>Dear Bapak / Ibu <?= $pica['username']; ?>,</p>

<p>Perpanjangan tenggat penyelesaian temuan untuk <?= $pica['business_unit_name']; ?> sudah di-update.</p>

<p>Dengan rincian temuan berikut :</p>

<p>"<?= $pica['finding']; ?>"</p>

<p>dengan nomor PICA <?= $pica['pica_number']; ?> </p>

<p>Due Date sebelumnya : <?= substr(date('F',strtotime($pica['date_due'])),0,3).'-'.date('Y',strtotime($pica['date_due'])); ?></p>

<p>Due Date setelahnya : <?= substr(date('F',strtotime($pica['date_extension'])),0,3).'-'.date('Y',strtotime($pica['date_extension'])); ?></p>

<p>Dengan adanya perpanjangan ini, dimohon agar dapat menyelesaikan temuan tersebut sebelum jatuh tempo.</p>

<p>Atas perhatian dan kerjasama Bapak/Ibu kami ucapkan terima kasih.</p>

</div>