<?php

namespace app\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use app\models\TbAuditIndustry;
use app\models\TbAuditLocation;
use app\models\TbAuditRegion;
use app\models\TbAuditBusinessUnit;
use app\models\TbAuditDivision;  
use app\models\TbAuditCategory;  
use app\models\TbAuditRating;  
use app\models\TbAuditUser;
use app\models\TbAuditPicaFindingTemp;
use app\models\TbAuditPicaHeader;
use app\models\TbAuditPicaCorrective;
use app\models\TbAuditPicaFinding;
use app\controllers\FileController;


/**
 * PicaController implements the CRUD actions for TbAuditPicaFinding model.
 */
class PicaFindingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new TbAuditPicaFinding model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateTemp()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = new TbAuditPicaFindingTemp();  
        $model->auditor_nik = Yii::$app->user->identity->nik;
        $model->auditor_name = Yii::$app->user->identity->username; 
        $model->auditor_position = Yii::$app->user->identity->position; 
        $model->rating_id= Yii::$app->request->post('rating_id');
        $model->rating_name=TbAuditRating::findOne($model->rating_id)->name;
        $model->category_id= Yii::$app->request->post('category_id');
        $model->category_name=TbAuditCategory::findOne($model->category_id)->name;
        $model->industry_id = Yii::$app->request->post('industry_id');
        $model->industry_name = TbAuditIndustry::findOne($model->industry_id)->name;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->region_id = Yii::$app->request->post('region_id');
        $model->region_name = TbAuditRegion::findOne($model->region_id)->name;
        $model->business_unit_id = Yii::$app->request->post('business_unit_id');
        $model->business_unit_name = TbAuditBusinessUnit::findOne($model->business_unit_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;
        $model->approval_status='Submitted';        
        $model->date_due=Yii::$app->request->post('date_due');
        $model->date_posted=Yii::$app->request->post('date_posted');
        
        if ($model->save()) {
            
            $model->id = Yii::$app->db->getLastInsertID();
            $findings = TbAuditPicaFindingTemp::find()->where(['business_unit_id' =>$model->business_unit_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','finding'=>$model,'findings'=>$findings];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionCreateTempOld()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = new TbAuditPicaFindingTemp2();  
        $model->number= Yii::$app->request->post('number');
        $model->auditor_nik = Yii::$app->user->identity->nik;
        $model->auditor_name = Yii::$app->user->identity->username; 
        $model->auditor_position = Yii::$app->user->identity->position; 
        $model->rating_id= Yii::$app->request->post('rating_id');
        $model->rating_name=TbAuditRating::findOne($model->rating_id)->name;
        $model->category_id= Yii::$app->request->post('category_id');
        $model->category_name=TbAuditCategory::findOne($model->category_id)->name;
        $model->industry_id = Yii::$app->request->post('industry_id');
        $model->industry_name = TbAuditIndustry::findOne($model->industry_id)->name;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->region_id = Yii::$app->request->post('region_id');
        $model->region_name = TbAuditRegion::findOne($model->region_id)->name;
        $model->business_unit_id = Yii::$app->request->post('business_unit_id');
        $model->business_unit_name = TbAuditBusinessUnit::findOne($model->business_unit_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;
        $model->approval_status='Submitted';        
        $model->date_due=Yii::$app->request->post('date_due');
        $model->date_posted=Yii::$app->request->post('date_posted');
        
        if ($model->save()) {
            
            $model->id = Yii::$app->db->getLastInsertID();
            $findings = TbAuditPicaFindingTemp2::find()->where(['number' =>$model->number])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','finding'=>$model,'findings'=>$findings];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionCreate()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = new TbAuditPicaFinding();  
        $model->auditor_nik = Yii::$app->user->identity->nik;
        $model->auditor_name = Yii::$app->user->identity->username; 
        $model->auditor_position = Yii::$app->user->identity->position;
        $model->team_leader_nik = Yii::$app->request->post('leader');
        $usr = TbAuditUser::find()->where(['nik'=>$model->team_leader_nik])->one();
        $model->team_leader_name = $usr->username;
        $model->team_leader_position = $usr->position;
        $model->number = Yii::$app->request->post('number');
        $model->finding = '';
        $model->finding_file = '';
        $model->rating_id= Yii::$app->request->post('rating_id');
        $model->rating_name=TbAuditRating::findOne($model->rating_id)->name;
        $model->category_id= Yii::$app->request->post('category_id');
        $model->category_name=TbAuditCategory::findOne($model->category_id)->name;
        $model->industry_id = Yii::$app->request->post('industry_id');
        $model->industry_name = TbAuditIndustry::findOne($model->industry_id)->name;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->region_id = Yii::$app->request->post('region_id');
        $model->region_name = TbAuditRegion::findOne($model->region_id)->name;
        $model->business_unit_id = Yii::$app->request->post('business_unit_id');
        $model->business_unit_name = TbAuditBusinessUnit::findOne($model->business_unit_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;
        $model->date_due=Yii::$app->request->post('date_due');
        $model->date_posted=Yii::$app->request->post('date_posted');
        $model->status ='Open';
        $model->order_number = 0;
        $model->next_nik = Yii::$app->user->identity->nik;

        if ($model->save()) {
            
            $model->id = Yii::$app->db->getLastInsertID();
            $findings = TbAuditPicaFinding::find()->where(['number' =>$model->number])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','finding'=>$model,'findings'=>$findings];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }
    
    public function actionUpdateTemp()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $model = TbAuditPicaFindingTemp::findOne($id);
        
        $model->auditor_nik = Yii::$app->user->identity->nik;
        $model->auditor_name = Yii::$app->user->identity->username; 
        $model->rating_id= Yii::$app->request->post('rating_id');
        $model->rating_name=TbAuditRating::findOne($model->rating_id)->name;
        $model->category_id= Yii::$app->request->post('category_id');
        $model->category_name=TbAuditCategory::findOne($model->category_id)->name;
        $model->industry_id = Yii::$app->request->post('industry_id');
        $model->industry_name = TbAuditIndustry::findOne($model->industry_id)->name;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->region_id = Yii::$app->request->post('region_id');
        $model->region_name = TbAuditRegion::findOne($model->region_id)->name;
        $model->business_unit_id = Yii::$app->request->post('business_unit_id');
        $model->business_unit_name = TbAuditBusinessUnit::findOne($model->business_unit_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;        
        $model->date_due=Yii::$app->request->post('date_due');  
        $model->date_posted=Yii::$app->request->post('date_posted');  

        if ($model->save()) {
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionUpdateTempOld()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $model = TbAuditPicaFindingTemp2::findOne($id);
        
        $model->auditor_nik = Yii::$app->user->identity->nik;
        $model->auditor_name = Yii::$app->user->identity->username; 
        $model->rating_id= Yii::$app->request->post('rating_id');
        $model->rating_name=TbAuditRating::findOne($model->rating_id)->name;
        $model->category_id= Yii::$app->request->post('category_id');
        $model->category_name=TbAuditCategory::findOne($model->category_id)->name;
        $model->industry_id = Yii::$app->request->post('industry_id');
        $model->industry_name = TbAuditIndustry::findOne($model->industry_id)->name;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->region_id = Yii::$app->request->post('region_id');
        $model->region_name = TbAuditRegion::findOne($model->region_id)->name;
        $model->business_unit_id = Yii::$app->request->post('business_unit_id');
        $model->business_unit_name = TbAuditBusinessUnit::findOne($model->business_unit_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;        
        $model->date_due=Yii::$app->request->post('date_due');  
        $model->date_posted=Yii::$app->request->post('date_posted');  

        if ($model->save()) {
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionUpdate()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $model = TbAuditPicaFinding::findOne($id);
        
        
        $model->auditor_nik = Yii::$app->user->identity->nik;
        $model->auditor_name = Yii::$app->user->identity->username; 
        $model->rating_id= Yii::$app->request->post('rating_id');
        $model->rating_name=TbAuditRating::findOne($model->rating_id)->name;
        $model->category_id= Yii::$app->request->post('category_id');
        $model->category_name=TbAuditCategory::findOne($model->category_id)->name;
        $model->industry_id = Yii::$app->request->post('industry_id');
        $model->industry_name = TbAuditIndustry::findOne($model->industry_id)->name;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->region_id = Yii::$app->request->post('region_id');
        $model->region_name = TbAuditRegion::findOne($model->region_id)->name;
        $model->business_unit_id = Yii::$app->request->post('business_unit_id');
        $model->business_unit_name = TbAuditBusinessUnit::findOne($model->business_unit_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;        
        $model->date_due=Yii::$app->request->post('date_due');  
        $model->date_posted=Yii::$app->request->post('date_posted');  

        if ($model->save()) {
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionUpdateExtension()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $model = TbAuditPicaFinding::findOne($id);
        
        $model->date_extension = Yii::$app->request->post('date_extension');  
        $model->date_extension_request= Yii::$app->request->post('date_extension_request');  
        $model->lead_time_status = 'On Schedule';
        $model->date_updated = date('m-d-Y');  
        $model->is_extended = 1;

        if ($model->save()) {

            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','pica'=>$model];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionUpdate2()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $isfile = Yii::$app->request->post('isfile');
        $file_name='';
        
        $model = TbAuditPicaFinding::findOne($id);
        
        if($isfile === 'false'){
            
            if($_FILES['file']['name'] === ''){

                $response->data = ['message' => 'Maaf, file tidak boleh kosong.','status' => 'fail'];  
            
                return $response;

            }else{
                    
                $file = FileController::Upload($_FILES,$file_name);
                    
                if($file->data['status']==='fail'){
                     
                    return $file;
                }
            }
        
            $model->finding_file = $file_name;
        }
        
        $model->finding = Yii::$app->request->post('finding');
        $model->repeat_finding = Yii::$app->request->post('repeat_finding');

        if ($model->save()) {
            
            $findings = TbAuditPicaFinding::find()->where("number='{$model->number}'")->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','finding'=>$model, 'findings'=>$findings];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }
    
    public function actionUpdateTemp2()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $isfile = Yii::$app->request->post('isfile');
        $file_name='';
        
        $model = TbAuditPicaFindingTemp::findOne($id);
        
        if($isfile === 'false'){
            
            if($_FILES['file']['name'] === ''){

                $response->data = ['message' => 'Maaf, file tidak boleh kosong.','status' => 'fail'];  
            
                return $response;

            }else{
                    
                $file = FileController::Upload($_FILES,$file_name);
                    
                if($file->data['status']==='fail'){
                     
                    return $file;
                }
            }
        
            $model->finding_file = $file_name;
        }
        
        $model->finding = Yii::$app->request->post('finding');
        $model->repeat_finding = Yii::$app->request->post('repeat_finding');

        if ($model->save()) {
            
            $findings = TbAuditPicaFindingTemp::find()->where(['business_unit_id' =>$model->business_unit_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','finding'=>$model, 'findings'=>$findings];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionUpdateTemp2Old()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $isfile = Yii::$app->request->post('isfile');
        $file_name='';
        
        $model = TbAuditPicaFindingTemp2::findOne($id);
        
        if($isfile === 'false'){
            
            if(isset($_FILES)){
                    
                $file = FileController::Upload($_FILES,$file_name);
                    
                if($file->data['status']==='fail'){
                     
                    return $file;
                }
            }
        
            $model->finding_file = $file_name;
        }
        
        $model->finding = Yii::$app->request->post('finding');

        if ($model->save()) {
            
            $findings = TbAuditPicaFindingTemp2::find()->where(['business_unit_id' =>$model->business_unit_id,'number'=>$model->number])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','finding'=>$model, 'findings'=>$findings];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionFileExtension()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $isfile = Yii::$app->request->post('isfile');
        $file_name='';
        
        $model = TbAuditPicaFinding::findOne($id);
        
        if($isfile === 'false'){
            
            if(isset($_FILES)){
                    
                $file = FileController::UploadProgress($_FILES,$file_name,$model->number);
                    
                if($file->data['status']==='fail'){
                     
                    return $file;
                }
            }
        
            $model->extension_file = $file_name;
        }
        
        if ($model->save()) {
            
            $findings = TbAuditPicaFinding::find()->where("number ='{$model->number}' AND status <> 'Closed' ")->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','finding'=>$model, 'findings'=>$findings];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }
    
    public function actionDeleteFileTemp()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $model = TbAuditPicaFindingTemp::findOne($id);
        
        $industry_id=$model->industry_id;
        $region_id=$model->region_id;
        $business_unit_id=$model->business_unit_id;
        
        $file = FileController::Remove($model->finding_file);
        
        if($file->data['status']==='fail'){
                     
            return $file;
        }
        
        $model->finding_file ='';

        if ($model->save()) {
            
            $findings = TbAuditPicaFindingTemp::find()->where(['business_unit_id' =>$model->business_unit_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','finding'=>$model, 'findings'=>$findings];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionDeleteFile()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $model = TbAuditPicaFinding::findOne($id);
        
        $industry_id=$model->industry_id;
        $region_id=$model->region_id;
        $business_unit_id=$model->business_unit_id;
        
        $file = FileController::Remove($model->finding_file);
        
        if($file->data['status']==='fail'){
                     
            return $file;
        }
        
        $model->finding_file ='';

        if ($model->save()) {
            
            $findings = TbAuditPicaFinding::find()->where(['number' =>$model->number])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','finding'=>$model, 'findings'=>$findings];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

     public function actionDeleteExtensionFile()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $model = TbAuditPicaFinding::findOne($id);
        $number = str_replace("/","_",$model->number);        
        
        $file = FileController::Remove($number.'/'.$model->extension_file);
        
        if($file->data['status']==='fail'){
                     
            return $file;
        }
        
        $model->extension_file ='';

        if ($model->save()) {
            
            $findings = TbAuditPicaFinding::find()->where(['number' =>$model->number])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','finding'=>$model, 'findings'=>$findings];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionDeleteTemp()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        $transaction = Yii::$app->db->beginTransaction();
                        
        try {  

            $finding =  TbAuditPicaFindingTemp::findOne($id);               
            
            Yii::$app->db->createCommand("DELETE FROM tb_audit_pica_finding_temp WHERE id=$id")->execute(); 
            
            Yii::$app->db->createCommand("DELETE FROM tb_audit_pica_corrective_temp WHERE finding_id= $id")->execute(); 
            
            $transaction->commit();
            
            // Hapus File
            FileController::Remove($finding['finding_file']);
            
            $response->data = ['message' => 'Data Berhasil Dihapus','status' => 'success'];             
        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Data Gagal Dihapus','status' => 'fail'];
                         
        }

        return $response;
    }

    public function actionDeleteTempOld()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        $transaction = Yii::$app->db->beginTransaction();
                        
        try {            
            
            Yii::$app->db->createCommand("DELETE FROM tb_audit_pica_finding_temp2 WHERE id=$id")->execute(); 
            
            Yii::$app->db->createCommand("DELETE FROM tb_audit_pica_corrective_temp2 WHERE finding_id= $id")->execute(); 
            
            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Dihapus','status' => 'success'];             
        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Data Gagal Dihapus','status' => 'fail'];
                         
        }

        return $response;
    }

    public function actionDelete()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $number = Yii::$app->request->post('number');
        $finding = array();
        
        $transaction = Yii::$app->db->beginTransaction();
                        
        try { 

            $finding =  TbAuditPicaFinding::findOne($id);           
            
            Yii::$app->db->createCommand("DELETE FROM tb_audit_pica_finding WHERE id=$id")->execute(); 
            
            Yii::$app->db->createCommand("DELETE FROM tb_audit_pica_corrective WHERE finding_id= $id")->execute(); 

            $transaction->commit();

            // Hapus File
            FileController::Remove($finding['finding_file']);
            
            $response->data = ['message' => 'Data Berhasil Dihapus','status' => 'success'];             
        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Data Gagal Dihapus','status' => 'fail'];
                         
        }

        return $response;
    }

    protected function findModel($id)
    {
        if (($model = TbAuditPicaFinding::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGetAllTemp()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $business_unit_id = Yii::$app->request->post('business_unit_id');
        
        $transaction = Yii::$app->db->beginTransaction();  
        
        $repeat_temp = array(0=>['number'=>'']);

        $test = array();
        
        try {   
            
            $pica = TbAuditPicaFindingTemp::find()->where(['business_unit_id' =>$business_unit_id])->all();
            $repeat = TbAuditPicaHeader::find()->select('number')->where(['business_unit_id' =>$business_unit_id])->all();

            $test = array_merge($repeat_temp,$repeat);
            
            $transaction->commit();
            
            $response->data = ['pica' => $pica,'repeat'=>$test];             
        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['pica' =>0];
                         
        }   
        
        return $response;
    }

    public function actionGetAll()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        $number = Yii::$app->request->post('number'); 
        $leader = Yii::$app->request->post('leader');       
        $industry = Yii::$app->request->post('industry');
        $region = Yii::$app->request->post('region');
        $business_unit = Yii::$app->request->post('business_unit');
        $division = Yii::$app->request->post('division');
        $category = Yii::$app->request->post('category');
        $rating = Yii::$app->request->post('rating');

        $user =  Yii::$app->user->identity;
        $query = '';
        $session = Yii::$app->session;

        if($number){

            $query .=" AND number='$number' ";

        }
        
        if($category){
            
            $query .=" AND category_id=$category "; 

        }

         if($rating){
            
            $query .=" AND rating_id=$rating "; 

        }
        
        if($division){

            $query .=" AND division_id=$division ";                

        }
        
        if($business_unit){
             
            $query .=" AND business_unit_id=$business_unit ";                
            
        }
        
        if($region){

            $query .=" AND region_id=$region ";

        }
        
        if($industry){

            $query .=" AND industry_id=$industry ";

        }
        
        if($leader){

            $query .=" AND team_leader_nik='$leader'  ";

        }
        
        $pica_corrective = array();        
        
        $transaction = Yii::$app->db->beginTransaction();  
        
        try {

            $pica = TbAuditPicaFinding::findBySql("SELECT id,number,team_leader_name, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment , REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days  FROM tb_audit_pica_finding WHERE is_deleted=0 AND order_number>2 $query  ORDER BY number DESC")->all();

            $index_array = 0;

            foreach($pica as $index => $value){
                
                $corrective = array();

                foreach($value as $index2 => $value2){
                    
                    $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress,  (REPLACE(number,'/','_')+'/'+progress_file) AS progress_file, status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,COALESCE (date_extension,date_due) AS date_due  FROM tb_audit_pica_corrective WHERE finding_id={$value['id']}  AND is_deleted=0 ")->all();
                    
                    if(count($corrective) > 0){
                        
                        $pica_corrective[$index_array]['corrective_action'] = $corrective;
                        $pica_corrective[$index_array]['no']=$index_array+1;
                        $pica_corrective[$index_array][$index2] =   $value2;
                        
                    }
                }
                
                 if(count($corrective) > 0){$index_array++;}
                
            }

            $transaction->commit();
            
            $response->data = ['pica' => $pica_corrective];    

        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['pica' =>0];
                         
        }   
        
        return $response;
    }


        
    public function actionGetAllTempOld()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $business_unit_id = Yii::$app->request->post('business_unit_id');
        $number = Yii::$app->request->post('number');
        
        $transaction = Yii::$app->db->beginTransaction();  
        
        
        try {   
            $pica = TbAuditPicaFindingTemp2::find()->where(['business_unit_id' =>$business_unit_id,'number'=>$number])->all();
                       
            $transaction->commit();
            
            $response->data = ['pica' => $pica];             
        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['pica' =>0];
                         
        }   
        
        return $response;
    }

    public function actionSearch()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $number = Yii::$app->request->post('number');
               
        $pica = TbAuditPicaFinding::findBySql("SELECT id,number, REPLACE (finding,'<br>','\n') AS finding , location_id, division_id, category_id,rating_id,date_due,date_extension,date_extension_request,extension_file FROM tb_audit_pica_finding WHERE number='$number' AND status <> 'Closed' ")->all();

        if(count($pica)<=0){

             $response->data = ['status' => 'fail','message'=>'Data Yang Anda Cari Tidak Ada.'];    

        }
            
        $response->data = ['status' => 'success','pica' => $pica];             
       
        
        return $response;
    }
    
    public function actionPrint(){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON; 
        
        $pica = Yii::$app->request->post('pica');
        $approver = Yii::$app->user->identity;
        $pica_number = $pica['number'];  
        
        $transaction = Yii::$app->db->beginTransaction();        
                
        try{
            
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET is_printed=1 WHERE number = '$pica_number' AND (next_nik LIKE '%,{$approver['nik']}%' OR next_nik LIKE '%{$approver['nik']},%' OR next_nik = '{$approver['nik']}')")->execute();
            
            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Dicetak','status' => 'success','pica_number'=>$pica_number];             
            
        }catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Data Gagal Dicetak','status' => 'fail'];
                         
        }    
        return $response;
         
    }
    
    public function actionUpdateComment()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $comment = str_replace("'","''",Yii::$app->request->post('comment'));
        $number = Yii::$app->request->post('number');
        $order_number = Yii::$app->request->post('order_number');
        $user =  Yii::$app->user->identity;
        $findings = array();
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try{
            
            //update comment
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET comment='{$comment}' WHERE id=$id ")->execute();
                    
            // Update comment
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET comment='{$comment}' WHERE finding_id=$id ")->execute();
            
            $pica = TbAuditPicaFinding::findBySql("SELECT id,number, REPLACE (finding,'<br>','\n') AS finding , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,date_due,status,REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment ,order_number FROM tb_audit_pica_finding  WHERE number = '$number'  AND is_deleted=0 ")->all();
            
            $index_array = 0;
            
            $no = 1;
            
            foreach($pica as $index => $value){
                
                $corrective = array();
                
                foreach($value as $index2 => $value2){
                    
                    if($user->role_name==='Progress Approver'){
                            
                        $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,next_name,next_position,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='$number' AND finding_id={$value['id']} AND division_id IN ($user->division_id) AND order_number={$order_number}  AND is_deleted=0 ")->all();
                            
                    }else{
                            
                        $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,next_name,next_position,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='$number' AND finding_id={$value['id']} AND (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik ='{$user['nik']}' )  AND is_deleted=0 ")->all();
                            
                    }
                       
                    if(count($corrective) > 0){
                        
                        $findings[$index_array]['corrective_action'] = $corrective;
                        $findings[$index_array]['no']=$no;
                        $findings[$index_array][$index2] =   $value2;
                        
                    }
                }
                
                if(count($corrective) > 0){$index_array++;$no++;}
                
            }
            
            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','findings'=>$findings];  
            
        }catch(Exception $e){
                                    
            $transaction->rollBack();
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response;    
    }

    public function actionUpdateCoordinatorComment()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $comment = str_replace("'","''",Yii::$app->request->post('comment'));
        $number = Yii::$app->request->post('number');
        $order_number = Yii::$app->request->post('order_number');
        $user =  Yii::$app->user->identity;
        $findings = array();
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try{
            
            //update comment
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET coordinator_comment='{$comment}' WHERE id=$id ")->execute();
                    
            // Update comment
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET coordinator_comment='{$comment}' WHERE finding_id=$id ")->execute();
            
            $pica = TbAuditPicaFinding::findBySql("SELECT id,number, REPLACE (finding,'<br>','\n') AS finding , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,date_due,status,REPLACE (coordinator_comment,'<br>','\n') AS coordinator_comment ,REPLACE (div_head_comment,'<br>','\n') AS div_head_comment ,REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment ,order_number FROM tb_audit_pica_finding  WHERE number = '$number' AND is_deleted=0 ")->all();
            
            $index_array = 0;
            
            $no = 1;
            
            foreach($pica as $index => $value){
                
                $corrective = array();
                
                foreach($value as $index2 => $value2){
                    
                    if($user->role_name==='Progress Approver'){
                            
                        $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,next_name,next_position,date_progress,date_due,REPLACE (coordinator_comment,'<br>','\n') AS coordinator_comment ,REPLACE (div_head_comment,'<br>','\n') AS div_head_comment ,REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment   FROM tb_audit_pica_corrective WHERE number='$number' AND finding_id={$value['id']} AND division_id IN ($user->division_id) AND order_number={$order_number}  AND is_deleted=0  ")->all();
                            
                    }else{
                            
                        $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,next_name,next_position,date_progress,date_due,REPLACE (coordinator_comment,'<br>','\n') AS coordinator_comment ,REPLACE (div_head_comment,'<br>','\n') AS div_head_comment ,REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment   FROM tb_audit_pica_corrective WHERE number='$number' AND finding_id={$value['id']} AND (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik ='{$user['nik']}' )  AND is_deleted=0  ")->all();
                            
                    }
                       
                    if(count($corrective) > 0){
                        
                        $findings[$index_array]['corrective_action'] = $corrective;
                        $findings[$index_array]['no']=$no;
                        $findings[$index_array][$index2] =   $value2;
                        
                    }
                }
                
                if(count($corrective) > 0){$index_array++;$no++;}
                
            }
            
            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','findings'=>$findings];  
            
        }catch(Exception $e){
                                    
            $transaction->rollBack();
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response;    
    }
    
    public function actionUpdateSupervisorComment()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $comment = str_replace("'","''",Yii::$app->request->post('comment'));
        $number = Yii::$app->request->post('number');
        $order_number = Yii::$app->request->post('order_number');
        $approval = Yii::$app->request->post('approval');
        $user =  Yii::$app->user->identity;
        $findings = array();
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try{
            
            //update comment
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET supervisor_comment='{$comment}', approval='{$approval}' WHERE id=$id ")->execute();
            
            // Update order number
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET supervisor_comment='{$comment}' WHERE finding_id=$id ")->execute();
            
            $pica = TbAuditPicaFinding::findBySql("SELECT id,number, REPLACE (finding,'<br>','\n') AS finding , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,date_due,status,REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment ,order_number,approval FROM tb_audit_pica_finding  WHERE number = '$number' AND is_deleted=0 ")->all();
            
            $index_array = 0;
            
            $no = 1;
            
            foreach($pica as $index => $value){
                
                $corrective = array();
                
                foreach($value as $index2 => $value2){
                    
                    if($user->role_name==='Progress Approver'){
                            
                        $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,next_name,next_position,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='$number' AND finding_id={$value['id']} AND division_id IN ($user->division_id) AND order_number={$order_number}  AND is_deleted=0  ")->all();
                            
                    }else{
                            
                        $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,next_name,next_position,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='$number' AND finding_id={$value['id']} AND (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik ='{$user['nik']}' )  AND is_deleted=0 ")->all();
                            
                    }
                       
                    if(count($corrective) > 0){
                        
                        $findings[$index_array]['corrective_action'] = $corrective;
                        $findings[$index_array]['no']=$no;
                        $findings[$index_array][$index2] =   $value2;
                        
                    }
                }
                
                if(count($corrective) > 0){$index_array++;$no++;}
                
            }
            
            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','findings'=>$findings];  
            
        }catch(Exception $e){
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response;  
    }

    public function actionUpdateDivheadComment()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $comment = str_replace("'","''",Yii::$app->request->post('comment'));
        $number = Yii::$app->request->post('number');
        $order_number = Yii::$app->request->post('order_number');
        $user =  Yii::$app->user->identity;
        $findings = array();
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try{
            
            //update comment
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET div_head_comment='{$comment}' WHERE id=$id ")->execute();
            
            // Update order number
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET div_head_comment='{$comment}' WHERE finding_id=$id ")->execute();
            
            $pica = TbAuditPicaFinding::findBySql("SELECT id,number, REPLACE (finding,'<br>','\n') AS finding , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,date_due,status,REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment ,REPLACE (coordinator_comment,'<br>','\n') AS coordinator_comment ,REPLACE (div_head_comment,'<br>','\n') AS div_head_comment ,order_number FROM tb_audit_pica_finding  WHERE number = '$number'  AND is_deleted=0 ")->all();
            
            $index_array = 0;
            
            $no = 1;
            
            foreach($pica as $index => $value){
                
                $corrective = array();
                
                foreach($value as $index2 => $value2){
                    
                    if($user->role_name==='Progress Approver'){
                            
                        $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,next_name,next_position,date_progress,date_due,REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment ,REPLACE (coordinator_comment,'<br>','\n') AS coordinator_comment ,REPLACE (div_head_comment,'<br>','\n') AS div_head_comment  FROM tb_audit_pica_corrective WHERE number='$number' AND finding_id={$value['id']} AND division_id IN ($user->division_id) AND order_number={$order_number}  AND is_deleted=0 ")->all();
                            
                    }else{
                            
                        $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,next_name,next_position,date_progress,date_due,REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment ,REPLACE (coordinator_comment,'<br>','\n') AS coordinator_comment ,REPLACE (div_head_comment,'<br>','\n') AS div_head_comment  FROM tb_audit_pica_corrective WHERE number='$number' AND finding_id={$value['id']} AND (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik ='{$user['nik']}' )  AND is_deleted=0 ")->all();
                            
                    }
                       
                    if(count($corrective) > 0){
                        
                        $findings[$index_array]['corrective_action'] = $corrective;
                        $findings[$index_array]['no']=$no;
                        $findings[$index_array][$index2] =   $value2;
                        
                    }
                }
                
                if(count($corrective) > 0){$index_array++;$no++;}
                
            }
            
            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','findings'=>$findings];  
            
        }catch(Exception $e){
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response;  
    }
}
