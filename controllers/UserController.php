<?php

namespace app\controllers;

use Yii;
use app\models\TbAuditUser;
use app\models\TbAuditRole;
use app\models\TbAuditIndustry;
use app\models\TbAuditLocation;
use app\models\TbAuditRegion;
use app\models\TbAuditBusinessUnit;
use app\models\TbAuditDivision;
use app\models\TbAuditWorkflow;
use app\models\TbAuditPicaCorrective;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;
use yii\web\Response;
use app\controllers\EmailController;
use app\models\PasswordResetRequestForm;


/**
 * UserController implements the CRUD actions for TbAuditUser model.
 */
class UserController extends Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];  
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }
    
     /**
     * Creates a new TbAuditUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    { 
		//echo("call that");
		//print_r("call this");
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $data = Yii::$app->request->post('data');
        $id = Yii::$app->request->post('id');
		
		$industry_temp = implode(",",$data['Industry']);
        
        if(!intval($id)){
			//echo "a";
			//print_r($data);
            //$user = TbAuditUser::find()->where("nik = '{$data['NIK']}' AND industry_id='{$data['Industry']}' ")->one();
			//$user = TbAuditUser::find()->where("nik = '{$data['NIK']}' AND industry_id='{implode(",",$data['Industry'])}' ")->one();
			$user = TbAuditUser::find()->where("nik = '{$data['NIK']}' AND industry_id='{$industry_temp}' ")->one();
			//$user = TbAuditUser::find()->where("nik = '{$data['NIK']}' AND industry_id=implode(",",'{$data['Industry']}') ")->one();
			//echo "b";
            if($user) {
                $response->data = ['message' => " NIK : {$data['NIK']} sudah terdaftar",'status' => 'fail'];  
                return $response;    
            }  
     
            $model = new TbAuditUser();
            
        }else{
            
            $model = $this->findModel($data['id']);
            
        }
        
        $model->nik                 = $data['NIK'];
        $model->username            = $data['name'];
        $model->email               = $data['email'];  
        $model->role_id             = $data['role'];
        $model->position            = $data['position'];
        $model->role_name           = TbAuditRole::findOne($model->role_id)->name;        
        // $model->industry_id = $data['Industry'];
        // $model->location_id = $data['Location'];
        // $model->region_id = $data['Region'];  
        $model->industry_id         = implode(",",$data['Industry']);
        $model->location_id         = implode(",",$data['Location']);
        $model->region_id           = implode(",",$data['Region']);
        $model->business_unit_id    = implode(",",$data['BusinessUnit']);
        $model->division_id         = implode(",",$data['Division']);
        $model->user_name           = Yii::$app->user->identity->username;
        $model->user_nik            = (string) Yii::$app->user->identity->nik;  
        //$model->password_hash       = '$2y$13$JrC3Cb7DpDqYGLorGV.vI.VF5gFfzRBEiePKKq7.QgfXopxv1S9MW'; 
		$model->password_hash       = '$2y$13$PPMiTQ2tE/.dgnjuoEwtt.1ZCfeP2XOfcDLJ90ed1i2Gxuo5rd/Ty'; 
        
        $transaction = TbAuditUser::getDb()->beginTransaction();
        try {
            $model->save();
            if(!intval($id)){
            
                if(!$this->sendEmail($model)){
                
                    $response->data = ['message' => 'Email Gagal Dikirim','status' => 'fail'];  
                    $transaction->rollBack();
                    return $response;
                
                }
            }
            
            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];            
            
        } catch(Exception $e) {
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];           
            $transaction->rollBack();
            
        }
        
        return $response;

    }

    /**
     * Deletes an existing TbAuditUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
         
        // Cek apakah berelasi dengan table Workflow
        $isRelated = TbAuditWorkflow::find()->where("actor_nik = '{$model->nik}'")->all();
          
        if($isRelated){
            
          $response->data = ['message' => 'Data Gagal Dihapus karena berelasi dengan Workflow','status' => 'fail'];
          return $response;
          
        }
        
        // Cek apakah berelasi dengan table Pica
        $isRelated2 = TbAuditPicaCorrective::find()->where("auditor_nik = '{$model->nik}' OR pic_nik='{$model->nik}' ")->all();
        
        if($isRelated2){
            
          $response->data = ['message' => 'Data Gagal Dihapus karena berelasi dengan PICA','status' => 'fail'];
          return $response;
          
        }
        
        if ($model->delete()) {
            
            $response->data = ['message' => 'Data Berhasil Dihapus','status' => 'success'];             
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Dihapus','status' => 'fail'];             
            
        }      
        
        return $response;      
    }
    /**
     * Finds the TbAuditUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TbAuditUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbAuditUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionKiranaku(){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $nik = Yii::$app->request->post('nik');        
            
        //$kiranaku = Yii::$app->db2->createCommand("SELECT k.nik, k.nama,k.email,k.posst FROM FROM [ReportTemplate].[rpt].[vw_KaryawanDivisi] AS k WHERE k.nik =$nik ")->queryOne();
		$kiranaku = Yii::$app->db->createCommand("SELECT k.nik, k.nama,k.email,k.posst FROM [ReportTemplate].[rpt].[vw_KaryawanDivisi] AS k WHERE k.nik = '{$nik}' ")->queryOne();
        if($kiranaku){
            $response->data = $kiranaku;            
        }else{
            //$response->data=0;
			$response->data = ['nik' => '','nama' => '','email' => '','posst'=> ''];
        }
        
        return $response;
        
    
    }
    
    private function getName($param){
        
        $arr = array();
        
        foreach($param AS $index => $value){
            
            array_push($arr,$value['name']);
        }  
        
        return implode(",",$arr);
    }
    
    private function sendEmail($user)
    {

        if( intval($user->industry_id) === 1){    


            return EmailController::sendUser($user,'createNewUser-html');

        }else{

            $model = new PasswordResetRequestForm();
            $model->email = $user['email'];

            if ($model->validate()) {
                
                if ($model->sendEmail()) {
                    
                    return true;
            
                } else {
                    
                    return false;

                }
            }

        }
        
    }
}
