<?php

namespace app\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;
use yii\web\Response;


/**
 * WorkFlowController implements the CRUD actions for TbAuditWorkflow model.
 */
class FileController extends Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];  
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }
    
    public static function Upload($file,&$file_name=''){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $target_dir = "uploads";
        $target_file = $target_dir.'/'. basename($file["file"]["name"]);
        $file_name = basename($file["file"]["name"]);
        
        //$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        
        //Check if folder already exists
//        if(!is_dir($target_dir)){
//            mkdir($target_dir,0777,true);
//        }
                
        // Check file size
        //if ($_FILES["pica"]["size"] > 500000) {
            //echo "Sorry, your file is too large.";
            //$uploadOk = 0;
        //}
        
        // Check if file already exists
        if (file_exists($target_file)) {
            
            $response->data = ['message' => 'Maaf, file sudah pernah ada.','status' => 'fail'];  
            
            return $response;
        }

        if (!move_uploaded_file($file["file"]["tmp_name"], $target_file)) {
            
            $response->data = ['message' => 'Sorry, there was an error uploading your file.','status' => 'fail'];  
            
            return $response;
            
        }
        
        $response->data = ['message' => 'Data Berhasil Disimpan.','status' => 'success'];  
            
        return $response;
        
    }
    
    public static function UploadProgress($file,&$file_name='',$pica_number){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $number = str_replace("/","_",$pica_number);
        
        $target_dir = "uploads/$number";
        $target_file = $target_dir.'/'. basename($file["file"]["name"]);
        $file_name = basename($file["file"]["name"]);
        
        //$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        
        //Check if folder already exists
        if(!is_dir($target_dir)){
            mkdir($target_dir,0777,true);
        }
                
        // Check file size
        //if ($_FILES["pica"]["size"] > 500000) {
            //echo "Sorry, your file is too large.";
            //$uploadOk = 0;
        //}
        
        // Check if file already exists
        if (file_exists($target_file)) {
            
            $response->data = ['message' => 'Sorry, file already exists.','status' => 'fail'];  
            
            return $response;
        }

        if (!move_uploaded_file($file["file"]["tmp_name"], $target_file)) {
            
            $response->data = ['message' => 'Sorry, there was an error uploading your file.','status' => 'fail'];  
            
            return $response;
            
        }
        
        $response->data = ['message' => 'Data Berhasil Disimpan.','status' => 'success'];  
            
        return $response;
        
    }
    
    public static function Remove($file_name)
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $target_dir = "uploads";
        $target_file = $target_dir.'/'.$file_name;

        if (file_exists($target_file)) {

            //Check if file already exists
            if(!unlink($target_file)){
            
                $response->data = ['message' => 'Sorry, File Remove Fail.','status' => 'fail'];  
            
                return $response;
            
            }
        }
        
        $response->data = ['message' => 'File Has Been Removed','status' => 'success'];             
            
        
        return $response;
    }

    
    

    /**
     * Finds the TbAuditWorkflow model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TbAuditWorkflow the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbAuditWorkflow::findOne($id)) !== null) {
            return $model;
        } else {
            return false;
        }
    }
    
    
}
