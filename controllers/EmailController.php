<?php

namespace app\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\Response;
use app\models\TbAuditUser;
use app\models\TbAuditWorkFlow;

class EmailController extends Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];  
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }
    
    public static function send($pica_number,$nik,$email_body)
    {
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;   
        $transaction = Yii::$app->db->beginTransaction();

        try{
            
            foreach ($nik as $index => $value){
                
                $user = TbAuditUser::find()->where("nik ='{$value}' ")->one();
                   
                //send email to PIC and His Supervisor  
                $email['username']=$user['username'];
                $email['pica_number']=$pica_number;
                                                
                $send = Yii::$app->mailer->compose(
                        ['html' => $email_body],
                        ['pica' => $email]
                )
                ->setFrom(['no-reply@kiranamegatara.com'=> 'Internal Audit Department'])
                // ->setTo($user['email'])
                ->setTo('syaiful@kiranamegatara.com')
                ->setSubject('Internal Audit Tracking System')
                ->send();
             
                if(!$send){
                        
                    $response->data = ['message' => 'Email Gagal Dikirim','status' => 'fail'];
                        
                    return $response;
                }
            }
            
            $transaction->commit();
            
            $response->data = ['message' => 'Email Berhasil Dikirim','status' => 'success'];             
            
        }catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Email Gagal Dikirim','status' => 'fail'];
            
        } 
        
        return $response;
        
    }

    public static function sendAlert($data,$user,$email_body)
    {
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;   
        $transaction = Yii::$app->db->beginTransaction();

        try{

                //send email to PIC and His Supervisor  
                $email['username']=$user['username'];
                $email['data']=$data;
                                                
                $send = Yii::$app->mailer->compose(
                        ['html' => $email_body],
                        ['pica' => $email]
                )
                ->setFrom(['no-reply@kiranamegatara.com'=> 'Internal Audit Department'])
                // ->setTo($user['email'])
                ->setTo('syaiful@kiranamegatara.com')
                ->setSubject('Outstanding PICA')
                ->send();
             
                if(!$send){
                        
                    $response->data = ['message' => 'Email Gagal Dikirim','status' => 'fail'];
                        
                    return $response;
                }
            
            $transaction->commit();
            
            $response->data = ['message' => 'Email Berhasil Dikirim','status' => 'success'];             
            
        }catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Email Gagal Dikirim','status' => 'fail'];
            
        } 
        
        return $response;
        
    }

    public static function sendUser($user,$email_body)
    {
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;   
        $transaction = Yii::$app->db->beginTransaction();

        try{

                $send = Yii::$app->mailer->compose(
                        ['html' => $email_body],
                        ['user' => $user]
                )
                ->setFrom(['no-reply@kiranamegatara.com'=> 'Internal Audit Department'])
                // ->setTo($user['email'])
                ->setTo('syaiful@kiranamegatara.com')
                ->setSubject('User Internal Audit Tracking System')
                ->send();
             
                if(!$send){
                        
                    return false;
                }
            
            $transaction->commit();
            
            $response->data = ['message' => 'Email Berhasil Dikirim','status' => 'success'];             
            
        }catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Email Gagal Dikirim','status' => 'fail'];
            
        } 
        
        return true;
        
    }

    public static function sendExtension($pica_number,$nik,$email_body,$finding)
    {
       
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;   
        $transaction = Yii::$app->db->beginTransaction();

        try{
            
            foreach ($nik as $index => $value){
                
                $user = TbAuditUser::find()->where("nik ='{$value}' ")->one();
                   
                //send email to PIC and His Supervisor  
                $email['username']=$user['username'];
                $email['pica_number']=$pica_number;
                $email['finding']=$finding['finding'];
                $email['date_due']=$finding['date_due'];
                $email['date_extension']=$finding['date_extension'];
                $email['business_unit_name']=$finding['business_unit_name'];
                                                
                $send = Yii::$app->mailer->compose(
                        ['html' => $email_body],
                        ['pica' => $email]
                )
                ->setFrom(['no-reply@kiranamegatara.com'=> 'Internal Audit Department'])
                // ->setTo($user['email'])
                ->setTo('syaiful@kiranamegatara.com')
                ->setSubject('Due Date Extension')
                ->send();
             
                if(!$send){
                        
                    $response->data = ['message' => 'Email Gagal Dikirim','status' => 'fail'];
                        
                    return $response;
                }
            }
            
            $transaction->commit();
            
            $response->data = ['message' => 'Email Berhasil Dikirim','status' => 'success'];             
            
        }catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Email Gagal Dikirim','status' => 'fail'];
            
        } 
        
        return $response;
        
    }

    public static function sendClose($corrective,$order_number){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;                

        $approver = Yii::$app->user->identity;
        $nik = array();
        $nik_temp = array();
        $pica_number = '';
        $email_body = ''; 
        $email = array();
        
        $transaction = Yii::$app->db->beginTransaction();
        // echo $order_number;
        // exit();
        try{
            
            switch($order_number){
                            
                case 5:
                    $next_order_number = 3;
                    $email_body = 'picaFinal-html';   
                break;
                    
                default :
                   ;
            }
            
            foreach($corrective as $index2 => $value2){
                
                $pica_number = $value2['number'];
                
                //cek Workflow
                if($value2['location_name'] === 'Head Office'){
                
                    //cek Workflow
                    $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND division_id={$value2['division_id']} AND order_number IN (3,4) ORDER BY order_number ASC ")->all();                                    
                
                }else{
                    
                    //cek Workflow
                    $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']} AND order_number IN (3,4) ORDER BY order_number ASC ")->all();                                    
                
                }               
                        
                foreach($workflow as $index => $value){
                            
                    $nik_temp = explode(',',$value['actor_nik']);
                
                    foreach ($nik_temp as $index_temp => $value_temp){
                    
                        $nik[$value_temp] = $value_temp;
                    }
                }
                    
            }

            foreach ($nik as $index => $value){
                
                $user = TbAuditUser::find()->where("nik ='{$value}' ")->one();
                   
                //send email to PIC and His Supervisor  
                $email['username'] = $user['username'];
                $email['pica_number'] = $pica_number;
                $email['finding'] = $corrective[0]['finding'];
                                                
                $send = Yii::$app->mailer->compose(
                        ['html' => $email_body],
                        ['pica' => $email]
                )
                ->setFrom(['no-reply@kiranamegatara.com'=> 'Internal Audit Department'])
                // ->setTo($user['email'])
                ->setTo('syaiful@kiranamegatara.com')
                ->setSubject('Internal Audit Tracking System')
                ->send();
             
                if(!$send){
                        
                    $response->data = ['message' => 'Email Gagal Dikirim','status' => 'fail'];
                        
                    return $response;
                }
            }
            
                                         
            $transaction->commit();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
            
        }catch(Exception $e){
                                    
            $transaction->rollBack();
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response;    
    }
}

