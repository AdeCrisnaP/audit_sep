<?php

namespace app\controllers;

use Yii;
use app\models\TbAuditPicaFinding;
use app\models\TbAuditPicaCorrective;
use app\models\TbAuditPicaFindingTemp;
use app\models\TbAuditPicaCorrectiveTemp;
use app\models\TbAuditPicaFindingTemp2;
use app\models\TbAuditPicaCorrectiveTemp2;
use app\models\TbAuditPicaHeader;
use app\controllers\WorkFlowController;
use app\controllers\EmailController;
use app\controllers\FileController;
use app\controllers\PicaProgressController;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use app\models\TbAuditIndustry;
use app\models\TbAuditWorkflow;
use app\models\TbAuditRegion;
use app\models\TbAuditBusinessUnit;
use app\models\TbAuditUser;
use app\models\TbAuditRating;
use app\models\TbAuditCategory;
use app\models\TbAuditLocation;
use app\models\TbAuditDivision;
use app\models\TbAuditMandatory;
use app\models\TbAuditTypeAudit;




/**
 * PicaController implements the CRUD actions for TbAuditPicaFinding model.
 */
class PicaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new TbAuditPicaFinding model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    

    /**
     * Updates an existing TbAuditPicaFinding model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function generatePicaNumberx($year=0,$posting=0,$buCode=0,$type=null,$typeaudit=null,$nolha=null){
        $month = $posting[0];
        $angka_romawi = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $numberp =  TbAuditPicaHeader::find()
                    ->select("number")
                    ->where("YEAR(date_from)='$year'")
                    ->orderBy("number DESC")
                    ->limit(1)
                    ->one();
            
            $total = $numberp ? $numberp->number : null;
            if($total){
                $total = explode("/", $total);
                $total = $total[0]*1;
            } else {
                $total = 0;
            }
            //echo json_encode($total);
            //exit();  
            //$pica_number =$this->getAngka($total+1).'/PICA-CIAKM/'.$buCode.'/'.$angka_romawi[intval($month[0])].'/'.$year;
            if($type==null){
                $number =$this->getAngka($total+1).'/PICA/CIA/'.$angka_romawi[intval($month)].'/'.$year;   
            } else if($type=='lha'){
                if($typeaudit=='1'){ // regular
                    $number = $nolha.'/LHA/CIA/'.$angka_romawi[intval($month)].'/'.$year; 
                } else if($typeaudit=='2'){ // special audit
                    $number = $nolha.'/LHAK/CIA/'.$angka_romawi[intval($month)].'/'.$year;
                }
                
            }            
            return $number;

    }
    public function actionCreate(){

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $project = Yii::$app->request->post('project');        
        $industry_id = Yii::$app->request->post('industry_id');
        $industry_name = TbAuditIndustry::findOne($industry_id)->name;        
        $region_id = Yii::$app->request->post('region_id');
        $region_name = TbAuditRegion::findOne($region_id)->name;
        $business_unit_id = Yii::$app->request->post('business_unit_id');
        $business_unit_name = TbAuditBusinessUnit::findOne($business_unit_id)->name;
        $posting_date =Yii::$app->request->post('posting_date');
        $from_date =Yii::$app->request->post('from_date');
        $to_date =Yii::$app->request->post('to_date');
        $comment = Yii::$app->request->post('comment');
        $teamleader = Yii::$app->request->post('teamleader');
        $pica_corrective = array();
        $pica_number = '';
        $approval_status = 'PICA Submitted';
        $angka_romawi = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $year = date("Y");
        $auditor = Yii::$app->user->identity;
        $team_leader = TbAuditUser::find()->where(['nik'=>$teamleader])->one();
        $order_number = 0;
        $next_order_number = $order_number + 1;
        $email_body ='picaAwaitingApproval-html';
        $nik_temp=array();
        $nik = array();
        $file_name='';
        $posting = explode('-',$posting_date);

        $typeaudit = Yii::$app->request->post('typeaudit');
        $nolha = Yii::$app->request->post('nolha');
        
        $transaction = Yii::$app->db->beginTransaction();  

        // echo $typeaudit."==".$nolha;
        // exit();     
                       
        try {              
            
            //cek jumlah pica pada tahun berjalan
            //$total = TbAuditPicaHeader::find()->where("YEAR(date_from)='$year'")->all();   
            // $numberp =  TbAuditPicaHeader::find()
            //         ->select("number")
            //         ->where("YEAR(date_from)='$year'")
            //         ->orderBy("number DESC")
            //         ->limit(1)
            //         ->one();
           
            // $total = $numberp->number; 
            // if($total!=null || trim($total)!=""){
            //     $total = explode("/", $total);
            //     $total = $total[0]*1;
            // } else {
            //     $total = 0;
            // } 
           
            //tarik kode business unit            
            $buCode = TbAuditBusinessUnit::findOne($business_unit_id)->code;
            
            $typeauditid = $typeaudit;    
            $typeauditname = TbAuditTypeAudit::findOne($typeauditid)->name;
            //ubah bulan berjalan menjadi romawi
            //$pica_number =$this->getAngka($total+1).'/PICA-CIAKM/'.$buCode.'/'.$angka_romawi[intval($month[0])].'/'.$year;

            //add temporary 2017
            $year = $posting[2];
            
            $pica_number = $this->generatePicaNumberx($year,$posting,$buCode);
            $lha_number = $this->generatePicaNumberx($year,$posting,$buCode,'lha',$typeaudit,$nolha);
            // echo json_encode($pica_number)." | ".json_encode($lha_number);
            // exit();
            
            if($_FILES['file']['name'] !==''){
                    
                $file = FileController::Upload($_FILES,$file_name);
                    
                if($file->data['status']==='fail'){
                     
                    $transaction->rollBack();
                        
                    return $file;
                }
            }
            
            // insert pica header
            // Yii::$app->db->createCommand("INSERT INTO tb_audit_pica_header (number,lha_number,type_audit_id,type_audit,auditor_nik,auditor_name,auditor_position,team_leader_nik,team_leader_name,team_leader_position,industry_id,industry_name,region_id,region_name,business_unit_id,business_unit_name,pica_file,project_name,date_from,date_to,date_posted)
            //                                 VALUES ('$pica_number','$lha_number','$typeauditid','$typeauditname','{$auditor['nik']}','{$auditor['username']}','{$auditor['position']}','{$team_leader['nik']}','{$team_leader['username']}','{$team_leader['position']}',$industry_id,'$industry_name',$region_id,'$region_name',$business_unit_id,'$business_unit_name','$file_name','$project','{$from_date}','{$to_date}','{$posting_date}');")->execute(); 
            Yii::$app->db->createCommand("INSERT INTO tb_audit_pica_header (number,lha_number,type_audit_id,type_audit,auditor_nik,auditor_name,auditor_position,team_leader_nik,team_leader_name,team_leader_position,industry_id,industry_name,region_id,region_name,business_unit_id,business_unit_name,pica_file,date_from,date_to,date_posted)
                                            VALUES ('$pica_number','$lha_number','$typeauditid','$typeauditname','{$auditor['nik']}','{$auditor['username']}','{$auditor['position']}','{$team_leader['nik']}','{$team_leader['username']}','{$team_leader['position']}',$industry_id,'$industry_name',$region_id,'$region_name',$business_unit_id,'$business_unit_name','$file_name','{$from_date}','{$to_date}','{$posting_date}');")->execute(); 
        
            $pica = TbAuditPicaFindingTemp::find()->where("auditor_nik='{$auditor['nik']}' AND industry_id=$industry_id AND region_id=$region_id AND business_unit_id=$business_unit_id")->all();
            
            foreach($pica as $index => $value){
                
                if($value['location_name'] === 'Head Office'){
                
                    //cek Workflow
                    $workflow = TbAuditWorkflow::find()->where(" industry_id={$value['industry_id']} AND division_id={$value['division_id']} AND order_number=$next_order_number ")->one();                                    
                
                }else{
                    
                    //cek Workflow
                    $workflow = TbAuditWorkflow::find()->where(" industry_id={$value['industry_id']} AND region_id={$value['region_id']} AND business_unit_id={$value['business_unit_id']} AND division_id={$value['division_id']} AND order_number=$next_order_number ")->one();                                    
                }
                
                // insert PICA Finding
                Yii::$app->db->createCommand("INSERT INTO tb_audit_pica_finding (number,lha_number,type_audit_id,type_audit,repeat_finding,finding,finding_file,auditor_nik,auditor_name,auditor_position,team_leader_nik,team_leader_name,team_leader_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,
                                                location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,date_due,status,order_number,next_nik)
                                                VALUES ('$pica_number','$lha_number','$typeauditid','$typeauditname','{$value['repeat_finding']}','{$value['finding']}','{$value['finding_file']}','{$auditor['nik']}','{$auditor['username']}','{$auditor['position']}','{$team_leader['nik']}','{$team_leader['username']}','{$team_leader['position']}',{$value['rating_id']},'{$value['rating_name']}',
                                                {$value['category_id']},'{$value['category_name']}',$industry_id,'$industry_name',{$value['location_id']},'{$value['location_name']}',$region_id,'$region_name',$business_unit_id,
                                                '$business_unit_name',{$value['division_id']},'{$value['division_name']}','{$posting_date}','{$value['date_due']}','Open',$next_order_number,'{$workflow['actor_nik']}')")->execute(); 
                
                $finding_id = Yii::$app->db->getLastInsertID();
                $finding = $value['finding'];
                $finding_file = $value['finding_file'];
                $date_due = $value['date_due'];
                                
                // Insert PICA Corrective
                $pica_corrective = TbAuditPicaCorrectiveTemp::find()->where(['finding_id' =>$value['id']])->all();
                
                foreach($pica_corrective as $index2 => $value2){
                    
                    // insert PICA Corrective
                    Yii::$app->db->createCommand("INSERT INTO tb_audit_pica_corrective (number,auditor_nik,auditor_name,auditor_position,pic_nik,pic_name,pic_position,mandatory_id,mandatory_name,finding_id,finding,finding_file,corrective_action,status,lead_time_status,
                                                    industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_due,order_number,next_nik,next_name,next_position)
                                                    VALUES ('$pica_number','{$auditor['nik']}','{$auditor['username']}','{$auditor['position']}','{$value2['pic_nik']}','{$value2['pic_name']}','{$value2['pic_position']}',{$value2['mandatory_id']},'{$value2['mandatory_name']}',{$finding_id},'$finding',
                                                        '$finding_file','{$value2['corrective_action']}','Open','On Schedule',$industry_id,'$industry_name',{$value2['location_id']},'{$value2['location_name']}',$region_id,'$region_name',$business_unit_id,'$business_unit_name',{$value2['division_id']},'{$value2['division_name']}','$date_due',$next_order_number,'{$workflow['actor_nik']}','{$workflow['actor_name']}','{$workflow['actor_position']}');")->execute(); 
                    
                }
                                
                $nik_temp = explode(',',$workflow['actor_nik']);
                
                foreach ($nik_temp as $index => $value){
                    
                    $nik[$value] = $value;
                }
                
                if(count($workflow) <=0){
                        
                    if($_FILES['file']['name'] !==''){
                    
                        $file = FileController::Remove($file_name);
                    
                    }
                    
                    $transaction->rollBack();
                        
                    $response->data = ['message' => "Workflow {$value2['location_name']}, {$value2['division_name']} Tidak Ditemukan. Silakan Hubungi Administrator Internal Audit.",'status' => 'fail'];  
                        
                    return $response;
                    
                }
                
                // Next workflow
                $message1 = WorkFlowController::nextFlow($comment,$pica_number,$approval_status,$workflow);
                    
                if($message1->data['status']==='fail'){
                    
                    if($_FILES['file']['name'] !==''){
                    
                        $file = FileController::Remove($file_name);
                    
                    }
                    
                    $transaction->rollBack();
            
                    return $message1;
                
                }
                
            }
            
            // Send Email & inbox
            $message2 = EmailController::send($pica_number,$nik,$email_body);
                    
            if($message2->data['status']==='fail'){
                    
                if($_FILES['file']['name'] !==''){
                    
                    $file = FileController::Remove($file_name);
                    
                }
                    
                $transaction->rollBack();
            
                return $message2;
                
            }
            
            // Hapus data pica finding temporary
            Yii::$app->db->createCommand("DELETE FROM tb_audit_pica_finding_temp WHERE auditor_nik='{$auditor['nik']}' AND industry_id=$industry_id AND region_id=$region_id AND business_unit_id=$business_unit_id")->execute();
            
            if(count(TbAuditPicaFindingTemp::find()->all()) <=0){
                
                Yii::$app->db->createCommand(" TRUNCATE TABLE tb_audit_pica_finding_temp ")->execute();
                
            }
            
            // Hapus data pica corrective temporary
            Yii::$app->db->createCommand("DELETE FROM tb_audit_pica_corrective_temp WHERE auditor_nik='{$auditor['nik']}' AND industry_id=$industry_id AND region_id=$region_id AND business_unit_id=$business_unit_id")->execute();
            
            if(count(TbAuditPicaCorrectiveTemp::find()->all()) <=0){
                
                Yii::$app->db->createCommand(" TRUNCATE TABLE tb_audit_pica_corrective_temp ")->execute();
                
            }
            
            $transaction->commit();
            
            $response->data = ['message' => 'Nomor PICA Anda : '.$pica_number,'status' => 'success'];             
            
        }catch (Exception $e) {
                     
            if(isset($_FILES)){
                    
                $file = FileController::Remove($file_name);
                    
            }
            
            $transaction->rollBack();
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response;
    }
    
    public function actionApprove(){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;                

        $comment = Yii::$app->request->post('comment');
        $pica = Yii::$app->request->post('pica');
        $order_number = intval(Yii::$app->request->post('order_number'));
        $teamleaderNik = Yii::$app->request->post('teamleader');
        $teamLeader = TbAuditUser::findOne(['nik'=>$teamleaderNik]);
        $teamLeaderName = $teamLeader['username'];
        $teamLeaderPosition = $teamLeader['position'];
        $project = Yii::$app->request->post('project');
        $posting_date = Yii::$app->request->post('posting_date');
        $from_date = Yii::$app->request->post('from_date');
        $to_date = Yii::$app->request->post('to_date');
        $next_order_number = $order_number + 1;
        $user =  Yii::$app->user->identity;
        $nik = array();
        $nik_temp = array();
        $pica_number = '';
        $corrective = array();   




        $transaction = Yii::$app->db->beginTransaction();
 
            
        try{
            
            switch($order_number){
                    
                case 0:
                    $approval_status  ='PICA Resubmitted'; 
                    $email_body = 'picaAwaitingApproval-html';  
                break;

                case 1:
                    $approval_status  ='PICA Approved'; 
                    $email_body = 'picaAwaitingApproval-html';  
                break;
                    
                case 2:
                    $approval_status  ='PICA Approved'; 
                    $email_body = 'picaAwaitingProgresss-html';    
                break;
                    
                case 3:
                    $approval_status  = 'Progress Submitted'; 
                    $email_body = 'picaAwaitingApproval-html';  
                break;
                
                case 4:
                    $approval_status  ='Progress Approved'; 
                    $email_body = 'picaAwaitingApproval-html';  
                break;
                    
                case 5:
                    $approval_status  ='Final Approved'; 
                    $email_body = '';//'picaFinal-html';  
                break;

                case 7:
                    $approval_status  ='PICA Extension'; 
                    $email_body = 'picaExtension-html';   
                break;
                    
                default :
                    ;
            }
            
            //Resubmitted By Admin
            if($order_number === 0){

                $number = Yii::$app->request->post('number');
                $posting_date = Yii::$app->request->post('posting_date');
                $file_name = '';

                if($_FILES['file']['name'] !==''){
                    
                    $file = FileController::Upload($_FILES,$file_name);
                    
                    if($file->data['status']==='fail'){
                     
                        $transaction->rollBack();
                        
                        return $file;
                    }
                }

                // Update PICA Header
                Yii::$app->db->createCommand("UPDATE tb_audit_pica_header SET project_name='$project',team_leader_nik='$teamleaderNik',team_leader_name='$teamLeaderName',team_leader_position='$teamLeaderPosition',pica_file='$file_name',date_posted='{$posting_date}',date_from='{$from_date}',date_to='{$to_date}' WHERE number='{$number}' ")->execute(); 
                
                $pica = TbAuditPicaFinding::find()->where("number='{$number}'")->all();

                foreach($pica as $index => $value){
                    
                    $pica_number = $value['number'];
                
                    if($value['location_name'] === 'Head Office'){
                
                        //cek Workflow
                        $workflow = TbAuditWorkflow::find()->where(" industry_id={$value['industry_id']} AND division_id={$value['division_id']} AND order_number=$next_order_number ")->one();                                    
                
                    }else{
                    
                        //cek Workflow
                        $workflow = TbAuditWorkflow::find()->where(" industry_id={$value['industry_id']} AND region_id={$value['region_id']} AND business_unit_id={$value['business_unit_id']} AND division_id={$value['division_id']} AND order_number=$next_order_number ")->one();                                    
                
                    }

                    if(count($workflow) <=0){
                        
                        $transaction->rollBack();
                        
                        $response->data = ['message' => "Workflow {$value['location_name']}, {$value['division_name']} Tidak Ditemukan. Silakan Hubungi Administrator Internal Audit.",'status' => 'fail'];  
                        
                        return $response;
                    
                    }
                    
                    $nik_temp = explode(',',$workflow['actor_nik']);
                
                    foreach ($nik_temp as $index_temp => $value_temp){
                    
                        $nik[$value_temp] = $value_temp;
                    }
                                 
                    // Update PICA Finding
                    Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET approval='Approved', order_number=$next_order_number, next_nik='{$workflow['actor_nik']}', team_leader_nik='$teamleaderNik',team_leader_name='$teamLeaderName',team_leader_position='$teamLeaderPosition',date_posted='{$posting_date}' WHERE id={$value['id']}")->execute(); 
                    
                    // Update PICA Corrective
                    Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET order_number=$next_order_number, next_nik='{$workflow['actor_nik']}', next_name='{$workflow['actor_name']}', next_position='{$workflow['actor_position']}', date_created='{$posting_date}' WHERE finding_id={$value['id']}")->execute(); 
                
                    // Next workflow
                    $message1 = WorkFlowController::nextFlow($comment,$value['number'],$approval_status,$workflow);
                    
                    if($message1->data['status']==='fail'){
                
                        $transaction->rollBack();
            
                        return $message1;
                
                    }
                    
                }
                
                // Send Email & inbox
                $message2 = EmailController::send($pica_number,$nik,$email_body);
                    
                if($message2->data['status']==='fail'){
                    
                    $transaction->rollBack();
            
                    return $message2;
                
                }
                
            }

            //Approved by IA Coordinator
            if($order_number === 1){
                //die("aaaaa");

                foreach($pica as $index => $value){
                    
                    $pica_number = $value['number'];
                    
                    if($value['location_name'] === 'Head Office'){
                
                        //cek Workflow
                        $workflow = TbAuditWorkflow::find()->where(" industry_id={$value['industry_id']} AND division_id={$value['division_id']} AND order_number=$next_order_number ")->one();   
                        $a = "Select * from tb_audit_workflow where industry_id={$value['industry_id']} AND division_id={$value['division_id']} AND order_number=$next_order_number";                                  
                
                    }else{
                    
                        //cek Workflow
                        $workflow = TbAuditWorkflow::find()->where(" industry_id={$value['industry_id']} AND region_id={$value['region_id']} AND business_unit_id={$value['business_unit_id']} AND division_id={$value['division_id']} AND order_number=$next_order_number ")->one();           

                        $a = "Select * from tb_audit_workflow where  industry_id={$value['industry_id']} AND region_id={$value['region_id']} AND business_unit_id={$value['business_unit_id']} AND division_id={$value['division_id']} AND order_number=$next_order_number ";
                
                    }
                    
                    
                    

                     if(count($workflow) <=0){
                        
                        $transaction->rollBack();
                        
                        $response->data = ['message' => "Workflow {$value['location_name']}, {$value['division_name']} Tidak Ditemukan. Silakan Hubungi Administrator Internal Audit.",'status' => 'fail'];  
                        
                        return $response;
                    
                    }
                    
                    $nik_temp = explode(',',$workflow['actor_nik']);
                    
                    foreach ($nik_temp as $index_temp => $value_temp){
                    
                        $nik[$value_temp] = $value_temp;
                    }
                    
                    // Update PICA Finding
                    Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET approval='Approved', order_number=$next_order_number, next_nik='{$workflow['actor_nik']}' WHERE id={$value['id']}")->execute(); 
                    
                    // Update PICA Corrective
                    Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET order_number=$next_order_number, next_nik='{$workflow['actor_nik']}', next_name='{$workflow['actor_name']}', next_position='{$workflow['actor_position']}' WHERE finding_id={$value['id']}")->execute(); 

                    $b[] = "UPDATE tb_audit_pica_finding SET approval='Approved', order_number=$next_order_number, next_nik='{$workflow['actor_nik']}' WHERE id={$value['id']} <||> UPDATE tb_audit_pica_corrective SET order_number=$next_order_number, next_nik='{$workflow['actor_nik']}', next_name='{$workflow['actor_name']}', next_position='{$workflow['actor_position']}' WHERE finding_id={$value['id']} ";
                    
                    // die("UPDATE tb_audit_pica_finding SET approval='Approved', order_number=$next_order_number, next_nik='{$workflow['actor_nik']}' WHERE id={$value['id']}");
                
                    // Next workflow
                    $message1 = WorkFlowController::nextFlow($comment,$value['number'],$approval_status,$workflow);

                    $arraycheck1[] = $message1->data['status'];


                    //$arraycheck1[] = $message1->data['status'];

                    if($message1->data['status']==='fail'){
                
                        $transaction->rollBack();
            
                        return $message1;
                
                    }
                    
                } 

                // echo json_encode($x);

                // echo json_encode($arraycheck1);

                // exit();
                
                // Send Email & inbox
                $message2 = EmailController::send($pica_number,$nik,$email_body);
                    
                if($message2->data['status']==='fail'){
                    
                    $transaction->rollBack();
            
                    return $message2;
                
                }

                
            }
            
            //Approved by IA Division
            if($order_number === 2){
                
                foreach($pica as $index => $value){
                    
                    foreach($value['corrective_action'] as $index2 => $value2){
                
                        if($value2['location_name'] === 'Head Office'){
                
                            //cek Workflow
                            $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND division_id={$value2['division_id']} AND order_number IN (3,4) ORDER BY order_number ASC ")->all();                                    
                
                        }else{
                    
                            //cek Workflow
                            $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']} AND order_number IN (3,4) ORDER BY order_number ASC ")->all();                                    
                
                        }

                         if(count($workflow) <=0){
                        
                            $transaction->rollBack();
                        
                            $response->data = ['message' => "Workflow {$value2['location_name']}, {$value2['division_name']} Tidak Ditemukan. Silakan Hubungi Administrator Internal Audit.",'status' => 'fail'];  
                        
                            return $response;
                    
                        }

                                                
                        $pica_number = $value2['number'];
              
                        // Update PICA Finding
                        Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET approval='Approved', order_number=$next_order_number, next_nik='{$workflow[0]['actor_nik']}', status='Open' WHERE id={$value2['finding_id']}")->execute(); 
                    
                        // Update PICA Corrective
                        Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET order_number=$next_order_number, next_nik='{$workflow[0]['actor_nik']}',next_name='{$workflow[0]['actor_name']}', next_position='{$workflow[0]['actor_position']}', status='Open' WHERE id={$value2['id']}")->execute(); 
                
                        // Next workflow
                        $message1 = WorkFlowController::nextFlow($comment,$value2['number'],$approval_status,$workflow[0]);
                    
                        if($message1->data['status']==='fail'){
                
                            $transaction->rollBack();
            
                            return $message1;
                
                        }
                    
                        foreach($workflow as $index => $value3){
                            
                            $nik_temp = explode(',',$value3['actor_nik']);
                            
                            foreach ($nik_temp as $index_temp => $value_temp){
                    
                                $nik[$value_temp] = $value_temp;
                            
                            }
                            
                        }
                
                    }
                }
                
                // Send Email & inbox
                $message2 = EmailController::send($pica_number,$nik,$email_body);
                    
                if($message2->data['status']==='fail'){
                    
                    $transaction->rollBack();
            
                    return $message2;
                
                }
                
            }
            
            //Approved by PIC 
            if($order_number === 3){
                
                foreach($pica as $index => $value){
                        
                    $pica_number = $value['number'];

                    if($value['status'] === 'On Progress'){
                        
                        //Set complete=1
                        Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET complete=1 WHERE id={$value['id']}")->execute(); 

                        if($value['location_name'] === 'Head Office'){
                            
                            // Cek Apakah Di Finding Yang Sama Masih Ada Yang Belum Di Submit.
                            $unfinish = TbAuditPicaCorrective::find()->where(" number='{$value['number']}' AND mandatory_name='Yes' AND finding_id={$value['finding_id']} AND complete=0 AND id <> {$value['id']} AND industry_id={$value['industry_id']} AND division_id={$value['division_id']} ")->one();
                            
                        }else{
                            
                            // Cek Apakah Di Finding Yang Sama Masih Ada Yang Belum Di Submit.
                            $unfinish = TbAuditPicaCorrective::find()->where(" number='{$value['number']}' AND mandatory_name='Yes' AND finding_id={$value['finding_id']} AND complete=0 AND id <> {$value['id']} AND industry_id={$value['industry_id']} AND region_id={$value['region_id']} AND business_unit_id={$value['business_unit_id']} ")->one();
                            
                        }

                        if($value['location_name'] === 'Head Office'){
                
                                //cek Workflow
                                $workflow = TbAuditWorkflow::find()->where(" industry_id={$value['industry_id']} AND division_id={$value['division_id']} AND order_number=$next_order_number ORDER BY order_number ASC ")->one();                                    
              
                        }else{
                    
                                //cek Workflow
                                $workflow = TbAuditWorkflow::find()->where(" industry_id={$value['industry_id']} AND region_id={$value['region_id']} AND business_unit_id={$value['business_unit_id']} AND division_id={$value['division_id']} AND order_number=$next_order_number ORDER BY order_number ASC ")->one();                                    
                
                        }    

                        if(count($workflow) <=0){
                        
                                $transaction->rollBack();
                        
                                $response->data = ['message' => "Workflow {$value['location_name']}, {$value['division_name']} Tidak Ditemukan. Silakan Hubungi Administrator Internal Audit.",'status' => 'fail'];  
                        
                                return $response;
                    
                        }   

                        // Next workflow
                        $message1 = WorkFlowController::nextFlow($comment,$value['number'],$approval_status,$workflow);
                    
                        if($message1->data['status']==='fail'){
                
                                $transaction->rollBack();
            
                                return $message1;
                
                        }                  

                        if(count($unfinish)<=0){
                                
                            if($value['location_name'] === 'Head Office'){
                                    
                                // Update PICA Corrective
                                Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET order_number=$next_order_number, next_nik='{$workflow['actor_nik']}', next_name='{$workflow['actor_name']}', next_position='{$workflow['actor_position']}' WHERE number='{$value['number']}' AND finding_id={$value['finding_id']} AND industry_id={$value['industry_id']} AND division_id={$value['division_id']}  ")->execute(); 
                                    
                            }else{

                                // Update PICA Corrective
                                Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET order_number=$next_order_number, next_nik='{$workflow['actor_nik']}', next_name='{$workflow['actor_name']}', next_position='{$workflow['actor_position']}' WHERE number='{$value['number']}' AND finding_id={$value['finding_id']} AND industry_id={$value['industry_id']} AND region_id={$value['region_id']} AND business_unit_id={$value['business_unit_id']} ")->execute(); 
                                                                    
                            }
    
                            // Update PICA Finding
                            Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET approval='Approved',status='On Progress',order_number=$next_order_number, next_nik='{$workflow['actor_nik']}' WHERE id={$value['finding_id']}")->execute(); 
                            
                            $nik_temp = explode(',',$workflow['actor_nik']);
                            
                            foreach ($nik_temp as $index_temp => $value_temp){
                    
                                $nik[$value_temp] = $value_temp;
                            
                            }
                            
                        }

                    }
                }
                                
                if(count($nik) > 0){
                    
                    // Send Email & inbox
                    $message2 = EmailController::send($pica_number,$nik,$email_body);
                    
                    if($message2->data['status']==='fail'){
                    
                        $transaction->rollBack();
            
                        return $message2;
                        
                    }
                    
                }
                
            }
            
            //Div Head
            if($order_number === 4){

                foreach($pica as $index => $value){

                    // Update PICA Finding Reset Comment Div Head
                    Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET supervisor_comment='' WHERE id={$value['id']} AND approval<>'Rejected' ")->execute(); 
                    
                    $pica_number = $value['number'];

                    if($value['approval'] ==='Approved'){

                        if($value['status'] ==='On Progress'){
                                       
                            foreach($value['corrective_action'] as $index2 => $value2){

                                if($value2['location_name'] === 'Head Office'){
                
                                    //cek Workflow
                                    $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND division_id={$value2['division_id']} AND order_number=$next_order_number ORDER BY order_number ASC ")->one();                                    
                
                                }else{
                    
                                    //cek Workflow
                                    $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']} AND order_number=$next_order_number ORDER BY order_number ASC ")->one();                                    
                
                                }

                                if(count($workflow) <=0){
                        
                                    $transaction->rollBack();
                        
                                    $response->data = ['message' => "Workflow {$value2['location_name']}, {$value2['division_name']} Tidak Ditemukan. Silakan Hubungi Administrator Internal Audit.",'status' => 'fail'];  
                        
                                    return $response;
                    
                                }
                            
                                $nik_temp = explode(',',$workflow['actor_nik']);
                            
                                foreach ($nik_temp as $index_temp => $value_temp){
                    
                                    $nik[$value_temp] = $value_temp;
                            
                                }
                            
                                PicaProgressController::createLog($value2);
                        
                                // Update PICA Corrective
                                Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET order_number=$next_order_number, next_nik='{$workflow['actor_nik']}', next_name='{$workflow['actor_name']}', next_position='{$workflow['actor_position']}' WHERE id={$value2['id']}")->execute(); 
                
                                // Next workflow
                                $message1 = WorkFlowController::nextFlow($comment,$value2['number'],$approval_status,$workflow);
                    
                                if($message1->data['status']==='fail'){
                
                                    $transaction->rollBack();
            
                                    return $message1;
                
                                }
                
                            }
                        
                        }

                    }else{

                        $this->reject3($comment,$value['corrective_action'],$order_number);

                    }
                }
                
                // Send Email & inbox
                $message2 = EmailController::send($pica_number,$nik,$email_body);
                    
                if($message2->data['status']==='fail'){
                    
                    $transaction->rollBack();
            
                    return $message2;
                
                }
                
            }
            
            //Final Approve
            if($order_number === 5){
                $d=0; 
                $v='';
                // next_nik
                foreach($pica as $index => $value){
                    $d=$d+1;
                    $pica_number = $value['number'];
                    $w='';
                    $corrective_action = TbAuditPicaCorrective::find()->where(" finding_id = {$value['id']} AND order_number=$order_number AND (  next_nik LIKE '%{$user['nik']},%' OR next_nik ='{$user['nik']}' ) ")->all();

                    $a[] = "finding_id = {$value['id']} AND order_number=$order_number AND (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik ='{$user['nik']}' ) ";
                    $e=0;

                    foreach($corrective_action as $index2 => $value2){
                        $e++;
                        $b[]=  " {$value2['location_name'] }industry_id={$value2['industry_id']}  AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']}  AND order_number={$value2['order_number']}";
                            //echo json_encode($b);
                            
                        $w[] = $value2['status'];

                        if($value2['status']==='Closed'){
                             // echo $value2['location_name'];
                             // exit();
                             if($value2['location_name'] === 'Head Office'){

                                //cek Workflow
                                $workflow = TbAuditWorkflow::find()->where("industry_id={$value2['industry_id']} AND division_id={$value2['division_id']}  AND order_number={$value2['order_number']} ")->one();                                    


                             }else{

                                //cek Workflow
                                $workflow = TbAuditWorkflow::find()->where("industry_id={$value2['industry_id']}  AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']}  AND order_number={$value2['order_number']} ")->one();


                             }
                             // exit();
                            //  $v[] = " {$value2['location_name']} industry_id={$value2['industry_id']}  AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']}  AND order_number={$value2['order_number']}";
                            // echo json_encode($v);
                            //  exit();
                            //echo json_encode($workflow)."<br>";
                            if(count($workflow) <=0){
                            
                                $transaction->rollBack();
                        
                                $response->data = ['message' => "Workflow {$value['location_name']}, {$value['division_name']} Tidak Ditemukan. Silakan Hubungi Administrator Internal Audit.",'status' => 'fail'];  
                        
                                return $response;
                    
                            }
                            $c[] = "$comment -- {$value2['number']} -- $approval_status -- ";
                             // echo json_encode($c);
                             // exit();
                            // Next workflow
                            WorkFlowController::finalFlow($comment,$value2['number'],$approval_status,$workflow);
                            
                            // Update PICA Corrective
                            Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET order_number=6, next_nik='', next_name='', next_position='' WHERE id={$value2['id']}")->execute(); 

                        
                            
                        }else{
                            
                            $this->reject2($comment,$corrective_action,$order_number);
                            //exit();
                            // $t[]=$comment." | ".$corrective_action." | ".$order_number;
                            // echo json_encode($t);
                            // exit();
                        }
                          
                    } 
                     

                } 
                // 5646,5614,5656,5680

                // Set Status Closed
                foreach($pica as $index => $value){
                
                    $corrective = TbAuditPicaCorrective::find()->where("(status='Open' OR status='On Progress') AND mandatory_name='Yes' AND finding_id='{$value['id']}' ")->all();                                    
                    //$c[] = count($corrective);

                    if(count($corrective) > 0){
                         
                        Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET status='On Progress' WHERE id={$value['id']}")->execute();
                        echo "UPDATE tb_audit_pica_finding SET status='On Progress' WHERE id={$value['id']} <br >";
                        
                    }else{

                        $corrective_closed = TbAuditPicaCorrective::find()->where(" status='Closed' AND mandatory_name='Yes' AND finding_id='{$value['id']}' ")->orderBy('date_closed DESC')->all();                                    
                        
                        Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET approval='Approved', status='Closed', date_closed='{$corrective_closed[0]['date_closed']}' WHERE id={$value['id']}")->execute();

                        EmailController::sendClose($corrective_closed,$order_number);
                    
                    }
                   
                }
                //echo json_encode($c);
                //    exit();
                
                //echo json_encode($comment."----".$corrective_action."---".$order_number); 
                //echo json_encode($c); 
                //exit();

                // Set Status Closed
                // foreach($pica as $index => $value){
                
                //     $corrective = TbAuditPicaCorrective::find()->where("(status='Open' OR status='On Progress') AND mandatory_name='Yes' AND finding_id='{$value['id']}' ")->all();  

                //    // $s[] = "select * from tb_audit_pica_corrective where (status='Open' OR status='On Progress') AND mandatory_name='Yes' AND finding_id='{$value['id']}' ";
                    
                //     // echo json_encode($corrective);exit();
                //     if(count($corrective) > 0){
                        
                //         Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET status='On Progress' WHERE id={$value['id']}")->execute();
                // //        $r[] = "UPDATE tb_audit_pica_finding SET status='On Progress' WHERE id={$value['id']}";
                //     // //     echo json_encode($corrective);
                //     // // exit(); 
                //     }else{

                //         $corrective_closed = TbAuditPicaCorrective::find()->where(" status='Closed' AND mandatory_name='Yes' AND finding_id='{$value['id']}' ")->orderBy('date_closed DESC')->one();    
                //         // $corrective_closedall = TbAuditPicaCorrective::find()->where(" status='Closed' AND mandatory_name='Yes' AND finding_id='{$value['id']}' ")->orderBy('date_closed DESC')->all();                                 
                //   //      $cc[] =  "select * from tb_audit_pica_corrective where status='Closed' AND mandatory_name='Yes' AND finding_id='{$value['id']}' order by date_closed DESC";

                //         Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET approval='Approved', status='Closed', date_closed='{$corrective_closed['date_closed']}' WHERE id={$value['id']}")->execute();
                //         //$u[] = " update tb_audit_pica_finding set approval='Approved', status='Closed', date_closed='{$corrective_closed[0]['date_closed']}' where id={$value['id']}";
                //         //$tr[] = $corrective_closed['date_closed'];
                //         //echo json_encode($tr);exit();
                //         EmailController::sendClose($corrective_closed,$order_number);
                //         //exit();
                //         // echo json_encode($corrective_closedall);exit();
                //         // $aa[] = $corrective_closedall." | ".$order_number;
                //     }
                    
                   
                // } 
                //echo json_encode($aa);exit();
            }    
                    
            
            //Pica Extension
            if($order_number === 7){

                $corrective_action = array();
               
                foreach($pica as $index => $value){
                    
                    $pica_number = $value['number'];

                    if(intval($value['is_extended']) === 1){
                    
                        $corrective_action = TbAuditPicaCorrective::find()->where(" finding_id = {$value['id']}")->all();
                    
                        foreach($corrective_action as $index2 => $value2){

                            if($value2['location_name'] === 'Head Office'){
                
                                //cek Workflow
                                $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND division_id={$value2['division_id']} AND order_number IN (1,3,4) ORDER BY order_number ASC ")->all();                                    
                
                            }else{
                    
                                //cek Workflow
                                $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']} AND order_number IN (1,3,4) ORDER BY order_number ASC ")->all();                                    
                
                            }

                            if(count($workflow) <=0){
                        
                                $transaction->rollBack();
                        
                                $response->data = ['message' => "Workflow {$value2['location_name']}, {$value2['division_name']} Tidak Ditemukan. Silakan Hubungi Administrator Internal Audit.",'status' => 'fail'];  
                        
                                return $response;
                    
                            }

                            foreach($workflow as $index => $value3){
                            
                                $nik_temp = explode(',',$value3['actor_nik']);
                            
                                foreach ($nik_temp as $index_temp => $value_temp){
                    
                                    $nik[$value_temp] = $value_temp;
                            
                                }
                            
                            }

                        }
                        
                        if(count($nik) >0){
                            
                            // Send Email & inbox
                            $message2 = EmailController::sendExtension($pica_number,$nik,$email_body,$value);
                    
                            if($message2->data['status']==='fail'){
                    
                                $transaction->rollBack();
            
                                return $message2;
                
                            }

                            $nik = array();

                        }

                    }

                    Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET is_extended = 0 WHERE id={$value['id']}")->execute();
                    
                }


            }

            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
            
        }catch(Exception $e){
                                    
            $transaction->rollBack();
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response;    
    }
    
    public function getAngka($angka=0){
        
        if($angka < 10){
            
            return '00'.$angka;
            
        }elseif($angka>=10 && $angka<=99 ){
            
            return '0'.$angka;
        }else{
            
            return $angka;
        }
    }
    
    public function actionGetAll(){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $pica_number = Yii::$app->request->post('pica_number');
        $order_number = Yii::$app->request->post('order_number');
        
        $user =  Yii::$app->user->identity;
        $query ='';
        $no = 1;
        $pica_corrective = array();
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try {   
            
            $pica = TbAuditPicaFinding::findBySql("SELECT id,number, REPLACE (finding,'<br>','\n') AS finding , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE(date_extension,date_due) AS date_due,status,REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment ,REPLACE (coordinator_comment,'<br>','\n') AS coordinator_comment ,REPLACE (div_head_comment,'<br>','\n') AS div_head_comment ,order_number,approval FROM tb_audit_pica_finding WHERE number = '$pica_number' AND is_deleted=0 ")->all();
            
            $pica_header =  TbAuditPicaHeader::find()->where("number ='$pica_number'")->one();
            
            $index_array = 0;
            
            foreach($pica as $index => $value){
                
                $corrective = array();
                
                foreach($value as $index2 => $value2){
                                       
                        
                    if($user->role_name==='Progress Approver'){
                            
                        $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,next_name,next_position,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='$pica_number' AND finding_id={$value['id']} AND division_id IN ($user->division_id) AND order_number={$order_number} AND is_deleted=0 ")->all();
                            
                    }else if($user->role_name==='PIC') {

                         $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,next_name,next_position,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='$pica_number' AND finding_id={$value['id']} AND (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik ='{$user['nik']}' ) AND complete=0  AND is_deleted=0 ")->all();

                    }else{
                            
                        $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,next_name,next_position,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='$pica_number' AND finding_id={$value['id']} AND (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik ='{$user['nik']}' )  AND is_deleted=0  ")->all();
                            
                    }
                                          
                    if(count($corrective) > 0){
                        
                        $pica_corrective[$index_array]['corrective_action'] = $corrective;
                        $pica_corrective[$index_array]['no']=$no;
                        $pica_corrective[$index_array][$index2] =   $value2;
                        
                    }
                }
                
                if(count($corrective) > 0){$index_array++;$no++;}
                
            }
            
            $transaction->commit();
       
            $response->data = ['pica' => $pica_corrective,'pica_header' => $pica_header];             
        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['pica' =>0];
                         
        }   
        
        return $response;
    }

    public function actionGet(){

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $industry = Yii::$app->request->post('industry');
        $region = Yii::$app->request->post('region');
        $business_unit = Yii::$app->request->post('business_unit');

        $user =  Yii::$app->user->identity;
        $query = '';
        $session = Yii::$app->session;

        if($business_unit){

            $query .=" AND business_unit_id=$business_unit ";

        }
        
        if($region){

            $query .=" AND region_id=$region ";

        }
        
        if($industry){

            $query .=" AND industry_id=$industry ";

        }
        
        $pica_corrective = array();
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try {

            $pica = TbAuditPicaFinding::findBySql("SELECT TOP 100 id,number, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days FROM tb_audit_pica_finding WHERE (status='On Progress' OR status='Open')  AND is_deleted=0 AND order_number>2 $query ORDER BY number DESC ")->all();
               
            $index_array = 0;
            
            foreach($pica as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, (REPLACE(number,'/','_')+'/'+progress_file) AS progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,COALESCE (date_extension,date_due) AS date_due  FROM tb_audit_pica_corrective WHERE finding_id={$value['id']}  AND is_deleted=0 ")->all();
                    
                    if(count($corrective) > 0){
                        
                        $pica_corrective[$index_array]['corrective_action'] = $corrective;
                        $pica_corrective[$index_array]['no']=$index+1;
                        $pica_corrective[$index_array][$index2] =   $value2;
                        
                    }
                }
                
                if(count($pica_corrective) > 0){$index_array++;}
                
            }

            $transaction->commit();
            
            $response->data = ['pica' => $pica_corrective];    

        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['pica' =>0];
                         
        }   
        
        return $response;
        
        // $response = Yii::$app->response;
        // $response->format = \yii\web\Response::FORMAT_JSON;
                
        // $industry = Yii::$app->request->post('industry');
        // $region = Yii::$app->request->post('region');
        // $business_unit = Yii::$app->request->post('business_unit');

        // $user =  Yii::$app->user->identity;
        // $query = '';
        // $session = Yii::$app->session;

        // if($business_unit){

        //     $query .=" AND business_unit_id=$business_unit ";

        // }
        
        // if($region){

        //     $query .=" AND region_id=$region ";

        // }
        
        // if($industry){

        //     $query .=" AND industry_id=$industry ";

        // }
        
        // $pica_corrective = array();
        
        // $transaction = Yii::$app->db->beginTransaction();
        
        // try {

        //     $pica = TbAuditPicaFinding::findBySql("SELECT TOP 100 id,number, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days FROM tb_audit_pica_finding WHERE (status='On Progress' OR status='Open')  AND is_deleted=0 $query ORDER BY number DESC ")->all();
        //     echo $pica;   
        //     $index_array = 0;
            
        //     foreach($pica as $index => $value){
                
        //         foreach($value as $index2 => $value2){
                    
        //             $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,COALESCE (date_extension,date_due) AS date_due  FROM tb_audit_pica_corrective WHERE finding_id={$value['id']}  AND is_deleted=0 ")->all();
                    
        //             if(count($corrective) > 0){
                        
        //                 $pica_corrective[$index_array]['corrective_action'] = $corrective;
        //                 $pica_corrective[$index_array]['no']=$index+1;
        //                 $pica_corrective[$index_array][$index2] =   $value2;
                        
        //             }
        //         }
                
        //         if(count($pica_corrective) > 0){$index_array++;}
                
        //     }

        //     $transaction->commit();
            
        //     $response->data = ['pica' => $pica_corrective];    

        // } 
        // catch (Exception $e) {
                                    
        //     $transaction->rollBack();
            
        //     $response->data = ['pica' =>0];
                         
        // }   
        
        // return $response;

    }
    
    public function actionGetAllTemp(){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $industry_id = Yii::$app->request->post('industry_id');
        $region_id = Yii::$app->request->post('region_id');
        $business_unit_id = Yii::$app->request->post('business_unit_id');
        $auditor = Yii::$app->user->identity;
        
        $transaction = Yii::$app->db->beginTransaction();
        
        $pica_corrective = array();
        
        try {   
            $pica = TbAuditPicaFindingTemp::findBySql("SELECT id,repeat_finding,REPLACE (finding,'<br>','\n') AS finding , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,date_due FROM tb_audit_pica_finding_temp WHERE auditor_nik='{$auditor['nik']}' AND industry_id=$industry_id AND region_id=$region_id AND business_unit_id=$business_unit_id ")->all();

            foreach($pica as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $pica_corrective[$index]['corrective_action'] =TbAuditPicaCorrectiveTemp::findBySql("SELECT id,finding_id,REPLACE (corrective_action,'<br>','\n') AS corrective_action,auditor_nik, auditor_name, pic_nik,pic_name,pic_position,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name  FROM tb_audit_pica_corrective_temp WHERE finding_id ={$value['id']} ")->all();
                    $pica_corrective[$index]['no']=$index+1;
                    $pica_corrective[$index][$index2] = $value2;  
                }
                
            }
            
            $transaction->commit();
            
            $response->data = ['pica' => $pica_corrective];             
        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['pica' =>0];
                         
        }   
        
        return $response;
    }

    public function actionGetAllTempOld(){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $number = Yii::$app->request->post('number');
        $industry_id = Yii::$app->request->post('industry_id');
        $region_id = Yii::$app->request->post('region_id');
        $business_unit_id = Yii::$app->request->post('business_unit_id');
        $auditor = Yii::$app->user->identity;
        
        $transaction = Yii::$app->db->beginTransaction();
        
        $pica_corrective = array();
        
        try {   
            
            $pica = TbAuditPicaFindingTemp2::findBySql("SELECT id,REPLACE (finding,'<br>','\n') AS finding , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,date_due FROM tb_audit_pica_finding_temp2 WHERE auditor_nik='{$auditor['nik']}' AND industry_id=$industry_id AND region_id=$region_id AND business_unit_id=$business_unit_id  AND number=$number ")->all();

            foreach($pica as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $pica_corrective[$index]['corrective_action'] =TbAuditPicaCorrectiveTemp2::findBySql("SELECT id,finding_id,REPLACE (corrective_action,'<br>','\n') AS corrective_action,auditor_nik, auditor_name, pic_nik,pic_name,pic_position,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name  FROM tb_audit_pica_corrective_temp2 WHERE finding_id ={$value['id']} ")->all();
                    $pica_corrective[$index]['no']=$index+1;
                    $pica_corrective[$index][$index2] = $value2;  
                }
                
            }
            
            $transaction->commit();
            
            $response->data = ['pica' => $pica_corrective];             
        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['pica' =>0];
                         
        }   
        
        return $response;
    }
    
    public function actionGetByBusinessUnitId(){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $business_unit_id = Yii::$app->request->post('business_unit_id');
        
        $transaction = Yii::$app->db->beginTransaction();
        
        $pica_corrective = array();
        
        try {   
            $pica = TbAuditPicaFinding::find()->where(['business_unit_id' =>$business_unit_id])->all();
            
            foreach($pica as $index => $value){
                
                foreach($value as $index2 => $value2){
                
                    $pica_corrective[$index]['corrective_action'] =TbAuditPicaCorrective::find()->where(['business_unit_id' =>$value['business_unit_id'],'finding_id'=>$value['id']])->all();
                    $pica_corrective[$index]['no']=$index+1;
                    $pica_corrective[$index][$index2] =   $value2;  
                }
                
            }
            
            $transaction->commit();
            
            $response->data = ['pica' => $pica_corrective];             
        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['pica' =>0];
                         
        }   
        
        return $response;
    }
    
    public function actionReject(){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;                

        $comment = Yii::$app->request->post('comment');
        $pica = Yii::$app->request->post('pica');
        $order_number = intval(Yii::$app->request->post('order_number'));
        $approver = Yii::$app->user->identity;
        $nik = array();
        $nik_temp = array();
        $pica_number = '';
        $approval_status  =''; 
        $email_body = ''; 
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try{

              switch($order_number){
                    
                  case 1:
                      $next_order_number = 0;
                      $approval_status  ='PICA Needs Update'; 
                      $email_body = 'picaRejected-html'; 
                  break;
                    
                  case 2:
                      $next_order_number = 0;
                      $approval_status  ='PICA Needs Update'; 
                      $email_body = 'picaRejected-html'; 
                  break;
                                                            
                  case 4:
                      $next_order_number = 3;
                      $approval_status  ='Progress Needs Update'; 
                      $email_body = 'progressRejected-html'; 
                  break;
                    
                  case 5:
                      $next_order_number = 3;
                      $approval_status  ='Progress Needs Update'; 
                      $email_body = 'progressRejected-html'; 
                  break;
                    
                  default :
                            ;
              }

            if($order_number === 1 || $order_number === 2){

                 foreach($pica as $index => $value){
            
                    $pica_number = $value['number'];

                    //cek Workflow
                    if($value['location_name'] === 'Head Office'){
                
                        //cek Workflow
                        $workflow = TbAuditWorkflow::find()->where(" industry_id={$value['industry_id']} AND division_id={$value['division_id']} AND order_number=$next_order_number ORDER BY order_number ASC ")->one();                                    
                
                    }else{
                    
                        //cek Workflow
                        $workflow = TbAuditWorkflow::find()->where(" industry_id={$value['industry_id']} AND region_id={$value['region_id']} AND business_unit_id={$value['business_unit_id']} AND division_id={$value['division_id']} AND order_number=$next_order_number ORDER BY order_number ASC ")->one();                                    
                        
                    }

                    // Next workflow
                    $message1 = WorkFlowController::nextFlow($comment,$value['number'],$approval_status,$workflow);
                    
                    if($message1->data['status']==='fail'){
                
                        return $message1;
                    }
                    
                    $nik_temp = explode(',',$workflow['actor_nik']);
                
                    foreach ($nik_temp as $index_temp => $value_temp){
                    
                        $nik[$value_temp] = $value_temp;
                    }
                    
                    // Update order number
                    Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET complete=0, order_number=$next_order_number, next_nik='{$workflow['actor_nik']}', next_name='{$workflow['actor_name']}', next_position='{$workflow['actor_position']}' WHERE finding_id={$value['id']} AND (next_nik LIKE '%,{$approver['nik']}%' OR next_nik LIKE '%{$approver['nik']},%' OR next_nik = '{$approver['nik']}' ) ")->execute();

                    Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET approval='Rejected', order_number=$next_order_number, next_nik='{$workflow['actor_nik']}' WHERE id={$value['id']} AND (next_nik LIKE '%,{$approver['nik']}%' OR next_nik LIKE '%{$approver['nik']},%' OR next_nik = '{$approver['nik']}' ) ")->execute();
                
                }

            }

            if($order_number === 4 || $order_number === 5){


                 foreach($pica as $index => $value){

                    $pica_number = $value['number'];
            
                    foreach($value['corrective_action'] as $index2 => $value2){
                
                    
                        //cek Workflow
                        if($value2['location_name'] === 'Head Office'){
                
                            //cek Workflow
                            $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND division_id={$value2['division_id']} AND order_number=$next_order_number ORDER BY order_number ASC ")->one();                                    
                
                        }else{
                    
                            //cek Workflow
                            $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']} AND order_number=$next_order_number ORDER BY order_number ASC ")->one();                                    
                        
                        }

                                    
                        // Next workflow
                        $message1 = WorkFlowController::nextFlow($comment,$value2['number'],$approval_status,$workflow);
                    
                        if($message1->data['status']==='fail'){
                
                            return $message1;
                        }
                    
                        $nik_temp = explode(',',$workflow['actor_nik']);
                
                        foreach ($nik_temp as $index_temp => $value_temp){
                    
                            $nik[$value_temp] = $value_temp;
                        }
                    
                        // Update order number
                        Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET complete=0, order_number=$next_order_number, next_nik='{$workflow['actor_nik']}', next_name='{$workflow['actor_name']}', next_position='{$workflow['actor_position']}' WHERE id={$value2['id']} AND (next_nik LIKE '%,{$approver['nik']}%' OR next_nik LIKE '%{$approver['nik']},%' OR next_nik = '{$approver['nik']}' ) ")->execute();

                        Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET approval='Rejected', order_number=$next_order_number, next_nik='{$workflow['actor_nik']}' WHERE id={$value2['finding_id']} AND (next_nik LIKE '%,{$approver['nik']}%' OR next_nik LIKE '%{$approver['nik']},%' 
                        OR next_nik = '{$approver['nik']}' ) ")->execute();
                    }
                }

            }
            
            // Send Email & inbox
            $message2 = EmailController::send($pica_number,$nik,$email_body);
                    
            if($message2->data['status']==='fail'){
                    
                $transaction->rollBack();
            
                return $message2;
                
            }
            
            $transaction->commit();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
            
        }catch(Exception $e){
                                    
            $transaction->rollBack();
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response;    
    }

    public function reject3($comment,$corrective,$order_number){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;                

        $approver = Yii::$app->user->identity;
        $nik = array();
        $nik_temp = array();
        $pica_number = '';
        $approval_status  =''; 
        $email_body = ''; 
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try{
            
            switch($order_number){
                            
                case 4:
                    $next_order_number = 3;
                    $approval_status  ='Progress Needs Update'; 
                    $email_body = 'progressRejected-html'; 
                break;
                    
                default :
                   ;
            }
            
            foreach($corrective as $index2 => $value2){

                $pica_number = $value2['number'];
                
                //cek Workflow
                if($value2['location_name'] === 'Head Office'){
                
                    //cek Workflow
                    $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND division_id={$value2['division_id']} AND order_number = 3 ORDER BY order_number ASC ")->all();                                    
                
                }else{

                    //cek Workflow
                    $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']} AND order_number = 3 ORDER BY order_number ASC ")->all();                                    
                
                }

                // Next workflow
                $message1 = WorkFlowController::nextFlow($comment,$value2['number'],$approval_status,$workflow[0]);
                    
                if($message1->data['status']==='fail'){
                
                    return $message1;
                }
                        
                foreach($workflow as $index => $value){
                            
                    $nik_temp = explode(',',$value['actor_nik']);
                
                    foreach ($nik_temp as $index_temp => $value_temp){
                    
                        $nik[$value_temp] = $value_temp;
                    }
                }
                    
                // Update order number
                Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET order_number=$next_order_number, next_nik='{$workflow[0]['actor_nik']}', next_name='{$workflow[0]['actor_name']}', next_position='{$workflow[0]['actor_position']}',complete=0 WHERE id={$value2['id']} AND (next_nik LIKE '%,{$approver['nik']}%' OR next_nik LIKE '%{$approver['nik']},%' OR next_nik = '{$approver['nik']}') ")->execute();
                
            }

            if(count($nik)){
            
                // Send Email & inbox
                $message2 = EmailController::send($pica_number,$nik,$email_body);
                    
                if($message2->data['status']==='fail'){
                    
                    $transaction->rollBack();
            
                    return $message2;
                
                }
                
            }
                 
            $transaction->commit();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
            
        }catch(Exception $e){
                                    
            $transaction->rollBack();
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response;    
    }
    
    public function reject2($comment,$corrective,$order_number){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;                

        $approver = Yii::$app->user->identity;
        $nik = array();
        $nik_temp = array();
        $pica_number = '';
        $approval_status  =''; 
        $email_body = ''; 
        
        $transaction = Yii::$app->db->beginTransaction();
        
         try{
            // $response = "tes";
            switch($order_number){
                            
                case 5:
                    $next_order_number = 3;
                    $approval_status  ='Progress Needs Update'; 
                    $email_body = 'progressRejected-html'; 
                break;
                    
                default :
                   ;
            }
            //$response = $next_order_number;
            //$response = count($corrective);

            foreach($corrective as $index2 => $value2){

                if($value2['status'] !='Closed'){
                    

            
                    $pica_number = $value2['number'];
                
                    //cek Workflow
                    if($value2['location_name'] === 'Head Office'){
                
                        //cek Workflow
                        $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND division_id={$value2['division_id']} AND order_number IN (3,4) ORDER BY order_number ASC ")->all();                                    
                
                    }else{
                    
                        //cek Workflow
                        $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']} AND order_number IN (3,4) ORDER BY order_number ASC ")->all();                                    
                
                    }
                    // echo "industry_id={$value2['industry_id']} AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']} AND order_number IN (3,4) ORDER BY order_number ASC <br />";
                    // if($workflow)
                    // echo json_encode($workflow)."    industry_id={$value2['industry_id']} AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']} AND order_number IN (3,4) ORDER BY order_number ASC <br />";
                
                    // // Next workflow
                     $message1 = WorkFlowController::nextFlow($comment,$value2['number'],$approval_status,$workflow[0]);
                    
                    if($message1->data['status']==='fail'){
                
                        return $message1;
                    }
                        
                    foreach($workflow as $index => $value){
                            
                        $nik_temp = explode(',',$value['actor_nik']);
                
                        foreach ($nik_temp as $index_temp => $value_temp){
                    
                            $nik[$value_temp] = $value_temp;
                        }
                    }
                    
                    // Update order number
                    Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET order_number=$next_order_number, next_nik='{$workflow[0]['actor_nik']}', next_name='{$workflow[0]['actor_name']}', next_position='{$workflow[0]['actor_position']}',complete=0 WHERE id={$value2['id']} AND (next_nik LIKE '%,{$approver['nik']}%' OR next_nik LIKE '%{$approver['nik']},%' OR next_nik = '{$approver['nik']}') ")->execute();
                    // echo "UPDATE tb_audit_pica_corrective SET order_number=$next_order_number, next_nik='{$workflow[0]['actor_nik']}', next_name='{$workflow[0]['actor_name']}', next_position='{$workflow[0]['actor_position']}',complete=0 WHERE id={$value2['id']} AND (next_nik LIKE '%,{$approver['nik']}%' OR next_nik LIKE '%{$approver['nik']},%' OR next_nik = '{$approver['nik']}') <br />"; 
                }
                        
            } 
             //exit();
            // $response->data = ['message' => json_encode($nm),'status' => 'success'];        
            // echo json_encode($nik);
            // exit();
            if(count($nik)){
            
                // Send Email & inbox
                $message2 = EmailController::send($pica_number,$nik,$email_body);
                //echo json_encode($message2)." || ".$message2->data['status'];    
                if($message2->data['status']==='fail'){
                    
                     $transaction->rollBack();
            
                    return $message2;
                
                }

                
            }
                 
            $transaction->commit();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
            
        }catch(Exception $e){
                                    
            $transaction->rollBack();
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        //alert("ssss");  
         return $response;
        //return "tes";    
    }

    public function actionEdit(){
        
        $number = Yii::$app->request->post('number');

        $this->redirect(['site/pica-edit','id'=>1]);
         
    }

    public function actionDeleteFile()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $model = TbAuditPicaHeader::findOne($id);

        $file = FileController::Remove($model->pica_file);
        
        $model->pica_file ='';

        if ($model->save()) {
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','header'=>$model];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }   

        
        
        if($file->data['status']==='fail'){
                     
            return $file;
        }   
        
        return $response;
    }

    public function actionUpload(){
        //add 30.8.2017 ayy
        $isdelete=0;

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        $file_name = '';
        $finding_id=0;
        
        try {              
            
            if($_FILES['file']['name'] ==='pica_template.xls'){

                $target_dir = "uploads";

                $target_file = $target_dir.'/'.$_FILES['file']['name'];

                if (file_exists($target_file)) {

                    FileController::Remove($_FILES['file']['name']);

                }

                FileController::Upload($_FILES,$file_name);

                $data = \moonland\phpexcel\Excel::import($target_file);

                foreach($data as $index => $value){

                    $industry_id = TbAuditIndustry::find()->where(['name'=>$value['industry_name']])->one()->id;
                    //update 21-07-2017
                    // if($value['industry_name']==='Crumb Rubber'){

                        $auditor = Yii::$app->db2->createCommand("SELECT k.nik, k.username as nama,k.email,k.position as posst FROM tbl_audit_user AS k WHERE k.nik ='{$value['auditor_nik']}' ")->queryOne(); //Yii::$app->db2->createCommand("SELECT k.nik, k.nama,k.email,k.posst FROM tbl_karyawan AS k WHERE k.nik ='{$value['auditor_nik']}' ")->queryOne();
                    
                        $leader = Yii::$app->db2->createCommand("SELECT k.nik, k.username as nama,k.email,k.position as posst FROM tbl_audit_user AS k WHERE k.nik ='{$value['team_leader_nik']}' ")->queryOne(); //Yii::$app->db2->createCommand("SELECT k.nik, k.nama,k.email,k.posst FROM tbl_karyawan AS k WHERE k.nik ='{$value['team_leader_nik']}' ")->queryOne();
                    
                        $pic = Yii::$app->db2->createCommand("SELECT k.nik, k.username as nama,k.email,k.position as posst FROM tbl_audit_user AS k WHERE k.nik ='{$value['pic_nik']}' ")->queryOne(); //Yii::$app->db2->createCommand("SELECT k.nik, k.nama,k.email,k.posst FROM tbl_karyawan AS k WHERE k.nik ='{$value['pic_nik']}' ")->queryOne();
                    
                    // }else{

                    //     $auditor = Yii::$app->db->createCommand("SELECT k.nik, k.username,k.email,k.position FROM tb_audit_user AS k WHERE k.nik ='{$value['auditor_nik']}' AND k.industry_id='$industry_id' ")->queryOne();
                    
                    //     $leader = Yii::$app->db->createCommand("SELECT k.nik, k.username,k.email,k.position FROM tb_audit_user AS k WHERE k.nik ='{$value['team_leader_nik']}' AND k.industry_id='$industry_id' ")->queryOne();
                    
                    //     $pic = Yii::$app->db->createCommand("SELECT k.nik, k.username,k.email,k.position FROM tb_audit_user AS k WHERE k.nik ='{$value['pic_nik']}' AND k.industry_id='$industry_id' ")->queryOne();


                    // }

                    if($value['next_nik'] === null){

                        $next_nik = '' ;
                        $next_name = '' ;
                        $next_position = '' ;

                    }else{

                        $namaArr = array();
                        $positionArr = array();
                        
                        $next = explode(',', $value['next_nik']);

                        foreach($next as $index2 => $value2){
                            //update 21-07-2017
                            // if($value['industry_name']==='Crumb Rubber'){

                                $next2 = Yii::$app->db2->createCommand("SELECT k.nik, k.username as nama,k.email,k.position as posst FROM tbl_audit_user AS k WHERE k.nik IN ({$value2}) ")->queryOne(); //Yii::$app->db2->createCommand("SELECT k.nik, k.nama,k.email,k.posst FROM tbl_karyawan AS k WHERE k.nik IN ({$value2}) ")->queryOne();
                            
                            // }else{

                            //     $next2 = Yii::$app->db2->createCommand("SELECT k.nik, k.username,k.email,k.position FROM tb_audit_user AS k WHERE k.nik IN ({$value2})  AND k.industry_id='$industry_id' ")->queryOne();

                            // }

                            $namaArr[$index2] = $next2['nama'];
                            $positionArr[$index2] = $next2['posst'];

                        }

                        $next_nik = $value['next_nik'] ;
                        $next_name = implode(',',$namaArr) ;
                        $next_position = implode(',', $positionArr) ;

                    }

                    //cek pada pica header
                    $pica_header = TbAuditPicaHeader::find()->where(['number'=>$value['number'],'is_deleted'=>$isdelete])->one();

                    if(!$pica_header){
                        
                        $model_header = new TbAuditPicaHeader();
                        $model_header->number = $value['number'] ;
                        $model_header->auditor_nik = (string) $value['auditor_nik'] ;
                        $model_header->auditor_name = $auditor['nama'] ;
                        $model_header->auditor_position = $auditor['posst'] ;
                        $model_header->team_leader_nik = (string) $value['team_leader_nik'];
                        $model_header->team_leader_name = $leader['nama'] ;
                        $model_header->team_leader_position = $leader['posst'] ;
                        $model_header->industry_id  = $industry_id;
                        $model_header->industry_name  = $value['industry_name'];
                        $model_header->region_id  = TbAuditRegion::find()->where(['name'=>$value['region_name']])->one()->id;
                        $model_header->region_name  = $value['region_name'];
                        $model_header->business_unit_id  = TbAuditBusinessUnit::find()->where(['name'=>$value['business_unit_name']])->one()->id;
                        $model_header->business_unit_name  = $value['business_unit_name'];
                        $model_header->pica_file  = $value['pica_file'] === null ? '' : $value['pica_file'];
                        $model_header->project_name  = $value['project_name'];
                        $model_header->date_from  = date('m-d-Y',strtotime(str_replace('/','-',$value['date_from'])));
                        $model_header->date_to  = date('m-d-Y',strtotime(str_replace('/','-',$value['date_to'])));
                        $model_header->date_posted  = date('m-d-Y',strtotime(str_replace('/','-',$value['date_posted'])));
                        $model_header->save();

                    }
                               
                    //cek pada   pica finding
                     $pica_finding = TbAuditPicaFinding::find()->where(['number'=>$value['number'],'location_name'=>$value['finding_location_name'],'division_name'=>$value['finding_division_name'],'rating_name'=>$value['rating_name'],'category_name'=>$value['category_name'],'finding'=>$value['finding'],'is_deleted'=>$isdelete])->one();
 
                     if(!$pica_finding){

                        $model_finding = new TbAuditPicaFinding();
                        $model_finding->number = $value['number'] ;
                        $model_finding->finding = $value['finding'];
                        $model_finding->finding_file = $value['finding_file'];
                        $model_finding->auditor_nik = (string) $value['auditor_nik'] ;
                        $model_finding->auditor_name = $auditor['nama'] ;
                        $model_finding->auditor_position = $auditor['posst'] ;
                        $model_finding->team_leader_nik = (string) $value['team_leader_nik'];
                        $model_finding->team_leader_name = $leader['nama'] ;
                        $model_finding->team_leader_position = $leader['posst'] ;
                        $model_finding->comment = $value['auditor_comment'] === null ? '': $value['auditor_comment'];
                        $model_finding->rating_id = TbAuditRating::find()->where(['name'=>$value['rating_name']])->one()->id;
                        $model_finding->rating_name = $value['rating_name'];
                        $model_finding->category_id = TbAuditCategory::find()->where(['name'=>$value['category_name']])->one()->id;
                        $model_finding->category_name = $value['category_name'];
                        $model_finding->industry_id  = $industry_id;
                        $model_finding->industry_name  = $value['industry_name'];
                        $model_finding->region_id  = TbAuditRegion::find()->where(['name'=>$value['region_name']])->one()->id;
                        $model_finding->region_name  = $value['region_name'];
                        $model_finding->business_unit_id  = TbAuditBusinessUnit::find()->where(['name'=>$value['business_unit_name']])->one()->id;
                        $model_finding->business_unit_name  = $value['business_unit_name'];
                        $model_finding->location_id = TbAuditLocation::find()->where(['name'=>$value['finding_location_name']])->one()->id;
                        $model_finding->location_name = $value['finding_location_name'];
                        $model_finding->division_id = TbAuditDivision::find()->where(['name'=>$value['finding_division_name']])->one()->id;
                        $model_finding->division_name = $value['finding_division_name'];
                        $model_finding->date_posted   = date('m-d-Y',strtotime(str_replace('/','-',$value['date_posted'])));
                        $model_finding->date_due   = date('m-d-Y',strtotime(str_replace('/','-',$value['date_due'])));
                        $model_finding->date_closed   = date('m-d-Y',strtotime(str_replace('/','-',$value['date_closed']))) === '01-01-1970' ? null : date('m-d-Y',strtotime(str_replace('/','-',$value['date_closed'])));
                        $model_finding->status = $value['finding_status'];
                        $model_finding->lead_time_status = $value['finding_lead_time_status'];
                        $model_finding->is_printed =1;
                        $model_finding->save();
                        $finding_id = $model_finding->id;

                    }else{

                        $finding_id = $pica_finding['id'];

                    }

                    //cek pada pica corrective
                    $pica_corrective = TbAuditPicaCorrective::find()->where(['number'=>$value['number'],'location_name'=>$value['corrective_location_name'],'division_name'=>$value['corrective_division_name'],'pic_nik'=>$value['pic_nik'],'mandatory_name'=>$value['mandatory_name'],'order_number'=>$value['order_number'],'next_nik'=>$next_nik,'corrective_action'=>$value['corrective_action'],'is_deleted'=>$isdelete])->one();

                    if(!$pica_corrective){
                       
                        //insert pica corrective
                        $model_corrective = new TbAuditPicaCorrective();
                        $model_corrective->number = $value['number'] ;
                        $model_corrective->finding_id = $finding_id;
                        $model_corrective->auditor_nik = (string) $value['auditor_nik'] ;
                        $model_corrective->auditor_name = $auditor['nama'] ;
                        $model_corrective->auditor_position = $auditor['posst'] ;
                        $model_corrective->pic_nik = (string) $value['pic_nik'] ;
                        $model_corrective->pic_name = $pic['nama'] ;
                        $model_corrective->pic_position = $pic['posst'] ;
                        $model_corrective->finding = $value['finding'];
                        $model_corrective->finding_file = $value['finding_file'];
                        $model_corrective->corrective_action = $value['corrective_action'];
                        $model_corrective->latest_progress = $value['latest_progress'] === null ? '' : $value['latest_progress'] ;
                        $model_corrective->progress_file = $value['progress_file'] === null ? '' : $value['progress_file'] ;
                        $model_corrective->status = $value['corrective_status'];
                        $model_corrective->lead_time_status = $value['corrective_lead_time_status'];
                        $model_corrective->mandatory_id = TbAuditMandatory::find()->where(['name'=>$value['mandatory_name']])->one()->id;
                        $model_corrective->mandatory_name = $value['mandatory_name'] ;
                        $model_corrective->industry_id  = $industry_id;
                        $model_corrective->industry_name  = $value['industry_name'];
                        $model_corrective->region_id  = TbAuditRegion::find()->where(['name'=>$value['region_name']])->one()->id;
                        $model_corrective->region_name  = $value['region_name'];
                        $model_corrective->business_unit_id  = TbAuditBusinessUnit::find()->where(['name'=>$value['business_unit_name']])->one()->id;
                        $model_corrective->business_unit_name  = $value['business_unit_name'];
                        $model_corrective->location_id = TbAuditLocation::find()->where(['name'=>$value['corrective_location_name']])->one()->id;
                        $model_corrective->location_name = $value['corrective_location_name'];
                        $model_corrective->division_id = TbAuditDivision::find()->where(['name'=>$value['corrective_division_name'],'industry_id'=>$industry_id])->one()->id;
                        $model_corrective->division_name = $value['corrective_division_name'];
                        $model_corrective->order_number = $value['order_number'] ;
                        $model_corrective->complete = intval($value['order_number']) === 6 ? 1 : 0 ;
                        $model_corrective->next_nik = (string) $next_nik ;
                        $model_corrective->next_name = $next_name ;
                        $model_corrective->next_position = $next_position ;
                        $model_corrective->date_progress   = date('m-d-Y',strtotime(str_replace('/','-',$value['date_progress']))) === '01-01-1970' ? null : date('m-d-Y',strtotime(str_replace('/','-',$value['date_progress'])));
                        $model_corrective->date_closed   = date('m-d-Y',strtotime(str_replace('/','-',$value['date_closed']))) === '01-01-1970' ? null : date('m-d-Y',strtotime(str_replace('/','-',$value['date_closed'])));
                        $model_corrective->date_due  = date('m-d-Y',strtotime(str_replace('/','-',$value['date_due'])));
                        $model_corrective->save();

                    }
                   

                }
            
                $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];    
            }         
            
        }catch (Exception $e) {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response;
                                
    }

    public function actionSearch()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $number = Yii::$app->request->post('number');

        $pica = array();
        $corrective = array();
        $pica_corrective = array();
        $index_array = 0;
       
        $pica = TbAuditPicaFinding::find()->where(['number' =>$number])->all();

        if(count($pica)<=0){

             $response->data = ['status' => 'fail','message'=>'Data Yang Anda Cari Tidak Ada.'];    

        }else{

            foreach($pica as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,COALESCE (date_extension,date_due) AS date_due  FROM tb_audit_pica_corrective WHERE finding_id={$value['id']}  AND is_deleted=0 ")->all();
                    
                    if(count($corrective) > 0){
                        
                        $pica_corrective[$index_array]['corrective_action'] = $corrective;
                        $pica_corrective[$index_array]['no']=$index+1;
                        $pica_corrective[$index_array][$index2] =   $value2;
                        
                    }
                }
                
                if(count($pica_corrective) > 0){$index_array++;}
                
            }

            $response->data = ['status' => 'success','pica' => $pica_corrective];    

        }
            
                 
       
        
        return $response;
    }

    public function actionSwitch(){

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $corrective = Yii::$app->request->post('corrective');

        $approval_status  ='PICA Switched'; 
        $email_body = 'picaAwaitingProgresss-html'; 
        $comment = '';
        $next_order_number = 3;

        $transaction = Yii::$app->db->beginTransaction();
        
        try{
            

            //Approved by IA Division
            foreach($corrective as $index => $value2){

                if($value2['status'] !=='Closed'){

                    if($value2['location_name'] === 'Head Office'){
                
                        //cek Workflow
                        $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND division_id={$value2['division_id']} AND order_number IN (3,4) ORDER BY order_number ASC ")->all();                                    
                
                    }else{
                    
                        //cek Workflow
                        $workflow = TbAuditWorkflow::find()->where(" industry_id={$value2['industry_id']} AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']} AND division_id={$value2['division_id']} AND order_number IN (3,4) ORDER BY order_number ASC ")->all();                                    
                
                    }

                    if(count($workflow) <=0){
                        
                        $transaction->rollBack();
                        
                        $response->data = ['message' => "Workflow {$value2['location_name']}, {$value2['division_name']} Tidak Ditemukan. Silakan Hubungi Administrator Internal Audit.",'status' => 'fail'];  
                        
                        return $response;
                    
                    }

                                                
                    $pica_number = $value2['number'];
              
                    // Update PICA Finding
                    Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET order_number=$next_order_number, next_nik='{$workflow[0]['actor_nik']}' WHERE id={$value2['finding_id']}")->execute(); 
                    
                    // Update PICA Corrective
                    Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET order_number=$next_order_number, next_nik='{$workflow[0]['actor_nik']}',next_name='{$workflow[0]['actor_name']}', next_position='{$workflow[0]['actor_position']}', status='Open' WHERE id={$value2['id']}")->execute(); 
                
                    // Next workflow
                    $message1 = WorkFlowController::nextFlow($comment,$value2['number'],$approval_status,$workflow[0]);
                    
                    if($message1->data['status']==='fail'){
                
                        $transaction->rollBack();
            
                        return $message1;
                
                    }
                    
                    foreach($workflow as $index => $value3){
                            
                        $nik_temp = explode(',',$value3['actor_nik']);
                            
                        foreach ($nik_temp as $index_temp => $value_temp){
                    
                            $nik[$value_temp] = $value_temp;
                            
                        }
                            
                    }
                }
                
                   
            }
                
            // Send Email & inbox
            $message2 = EmailController::send($pica_number,$nik,$email_body);
                    
            if($message2->data['status']==='fail'){
                    
                $transaction->rollBack();
            
                return $message2;
                
            }
                
            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
            
        }catch(Exception $e){
                                    
            $transaction->rollBack();
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response; 

    }
       
    
}
