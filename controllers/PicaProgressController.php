<?php

namespace app\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use app\models\TbAuditPicaProgress;
use app\models\TbAuditPicaFinding;
use app\models\TbAuditPicaCorrective;
use yii\data\ActiveDataProvider;


/**
 * PicaController implements the CRUD actions for TbAuditPicaProgress model.
 */
class PicaProgressController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TbAuditPicaProgress models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TbAuditPicaProgressProgress::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TbAuditPicaProgress model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TbAuditPicaProgress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $pica = Yii::$app->request->post('pica');
        $comment = Yii::$app->request->post('comment');
        $approval_status = 'Progress Submitted';
        $approver = Yii::$app->user->identity;
        $industry_id = Yii::$app->request->post('industry_id');
        $industry_name = TbAuditIndustry::findOne($industry_id)->name;        
        $region_id = Yii::$app->request->post('region_id');
        $region_name = TbAuditRegion::findOne($region_id)->name;
        $business_unit_id = Yii::$app->request->post('business_unit_id');
        $business_unit_name = TbAuditBusinessUnit::findOne($business_unit_id)->name;
        
        $email_body = 'picaAwaitingApproval-html';  
        
        
        $transaction = Yii::$app->db->beginTransaction();        
                       
        try {              
                
                foreach($pica as $index => $value){
                    
                    $finding = $value['finding'];
                    $finding_file = $value['finding_file'];
                    $date_due = $value['date_due'];
                
                    // Insert PICA Corrective
                    foreach($value['corrective_action'] as $index2 => $value2){
                    
                        $next_order_number = $value2['order_number'] + 1;
                        
                        //cek Workflow
                        $workflow = TbAuditWorkflow::find()->where("industry_id={$value2['industry_id']} AND division_id={$value2['division_id']} AND order_number=$next_order_number")->one();                                    
                    
                        // insert PICA Progress
                        Yii::$app->db->createCommand("INSERT INTO tb_audit_pica_progress (number,finding,finding_file,corrective_action,progress,progress_file,progress_date,auditor_nik,auditor_name,auditor_position,pic_nik,pic_name,pic_position,rating_id,rating_name,,industry_id,industry_name,
                                                        location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,approval_status,date_due)
                                                        VALUES ();")->execute(); 
                    
                        // Next workflow
                        $message1 = WorkFlowController::nextFlow($comment,$value2['number'],$approval_status,$workflow);
                    
                        if($message1->data['status']==='fail'){
                
                            $transaction->rollBack();
            
                            return $message1;
                
                        }
                    
                        // Send Email & inbox
                        $message2 = EmailController::send($comment,$value2['number'],$workflow,$approval_status,$approver,$email_body);
                    
                        if($message2->data['status']==='fail'){
                
                            $transaction->rollBack();
            
                            return $message2;
                
                        }
                                                        
                    }
                
                }
            
                $transaction->commit();
            
                $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response;
    }

    /**
     * Updates an existing TbAuditPicaProgress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $progress = Yii::$app->request->post('progress');
        
        $model = $this->findModel($id);

        if ($model->save()) {
            
            $response->data = ['message' => 'Data Berhasil Diperbarui','status' => 'success'];             
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Diperbarui','status' => 'fail'];             
            
        }     
        
        return $response;   
        
    }

    /**
     * Deletes an existing TbAuditPicaProgress model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TbAuditPicaProgress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TbAuditPicaProgress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbAuditPicaProgress::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
    public static function createLog($corrective)
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $transaction = Yii::$app->db->beginTransaction();      

        try {    

                $comment = str_replace("'","''",$corrective['comment']);          
                
                // insert PICA Progress
                Yii::$app->db->createCommand("INSERT INTO tb_audit_pica_progress(number,finding_id,auditor_nik,auditor_name,auditor_position,pic_nik,pic_name,pic_position,finding,finding_file,corrective_id,corrective_action,latest_progress,
                                                progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,
                                                order_number,next_nik,date_progress,date_due,comment) VALUES ('{$corrective['number']}',{$corrective['finding_id']},'{$corrective['auditor_nik']}','{$corrective['auditor_name']}','{$corrective['auditor_position']}','{$corrective['pic_nik']}','{$corrective['pic_name']}','{$corrective['pic_position']}','{$corrective['finding']}','{$corrective['finding_file']}',{$corrective['id']},'{$corrective['corrective_action']}','{$corrective['latest_progress']}',
                                                '{$corrective['progress_file']}','{$corrective['status']}','{$corrective['lead_time_status']}',{$corrective['mandatory_id']},'{$corrective['mandatory_name']}',{$corrective['industry_id']},'{$corrective['industry_name']}',{$corrective['location_id']},'{$corrective['location_name']}',{$corrective['region_id']},'{$corrective['region_name']}',{$corrective['business_unit_id']},'{$corrective['business_unit_name']}',{$corrective['division_id']},'{$corrective['division_name']}',
                                                {$corrective['order_number']},'{$corrective['next_nik']}','{$corrective['date_progress']}','{$corrective['date_due']}','$comment');")->execute(); 
                
                $transaction->commit();
            
                $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }   
        
        return $response;
    }

    public function actionGetAll(){
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
                
        $number = Yii::$app->request->post('number');        
        $industry = Yii::$app->request->post('industry');
        $region = Yii::$app->request->post('region');
        $business_unit = Yii::$app->request->post('business_unit');
        $status = Yii::$app->request->post('status');
        $from = Yii::$app->request->post('from') !== null ? '01-'.Yii::$app->request->post('from') : Yii::$app->request->post('from');
        $to = Yii::$app->request->post('to') !== null ? '31-'.Yii::$app->request->post('to') : Yii::$app->request->post('to');
       
        $user =  Yii::$app->user->identity;
        $query = '';
        $session = Yii::$app->session;

        if($number){

            $query .=" AND number='$number' ";

        }

        if($from && $to){

            $from_date = date('Y-m-d',strtotime($from));
            $to_date = date('Y-m-d',strtotime($to));

            $query .=" AND date_created BETWEEN '$from_date' AND '$to_date' ";                

        }elseif($from){
            
             $from_date = date('Y-m-d',strtotime($from));
           
             $query .=" AND date_created >= '$from_date' "; 

        }
        
        if($status){
             
            $query .=" AND status='$status' ";                
            
        }
        
        if($business_unit){

            $query .=" AND business_unit_id=$business_unit ";

        }
        
        if($region){

            $query .=" AND region_id=$region ";

        }
        
        if($industry){

            $query .=" AND industry_id=$industry ";

        }
        
        $pica_corrective = array();
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try {

            $pica = TbAuditPicaFinding::findBySql("SELECT id,number, REPLACE (finding,'<br>','\n') AS finding ,  REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days  FROM tb_audit_pica_finding WHERE is_deleted=0 AND order_number>2 $query ORDER BY number DESC ")->all();
                
            $index_array = 0;
            
            foreach($pica as $index => $value){
                
                $corrective = array();

                foreach($value as $index2 => $value2){
                    
                    $corrective = TbAuditPicaProgress::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, (REPLACE(number,'/','_')+'/'+progress_file) AS progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress FROM tb_audit_pica_progress WHERE finding_id={$value['id']}  AND is_deleted=0  ORDER BY corrective_id ASC")->all();
                    
                    if(count($corrective) > 0){
                        
                        $pica_corrective[$index_array]['corrective_action'] = $corrective;
                        $pica_corrective[$index_array]['no']=$index_array+1;
                        $pica_corrective[$index_array][$index2] =   $value2;
                        
                    }
                }
                
                 if(count($corrective) > 0){$index_array++;}
                
            }

            $transaction->commit();
            
            $response->data = ['pica' => $pica_corrective];    

        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['pica' =>0];
                         
        }   
        
        return $response;
    }

}
