<?php

namespace app\controllers;

use Yii;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use app\models\TbAuditIndustry;
use app\models\TbAuditLocation;
use app\models\TbAuditRegion;
use app\models\TbAuditBusinessUnit;
use app\models\TbAuditDivision;  
use app\models\TbAuditUser; 
use app\models\TbAuditPicaCorrectiveTemp;
use app\models\TbAuditPicaFindingTemp;
use app\models\TbAuditPicaCorrectiveTemp2;
use app\models\TbAuditPicaFindingTemp2;
use app\models\TbAuditPicaFinding;
use app\models\TbAuditPicaCorrective;
use app\models\TbAuditMandatory;
use app\controllers\FileController;


/**
 * PicaController implements the CRUD actions for TbAuditPicaCorrective model.
 */
class PicaCorrectiveController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Updates an existing TbAuditPicaCorrective model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $isfile = Yii::$app->request->post('isfile');
        $user = Yii::$app->user->identity;
        $file_name='';
        
        $model = TbAuditPicaCorrective::findOne($id);
        
        if($isfile === 'false' && $_FILES['file']['name'] !=='' ){
            
            if(isset($_FILES)){
                    
                $file = FileController::UploadProgress($_FILES,$file_name,$model->number);
                    
                if($file->data['status']==='fail'){
                     
                    return $file;
                }
            }
        
            $model->progress_file = $file_name;
        }
        
        $model->latest_progress = Yii::$app->request->post('progress');
        $model->date_progress= date('Y-m-d h:i:s');
        $model->status= 'On Progress';

        if ($model->save()) {
            
            if($user->division_id === '37'){
        
                $correctives = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='{$model->number}' AND finding_id={$model->finding_id} AND (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik = '{$user['nik']}') AND order_number=$model->order_number  AND is_deleted=0 ")->all();
            
            }else{
            
                $correctives = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='{$model->number}' AND finding_id={$model->finding_id} AND ( (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik = '{$user['nik']}') OR division_id IN ($user->division_id)  AND order_number=$model->order_number )  AND is_deleted=0 ")->all();
            }
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','corrective'=>$model, 'correctives'=>$correctives];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
        
    }
        
     public function actionUpdate2()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $pica = Yii::$app->request->post('pica');
        $status = Yii::$app->request->post('status');

        foreach($pica as $index => $value){
            
            $model = TbAuditPicaCorrective::findOne(intval($pica['id']));
            $model->status = $status;
            $model->date_closed = $pica['date_progress'];
            if($model->save()){
                
                $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];
                 
            }else{

                $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
            }
            
        }
        
        
        return $response;
        
    }
    
    public function actionDeleteFile()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $user = Yii::$app->user->identity;
        $model = TbAuditPicaCorrective::findOne($id);
                        
        $file = FileController::Remove($model->finding_file);
        
        if($file->data['status']==='fail'){
                     
            return $file;
        }
        
        $model->progress_file ='';

        if ($model->save()) {
            
            if($user->division_id === '37'){
        
                $correctives = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='{$model->number}' AND finding_id={$model->finding_id} AND (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik = '{$user['nik']}') AND order_number=$model->order_number  AND is_deleted=0 ")->all();
            
            }else{
            
                $correctives = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='{$model->number}' AND finding_id={$model->finding_id} AND ( (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik = '{$user['nik']}') OR division_id IN ($user->division_id)  AND order_number=$model->order_number )  AND is_deleted=0 ")->all();
            }
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','corrective'=>$model, 'correctives'=>$correctives];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }
    
    public function actionCreateTemp()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = new TbAuditPicaCorrectiveTemp();        
        $model->auditor_nik = Yii::$app->user->identity->nik;
        $model->auditor_name = Yii::$app->user->identity->username; 
        $model->finding_id= Yii::$app->request->post('finding_id');             
        $model->industry_id = Yii::$app->request->post('industry_id');
        $model->industry_name = TbAuditIndustry::findOne($model->industry_id)->name;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->region_id = Yii::$app->request->post('region_id');
        $model->region_name = TbAuditRegion::findOne($model->region_id)->name;
        $model->business_unit_id = Yii::$app->request->post('business_unit_id');
        $model->business_unit_name = TbAuditBusinessUnit::findOne($model->business_unit_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;
        $model->pic_nik = Yii::$app->request->post('pic_nik');
        $pic = TbAuditUser::find()->where("nik='$model->pic_nik'")->one();
        $model->pic_name = $pic->username;
        $model->pic_position = $pic->position;
        $model->mandatory_id = Yii::$app->request->post('mandatory_id');
        $model->mandatory_name = TbAuditMandatory::findOne($model->mandatory_id)->name;
        $model->corrective_action = '""';

        if ($model->save()) {
            
            $model->id = Yii::$app->db->getLastInsertID();
            $correctives = TbAuditPicaCorrectiveTemp::find()->where(['finding_id' =>$model->finding_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','corrective'=>$model, 'correctives'=>$correctives];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionCreateTempOld()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = new TbAuditPicaCorrectiveTemp2();     
        $model->number = Yii::$app->request->post('number'); 
        $model->auditor_nik = Yii::$app->user->identity->nik;
        $model->auditor_name = Yii::$app->user->identity->username; 
        $model->finding_id= Yii::$app->request->post('finding_id');             
        $model->industry_id = Yii::$app->request->post('industry_id');
        $model->industry_name = TbAuditIndustry::findOne($model->industry_id)->name;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->region_id = Yii::$app->request->post('region_id');
        $model->region_name = TbAuditRegion::findOne($model->region_id)->name;
        $model->business_unit_id = Yii::$app->request->post('business_unit_id');
        $model->business_unit_name = TbAuditBusinessUnit::findOne($model->business_unit_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;
        $model->pic_nik = Yii::$app->request->post('pic_nik');
        $pic = TbAuditUser::find()->where("nik='$model->pic_nik'")->one();
        $model->pic_name = $pic->username;
        $model->pic_position = $pic->position;
        $model->mandatory_id = Yii::$app->request->post('mandatory_id');
        $model->mandatory_name = TbAuditMandatory::findOne($model->mandatory_id)->name;
        $model->corrective_action = '""';

        if ($model->save()) {
            
            $model->id = Yii::$app->db->getLastInsertID();
            $correctives = TbAuditPicaCorrectiveTemp2::find()->where(['finding_id' =>$model->finding_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','corrective'=>$model, 'correctives'=>$correctives];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionCreate()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = new TbAuditPicaCorrective();    
        $model->number = Yii::$app->request->post('number');    
        $model->auditor_nik = Yii::$app->user->identity->nik;
        $model->auditor_name = Yii::$app->user->identity->username; 
        $model->auditor_position = Yii::$app->user->identity->position; 
        $model->finding_id= Yii::$app->request->post('finding_id');             
        $model->industry_id = Yii::$app->request->post('industry_id');
        $model->industry_name = TbAuditIndustry::findOne($model->industry_id)->name;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->region_id = Yii::$app->request->post('region_id');
        $model->region_name = TbAuditRegion::findOne($model->region_id)->name;
        $model->business_unit_id = Yii::$app->request->post('business_unit_id');
        $model->business_unit_name = TbAuditBusinessUnit::findOne($model->business_unit_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;
        $model->pic_nik = Yii::$app->request->post('pic_nik');
        $pic = TbAuditUser::find()->where("nik='$model->pic_nik'")->one();
        $model->pic_name = $pic->username;
        $model->pic_position = $pic->position;
        $model->mandatory_id = Yii::$app->request->post('mandatory_id');
        $model->mandatory_name = TbAuditMandatory::findOne($model->mandatory_id)->name;
        $model->corrective_action = '';
        $model->status='Open';
        $model->lead_time_status='On Schedule';
        $model->order_number =0;
        $model->next_nik = Yii::$app->user->identity->nik;
        $model->next_name = Yii::$app->user->identity->username; 
        $model->next_position = Yii::$app->user->identity->position; 

        if ($model->save()) {
            
            $correctives = TbAuditPicaCorrective::find()->where(['finding_id' =>$model->finding_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','corrective'=>$model, 'correctives'=>$correctives];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }
    
    public function actionUpdateTemp()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        $model = TbAuditPicaCorrectiveTemp::findOne($id);        
        $model->auditor_nik = Yii::$app->user->identity->nik;
        $model->auditor_name = Yii::$app->user->identity->username;      
        $model->pic_nik = Yii::$app->request->post('pic_nik');
        $pic = TbAuditUser::find()->where("nik='$model->pic_nik'")->one();
        $model->pic_name = $pic->username;
        $model->pic_position = $pic->position;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;
        $model->mandatory_id = Yii::$app->request->post('mandatory_id');
        $model->mandatory_name = TbAuditMandatory::findOne($model->mandatory_id)->name;
        $model->corrective_action = Yii::$app->request->post('corrective_action')==='""' ? $model->corrective_action : Yii::$app->request->post('corrective_action') ;

        if ($model->save()) {
            
            $correctives = TbAuditPicaCorrectiveTemp::find()->where(['finding_id' =>$model->finding_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','correctives'=>$correctives];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionUpdateCorrectiveAction()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        $model = TbAuditPicaCorrective::findOne($id);        
        $model->corrective_action = Yii::$app->request->post('corrective_action')==='""' ? $model->corrective_action : Yii::$app->request->post('corrective_action') ;

        if ($model->save()) {
            
            $correctives = TbAuditPicaCorrective::find()->where(['finding_id' =>$model->finding_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','correctives'=>$correctives];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionUpdateSwitch()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        $model = TbAuditPicaCorrective::findOne($id);        
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;
        $model->mandatory_id = Yii::$app->request->post('mandatory_id');
        $model->mandatory_name = TbAuditMandatory::findOne($model->mandatory_id)->name;
        $model->pic_nik = Yii::$app->request->post('pic_nik');
        $model->pic_name = TbAuditUser::find()->where("nik='{$model->pic_nik}'")->One()->username;
        $model->pic_position = TbAuditUser::find()->where("nik='{$model->pic_nik}'")->One()->position;
        $model->status = Yii::$app->request->post('status');

        if ($model->save()) {
            
            $correctives = TbAuditPicaCorrective::find()->where(['finding_id' =>$model->finding_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','correctives'=>$correctives];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionUpdateTempOld()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        $model = TbAuditPicaCorrectiveTemp2::findOne($id);        
        $model->auditor_nik = Yii::$app->user->identity->nik;
        $model->auditor_name = Yii::$app->user->identity->username;      
        $model->pic_nik = Yii::$app->request->post('pic_nik');
        $pic = TbAuditUser::find()->where("nik='$model->pic_nik'")->one();
        $model->pic_name = $pic->username;
        $model->pic_position = $pic->position;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;
        $model->mandatory_id = Yii::$app->request->post('mandatory_id');
        $model->mandatory_name = TbAuditMandatory::findOne($model->mandatory_id)->name;
        $model->corrective_action = Yii::$app->request->post('corrective_action')==='""' ? $model->corrective_action : Yii::$app->request->post('corrective_action') ;

        if ($model->save()) {
            
            $correctives = TbAuditPicaCorrectiveTemp2::find()->where(['finding_id' =>$model->finding_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','correctives'=>$correctives];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionUpdateEdit()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        $model = TbAuditPicaCorrective::findOne($id);        
        $model->pic_nik = Yii::$app->request->post('pic_nik');
        $pic = TbAuditUser::find()->where("nik='$model->pic_nik'")->one();
        $model->pic_name = $pic->username;
        $model->pic_position = $pic->position;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;
        $model->mandatory_id = Yii::$app->request->post('mandatory_id');
        $model->mandatory_name = TbAuditMandatory::findOne($model->mandatory_id)->name;
        $model->corrective_action = Yii::$app->request->post('corrective_action')==='""' ? $model->corrective_action : Yii::$app->request->post('corrective_action') ;

        if ($model->save()) {
            
            $correctives = TbAuditPicaCorrective::find()->where(['finding_id' =>$model->finding_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','correctives'=>$correctives];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionUpdateComment()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        $model = TbAuditPicaCorrective::findOne($id);        
        $model->corrective_action = Yii::$app->request->post('corrective_action')==='""' ? $model->corrective_action : Yii::$app->request->post('corrective_action') ;

        if ($model->save()) {
            
            $correctives = TbAuditPicaCorrective::find()->where(['finding_id' =>$model->finding_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','correctives'=>$correctives];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionEdit()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        $model = TbAuditPicaCorrective::findOne($id);        
        $model->auditor_nik = Yii::$app->user->identity->nik;
        $model->auditor_name = Yii::$app->user->identity->username;      
        $model->pic_nik = Yii::$app->request->post('pic_nik');
        $pic = TbAuditUser::find()->where("nik='$model->pic_nik'")->one();
        $model->pic_name = $pic->username;
        $model->pic_position = $pic->position;
        $model->location_id = Yii::$app->request->post('location_id');
        $model->location_name = TbAuditLocation::findOne($model->location_id)->name;
        $model->division_id = Yii::$app->request->post('division_id');
        $model->division_name = TbAuditDivision::findOne($model->division_id)->name;
        $model->mandatory_id = Yii::$app->request->post('mandatory_id');
        $model->mandatory_name = TbAuditMandatory::findOne($model->mandatory_id)->name;
        $model->corrective_action = Yii::$app->request->post('corrective_action')==='""' ? $model->corrective_action : Yii::$app->request->post('corrective_action') ;

        if ($model->save()) {
            
            $correctives = TbAuditPicaCorrective::find()->where(['finding_id' =>$model->finding_id])->all();
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success','correctives'=>$correctives];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }
    
    public function actionDeleteTemp()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        $model = TbAuditPicaCorrectiveTemp::findOne($id);        
        
        if ($model->delete()) {
            
            $response->data = ['message' => 'Data Berhasil Dihapus','status' => 'success'];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionDeleteTempOld()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        $model = TbAuditPicaCorrectiveTemp2::findOne($id);        
        
        if ($model->delete()) {
            
            $response->data = ['message' => 'Data Berhasil Dihapus','status' => 'success'];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    public function actionDelete()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        $model = TbAuditPicaCorrective::findOne($id);        
        
        if ($model->delete()) {
            
            $response->data = ['message' => 'Data Berhasil Dihapus','status' => 'success'];  
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;
    }

    /**
     * Finds the TbAuditPicaCorrective model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TbAuditPicaCorrective the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbAuditPicaCorrective::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGetAll()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        
        $pica_number= Yii::$app->request->post('pica_number');           
        $finding_id = Yii::$app->request->post('finding_id');
        $order_number = Yii::$app->request->post('order_number');
        $user =  Yii::$app->user->identity;
        
        if($user->division_id === '37'){
        
            $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='$pica_number' AND finding_id={$finding_id} AND (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik = '{$user['nik']}') AND order_number=$order_number  AND is_deleted=0 ")->all();
            
        }else{
            
            $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='$pica_number' AND finding_id={$finding_id} AND ( (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik = '{$user['nik']}') OR division_id IN ($user->division_id)  AND order_number=$order_number )  AND is_deleted=0 ")->all();
        }
        
        if($corrective)
        {
            
            $response->data = ['corrective' =>$corrective,'pica_number'=>str_replace('/','_',$pica_number)];
            
        }else{
            
            $response->data = ['corrective' =>0];
            
        }
        
        return $response; 
        
    }

     public function actionGetAll2()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        
        $pica_number= Yii::$app->request->post('pica_number');           
        $finding_id = Yii::$app->request->post('finding_id');
        $order_number = Yii::$app->request->post('order_number');
        $user =  Yii::$app->user->identity;
        
        if($user->division_id === '37'){
        
            $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='$pica_number' AND finding_id={$finding_id} AND (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik = '{$user['nik']}') AND order_number=$order_number AND is_deleted=0  ")->all();
            
        }else{
            
            $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,date_due,comment  FROM tb_audit_pica_corrective WHERE number='$pica_number' AND finding_id={$finding_id} AND ( (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik = '{$user['nik']}') OR division_id IN ($user->division_id)  AND order_number=$order_number AND status <>'closed' )  AND is_deleted=0 ")->all();
        }
        
        if($corrective)
        {
            
            $response->data = ['corrective' =>$corrective,'pica_number'=>str_replace('/','_',$pica_number)];
            
        }else{
            
            $response->data = ['corrective' =>0];
            
        }
        
        return $response; 
        
    }

    public function actionGetAllEdit()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        
        $finding_id= Yii::$app->request->post('finding_id');     
        $pica_number= Yii::$app->request->post('pica_number');     

        
        $corrective = TbAuditPicaCorrective::find()->where(['finding_id' => $finding_id,'number'=>$pica_number])->all();
        $finding = TbAuditPicaFinding::findOne($finding_id);
        
        if($corrective)
        {
            
            $response->data = ['corrective' =>$corrective,'finding'=>$finding];
            
        }else{
            
            $response->data = ['corrective' =>0,'finding'=>$finding];
            
        }
        
        return $response;  
        
    }
    
    public function actionGetAllTemp()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        
        $finding_id= Yii::$app->request->post('finding_id');           
        
        $corrective = TbAuditPicaCorrectiveTemp::find()->where(['finding_id' => $finding_id])->all();
        $finding = TbAuditPicaFindingTemp::findOne($finding_id);
        
        if($corrective)
        {
            
            $response->data = ['corrective' =>$corrective,'finding_temp'=>$finding];
            
        }else{
            
            $response->data = ['corrective' =>0,'finding_temp'=>$finding];
            
        }
        
        return $response; 
        
    }

    public function actionGetAllSwitch()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        
        $finding_id= Yii::$app->request->post('finding_id');           
        
        $corrective = TbAuditPicaCorrective::find()->where(['finding_id' => $finding_id])->all();
        $finding = TbAuditPicaFinding::findOne($finding_id);
        
        if($corrective)
        {
            
            $response->data = ['corrective' =>$corrective,'finding'=>$finding];
            
        }else{
            
            $response->data = ['corrective' =>0,'finding'=>$finding];
            
        }
        
        return $response; 
        
    }

    public function actionGetAllTempOld()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        
        $finding_id= Yii::$app->request->post('finding_id');           
        
        $corrective = TbAuditPicaCorrectiveTemp2::find()->where(['finding_id' => $finding_id])->all();
        $finding = TbAuditPicaFindingTemp2::findOne($finding_id);
        
        if($corrective)
        {
            
            $response->data = ['corrective' =>$corrective,'finding_temp'=>$finding];
            
        }else{
            
            $response->data = ['corrective' =>0,'finding_temp'=>$finding];
            
        }
        
        return $response; 
        
    }
}
