<?php

namespace app\controllers;

use Yii;
use app\models\TbAuditLocation;
use app\models\TbAuditIndustry;
use app\models\TbAuditRegion;
use app\models\TbAuditBusinessUnit;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;
use yii\web\Response;


/**
 * RegionController implements the CRUD actions for TbAuditRegion model.
 */
class RegionController extends Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];  
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }
    
     /**
     * Creates a new TbAuditRegion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    { 
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = new TbAuditRegion(); 
        $model->name = Yii::$app->request->post('name');
        $model->industry_id = Yii::$app->request->post('industry_id');
        $model->location_id = Yii::$app->request->post('location_id');
        $model->user_name = Yii::$app->user->identity->username;
        $model->user_nik = (string) Yii::$app->user->identity->nik;
        
        if ($model->save()) {
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;

    }

    /**
     * Updates an existing TbAuditRegion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');
        $name = Yii::$app->request->post('name');
        $industry_id = Yii::$app->request->post('industry_id');
        $industry_name = TbAuditIndustry::findOne($industry_id)->name;
        $location_id = Yii::$app->request->post('location_id');
        $location_name = TbAuditLocation::findOne($location_id)->name;
        $user = Yii::$app->user->identity;
        $date_updated= date('Y-m-d h:i:s');

        $transaction = Yii::$app->db->beginTransaction();        
                       
        try {
            
            // update table region
            Yii::$app->db->createCommand("UPDATE tb_audit_region SET industry_id={$industry_id},location_id={$location_id},name='{$name}',user_nik='{$user->nik}',user_name='{$user->username}',date_updated='{$date_updated}' WHERE id={$id}")->execute();

            // update table workflow trans
            Yii::$app->db->createCommand("UPDATE tb_audit_workflow_trans SET industry_id={$industry_id},industry_name='{$industry_name}',location_id={$location_id},location_name='{$location_name}',region_name='{$name}' WHERE region_id={$id}")->execute();

             // update table pica header
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_header SET industry_id={$industry_id},industry_name='{$industry_name}',region_name='{$name}' WHERE region_id={$id}")->execute();

            // update table pica progress
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_progress SET industry_id={$industry_id},industry_name='{$industry_name}',region_name='{$name}' WHERE region_id={$id}")->execute();

            // update table pica finding
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET industry_id={$industry_id},industry_name='{$industry_name}', region_name='{$name}' WHERE region_id={$id}")->execute();

            // update table pica finding temp
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding_temp SET industry_id={$industry_id},industry_name='{$industry_name}', region_name='{$name}' WHERE region_id={$id}")->execute();

            // update table pica corrective
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET  industry_id={$industry_id},industry_name='{$industry_name}', region_name='{$name}' WHERE region_id={$id}")->execute();

            // update table pica corrective temp
            Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective_temp SET industry_id={$industry_id},industry_name='{$industry_name}', region_name='{$name}' WHERE region_id={$id}")->execute();

            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Diperbarui','status' => 'success'];             
            
        } 
        catch (Exception $e) {
                     
            $transaction->rollBack();
            
            $response->data = ['message' => 'Data Gagal Diperbarui','status' => 'fail'];             
                         
        }   
        
        return $response;    
    
    }

    /**
     * Deletes an existing TbAuditRegion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
               
        // Cek apakah berelasi dengan table Business Unit
        $isRelated = TbAuditBusinessUnit::find()->where("region_id = $id")->all();
        
        if($isRelated){
            
          $response->data = ['message' => 'Data Gagal Dihapus karena berelasi dengan Business Unit','status' => 'fail'];
          return $response;
          
        }
        
        if ($model->delete()) {
            
            $response->data = ['message' => 'Data Berhasil Dihapus','status' => 'success'];             
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Dihapus','status' => 'fail'];             
            
        }      
        
        return $response;      
    }
    /**
     * Finds the TbAuditRegion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TbAuditRegion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbAuditRegion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
