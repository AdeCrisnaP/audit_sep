<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\BaseJson;
use app\models\TbAuditIndustry;
use app\models\TbAuditStatus;
use app\models\TbAuditLocation;
use app\models\TbAuditRegion;
use app\models\TbAuditBusinessUnit;
use app\models\TbAuditLeadTimeStatus;
use app\models\TbAuditDivision;  
use app\models\TbAuditCategory;  
use app\models\TbAuditRating;  
use app\models\TbAuditUser; 
use app\models\TbAuditPicaFinding; 
use app\models\TbAuditPicaCorrective;
use app\models\TbAuditPicaProgress;
use app\models\TbAuditPicaHeader;
use app\models\TbAuditRole; 
use app\models\TbAuditMandatory;


class PdfController extends Controller {



    public function actionPica(){

        Yii::$app->response->format = 'pdf';
        $this->layout = '//printPdf';

        $number = Yii::$app->request->get('number');  

        $finding = array();

        $pica = TbAuditPicaHeader::find()->where(['number' => $number])->one();
        $find = TbAuditPicaFinding::find()->where(['number' => $number])->all();
        $user1 = TbAuditUser::find()->where("position LIKE '%Internal Audit Division Head%' ")->one();
        $user2 = TbAuditUser::find()->where("role_name = 'COO' AND industry_id={$pica['industry_id']}")->one();

        foreach($find as $index => $value){

            foreach($value as $index2 => $value2){

                $finding[$index]['corrective'] = TbAuditPicaCorrective::find()->where(['finding_id' =>$value['id']])->all();
                $finding[$index]['no']=$index+1;
                $finding[$index][$index2] = $value2;
            
            }

        }
        

        // Rotate the page
        // Yii::$container->set(Yii::$app->response->formatters['pdf']['class'], [
        //     'format' => [216, 356], // Legal page size in mm
        //     'orientation' => 'Landscape', // This value will be used when 'format' is an array only. Skipped when 'format' is empty or is a string
        //     'beforeRender' => function($mpdf, $data) {},
        // ]);

        return $this->render('pica',['pica'=>$pica,'finding'=>$finding,'user1'=>$user1,'user2'=>$user2]);
    }

}
