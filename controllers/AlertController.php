<?php

namespace app\controllers;

use Yii;
use app\models\TbAuditAlert;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;
use yii\web\Response;
use app\models\TbAuditUser; 
use app\controllers\EmailController;

/**
 * LocationController implements the CRUD actions for TbAuditLocation model.
 */
class AlertController extends Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];  
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }   

    /**
     * Updates an existing TbAuditLocation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        $id = Yii::$app->request->post('id');
        $alert1 = Yii::$app->request->post('alert1');
        $alert2 = Yii::$app->request->post('alert2');
        $user = Yii::$app->user->identity;
        $date_updated= date('Y-m-d h:i:s');

        $transaction = Yii::$app->db->beginTransaction();        
                       
        try {
            
            // update table location
            Yii::$app->db->createCommand("UPDATE tb_audit_alert SET alert1=$alert1,alert2=$alert2,user_nik='{$user->nik}',user_name='{$user->username}' WHERE id={$id}")->execute();

            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Diperbarui','status' => 'success'];             
            
        } 
        catch (Exception $e) {
                     
            $transaction->rollBack();
            
            $response->data = ['message' => 'Data Gagal Diperbarui','status' => 'fail'];             
                         
        }   
        
        return $response;    
    }

    public function actionStart(){

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        $this->setOverdue();

        $alert = TbAuditAlert::find()->where('is_deleted =0')->one();
        
        if( (date('t')-date('d')) === $alert['alert1'] || (date('t')-date('d')) === $alert['alert2']){

            $this->sendAlert();

        }

        $response->data = ['message' => 'Alert Berhasil Dieksekusi','status' => 'Success'];             
            
        return $response;     

    }

    public function actionManual(){

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        $this->setOverdue();

        $alert = TbAuditAlert::find()->where('is_deleted =0')->one();
       
        $this->sendAlert();
       
        $response->data = ['message' => 'Alert Berhasil Dieksekusi','status' => 'Success'];             
            
        return $response;     

    }

    protected function setOverDue(){

        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        //Update Over Due Finding
        Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET lead_time_status='Over Due'  WHERE status <> 'Closed' AND DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) > 0 ")->execute(); 

        // Update Over Due Corrective
        Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET lead_time_status='Over Due' WHERE status <> 'Closed' AND DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) > 0")->execute(); 

        $response->data = ['message' => 'Data Berhasil Diubah','status' => 'Success'];             
            
        return $response;                        
    }

    protected function sendAlert(){

        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        // Send to PIC
        $pic = TbAuditUser::find()->where('role_id=4')->all(); 
           
        foreach($pic as $index => $value){

            $data = array();
                
            $data = Yii::$app->db->createCommand("SELECT number, COUNT(*) AS jumlah FROM (SELECT number,finding_id FROM tb_audit_pica_corrective WHERE next_nik='{$value['nik']}' AND status <>'Closed' GROUP BY number,finding_id) A GROUP BY number")->queryAll(); 
                
            //Jika ada data, send email
            if($data){

                EmailController::sendAlert($data,$value,'alert-html');
            }
        }

        // Send to Div Head / Dir Ops
        $head = TbAuditUser::find()->where('role_id=5')->all(); 
           
        foreach($head as $index2 => $value2){
                
            $data2 = array();

            if(intval($value2['location_id']) === 1){
                    
                $data2 = Yii::$app->db->createCommand("SELECT number, COUNT(*) AS jumlah FROM (SELECT number,finding_id FROM tb_audit_pica_corrective WHERE (order_number BETWEEN 3 AND 4) AND status <>'Closed' AND industry_id={$value2['industry_id']} AND division_id IN ({$value2['division_id']}) GROUP BY number,finding_id) A GROUP BY number")->queryAll(); 
                
            }else{
                    
                $data2 = Yii::$app->db->createCommand("SELECT number, COUNT(*) AS jumlah FROM (SELECT number,finding_id FROM tb_audit_pica_corrective WHERE (order_number BETWEEN 3 AND 4) AND status <>'Closed' AND industry_id={$value2['industry_id']} AND region_id={$value2['region_id']} AND business_unit_id={$value2['business_unit_id']}  AND division_id IN ({$value2['division_id']}) GROUP BY number,finding_id) A GROUP BY number")->queryAll(); 

            }

            //Jika ada data, send email
            if($data2){

                EmailController::sendAlert($data2,$value2,'alert-html');
                    
            }

            $date = date('Y-m-d h:i:s');

            Yii::$app->db->createCommand("UPDATE tb_audit_alert SET date_updated='{$date}' ")->execute();             

        }

        $response->data = ['message' => 'Data Berhasil Diubah','status' => 'Success'];             
        return $response;     
    }

    
    /**
     * Finds the TbAuditLocation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TbAuditLocation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbAuditLocation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
