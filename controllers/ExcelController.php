<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\BaseJson;
use app\models\TbAuditIndustry;
use app\models\TbAuditStatus;
use app\models\TbAuditLocation;
use app\models\TbAuditRegion;
use app\models\TbAuditBusinessUnit;
use app\models\TbAuditLeadTimeStatus;
use app\models\TbAuditDivision;  
use app\models\TbAuditCategory;  
use app\models\TbAuditRating;  
use app\models\TbAuditUser; 
use app\models\TbAuditPicaFinding; 
use app\models\TbAuditPicaCorrective;
use app\models\TbAuditPicaProgress;
use app\models\TbAuditPicaHeader;
use app\models\TbAuditRole; 
use app\models\TbAuditMandatory;


class ExcelController extends Controller {

    public function actionPica(){

        $this->layout = '//printExcel';

        $industry = Yii::$app->request->get('industry') === 'undefined' || Yii::$app->request->get('industry') === 'null'  ?'':Yii::$app->request->get('industry');
        $region = Yii::$app->request->get('region') === 'undefined' || Yii::$app->request->get('region') === 'null' ?'':Yii::$app->request->get('region');
        $business_unit = Yii::$app->request->get('business_unit') === 'undefined' || Yii::$app->request->get('business_unit') === 'null' ?'':Yii::$app->request->get('business_unit');

        $user =  Yii::$app->user->identity;
        $query = '';

        if($industry){

            $query .=" AND industry_id=$industry ";

        }

        if($region){

            $query .=" AND region_id=$region ";

        }

        if($business_unit){

            $query .=" AND business_unit_id=$business_unit ";

        }
        
        $pica_corrective = array();

        if($query){
        
            $pica = TbAuditPicaFinding::findBySql("SELECT TOP 100 id,number, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days FROM tb_audit_pica_finding WHERE (status='On Progress' OR status='Open')  AND is_deleted=0 $query ORDER BY number DESC ")->all();
        
        }else{

            if($user->division_id !== '37'){
                
                if($user->location_id===1){

                    if($user->role_name ==='COO'){

                        $pica = TbAuditPicaFinding::findBySql("SELECT TOP 100 id,number, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days FROM tb_audit_pica_finding WHERE (status='On Progress' OR status='Open')  AND  industry_id =$user->industry_id  AND is_deleted=0 ")->all();

                    }else{
                
                        $pica = TbAuditPicaFinding::findBySql("SELECT TOP 100 id,number, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days FROM tb_audit_pica_finding WHERE (status='On Progress' OR status='Open')  AND  industry_id =$user->industry_id AND  division_id IN ($user->division_id)  AND is_deleted=0 ")->all();
                    }

                }else{
                    
                    $pica = TbAuditPicaFinding::findBySql("SELECT TOP 100 id,number, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment ,finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days FROM tb_audit_pica_finding WHERE (status='On Progress' OR status='Open') AND industry_id = $user->industry_id  AND region_id IN ($user->region_id) AND business_unit_id=$user->business_unit_id AND division_id IN ($user->division_id)  AND is_deleted=0 ")->all();
                }

            }else{

                $pica = TbAuditPicaFinding::findBySql("SELECT TOP 100 id,number, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days FROM tb_audit_pica_finding WHERE (status='On Progress' OR status='Open')  AND is_deleted=0  ORDER BY number DESC ")->all();
               
            }
        }  

        $index_array = 0;
            
        foreach($pica as $index => $value){
                
            foreach($value as $index2 => $value2){
                    
                $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,COALESCE (date_extension,date_due) AS date_due  FROM tb_audit_pica_corrective WHERE finding_id={$value['id']}  AND is_deleted=0 ")->all();
                    
                if(count($corrective) > 0){
                        
                    $pica_corrective[$index_array]['corrective'] = $corrective;
                    $pica_corrective[$index_array]['no']=$index+1;
                    $pica_corrective[$index_array][$index2] =   $value2;
                        
                }
            }
                
            if(count($pica_corrective) > 0){$index_array++;}
                
        }

        return $this->render('pica',['finding'=>$pica_corrective]);
    }

    public function actionFinding(){

        $this->layout = '//printExcel';

        $industry = Yii::$app->request->get('industry') === 'undefined' || Yii::$app->request->get('industry') === 'null' ?'':Yii::$app->request->get('industry');
        $region = Yii::$app->request->get('region') === 'undefined' || Yii::$app->request->get('industry') === 'null' ?'':Yii::$app->request->get('region');
        $business_unit = Yii::$app->request->get('business_unit') === 'undefined' || Yii::$app->request->get('industry') === 'null' ?'':Yii::$app->request->get('business_unit');
        $number = Yii::$app->request->get('number') === 'undefined' || Yii::$app->request->get('industry') === 'null' ?'':Yii::$app->request->get('number');
        $leader = Yii::$app->request->get('leader') === 'undefined' || Yii::$app->request->get('leader') === 'null' ?'':Yii::$app->request->get('leader');    
        $location = Yii::$app->request->get('location') === 'undefined'  || Yii::$app->request->get('location') === 'null' ?'':Yii::$app->request->get('location');
        $division = Yii::$app->request->get('division') === 'undefined' || Yii::$app->request->get('division') === 'null' ?'':Yii::$app->request->get('division');
        $category = Yii::$app->request->get('category') === 'undefined' || Yii::$app->request->get('category') === 'null' ?'':Yii::$app->request->get('category');
        $rating = Yii::$app->request->get('rating') === 'undefined' || Yii::$app->request->get('rating') === 'null' ?'':Yii::$app->request->get('rating');

        $user =  Yii::$app->user->identity;
        $query = '';
        
        if($number){

            $query .=" AND number='$number' ";

        }

        if($rating){
            
            $query .=" AND rating_id=$rating "; 

        }
        
        if($category){
            
            $query .=" AND category_id=$category "; 

        }
        
        if($division){

            $query .=" AND division_id=$division ";                

        }
        
        if($business_unit){
             
            $query .=" AND business_unit_id=$business_unit ";                
            
        }
        
        if($region){

            $query .=" AND region_id=$region ";

        }
        
        if($industry){

            $query .=" AND industry_id=$industry ";

        }
        
        if($leader){

            $query .=" AND team_leader_nik='$leader'  ";

        }

        $pica_corrective = array();

        if($query){
        
            $pica = TbAuditPicaFinding::findBySql("SELECT id,number,team_leader_name, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment , REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days  FROM tb_audit_pica_finding WHERE is_deleted=0 $query  ORDER BY number DESC")->all();
            
        }else{

            if($user->division_id !== '37'){
                
                $pica = TbAuditPicaFinding::findBySql("SELECT id,number,team_leader_name, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment , REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days  FROM tb_audit_pica_finding WHERE division_id IN ($user->division_id)  AND is_deleted=0 ")->all();
                
            }else{

               $pica = TbAuditPicaFinding::findBySql("SELECT id,number,team_leader_name, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment , REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days  FROM tb_audit_pica_finding WHERE is_deleted=0  ORDER BY number DESC")->all();
                
            }

        }  

        $index_array = 0;
            
        foreach($pica as $index => $value){
                
            foreach($value as $index2 => $value2){
                    
                $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,CONVERT(VARCHAR(10),date_progress,110) AS date_progress,COALESCE (date_extension,date_due) AS date_due  FROM tb_audit_pica_corrective WHERE finding_id={$value['id']}  AND is_deleted=0 ")->all();
                    
                if(count($corrective) > 0){
                        
                    $pica_corrective[$index_array]['corrective'] = $corrective;
                    $pica_corrective[$index_array]['no']=$index+1;
                    $pica_corrective[$index_array][$index2] =   $value2;
                        
                }
            }
                
            if(count($pica_corrective) > 0){$index_array++;}
                
        }

        return $this->render('pica-finding',['finding'=>$pica_corrective]);
    }

    public function actionProgress(){

        $this->layout = '//printExcel';

        $industry = Yii::$app->request->get('industry') === 'undefined' || Yii::$app->request->get('industry') === 'null' ?'':Yii::$app->request->get('industry');
        $region = Yii::$app->request->get('region') === 'undefined' || Yii::$app->request->get('region') === 'null' ?'':Yii::$app->request->get('region');
        $business_unit = Yii::$app->request->get('business_unit') === 'undefined' || Yii::$app->request->get('business_unit') === 'null' ?'':Yii::$app->request->get('business_unit');
        $number = Yii::$app->request->get('number') === 'undefined' || Yii::$app->request->get('number') === 'null' ?'':Yii::$app->request->get('number');
        $status = Yii::$app->request->get('status') === 'undefined' || Yii::$app->request->get('status') === 'null' ?'':Yii::$app->request->get('status');    
        $from = Yii::$app->request->get('from') === 'undefined' || Yii::$app->request->get('from') === 'null' ?'':'01-'.Yii::$app->request->get('from');
        $to = Yii::$app->request->get('to') === 'undefined' || Yii::$app->request->get('to') === 'null' ?'':'31-'.Yii::$app->request->get('to');

        $user =  Yii::$app->user->identity;
        $query = '';

        
        if($number){

            $query .=" AND number='$number' ";

        }

        if($from && $to){

            $from_date = date('Y-m-d',strtotime($from));
            $to_date = date('Y-m-d',strtotime($to));

            $query .=" AND date_created BETWEEN '$from_date' AND '$to_date' ";                

        }elseif($from){
            
             $from_date = date('Y-m-d',strtotime($from));
           
             $query .=" AND date_created >= '$from_date' "; 

        }
        
        if($status){
             
            $query .=" AND status='$status' ";                
            
        }
        
        if($business_unit){

            $query .=" AND business_unit_id=$business_unit ";

        }
        
        if($region){

            $query .=" AND region_id=$region ";

        }
        
        if($industry){

            $query .=" AND industry_id=$industry ";

        }

        $pica_corrective = array();

        $pica = TbAuditPicaFinding::findBySql("SELECT id,number, REPLACE (finding,'<br>','\n') AS finding ,  REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days  FROM tb_audit_pica_finding WHERE is_deleted=0 $query ORDER BY number DESC ")->all();
                
        $index_array = 0;
            
        foreach($pica as $index => $value){
                
            $corrective = array();

            foreach($value as $index2 => $value2){
                    
                $corrective = TbAuditPicaProgress::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress FROM tb_audit_pica_progress WHERE finding_id={$value['id']}  AND is_deleted=0  ORDER BY corrective_id ASC")->all();
                    
                if(count($corrective) > 0){
                        
                    $pica_corrective[$index_array]['corrective'] = $corrective;
                    $pica_corrective[$index_array]['no']=$index_array+1;
                    $pica_corrective[$index_array][$index2] =   $value2;
                        
                }
            }
                
            if(count($corrective) > 0){$index_array++;}
                
        }

        return $this->render('pica-progress',['finding'=>$pica_corrective]);
    }

    
}
