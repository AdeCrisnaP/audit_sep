<?php

namespace app\controllers;

use Yii;
use app\models\TbAuditWorkflow;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;
use yii\web\Response;
use app\models\TbAuditIndustry;
use app\models\TbAuditLocation;
use app\models\TbAuditRegion;
use app\models\TbAuditBusinessUnit;
use app\models\TbAuditDivision;
use app\models\TbAuditWorkflowTrans;


/**
 * WorkFlowController implements the CRUD actions for TbAuditWorkflow model.
 */
class WorkFlowController extends Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];  
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }
    
     /**
     * Creates a new TbAuditWorkflow model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    { 
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        
        $data = Yii::$app->request->post('data');
        $nik = Yii::$app->user->identity->nik;
        $username = Yii::$app->user->identity->username;
                          
        $transaction = Yii::$app->db->beginTransaction();
                        
        try { 
            
            $isExisted = TbAuditWorkflow::find()->where("business_unit_id={$data[0]['business_unit_id']} AND division_id={$data[0]['division_id']}")->all();

            foreach($data as $index => $value){ 
                
                $user_nik = $value['user_nik'];
                $user_name = $value['user_name'];
                $user_position = $value['user_position'];
                
                if(!$isExisted){
                    
                    Yii::$app->db->createCommand("INSERT INTO tb_audit_workflow (order_number,actor_nik,actor_name,actor_position,approval_name,approval_status,industry_id,location_id,region_id,business_unit_id,division_id,user_nik,user_name) VALUES ($index,'$user_nik','$user_name','$user_position','{$value['approval_name']}','{$value['approval_status']}',
                                            {$value['industry_id']},{$value['location_id']},{$value['region_id']},{$value['business_unit_id']},{$value['division_id']},'$nik','$username')")->execute(); 
                
                                            
                }else{
                    
                    Yii::$app->db->createCommand("UPDATE tb_audit_workflow SET actor_nik='$user_nik',actor_name = '$user_name',actor_position = '$user_position' WHERE industry_id={$value['industry_id']} AND location_id={$value['location_id']} AND region_id={$value['region_id']} AND business_unit_id={$value['business_unit_id']} AND division_id={$value['division_id']} AND order_number=$index ")->execute(); 
                    
                    if($value['location_name'] === 'Head Office'){
                        
                        Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET next_nik='$user_nik',next_name = '$user_name',next_position = '$user_position' WHERE industry_id={$value['industry_id']} AND division_id={$value['division_id']} AND order_number=$index ")->execute(); 

                        Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET next_nik='$user_nik' WHERE industry_id={$value['industry_id']} AND division_id={$value['division_id']} AND order_number=$index ")->execute();             
                        
                    }else{
                        
                        Yii::$app->db->createCommand("UPDATE tb_audit_pica_corrective SET next_nik='$user_nik',next_name = '$user_name',next_position = '$user_position' WHERE industry_id={$value['industry_id']} AND location_id={$value['location_id']} AND region_id={$value['region_id']} AND business_unit_id={$value['business_unit_id']} AND division_id={$value['division_id']} AND order_number=$index ")->execute(); 

                        Yii::$app->db->createCommand("UPDATE tb_audit_pica_finding SET next_nik='$user_nik' WHERE industry_id={$value['industry_id']} AND location_id={$value['location_id']} AND region_id={$value['region_id']} AND business_unit_id={$value['business_unit_id']} AND division_id={$value['division_id']} AND order_number=$index ")->execute(); 
                        
                    }
                }
            
            }
            
            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
        } 
        catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];
                         
        }
        
        return $response;

    }

    /**
     * Finds the TbAuditWorkflow model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TbAuditWorkflow the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbAuditWorkflow::findOne($id)) !== null) {
            return $model;
        } else {
            return false;
        }
    }
    
    public function actionGetAll()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        $division_id = Yii::$app->request->post('division_id');  
        $business_unit_id = Yii::$app->request->post('business_unit_id');  
        
        $workflow = TbAuditWorkflow::find()->where(['division_id' => $division_id,'business_unit_id' => $business_unit_id])->orderBy('order_number ASC')->all();
        
        if($workflow)
        {
            
            $response->data = ['workflow' =>$workflow];
            
        }else{
            
            $response->data = ['workflow' =>0];
            
        }
        
        return $response; 
        
    }

    public function actionView()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $user =  Yii::$app->user->identity;
        $number = Yii::$app->request->post('number');  
        $query = '';
        $query2 = '';

		//TODO hardcode division id
		if($user->division_id !== '5'){
        //if($user->division_id !== '37'){
                        
            $query = " AND division_id IN ($user->division_id)";
                        
            $query2 = " AND order_number >=4 ";
        }

        $workflow = TbAuditWorkflowTrans::findBySql("SELECT DISTINCT approval_status,from_user_name,REPLACE (comment,'<br>','\n') AS comment,to_user_name, date_created FROM tb_audit_workflow_trans WHERE pica_number='$number' $query $query2 ORDER BY date_created DESC ")->all();
                
        if($workflow)
        {
            
            $response->data = ['workflow' =>$workflow];
            
        }else{
            
            $response->data = ['workflow' =>[]];
            
        }
        
        return $response; 
        
    }
    
    public static function nextFlow($comment,$pica_number,$approval_status,$workflow)
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;                
        $approver = Yii::$app->user->identity;
        
        $transaction = Yii::$app->db->beginTransaction();   
        
        try { 
                 
            // Insert workflow trans
            Yii::$app->db->createCommand("INSERT INTO tb_audit_workflow_trans (pica_number,approval_status,from_user_nik,from_user_name,from_user_position,order_number,to_user_nik,to_user_name,to_user_position,comment,industry_id,
                                location_id,region_id,business_unit_id,division_id) VALUES('{$pica_number}','$approval_status','{$approver['nik']}',
                                '{$approver['username']}','{$approver['position']}',{$workflow['order_number']},'{$workflow['actor_nik']}','{$workflow['actor_name']}','{$workflow['actor_position']}','$comment','{$workflow['industry_id']}',
                                '{$workflow['location_id']}','{$workflow['region_id']}','{$workflow['business_unit_id']}','{$workflow['division_id']}')")->execute();
            
            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
            
        }catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Workflow Gagal Disimpan','status' => 'fail'];
            
        }   
        
        return $response;
        
    }
    
    public static function finalFlow($comment,$pica_number,$approval_status,$workflow)
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;                
        
        $approver = Yii::$app->user->identity;
        
        $transaction = Yii::$app->db->beginTransaction();   
        
        try { 
                 
            // Insert workflow trans
            Yii::$app->db->createCommand("INSERT INTO tb_audit_workflow_trans (pica_number,approval_status,from_user_nik,from_user_name,from_user_position,order_number,to_user_nik,to_user_name,to_user_position,comment,industry_id,
                                location_id,region_id,business_unit_id,division_id) VALUES('{$pica_number}','$approval_status','{$approver['nik']}',
                                '{$approver['username']}','{$approver['position']}',6,'-','-','-','$comment','{$workflow['industry_id']}',
                                '{$workflow['location_id']}','{$workflow['region_id']}','{$workflow['business_unit_id']}','{$workflow['division_id']}')")->execute();
            
            $transaction->commit();
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
            
        }catch (Exception $e) {
                                    
            $transaction->rollBack();
            
            $response->data = ['message' => 'Workflow Gagal Disimpan','status' => 'fail'];
            
        }   
        
        return $response;
        
    }
    
}
