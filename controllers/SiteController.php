<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\web\Request;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Session;
use yii\web\CookieCollection;
use yii\web\Cookie;
use yii\helpers\Url;
use yii\helpers\BaseJson;
//use yii\httpclient\Client;

use app\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\ContactForm;

use app\models\TbAuditIndustry;
use app\models\TbAuditStatus;
use app\models\TbAuditLocation;
use app\models\TbAuditRegion;
use app\models\TbAuditBusinessUnit;
use app\models\TbAuditLeadTimeStatus;
use app\models\TbAuditDivision;  
use app\models\TbAuditCategory;  
use app\models\TbAuditRating;  
use app\models\TbAuditUser; 
use app\models\TbAuditPicaFinding; 
use app\models\TbAuditPicaCorrective;
use app\models\TbAuditPicaProgress;
use app\models\TbAuditPicaHeader;
use app\models\TbAuditRole; 
use app\models\TbAuditWorkflow;
use app\models\TbAuditWorkflowTrans;
use app\models\TbAuditMandatory;
use app\models\TbAuditAlert;
use app\models\TbAuditTypeAudit;
use app\models\User;


/**
 * Site controller
 */
class SiteController extends Controller
{    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','request-password-reset','reset-password','contact','holder'],
                        'allow' => true,
                        //'roles' => ['@'],
                    ],
                    [
                        'actions' => ['logout', 'index','industry','business-unit','category','division','alert',
                                       'lead-time-status','location','pica','pica-create','pica-edit','pica-extension','pica-progress',
                                       'pica-finding','pica-scoring','pica-switch','rating','region','role','status','user','workflow','upload','pica-upload'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex2()
    {
        date_default_timezone_set('Asia/Jakarta');

        $items = TbAuditIndustry::find()
        ->select(['name', 'id'])
        ->where('is_deleted =0')
        ->indexBy('id')
        ->column(); 

        $model = new LoginForm();

        $sessionid = base64_encode(date("Y-m-d")).Yii::$app->request->get('c') ;

        //$nik = base64_decode(Yii::$app->request->get('c'));

        //$nik = '677';

        $identity = User::findByNik($nik);
        Yii::$app->user->login($identity);

        $model->nik = $nik;
        $model->industry = Yii::$app->request->get('b');
        $model->password = 'kirana21';

        return $this->render('holder', ['model' => $model,'items'=>$items]);
    }

        /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        date_default_timezone_set('Asia/Jakarta');

        //get token parameter
		//$request = Yii::$app->request;
		//$get = $request->get();
		//$token = $request->get('kpitoken');

        //echo $token;
		//$token = '1234';
		//$cookies = Yii::$app->response->cookies;

        $cookies = Yii::$app->request->cookies;
		
		/* if ($token !== null ) {
			$cookies->add(new \yii\web\Cookie([
				'name' => 'iats_token',
				'value' => $token,
			]));
		}
        */

        //if (($cookie = $cookies->get('iats_token')) !== null) {
        //    $token = $cookie->value;
        //}

        if (isset($cookies['iats_token']))
        //if ($token !== null )
        {

            //$nik = '677';
            //$identity = User::findByNik($nik);
            //Yii::$app->user->login($identity);

            $transaction = Yii::$app->db->beginTransaction();
            $user =  Yii::$app->user->identity;
            $query = '';
            $query2 = '';
            $button = array();
            $isPrinted = array();
            $orderNumber = array();
            $no=1;
            
            try {
                
                if($user->role_name ==='Progress Approver'){
                
                    if(  intval($user->location_id) === 1){
                        //echo "a";
                        $inbox = TbAuditPicaCorrective::findBySql("SELECT DISTINCT number,order_number,CONVERT(VARCHAR(10),date_created,110) AS date_created FROM tb_audit_pica_corrective WHERE industry_id={$user->industry_id} AND  division_id IN ({$user->division_id}) AND (order_number BETWEEN 3 AND 4)  AND is_deleted=0  ")->all();
                        
                    
                    }else{
                        //echo "a2";
                        $inbox = TbAuditPicaCorrective::findBySql("SELECT DISTINCT number,order_number,CONVERT(VARCHAR(10),date_created,110) AS date_created FROM tb_audit_pica_corrective WHERE industry_id={$user->industry_id} AND region_id={$user->region_id} AND business_unit_id IN ({$user->business_unit_id}) AND division_id IN ({$user->division_id})  AND (order_number BETWEEN 3 AND 4)  AND is_deleted=0  ")->all();
                    
                        
                    }
                    
                }else if($user->role_name ==='PIC'){
                    //echo "b";
                    $inbox = TbAuditPicaCorrective::findBySql("SELECT DISTINCT number,order_number,CONVERT(VARCHAR(10),date_created,110) AS date_created FROM tb_audit_pica_corrective WHERE (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik ='{$user['nik']}') AND complete=0  AND is_deleted=0  ")->all();


                }else{
                    //echo "c";
                    $inbox = TbAuditPicaCorrective::findBySql("SELECT DISTINCT number,order_number,CONVERT(VARCHAR(10),date_created,110) AS date_created FROM tb_audit_pica_corrective WHERE (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik ='{$user['nik']}')  AND is_deleted=0  ")->all();

                    //masuk 009
                }
                // echo "SELECT DISTINCT number,order_number,CONVERT(VARCHAR(10),date_created,110) AS date_created FROM tb_audit_pica_corrective WHERE (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik ='{$user['nik']}')  AND is_deleted=0  ";
                // echo "SELECT DISTINCT number,order_number,CONVERT(VARCHAR(10),date_created,110) AS date_created FROM tb_audit_pica_corrective WHERE (next_nik LIKE '%,{$user['nik']}%' OR next_nik LIKE '%{$user['nik']},%' OR next_nik ='{$user['nik']}')  AND is_deleted=0  ";
                // exit();
                $status = TbAuditStatus::find()->all();
                
                $inbox_temp=array();
                
                Yii::$app->session->set('inbox', count($inbox));
                
                if(count($inbox)>0){
                    
                    foreach($inbox as $index => $value){
                        
                        if($user->role_name ==='PIC'){

                            $corrective = TbAuditPicaCorrective::find()->where("number='{$value['number']}' AND (next_nik LIKE '%,{$user->nik}%' OR next_nik LIKE '%{$user->nik},%' OR next_nik = '{$user->nik}' ) AND complete=0 ")->all();
                        
                        }else{

                            $corrective = TbAuditPicaCorrective::find()->where("number='{$value['number']}' AND (next_nik LIKE '%,{$user->nik}%' OR next_nik LIKE '%{$user->nik},%' OR next_nik = '{$user->nik}' )")->all();
                            //masuk 009
                        }   
                        //echo "select * from tb_audit_pica_corrective where number='{$value['number']}' AND (next_nik LIKE '%,{$user->nik}%' OR next_nik LIKE '%{$user->nik},%' OR next_nik = '{$user->nik}' ";
                        //exit();
                        $picaFinding = TbAuditPicaFinding::find()->where("number='{$value['number']}'")->one();
                        
                        $pica_number = $value['number'];
                    
                        $isPrinted["$pica_number"] = $picaFinding['is_printed'];

                        $orderNumber["$pica_number"] = $value['order_number'];
                                        
                        foreach($value as $index2 => $value2){
                        
                            //TODO : hardcode division id not equal 37 (Internal Audit)
                            if($user->division_id !== '5'){
                            //if($user->division_id !== '37'){
                            
                                $query = " AND division_id IN ($user->division_id)";
                            
                                $query2 = " AND order_number >=4 ";
                            }
                        
                            if($user->role_name==='Progress Approver'){
                                
                                $finding = array();
                                
                                if(intval($user->location_id)===1){
                                    
                                    $finding = TbAuditPicaCorrective::findBySql("SELECT finding_id FROM tb_audit_pica_corrective WHERE number='{$value['number']}' AND industry_id={$user->industry_id} AND  division_id IN ({$user->division_id}) AND order_number ={$value['order_number']}  AND is_deleted=0   GROUP BY finding_id ")->all();
                                    
                                }else{
                                
                                    $finding = TbAuditPicaCorrective::findBySql("SELECT finding_id FROM tb_audit_pica_corrective WHERE number='{$value['number']}' AND industry_id={$user->industry_id} AND region_id={$user->region_id} AND business_unit_id IN ({$user->business_unit_id}) AND division_id IN ({$user->division_id}) AND order_number ={$value['order_number']}  AND is_deleted=0    GROUP BY finding_id ")->all();
                                }
                                
                            }else if($user->role_name==='PIC') {

                                $finding = TbAuditPicaCorrective::findBySql("SELECT finding_id FROM tb_audit_pica_corrective WHERE number='{$value['number']}' AND (next_nik LIKE '%,{$user->nik}%' OR next_nik LIKE '%{$user->nik},%' OR next_nik = '{$user->nik}' ) AND complete=0  AND is_deleted=0  GROUP BY finding_id ")->all();

                            }else{
                                
                                $finding = TbAuditPicaCorrective::findBySql("SELECT finding_id FROM tb_audit_pica_corrective WHERE number='{$value['number']}' AND (next_nik LIKE '%,{$user->nik}%' OR next_nik LIKE '%{$user->nik},%' OR next_nik = '{$user->nik}' )  AND is_deleted=0 AND next_nik!='' GROUP BY finding_id ")->all();
                                // echo "SELECT finding_id FROM tb_audit_pica_corrective WHERE number='{$value['number']}' AND (next_nik LIKE '%,{$user->nik}%' OR next_nik LIKE '%{$user->nik},%' OR next_nik = '{$user->nik}' )  AND is_deleted=0  GROUP BY finding_id ";
                                // exit();
                            }

                            
                            $button["$no"] = count($corrective)> 0 ? true : false;
                            $inbox_temp[$index]['finding'] =  count($finding);
                            $inbox_temp[$index]['no']= $no;
                            $inbox_temp[$index][$index2] = $value2;
                            //$inbox_temp[$index]['workflow'] =  TbAuditWorkflowTrans::findBySql("SELECT DISTINCT approval_status,from_user_name,REPLACE (comment,'<br>','\n') AS comment,to_user_name, date_created FROM tb_audit_workflow_trans WHERE pica_number='{$value['number']}' $query $query2 ORDER BY date_created DESC ")->all();
                        
                        }
                    
                        $no++;
                    }
                }
                
                $transaction->commit();
                
            } 
            catch (Exception $e) {
                $transaction->rollBack();
            }
            // echo "<pre>".json_encode($inbox_temp)."</pre>";
            // exit();  
            $data = BaseJson::encode($inbox_temp);
            $statusj = BaseJson::encode($status);
            $buttonj = BaseJson::encode($button);
            $printj = BaseJson::encode($isPrinted);
            $orderNumberj  = BaseJson::encode($orderNumber);
            
            return $this->render('index',['data'=>$data,'status'=>$statusj,"button"=>$buttonj,"print"=>$printj,'orderNumber'=>$orderNumberj]);
        }
        else
        {
            return $this->redirect('http://10.0.0.160/authapp/?original_appid=3');
        }

    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {

        date_default_timezone_set('Asia/Jakarta');

        //get token parameter
		$request = Yii::$app->request;
		$get = $request->get();
		$token = $request->get('kpitoken');

        //echo $token;
		
		$cookies = Yii::$app->response->cookies;
		
		if ($token !== null ) {
            //Yii::$app->user->logout();

			$cookies->add(new \yii\web\Cookie([
				'name' => 'iats_token',
				'value' => $token,
			]));

            $items = TbAuditIndustry::find()
                ->select(['name', 'id'])
				->where('is_deleted =0')
                ->indexBy('id')
                ->column(); 

            $model = new LoginForm();
       
            $sessionid = base64_encode(date("Y-m-d")).Yii::$app->request->get('c') ;

            //$nik = base64_decode(Yii::$app->request->get('c'));

            //$nik = '677';

            $identity = User::findByNik($nik);
            Yii::$app->user->login($identity);

            $model->nik = $nik;
            $model->industry = Yii::$app->request->get('b');
            $model->password = 'kirana21';

            return $this->render('login', ['model' => $model,'items'=>$items]);

            /* if ($model->login()) {

                return $this->render('login', ['model' => $model,'items'=>$items]);
            } */
		}
        else {
            return $this->redirect('http://10.0.0.160/authapp/?original_appid=3');
        }
         
		
    }

    public function actionHolder()
    {
        date_default_timezone_set('Asia/Jakarta');

        //get token parameter
		$request = Yii::$app->request;
		$get = $request->get();
		$token = $request->get('kpitoken');

        //echo $token;
		
		$cookies = Yii::$app->response->cookies;
		
		if ($token !== null ) {
            //Yii::$app->user->logout();

			$cookies->add(new \yii\web\Cookie([
				'name' => 'iats_token',
				'value' => $token,
			]));

            $items = TbAuditIndustry::find()
                ->select(['name', 'id'])
				->where('is_deleted =0')
                ->indexBy('id')
                ->column(); 

            $model = new LoginForm();
       
            $sessionid = base64_encode(date("Y-m-d")).Yii::$app->request->get('c') ;

            //$nik = base64_decode(Yii::$app->request->get('c'));

			//TODO RESTFULL CALL API 
            //$nik = '677';

			$nikTemp = TbAuditUser::findBySql("SELECT nik FROM [ReportTemplate].[rpt].[vw_KaryawanDivisi] WHERE token='{$token}' ")->one();
			
			$nik = $nikTemp['nik'];
			
            /* $nik = '';
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('POST')
                ->setUrl('http://10.0.0.160:1338/getuserinfobytoken')
                ->setData(['token' => $token, 'appid' => 0])
                ->send();
            if ($response->isOk) {
                $nik = $response->data['id'];
            } */

            //$identity = User::findByNik($nik);
            //Yii::$app->user->login($identity);

            $model->nik = $nik;
            $model->industry = Yii::$app->request->get('b');
            $model->password = 'kirana21';

            //return $this->render('holder', ['model' => $model,'items'=>$items]);

            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                return $this->goBack();
            } else {
                    return $this->render('holder', ['model' => $model,'items'=>$items]);
            }

		}
        else {
            return $this->redirect('http://10.0.0.160/authapp/?original_appid=3');
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        $user =  Yii::$app->user->identity;

        Yii::$app->user->logout();
		
		$cookies = Yii::$app->response->cookies;
		$cookies->remove('iats_token');

        // if($user->industry_id ==='2' || $user->industry_id == '3'){

        //    return $this->goHome();

        // }else{

        //     return $this->redirect('http://kiranaku.kiranamegatara.com/home/?q=logout');
        // }
        return $this->redirect('http://10.0.0.160/authapp/?original_appid=3');
    }
    
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Silakan cek email Anda untuk instruksi selanjutnya.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        try {

            $model = new ResetPasswordForm($token);

        }catch (InvalidParamException $e) {

            Yii::$app->session->setFlash('success', 'Token Anda sudah expired, silakan reset password ulang.');

            //throw new BadRequestHttpException($e->getMessage());

        }

        

        if(Yii::$app->request->post()){

            $password = Yii::$app->request->post('ResetPasswordForm');

            if($password['password']===$password['confirm']){

                if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            
                    Yii::$app->session->setFlash('success', 'New password was saved.');

                    return $this->goHome();
                }

            }else{

                    Yii::$app->session->setFlash('success', 'Password dan Confirm Tidak Sama. Ulangi lagi !');

                    //return $this->goHome();

            }
        
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionIndustry()
    {       
        $transaction = Yii::$app->db->beginTransaction();
        
        try {
            
            $industry =TbAuditIndustry::find()->where('is_deleted =0')->all();
            $dataIndustry = array();
            
            foreach($industry as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $dataIndustry[$index][$index2] = $value2;
                    $dataIndustry[$index]['no'] = $index+1;
                    
                }
                
            }
            $transaction->commit();
            
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }   

        $data = BaseJson::encode($dataIndustry);
        
        return $this->render('industry',['data'=>$data]);
    }
    
    public function actionLocation()
    {       
        $transaction = Yii::$app->db->beginTransaction();
        
        try {
            
            $location =  TbAuditLocation::find()->where('is_deleted =0')->all();
            
            $dataLocation = array();
            
            foreach($location as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $dataLocation[$index][$index2] = $value2;
                    $dataLocation[$index]['no'] = $index+1;
                    
                }
                
            }
            
            $transaction->commit();
            
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }   

        $data = BaseJson::encode($dataLocation);
        
        return $this->render('location',['data'=>$data]);
    }
    
    public function actionRating()
    {       
        $transaction = Yii::$app->db->beginTransaction();
        
        try {
            
            $rating =  TbAuditRating::find()->where('is_deleted =0')->all();
            
            $dataRating = array();
            
            foreach($rating as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $dataRating[$index][$index2] = $value2;
                    $dataRating[$index]['no'] = $index+1;
                    
                }
                
            }
            
            
            $transaction->commit();
            
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }   

        $data = BaseJson::encode($dataRating);
        
        return $this->render('rating',['data'=>$data]);
    }
    
    public function actionCategory()
    {       
        $transaction = Yii::$app->db->beginTransaction();
        
        try {
            $category =  TbAuditCategory::find()->where('is_deleted =0')->all();
            
            $dataCategory = array();
            
            foreach($category as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $dataCategory[$index][$index2] = $value2;
                    $dataCategory[$index]['no'] = $index+1;
                    
                }
                
            }
            
            
            $transaction->commit();
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }   

        $data = BaseJson::encode($dataCategory);
        
        return $this->render('category',['data'=>$data]);
    }
    
    public function actionStatus()
    {       
        $transaction = Yii::$app->db->beginTransaction();
        
        try {
            
            $status =TbAuditStatus::find()->where('is_deleted =0')->all();
            
            $dataStatus = array();
            
            foreach($status as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $dataStatus[$index][$index2] = $value2;
                    $dataStatus[$index]['no'] = $index+1;
                    
                }
                
            }
            
            
            $transaction->commit();
            
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }   

        $data = BaseJson::encode($dataStatus);
        
        return $this->render('status',['data'=>$data]);
    }
    
    public function actionLeadTimeStatus()
    {       
        $transaction = Yii::$app->db->beginTransaction();
        
        try {
            
            $leadTimeStatus =  TbAuditLeadTimeStatus::find()->where('is_deleted =0')->all();
            
            $dataLeadTimeStatus = array();
            
            foreach($leadTimeStatus as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $dataLeadTimeStatus[$index][$index2] = $value2;
                    $dataLeadTimeStatus[$index]['no'] = $index+1;
                    
                }
                
            }
            
            
            $transaction->commit();
            
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }   

        $data = BaseJson::encode($dataLeadTimeStatus);
        
        return $this->render('lead-time-status',['data'=>$data]);
    }
    
    public function actionRole()
    {       
        $transaction = Yii::$app->db->beginTransaction();
        
        try {
            
            $role =TbAuditRole::find()->where('is_deleted =0')->all();
            
            $dataRole = array();
            
            foreach($role as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $dataRole[$index][$index2] = $value2;
                    $dataRole[$index]['no'] = $index+1;
                    
                }
                
            }
            
            
            $transaction->commit();
            
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }   

        $data = BaseJson::encode($dataRole);
        
        return $this->render('role',['data'=>$data]);
    }
    
    public function actionRegion()
    {       
        $transaction    = Yii::$app->db->beginTransaction();
        $dataRegion     = array();
        $industry       = array();
        $location       = array();

        try {
            $region =  TbAuditRegion::find()->where('is_deleted =0')->all();
            
            $dataRating = array();
            
            // Double Loop Dibuat Untuk Teknik Penomoran
            foreach($region as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $dataRegion[$index][$index2]    = $value2;
                    $dataRegion[$index]['no']       = $index+1;
                    
                }
                
            }
            
            
            $industry = TbAuditIndustry::find()->where('is_deleted =0')->all();
            $location = TbAuditLocation::find()->where('is_deleted =0')->all();
                    
            $transaction->commit();
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }   

        $data   = BaseJson::encode($dataRegion);
        $data2  = BaseJson::encode($industry);
        $data3  = BaseJson::encode($location);
        
        return $this->render('region',['data'=>$data,'industry'=>$data2,'location'=>$data3]);
    }
    
    public function actionDivision()
    {       
        $transaction = Yii::$app->db->beginTransaction();
        
        try {
            
            $division =TbAuditDivision::find()->where('is_deleted =0')->all();  
            
            $dataDivision = array();
            
            foreach($division as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $dataDivision[$index][$index2] = $value2;
                    $dataDivision[$index]['no'] = $index+1;
                    
                }
                
            }
            
            
            $industry = TbAuditIndustry::find()->where('is_deleted =0')->all();
            $location = TbAuditLocation::find()->where('is_deleted =0')->all();

            $transaction->commit();
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }   

        $data = BaseJson::encode($dataDivision);
        $data2 = BaseJson::encode($industry);
        $data3 = BaseJson::encode($location);
        
        return $this->render('division',['data'=>$data,'industry'=>$data2,'location'=>$data3]);
    }
    
    public function actionBusinessUnit()
    {       
        $transaction = Yii::$app->db->beginTransaction();
        
        try {
            
            $business_unit =  TbAuditBusinessUnit::find()->where('is_deleted =0')->all();
            
            $dataBusiness_Unit = array();
            
            foreach($business_unit as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $dataBusiness_Unit[$index][$index2] = $value2;
                    $dataBusiness_Unit[$index]['no'] = $index+1;
                    
                }
                
            }
            
            
            $industry = TbAuditIndustry::find()->where('is_deleted =0')->all();
            $location = TbAuditLocation::find()->where('is_deleted =0')->all();
            $region = TbAuditRegion::find()->where('is_deleted =0')->all(); 
                    
            $transaction->commit();
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }   

        $data = BaseJson::encode($dataBusiness_Unit);
        $data2 = BaseJson::encode($industry);
        $data3 = BaseJson::encode($location);
        $data4 = BaseJson::encode($region);
        
        return $this->render('business-unit',['data'=>$data,'industry'=>$data2,'location'=>$data3,'region'=>$data4]);
    }
    
    public function actionUser()
    {       
		//print_r ("Session is not set",true) ;
        $transaction = Yii::$app->db->beginTransaction();   
        
        
        try {
            $user = TbAuditUser::find()->where('is_deleted =0')->all();     
            
            $dataUser = array();
            
            foreach($user as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $dataUser[$index][$index2] = $value2;
                    $dataUser[$index]['no'] = $index+1;
                    
                }
                
            }
            
            
            $business_unit = TbAuditBusinessUnit::find()->where('is_deleted =0')->all();
            $industry = TbAuditIndustry::find()->where('is_deleted =0')->all();
            $location = TbAuditLocation::find()->where('is_deleted =0')->all();
            $region = TbAuditRegion::find()->where('is_deleted =0')->all();            
            $division = TbAuditDivision::find()->where('is_deleted =0')->all();  
            $role = TbAuditRole::find()->where('is_deleted =0')->all();              
            $transaction->commit();
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }   

        $data = BaseJson::encode($dataUser);
        $data2 = BaseJson::encode($industry);
        $data3 = BaseJson::encode($location);
        $data4 = BaseJson::encode($region);
        $data5 = BaseJson::encode($business_unit);
        $data6 = BaseJson::encode($division);
        $data7 = BaseJson::encode($role);
        
        return $this->render('user',['data'=>$data,'business_unit'=>$data5,'industry'=>$data2,'location'=>$data3,'region'=>$data4,'division'=>$data6,'role'=>$data7]);
    }
    
    public function actionWorkflow()
    {
        $transaction = Yii::$app->db->beginTransaction();
      
        try {

            $business_unit = TbAuditBusinessUnit::find()->where('is_deleted =0')->all();
            $industry = TbAuditIndustry::find()->where('is_deleted =0')->all();
            $location = TbAuditLocation::find()->where('is_deleted =0')->all();
            $region = TbAuditRegion::find()->where('is_deleted =0')->all();    
            $division = TbAuditDivision::find()->where('is_deleted =0')->all();  
            $workflow =TbAuditWorkflow::findBySql("SELECT industry_id,location_id,
                                                    region_id,business_unit_id, division_id FROM ( SELECT industry_id,location_id,
                                                    region_id,business_unit_id, division_id, MAX (date_created) AS date_created FROM tb_audit_workflow GROUP by industry_id,location_id,
                                                    region_id,business_unit_id, division_id ) a ")->all();
            
            $dataWorkflow = array();
            
            foreach($workflow as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $dataWorkflow[$index][$index2] = $value2;
                    $dataWorkflow[$index]['no'] = $index+1;
                    
                }
                
            }
            
            
            $user = TbAuditUser::find()->all(); 
            
            $transaction->commit();
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }
        
        $data1 = BaseJson::encode($user);
        $data2 = BaseJson::encode($industry);
        $data3 = BaseJson::encode($location);
        $data4 = BaseJson::encode($region);
        $data5 = BaseJson::encode($business_unit);
        $data6 = BaseJson::encode($division);
        $data7 = BaseJson::encode($dataWorkflow);
              
        
        return $this->render('workflow',['users'=>$data1,'industry'=>$data2,'location'=>$data3,'region'=>$data4,'business_unit'=>$data5,'division'=>$data6,'workflow'=>$data7]);
        
    }
    
    public function actionPica()
    {
        
        $user =  Yii::$app->user->identity;
        $query = '';
        $session = Yii::$app->session;
        
        $pica_corrective = array();
        
        $transaction = Yii::$app->db->beginTransaction();
        
		//TODO hardcode division_id, location_id, role_id
        try {

                $industry = TbAuditIndustry::find()->where('is_deleted =0')->all();
                $region = TbAuditRegion::findBySql("SELECT r.id, r.name + ' ' + '(' + i.name + ')' AS name  FROM tb_audit_region AS r INNER JOIN tb_audit_industry AS i ON (i.id = r.industry_id)")->all();  
                $business_unit = TbAuditBusinessUnit::findBySql("SELECT r.id, r.name + ' ' + '(' + i.name + ')' AS name  FROM tb_audit_business_unit AS r INNER JOIN tb_audit_industry AS i ON (i.id = r.industry_id)")->all();
                //echo $industry."--=======================================================".$region."--==========================================================================".$business_unit;
            //if($user->division_id !== '37'){
			if($user->division_id !== '5'){
                //echo "aaaaaaaaaaa";
                if($user->location_id===1){
                    //echo "11111111";
                    if($user->role_name ==='COO' ){
                        //echo "00000000";
                        $pica = Yii::$app->db->createCommand("SELECT id,number,repeat_finding, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days FROM tb_audit_pica_finding WHERE (status='On Progress' OR status='Open')  AND  industry_id =$user->industry_id  AND is_deleted=0 AND order_number>2")->queryAll(); 

                    }else{
                        //echo "88888888";
                        $pica = Yii::$app->db->createCommand("SELECT id,number,repeat_finding, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days FROM tb_audit_pica_finding WHERE (status='On Progress' OR status='Open')  AND  industry_id =$user->industry_id AND  division_id IN ($user->division_id)  AND is_deleted=0 AND order_number>2")->queryAll(); 
                    }

                }else{
                    //add role ceo region and ho
                     //add for role ho
                    if($user->role_id == 10){
                        //echo "33333333";
                        $pica = Yii::$app->db->createCommand("SELECT id,number,repeat_finding, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days FROM tb_audit_pica_finding WHERE (status='On Progress' OR status='Open')  AND is_deleted=0 AND order_number>2 ORDER BY number DESC ")->queryAll(); 
                    } else {
                        $pica = Yii::$app->db->createCommand("SELECT id,number,repeat_finding, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment ,finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days FROM tb_audit_pica_finding WHERE (status='On Progress' OR status='Open') AND industry_id = $user->industry_id  AND region_id IN ($user->region_id) AND business_unit_id IN ($user->business_unit_id) AND division_id IN ($user->division_id)  AND is_deleted=0 AND order_number>2 ")->queryAll(); 
                    }
                }

            }else{
                //echo "bbbbbbbbb";
                $pica = Yii::$app->db->createCommand("SELECT id,number,repeat_finding, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days FROM tb_audit_pica_finding WHERE (status='On Progress' OR status='Open')  AND is_deleted=0 AND order_number>2 ORDER BY number DESC ")->queryAll(); 
               
            }
            
            $index_array = 0;
            
            foreach($pica as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, (REPLACE(number,'/','_')+'/'+progress_file) AS progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,COALESCE (date_extension,date_due) AS date_due  FROM tb_audit_pica_corrective WHERE finding_id={$value['id']}  AND is_deleted=0 ")->all();
                    
                    if(count($corrective) > 0){
                        
                        $pica_corrective[$index_array]['corrective_action'] = $corrective;
                        $pica_corrective[$index_array]['no']=$index+1;
                        $pica_corrective[$index_array][$index2] =   $value2;
                        
                    }
                }
                
                if(count($pica_corrective) > 0){$index_array++;}
                
            }

            $transaction->commit();
            
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }
          
        $data = BaseJson::encode($pica_corrective);
        $data2 = BaseJson::encode($industry);
        $data3 = BaseJson::encode($region);
        $data4 = BaseJson::encode($business_unit);
       
        
        return $this->render('pica',['data'=>$data,'industry'=>$data2,'region'=>$data3,'business_unit'=>$data4]);
        
    }
    
    public function actionPicaProgress()
    {
        
        $user =  Yii::$app->user->identity;
        $query = '';
        $session = Yii::$app->session;
        
        $pica_corrective = array();
        
        $transaction = Yii::$app->db->beginTransaction();
        
        try {

            $industry = TbAuditIndustry::find()->where('is_deleted =0')->all();
            $region = TbAuditRegion::findBySql("SELECT r.id, r.name + ' ' + '(' + i.name + ')' AS name  FROM tb_audit_region AS r INNER JOIN tb_audit_industry AS i ON (i.id = r.industry_id)")->all();  
            $business_unit = TbAuditBusinessUnit::findBySql("SELECT r.id, r.name + ' ' + '(' + i.name + ')' AS name  FROM tb_audit_business_unit AS r INNER JOIN tb_audit_industry AS i ON (i.id = r.industry_id)")->all();
            $status = TbAuditStatus::find()->where('is_deleted =0')->all();
            
            $pica = Yii::$app->db->createCommand("SELECT id,number,repeat_finding, REPLACE (finding,'<br>','\n') AS finding ,  REPLACE (comment,'<br>','\n') AS comment ,REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days  FROM tb_audit_pica_finding WHERE is_deleted=0 AND order_number>2 ORDER BY number DESC ")->queryAll(); 
                
            $index_array = 0;
            
            foreach($pica as $index => $value){
                
                $corrective = array();

                foreach($value as $index2 => $value2){
                    
                    $corrective = TbAuditPicaProgress::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, (REPLACE(number,'/','_')+'/'+progress_file) AS progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress FROM tb_audit_pica_progress WHERE finding_id={$value['id']}  AND is_deleted=0  ORDER BY corrective_id ASC")->all();
                    
                    if(count($corrective) > 0){
                        
                        $pica_corrective[$index_array]['corrective_action'] = $corrective;
                        $pica_corrective[$index_array]['no']=$index_array+1;
                        $pica_corrective[$index_array][$index2] =   $value2;
                        
                    }
                }
                
                 if(count($corrective) > 0){$index_array++;}
                
            }

            $transaction->commit();
            
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }

        $data = BaseJson::encode($pica_corrective);
        $data2 = BaseJson::encode($industry);
        $data3 = BaseJson::encode($region);
        $data4 = BaseJson::encode($business_unit);
        $data5 = BaseJson::encode($status);
        
        return $this->render('pica-progress',['data'=>$data,'industry'=>$data2,'region'=>$data3,'business_unit'=>$data4,'status'=>$data5]);
        
    }
    
    public function actionPicaScoring()
    {
        return $this->render('pica-scoring');
    }
    
    public function actionPicaFinding()
    {
        
        $user =  Yii::$app->user->identity;
        $query = '';
        $session = Yii::$app->session;
        
        $pica_corrective = array();
        
        $transaction = Yii::$app->db->beginTransaction();
        //TODO hardcode division_id
        try {
            
            $industry = TbAuditIndustry::find()->where('is_deleted =0')->all();
            $location = TbAuditLocation::find()->where('is_deleted =0')->all();
            $division = TbAuditDivision::findBySql("SELECT r.id, r.name + ' ' + '(' + i.name + ')' AS name  FROM tb_audit_division AS r INNER JOIN tb_audit_industry AS i ON (i.id = r.industry_id)")->all();
            $region = TbAuditRegion::findBySql("SELECT r.id, r.name + ' ' + '(' + i.name + ')' AS name  FROM tb_audit_region AS r INNER JOIN tb_audit_industry AS i ON (i.id = r.industry_id)")->all();  
            $business_unit = TbAuditBusinessUnit::findBySql("SELECT r.id, r.name + ' ' + '(' + i.name + ')' AS name  FROM tb_audit_business_unit AS r INNER JOIN tb_audit_industry AS i ON (i.id = r.industry_id)")->all();
            $teamLeader = TbAuditUser::find()->where("is_deleted =0 AND division_id='37'")->all();  
            $category = TbAuditCategory::find()->where("is_deleted =0")->all();
            $rating = TbAuditRating::find()->where("is_deleted =0")->all();  
            
			if($user->division_id !== '5'){
            //if($user->division_id !== '37'){
                
                $pica = Yii::$app->db->createCommand("SELECT id,number,team_leader_name,repeat_finding, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment , REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days  FROM tb_audit_pica_finding WHERE division_id IN ($user->division_id)  AND is_deleted=0 AND order_number>2")->queryAll(); 
                
            }else{

               $pica = Yii::$app->db->createCommand("SELECT id,number,team_leader_name,repeat_finding, REPLACE (finding,'<br>','\n') AS finding , REPLACE (comment,'<br>','\n') AS comment , REPLACE (supervisor_comment,'<br>','\n') AS supervisor_comment , finding_file, auditor_nik, auditor_name, auditor_position,rating_id,rating_name,category_id,category_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,date_posted,COALESCE (date_extension,date_due) AS date_due,status,lead_time_status, CASE lead_time_status WHEN 'Over Due' THEN DATEDIFF(day,COALESCE(date_extension,date_due),GETDATE()) ELSE 0 END AS days  FROM tb_audit_pica_finding WHERE is_deleted=0 AND order_number>2 ORDER BY number DESC")->queryAll(); 
                
            }
            
            $index_array = 0;
            
            foreach($pica as $index => $value){
                
                foreach($value as $index2 => $value2){
                    
                    $corrective = TbAuditPicaCorrective::findBySql("SELECT id,number,finding_id,auditor_nik, auditor_name, auditor_position,pic_nik,pic_name,pic_position,REPLACE (finding,'<br>','\n') AS finding, finding_file,REPLACE (corrective_action,'<br>','\n') AS corrective_action,REPLACE (latest_progress,'<br>','\n') AS latest_progress, progress_file,status,lead_time_status,mandatory_id,mandatory_name,industry_id,industry_name,location_id,location_name,region_id,region_name,business_unit_id,business_unit_name,division_id,division_name,order_number, next_nik,date_progress,COALESCE (date_extension,date_due) AS date_due  FROM tb_audit_pica_corrective WHERE finding_id={$value['id']}  AND is_deleted=0 ")->all();
                    
                    if(count($corrective) > 0){
                        
                        $pica_corrective[$index_array]['corrective_action'] = $corrective;
                        $pica_corrective[$index_array]['no']=$index+1;
                        $pica_corrective[$index_array][$index2] =   $value2;
                        
                    }
                }
                
                if(count($pica_corrective) > 0){$index_array++;}
                
            }
            $transaction->commit();
            
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }
          
        $data = BaseJson::encode($pica_corrective);
        $data2 = BaseJson::encode($industry);
        $data3 = BaseJson::encode($region);
        $data4 = BaseJson::encode($business_unit);
        $data5 = BaseJson::encode($teamLeader);
        $data6 = BaseJson::encode($category);
        $data7 = BaseJson::encode($division);
        $data8 = BaseJson::encode($location);
        $data9 = BaseJson::encode($rating);
        
        return $this->render('pica-finding',['data'=>$data,'industry'=>$data2,'region'=>$data3,'business_unit'=>$data4,'teamLeader'=>$data5,'category'=>$data6,'division'=>$data7,'location'=>$data8,'rating'=>$data9]);
    }
    
    public function actionPicaCreate()
    {
        
        $transaction = Yii::$app->db->beginTransaction();
		//TODO hardcode division_id and role_id
        try {

            $business_unit = TbAuditBusinessUnit::find()->where('is_deleted =0')->all();
            $industry = TbAuditIndustry::find()->where('is_deleted =0')->all();
            $location = TbAuditLocation::find()->where('is_deleted =0')->all();
            $region = TbAuditRegion::find()->where('is_deleted =0')->all();  
            $division = TbAuditDivision::find()->where('is_deleted =0')->all();  
            $category = TbAuditCategory::find()->where('is_deleted =0')->all();  
            $rating = TbAuditRating::find()->where('is_deleted =0')->all();  
            $user = TbAuditUser::find()->where('is_deleted =0')->all();  
            //$teamLeader = TbAuditUser::find()->where("is_deleted =0 AND division_id='37' AND role_id=6")->all();  
			$teamLeader = TbAuditUser::find()->where("is_deleted =0 AND division_id='5' AND role_id=6")->all();  
            $mandatory = TbAuditMandatory::find()->all(); 
            $typeAudit = TbAuditTypeAudit::find()->where('is_deleted =0')->all(); 
            
            $transaction->commit();
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }

        $data2 = BaseJson::encode($industry);
        $data3 = BaseJson::encode($location);
        $data4 = BaseJson::encode($region);
        $data5 = BaseJson::encode($business_unit);
        $data6 = BaseJson::encode($division);
        $data7 = BaseJson::encode($category);
        $data8 = BaseJson::encode($rating);
        $data9 = BaseJson::encode($user);
        $data10 = BaseJson::encode($mandatory);
        $data11 = BaseJson::encode($teamLeader);
        $data12 = BaseJson::encode($typeAudit);
        
        return $this->render('pica-create',['industry'=>$data2,'location'=>$data3,'region'=>$data4,'business_unit'=>$data5,'division'=>$data6,'category'=>$data7,'rating'=>$data8,'pic'=>$data9,'mandatory'=>$data10,'teamLeader'=>$data11,'typeAudit'=>$data12]);
    }
    
    public function actionPicaEdit()
    {
        $number = Yii::$app->request->get('id');
        $auditor =  Yii::$app->user->identity;
        $repeat_temp = array(0=>['number'=>'']);
        $repeat2 = array();

        $transaction = Yii::$app->db->beginTransaction();
		//TODO hardcode division_id and role_id
        try {

            $business_unit = TbAuditBusinessUnit::find()->where('is_deleted =0')->all();
            $industry = TbAuditIndustry::find()->where('is_deleted =0')->all();
            $location = TbAuditLocation::find()->where('is_deleted =0')->all();
            $region = TbAuditRegion::find()->where('is_deleted =0')->all();  
            $division = TbAuditDivision::find()->where('is_deleted =0')->all();  
            $category = TbAuditCategory::find()->where('is_deleted =0')->all();  
            $rating = TbAuditRating::find()->where('is_deleted =0')->all();  
            $user = TbAuditUser::find()->where('is_deleted =0')->all();  
            $mandatory = TbAuditMandatory::find()->all();  
            $header = TbAuditPicaHeader::find()->where("number ='{$number}' ")->all();  
            $findings = TbAuditPicaFinding::find()->where("number ='{$number}' AND next_nik='{$auditor['nik']}'")->all(); 
            $repeat = TbAuditPicaHeader::find()->select('number')->where(['business_unit_id' =>$header[0]['business_unit_id']])->all();
			$teamLeader = TbAuditUser::find()->where("is_deleted =0 AND division_id='5' AND role_id=6")->all();  
            //$teamLeader = TbAuditUser::find()->where("is_deleted =0 AND division_id='37' AND role_id=6")->all();  
            $repeat2 = array_merge($repeat_temp,$repeat);
            $transaction->commit();
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }

        if(count($findings) <= 0){
            return $this->redirect(['site/index']);
        }

        $data2 = BaseJson::encode($industry);
        $data3 = BaseJson::encode($location);
        $data4 = BaseJson::encode($region);
        $data5 = BaseJson::encode($business_unit);
        $data6 = BaseJson::encode($division);
        $data7 = BaseJson::encode($category);
        $data8 = BaseJson::encode($rating);
        $data9 = BaseJson::encode($user);
        $data10 = BaseJson::encode($mandatory);
        $data11 = BaseJson::encode($header);
        $data12 = BaseJson::encode($findings);
        $data13 = BaseJson::encode($teamLeader);
        $data14 = BaseJson::encode($repeat2);
        
        return $this->render('pica-edit',['industry'=>$data2,'location'=>$data3,'region'=>$data4,'business_unit'=>$data5,'division'=>$data6,
                                'category'=>$data7,'rating'=>$data8,'pic'=>$data9,'mandatory'=>$data10,'header'=>$data11,'findings'=>$data12,'teamLeader'=>$data13,'repeat'=>$data14]);
    }
    
    public function actionPicaExtension()
    {        
        $auditor =  Yii::$app->user->identity;

        $transaction = Yii::$app->db->beginTransaction();
      
        try {

            $location = TbAuditLocation::find()->where('is_deleted =0')->all();
            $division = TbAuditDivision::find()->where('is_deleted =0')->all();  
            $category = TbAuditCategory::find()->where('is_deleted =0')->all();  
            $rating = TbAuditRating::find()->where('is_deleted =0')->all();  
            $transaction->commit();
        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }

        $data1 = BaseJson::encode($location);
        $data2 = BaseJson::encode($division);
        $data3 = BaseJson::encode($category);
        $data4 = BaseJson::encode($rating);
        
        return $this->render('pica-extension',['location'=>$data1,'division'=>$data2,'category'=>$data3,'rating'=>$data4]);
    }

    public function actionPicaUpload(){

        $user =  Yii::$app->user->identity;
        $query = '';
        $session = Yii::$app->session;
        
        return $this->render('pica-upload');
    }

    public function actionPicaSwitch(){

        $auditor =  Yii::$app->user->identity;

        $transaction = Yii::$app->db->beginTransaction();
      
        try {

            $region = TbAuditRegion::find()->where('is_deleted =0')->all();
            $business_unit = TbAuditBusinessUnit::find()->where('is_deleted =0')->all();
            $industry = TbAuditIndustry::find()->where('is_deleted =0')->all();    
            $location = TbAuditLocation::find()->where('is_deleted =0')->all();
            $division = TbAuditDivision::find()->where('is_deleted =0')->all();  
            $category = TbAuditCategory::find()->where('is_deleted =0')->all();  
            $rating = TbAuditRating::find()->where('is_deleted =0')->all();  
            $mandatory = TbAuditMandatory::find()->all();  
            $user = TbAuditUser::find()->where('is_deleted =0')->all();  
            $status = TbAuditStatus::find()->where('is_deleted =0')->all();  
            $transaction->commit();

        } 
        catch (Exception $e) {
            $transaction->rollBack();
        }

        $data1 = BaseJson::encode($location);
        $data2 = BaseJson::encode($division);
        $data3 = BaseJson::encode($category);
        $data4 = BaseJson::encode($rating);
        $data5 = BaseJson::encode($industry);
        $data6 = BaseJson::encode($region);
        $data7 = BaseJson::encode($business_unit);
        $data8 = BaseJson::encode($mandatory);
        $data9 = BaseJson::encode($user);
        $data10 = BaseJson::encode($status);
        
        return $this->render('pica-switch',['location'=>$data1,'division'=>$data2,'category'=>$data3,'rating'=>$data4,'industry'=>$data5,'region'=>$data6,'business_unit'=>$data7,'mandatory'=>$data8,'pic'=>$data9,'status'=>$data10]);

    }

     public function actionAlert(){

        $alert = TbAuditAlert::find()->where('is_deleted =0')->all();
        
        $data = BaseJson::encode($alert);

        return $this->render('alert',['alert'=>$data]);

    }

}
