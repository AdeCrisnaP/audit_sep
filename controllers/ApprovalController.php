<?php

namespace app\controllers;

use Yii;
use app\models\TbAuditApproval;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;
use yii\web\Response;


/**
 * ApprovalController implements the CRUD actions for TbAuditApproval model.
 */
class ApprovalController extends Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];  
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }
    
     /**
     * Creates a new TbAuditApproval model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    { 
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        $model = new TbAuditApproval(); 
        $model->name = Yii::$app->request->post('name');
        $model->status = Yii::$app->request->post('status');
        $model->user_name = Yii::$app->user->identity->username;
        $model->user_nik = (string) Yii::$app->user->identity->nik;
        
        if ($model->save()) {
            
            $response->data = ['message' => 'Data Berhasil Disimpan','status' => 'success'];             
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Disimpan','status' => 'fail'];             
            
        }      
        
        return $response;

    }

    /**
     * Updates an existing TbAuditApproval model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        $model->name = Yii::$app->request->post('name');
        $model->user_name = Yii::$app->user->identity->username;
        $model->user_nik = (string) Yii::$app->user->identity->nik;
        $model->date_updated= date('Y-m-d h:i:s');
        
        if ($model->save()) {
            
            $response->data = ['message' => 'Data Berhasil Diperbarui','status' => 'success'];             
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Diperbarui','status' => 'fail'];             
            
        }      
        
        return $response;    
    }

    /**
     * Deletes an existing TbAuditApproval model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        $model->is_deleted = 1; 
        $model->user_name = Yii::$app->user->identity->username;
        $model->user_nik = (string) Yii::$app->user->identity->nik;
        $model->date_updated= date('Y-m-d h:i:s');
        
        if ($model->save()) {
            
            $response->data = ['message' => 'Data Berhasil Dihapus','status' => 'success'];             
            
        } else {
            
            $response->data = ['message' => 'Data Gagal Dihapus','status' => 'fail'];             
            
        }      
        
        return $response;      
    }
    /**
     * Finds the TbAuditApproval model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TbAuditApproval the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TbAuditApproval::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
