<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_pica_corrective_temp".
 *
 * @property integer $id
 * @property integer $finding_id
 * @property string $corrective_action
 * @property integer $auditor_nik
 * @property string $auditor_name
 * @property string $pic_nik
 * @property string $pic_name
 * @property string $pic_position
 * @property integer $mandatory_id
 * @property string $mandatory_name
 * @property integer $industry_id
 * @property string $industry_name
 * @property integer $location_id
 * @property string $location_name
 * @property integer $region_id
 * @property string $region_name
 * @property integer $business_unit_id
 * @property string $business_unit_name
 * @property integer $division_id
 * @property string $division_name
 */
class TbAuditPicaCorrectiveTemp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_pica_corrective_temp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['finding_id', 'auditor_nik', 'auditor_name', 'pic_nik', 'pic_name', 'pic_position', 'mandatory_id', 'mandatory_name', 'industry_id', 'industry_name', 'location_id', 'location_name', 'region_id', 'region_name', 'business_unit_id', 'business_unit_name', 'division_id', 'division_name'], 'required'],
            [['finding_id', 'auditor_nik', 'mandatory_id', 'industry_id', 'location_id', 'region_id', 'business_unit_id', 'division_id'], 'integer'],
            [['corrective_action', 'auditor_name', 'pic_nik', 'pic_name', 'pic_position', 'mandatory_name', 'industry_name', 'location_name', 'region_name', 'business_unit_name', 'division_name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'finding_id' => 'Finding ID',
            'corrective_action' => 'Corrective Action',
            'auditor_nik' => 'Auditor Nik',
            'auditor_name' => 'Auditor Name',
            'pic_nik' => 'Pic Nik',
            'pic_name' => 'Pic Name',
            'pic_position' => 'Pic Position',
            'mandatory_id' => 'Mandatory ID',
            'mandatory_name' => 'Mandatory Name',
            'industry_id' => 'Industry ID',
            'industry_name' => 'Industry Name',
            'location_id' => 'Location ID',
            'location_name' => 'Location Name',
            'region_id' => 'Region ID',
            'region_name' => 'Region Name',
            'business_unit_id' => 'Business Unit ID',
            'business_unit_name' => 'Business Unit Name',
            'division_id' => 'Division ID',
            'division_name' => 'Division Name',
        ];
    }
}
