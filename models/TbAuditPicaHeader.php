<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_pica_header".
 *
 * @property integer $id
 * @property string $number
 * @property string $auditor_nik
 * @property string $auditor_name
 * @property string $auditor_position
 * @property string $team_leader_nik
 * @property string $team_leader_name
 * @property string $team_leader_position
 * @property integer $industry_id
 * @property string $industry_name
 * @property integer $region_id
 * @property string $region_name
 * @property integer $business_unit_id
 * @property string $business_unit_name
 * @property string $pica_file
 * @property string $project_name
 * @property string $date_from
 * @property string $date_to
 * @property string $date_posted
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 *
 * @property TbAuditBusinessUnit $businessUnit
 * @property TbAuditIndustry $industry
 * @property TbAuditRegion $region
 */
class TbAuditPicaHeader extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_pica_header';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'auditor_nik', 'auditor_name', 'auditor_position', 'team_leader_nik', 'team_leader_name', 'team_leader_position', 'industry_id', 'industry_name', 'region_id', 'region_name', 'business_unit_id', 'business_unit_name', 'project_name'], 'required'],
            [['number', 'auditor_nik', 'auditor_name', 'auditor_position', 'team_leader_nik', 'team_leader_name', 'team_leader_position', 'industry_name', 'region_name', 'business_unit_name', 'pica_file', 'project_name'], 'string'],
            [['industry_id', 'region_id', 'business_unit_id', 'is_deleted'], 'integer'],
            [['date_from', 'date_to', 'date_posted', 'date_updated', 'date_created'], 'safe'],
            [['business_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditBusinessUnit::className(), 'targetAttribute' => ['business_unit_id' => 'id']],
            [['industry_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditIndustry::className(), 'targetAttribute' => ['industry_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditRegion::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'lha_number' => 'LHA Number',
            'type_audit_id' => 'Type Audit ID', 
            'type_audit' => 'Type Audit',
            'auditor_nik' => 'Auditor Nik',
            'auditor_name' => 'Auditor Name',
            'auditor_position' => 'Auditor Position',
            'team_leader_nik' => 'Team Leader Nik',
            'team_leader_name' => 'Team Leader Name',
            'team_leader_position' => 'Team Leader Position',
            'industry_id' => 'Industry ID',
            'industry_name' => 'Industry Name',
            'region_id' => 'Region ID',
            'region_name' => 'Region Name',
            'business_unit_id' => 'Business Unit ID',
            'business_unit_name' => 'Business Unit Name',
            'pica_file' => 'Pica File',
            'project_name' => 'Project Name',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'date_posted' => 'Date Posted',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessUnit()
    {
        return $this->hasOne(TbAuditBusinessUnit::className(), ['id' => 'business_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustry()
    {
        return $this->hasOne(TbAuditIndustry::className(), ['id' => 'industry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(TbAuditRegion::className(), ['id' => 'region_id']);
    }
}
