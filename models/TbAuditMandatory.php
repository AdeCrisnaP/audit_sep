<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_mandatory".
 *
 * @property integer $id
 * @property string $name
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 */
class TbAuditMandatory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_mandatory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['date_updated', 'date_created'], 'safe'],
            [['is_deleted'], 'integer'],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
        ];
    }
}
