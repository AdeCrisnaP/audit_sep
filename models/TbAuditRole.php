<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_role".
 *
 * @property integer $id
 * @property string $name
 * @property string $menu
 * @property string $user_nik
 * @property string $user_name
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 *
 * @property TbAuditUser[] $tbAuditUsers
 */
class TbAuditRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'menu', 'user_nik', 'user_name'], 'required'],
            [['name', 'menu', 'user_nik', 'user_name'], 'string'],
            [['date_updated', 'date_created'], 'safe'],
            [['is_deleted'], 'integer'],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'menu' => 'Menu',
            'user_nik' => 'User Nik',
            'user_name' => 'User Name',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditUsers()
    {
        return $this->hasMany(TbAuditUser::className(), ['role_id' => 'id']);
    }
}
