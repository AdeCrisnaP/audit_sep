<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_workflow_trans".
 *
 * @property integer $id
 * @property string $pica_number
 * @property string $approval_status
 * @property string $from_user_nik
 * @property string $from_user_name
 * @property string $from_user_position
 * @property integer $order_number
 * @property string $to_user_nik
 * @property string $to_user_name
 * @property string $to_user_position
 * @property string $comment
 * @property string $industry_id
 * @property string $location_id
 * @property string $region_id
 * @property string $business_unit_id
 * @property string $division_id
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 */
class TbAuditWorkflowTrans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_workflow_trans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pica_number', 'approval_status', 'from_user_nik', 'from_user_name', 'from_user_position', 'to_user_nik', 'to_user_name', 'to_user_position', 'industry_id', 'location_id', 'region_id', 'business_unit_id', 'division_id'], 'required'],
            [['pica_number', 'approval_status', 'from_user_nik', 'from_user_name', 'from_user_position', 'to_user_nik', 'to_user_name', 'to_user_position', 'comment', 'industry_id', 'location_id', 'region_id', 'business_unit_id', 'division_id'], 'string'],
            [['order_number', 'is_deleted'], 'integer'],
            [['date_updated', 'date_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pica_number' => 'Pica Number',
            'approval_status' => 'Approval Status',
            'from_user_nik' => 'From User Nik',
            'from_user_name' => 'From User Name',
            'from_user_position' => 'From User Position',
            'order_number' => 'Order Number',
            'to_user_nik' => 'To User Nik',
            'to_user_name' => 'To User Name',
            'to_user_position' => 'To User Position',
            'comment' => 'Comment',
            'industry_id' => 'Industry ID',
            'location_id' => 'Location ID',
            'region_id' => 'Region ID',
            'business_unit_id' => 'Business Unit ID',
            'division_id' => 'Division ID',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
        ];
    }
}
