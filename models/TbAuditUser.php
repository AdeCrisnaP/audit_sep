<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_user".
 *
 * @property integer $id
 * @property string $username
 * @property string $nik
 * @property string $email
 * @property integer $role_id
 * @property string $role_name
 * @property string $position
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $industry_id
 * @property string $location_id
 * @property string $region_id
 * @property string $business_unit_id
 * @property string $division_id
 * @property string $user_nik
 * @property string $user_name
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 *
 * @property TbAuditRole $role
 */
class TbAuditUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'nik', 'role_id', 'position', 'industry_id', 'location_id', 'region_id', 'business_unit_id', 'division_id', 'user_nik', 'user_name'], 'required'],
            [['username', 'nik', 'email', 'role_name', 'position', 'password_hash', 'password_reset_token', 'auth_key', 'industry_id', 'location_id', 'region_id', 'business_unit_id', 'division_id', 'user_nik', 'user_name'], 'string'],
            [['role_id', 'created_at', 'updated_at', 'status', 'is_deleted'], 'integer'],
            [['date_updated', 'date_created'], 'safe'],
            [['email'], 'unique'],
            [['industry_id', 'nik'], 'unique', 'targetAttribute' => ['industry_id', 'nik'], 'message' => 'The combination of Nik and Industry ID has already been taken.'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditRole::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'nik' => 'Nik',
            'email' => 'Email',
            'role_id' => 'Role ID',
            'role_name' => 'Role Name',
            'position' => 'Position',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'auth_key' => 'Auth Key',
            'industry_id' => 'Industry ID',
            'location_id' => 'Location ID',
            'region_id' => 'Region ID',
            'business_unit_id' => 'Business Unit ID',
            'division_id' => 'Division ID',
            'user_nik' => 'User Nik',
            'user_name' => 'User Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(TbAuditRole::className(), ['id' => 'role_id']);
    }
}
