<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_alert".
 *
 * @property integer $id
 * @property integer $alert1
 * @property integer $alert2
 * @property string $user_nik
 * @property string $user_name
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 */
class TbAuditAlert extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_alert';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alert1', 'alert2', 'is_deleted'], 'integer'],
            [['user_nik', 'user_name'], 'required'],
            [['user_nik', 'user_name'], 'string'],
            [['date_updated', 'date_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alert1' => 'Alert1',
            'alert2' => 'Alert2',
            'user_nik' => 'User Nik',
            'user_name' => 'User Name',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
        ];
    }
}
