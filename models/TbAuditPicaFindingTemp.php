<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_pica_finding_temp".
 *
 * @property integer $id
 * @property string $repeat_finding
 * @property string $finding
 * @property string $finding_file
 * @property integer $auditor_nik
 * @property string $auditor_name
 * @property string $auditor_position
 * @property integer $rating_id
 * @property string $rating_name
 * @property integer $category_id
 * @property string $category_name
 * @property integer $industry_id
 * @property string $industry_name
 * @property integer $location_id
 * @property string $location_name
 * @property integer $region_id
 * @property string $region_name
 * @property integer $business_unit_id
 * @property string $business_unit_name
 * @property integer $division_id
 * @property string $division_name
 * @property string $approval_status
 * @property string $date_due
 * @property string $date_posted
 */
class TbAuditPicaFindingTemp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_pica_finding_temp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['repeat_finding', 'finding', 'finding_file', 'auditor_name', 'auditor_position', 'rating_name', 'category_name', 'industry_name', 'location_name', 'region_name', 'business_unit_name', 'division_name', 'approval_status'], 'string'],
            [['auditor_nik', 'auditor_name', 'auditor_position', 'rating_id', 'rating_name', 'category_id', 'category_name', 'industry_id', 'industry_name', 'location_id', 'location_name', 'region_id', 'region_name', 'business_unit_id', 'business_unit_name', 'division_id', 'division_name', 'approval_status', 'date_due', 'date_posted'], 'required'],
            [['auditor_nik', 'rating_id', 'category_id', 'industry_id', 'location_id', 'region_id', 'business_unit_id', 'division_id'], 'integer'],
            [['date_due', 'date_posted'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'repeat_finding' => 'Repeat Finding',
            'finding' => 'Finding',
            'finding_file' => 'Finding File',
            'auditor_nik' => 'Auditor Nik',
            'auditor_name' => 'Auditor Name',
            'auditor_position' => 'Auditor Position',
            'rating_id' => 'Rating ID',
            'rating_name' => 'Rating Name',
            'category_id' => 'Category ID',
            'category_name' => 'Category Name',
            'industry_id' => 'Industry ID',
            'industry_name' => 'Industry Name',
            'location_id' => 'Location ID',
            'location_name' => 'Location Name',
            'region_id' => 'Region ID',
            'region_name' => 'Region Name',
            'business_unit_id' => 'Business Unit ID',
            'business_unit_name' => 'Business Unit Name',
            'division_id' => 'Division ID',
            'division_name' => 'Division Name',
            'approval_status' => 'Approval Status',
            'date_due' => 'Date Due',
            'date_posted' => 'Date Posted',
        ];
    }
}
