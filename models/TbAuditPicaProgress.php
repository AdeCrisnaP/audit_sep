<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_pica_progress".
 *
 * @property integer $id
 * @property string $number
 * @property string $finding_id
 * @property string $auditor_nik
 * @property string $auditor_name
 * @property string $auditor_position
 * @property string $pic_nik
 * @property string $pic_name
 * @property string $pic_position
 * @property string $finding
 * @property string $finding_file
 * @property integer $corrective_id
 * @property string $corrective_action
 * @property string $latest_progress
 * @property string $progress_file
 * @property string $status
 * @property string $lead_time_status
 * @property integer $mandatory_id
 * @property string $mandatory_name
 * @property integer $industry_id
 * @property string $industry_name
 * @property integer $location_id
 * @property string $location_name
 * @property integer $region_id
 * @property string $region_name
 * @property integer $business_unit_id
 * @property string $business_unit_name
 * @property integer $division_id
 * @property string $division_name
 * @property integer $order_number
 * @property string $next_nik
 * @property string $date_progress
 * @property string $date_due
 * @property string $comment
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 *
 * @property TbAuditBusinessUnit $businessUnit
 * @property TbAuditDivision $division
 * @property TbAuditIndustry $industry
 * @property TbAuditLocation $location
 * @property TbAuditRegion $region
 */
class TbAuditPicaProgress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_pica_progress';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'finding_id', 'auditor_nik', 'auditor_name', 'auditor_position', 'pic_nik', 'pic_name', 'pic_position', 'corrective_id', 'mandatory_id', 'mandatory_name', 'industry_id', 'industry_name', 'location_id', 'location_name', 'region_id', 'region_name', 'business_unit_id', 'business_unit_name', 'division_id', 'division_name', 'next_nik'], 'required'],
            [['number', 'finding_id', 'auditor_nik', 'auditor_name', 'auditor_position', 'pic_nik', 'pic_name', 'pic_position', 'finding', 'finding_file', 'corrective_action', 'latest_progress', 'progress_file', 'status', 'lead_time_status', 'mandatory_name', 'industry_name', 'location_name', 'region_name', 'business_unit_name', 'division_name', 'next_nik', 'comment'], 'string'],
            [['corrective_id', 'mandatory_id', 'industry_id', 'location_id', 'region_id', 'business_unit_id', 'division_id', 'order_number', 'is_deleted'], 'integer'],
            [['date_progress', 'date_due', 'date_updated', 'date_created'], 'safe'],
            [['business_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditBusinessUnit::className(), 'targetAttribute' => ['business_unit_id' => 'id']],
            [['division_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditDivision::className(), 'targetAttribute' => ['division_id' => 'id']],
            [['industry_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditIndustry::className(), 'targetAttribute' => ['industry_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditRegion::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'finding_id' => 'Finding ID',
            'auditor_nik' => 'Auditor Nik',
            'auditor_name' => 'Auditor Name',
            'auditor_position' => 'Auditor Position',
            'pic_nik' => 'Pic Nik',
            'pic_name' => 'Pic Name',
            'pic_position' => 'Pic Position',
            'finding' => 'Finding',
            'finding_file' => 'Finding File',
            'corrective_id' => 'Corrective ID',
            'corrective_action' => 'Corrective Action',
            'latest_progress' => 'Latest Progress',
            'progress_file' => 'Progress File',
            'status' => 'Status',
            'lead_time_status' => 'Lead Time Status',
            'mandatory_id' => 'Mandatory ID',
            'mandatory_name' => 'Mandatory Name',
            'industry_id' => 'Industry ID',
            'industry_name' => 'Industry Name',
            'location_id' => 'Location ID',
            'location_name' => 'Location Name',
            'region_id' => 'Region ID',
            'region_name' => 'Region Name',
            'business_unit_id' => 'Business Unit ID',
            'business_unit_name' => 'Business Unit Name',
            'division_id' => 'Division ID',
            'division_name' => 'Division Name',
            'order_number' => 'Order Number',
            'next_nik' => 'Next Nik',
            'date_progress' => 'Date Progress',
            'date_due' => 'Date Due',
            'comment' => 'Comment',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessUnit()
    {
        return $this->hasOne(TbAuditBusinessUnit::className(), ['id' => 'business_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivision()
    {
        return $this->hasOne(TbAuditDivision::className(), ['id' => 'division_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustry()
    {
        return $this->hasOne(TbAuditIndustry::className(), ['id' => 'industry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(TbAuditLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(TbAuditRegion::className(), ['id' => 'region_id']);
    }
}
