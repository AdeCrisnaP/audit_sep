<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_pica_finding".
 *
 * @property integer $id
 * @property string $number
 * @property string $repeat_finding
 * @property string $finding
 * @property string $finding_file
 * @property string $extension_file
 * @property string $auditor_nik
 * @property string $auditor_name
 * @property string $auditor_position
 * @property string $team_leader_nik
 * @property string $team_leader_name
 * @property string $team_leader_position
 * @property integer $rating_id
 * @property string $rating_name
 * @property integer $category_id
 * @property string $category_name
 * @property integer $industry_id
 * @property string $industry_name
 * @property integer $location_id
 * @property string $location_name
 * @property integer $region_id
 * @property string $region_name
 * @property integer $business_unit_id
 * @property string $business_unit_name
 * @property integer $division_id
 * @property string $division_name
 * @property string $date_posted
 * @property string $date_due
 * @property string $date_extension
 * @property string $date_extension_request
 * @property string $status
 * @property string $lead_time_status
 * @property integer $is_printed
 * @property integer $order_number
 * @property string $next_nik
 * @property string $coordinator_comment
 * @property string $div_head_comment
 * @property string $comment
 * @property string $supervisor_comment
 * @property string $date_closed
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 * @property integer $is_extended
 * @property string $approval
 *
 * @property TbAuditBusinessUnit $businessUnit
 * @property TbAuditCategory $category
 * @property TbAuditDivision $division
 * @property TbAuditIndustry $industry
 * @property TbAuditLocation $location
 * @property TbAuditRegion $region
 */
class TbAuditPicaFinding extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_pica_finding';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'auditor_nik', 'auditor_name', 'auditor_position', 'team_leader_nik', 'team_leader_name', 'team_leader_position', 'rating_id', 'rating_name', 'category_id', 'category_name', 'industry_id', 'industry_name', 'location_id', 'location_name', 'region_id', 'region_name', 'business_unit_id', 'business_unit_name', 'division_id', 'division_name'], 'required'],
            [['number', 'repeat_finding', 'finding', 'finding_file', 'extension_file', 'auditor_nik', 'auditor_name', 'auditor_position', 'team_leader_nik', 'team_leader_name', 'team_leader_position', 'rating_name', 'category_name', 'industry_name', 'location_name', 'region_name', 'business_unit_name', 'division_name', 'status', 'lead_time_status', 'next_nik', 'coordinator_comment', 'div_head_comment', 'comment', 'supervisor_comment', 'approval'], 'string'],
            [['rating_id', 'category_id', 'industry_id', 'location_id', 'region_id', 'business_unit_id', 'division_id', 'is_printed', 'order_number', 'is_deleted', 'is_extended'], 'integer'],
            [['date_posted', 'date_due', 'date_extension', 'date_extension_request', 'date_closed', 'date_updated', 'date_created'], 'safe'],
            [['business_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditBusinessUnit::className(), 'targetAttribute' => ['business_unit_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['division_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditDivision::className(), 'targetAttribute' => ['division_id' => 'id']],
            [['industry_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditIndustry::className(), 'targetAttribute' => ['industry_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditRegion::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'lha_number' => 'LHA Number',
            'type_audit_id' => 'Type Audit ID', 
            'type_audit' => 'Type Audit',
            'repeat_finding' => 'Repeat Finding',
            'finding' => 'Finding',
            'finding_file' => 'Finding File',
            'extension_file' => 'Extension File',
            'auditor_nik' => 'Auditor Nik',
            'auditor_name' => 'Auditor Name',
            'auditor_position' => 'Auditor Position',
            'team_leader_nik' => 'Team Leader Nik',
            'team_leader_name' => 'Team Leader Name',
            'team_leader_position' => 'Team Leader Position',
            'rating_id' => 'Rating ID',
            'rating_name' => 'Rating Name',
            'category_id' => 'Category ID',
            'category_name' => 'Category Name',
            'industry_id' => 'Industry ID',
            'industry_name' => 'Industry Name',
            'location_id' => 'Location ID',
            'location_name' => 'Location Name',
            'region_id' => 'Region ID',
            'region_name' => 'Region Name',
            'business_unit_id' => 'Business Unit ID',
            'business_unit_name' => 'Business Unit Name',
            'division_id' => 'Division ID',
            'division_name' => 'Division Name',
            'date_posted' => 'Date Posted',
            'date_due' => 'Date Due',
            'date_extension' => 'Date Extension',
            'date_extension_request' => 'Date Extension Request',
            'status' => 'Status',
            'lead_time_status' => 'Lead Time Status',
            'is_printed' => 'Is Printed',
            'order_number' => 'Order Number',
            'next_nik' => 'Next Nik',
            'coordinator_comment' => 'Coordinator Comment',
            'div_head_comment' => 'Div Head Comment',
            'comment' => 'Comment',
            'supervisor_comment' => 'Supervisor Comment',
            'date_closed' => 'Date Closed',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
            'is_extended' => 'Is Extended',
            'approval' => 'Approval',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessUnit()
    {
        return $this->hasOne(TbAuditBusinessUnit::className(), ['id' => 'business_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(TbAuditCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivision()
    {
        return $this->hasOne(TbAuditDivision::className(), ['id' => 'division_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustry()
    {
        return $this->hasOne(TbAuditIndustry::className(), ['id' => 'industry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(TbAuditLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(TbAuditRegion::className(), ['id' => 'region_id']);
    }
}
