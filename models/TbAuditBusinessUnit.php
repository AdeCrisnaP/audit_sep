<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_business_unit".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $industry_id
 * @property integer $location_id
 * @property integer $region_id
 * @property string $user_nik
 * @property string $user_name
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 *
 * @property TbAuditIndustry $industry
 * @property TbAuditLocation $location
 * @property TbAuditRegion $region
 * @property TbAuditPicaCorrective[] $tbAuditPicaCorrectives
 * @property TbAuditPicaFinding[] $tbAuditPicaFindings
 * @property TbAuditPicaHeader[] $tbAuditPicaHeaders
 * @property TbAuditPicaProgress[] $tbAuditPicaProgresses
 * @property TbAuditWorkflow[] $tbAuditWorkflows
 */
class TbAuditBusinessUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_business_unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'industry_id', 'location_id', 'region_id', 'user_nik', 'user_name'], 'required'],
            [['name', 'code', 'user_nik', 'user_name'], 'string'],
            [['industry_id', 'location_id', 'region_id', 'is_deleted'], 'integer'],
            [['date_updated', 'date_created'], 'safe'],
            [['code', 'industry_id', 'location_id', 'name', 'region_id'], 'unique', 'targetAttribute' => ['code', 'industry_id', 'location_id', 'name', 'region_id'], 'message' => 'The combination of Name, Code, Industry ID, Location ID and Region ID has already been taken.'],
            [['industry_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditIndustry::className(), 'targetAttribute' => ['industry_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditRegion::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'industry_id' => 'Industry ID',
            'location_id' => 'Location ID',
            'region_id' => 'Region ID',
            'user_nik' => 'User Nik',
            'user_name' => 'User Name',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustry()
    {
        return $this->hasOne(TbAuditIndustry::className(), ['id' => 'industry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(TbAuditLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(TbAuditRegion::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditPicaCorrectives()
    {
        return $this->hasMany(TbAuditPicaCorrective::className(), ['business_unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditPicaFindings()
    {
        return $this->hasMany(TbAuditPicaFinding::className(), ['business_unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditPicaHeaders()
    {
        return $this->hasMany(TbAuditPicaHeader::className(), ['business_unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditPicaProgresses()
    {
        return $this->hasMany(TbAuditPicaProgress::className(), ['business_unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditWorkflows()
    {
        return $this->hasMany(TbAuditWorkflow::className(), ['business_unit_id' => 'id']);
    }
}
