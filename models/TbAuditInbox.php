<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_inbox".
 *
 * @property integer $id
 * @property string $pica_number
 * @property string $comment
 * @property string $approval_status
 * @property string $from_user_nik
 * @property string $from_user_name
 * @property string $from_user_position
 * @property integer $industry_id
 * @property string $industry_name
 * @property integer $location_id
 * @property string $location_name
 * @property integer $region_id
 * @property string $region_name
 * @property integer $business_unit_id
 * @property string $business_unit_name
 * @property integer $division_id
 * @property string $division_name
 * @property integer $order_number
 * @property string $to_user_nik
 * @property string $to_user_name
 * @property string $to_user_position
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 */
class TbAuditInbox extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_inbox';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pica_number', 'approval_status', 'from_user_nik', 'from_user_name', 'from_user_position', 'industry_id', 'industry_name', 'location_id', 'location_name', 'region_id', 'region_name', 'business_unit_id', 'business_unit_name', 'division_id', 'division_name', 'to_user_nik', 'to_user_name', 'to_user_position'], 'required'],
            [['pica_number', 'comment', 'approval_status', 'from_user_nik', 'from_user_name', 'from_user_position', 'industry_name', 'location_name', 'region_name', 'business_unit_name', 'division_name', 'to_user_nik', 'to_user_name', 'to_user_position'], 'string'],
            [['industry_id', 'location_id', 'region_id', 'business_unit_id', 'division_id', 'order_number', 'is_deleted'], 'integer'],
            [['date_updated', 'date_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pica_number' => 'Pica Number',
            'comment' => 'Comment',
            'approval_status' => 'Approval Status',
            'from_user_nik' => 'From User Nik',
            'from_user_name' => 'From User Name',
            'from_user_position' => 'From User Position',
            'industry_id' => 'Industry ID',
            'industry_name' => 'Industry Name',
            'location_id' => 'Location ID',
            'location_name' => 'Location Name',
            'region_id' => 'Region ID',
            'region_name' => 'Region Name',
            'business_unit_id' => 'Business Unit ID',
            'business_unit_name' => 'Business Unit Name',
            'division_id' => 'Division ID',
            'division_name' => 'Division Name',
            'order_number' => 'Order Number',
            'to_user_nik' => 'To User Nik',
            'to_user_name' => 'To User Name',
            'to_user_position' => 'To User Position',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
        ];
    }
}
