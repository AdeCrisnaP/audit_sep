<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_region".
 *
 * @property integer $id
 * @property string $name
 * @property integer $industry_id
 * @property integer $location_id
 * @property string $user_nik
 * @property string $user_name
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 *
 * @property TbAuditBusinessUnit[] $tbAuditBusinessUnits
 * @property TbAuditPicaCorrective[] $tbAuditPicaCorrectives
 * @property TbAuditPicaFinding[] $tbAuditPicaFindings
 * @property TbAuditPicaHeader[] $tbAuditPicaHeaders
 * @property TbAuditPicaProgress[] $tbAuditPicaProgresses
 * @property TbAuditIndustry $industry
 * @property TbAuditLocation $location
 * @property TbAuditWorkflow[] $tbAuditWorkflows
 */
class TbAuditRegion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'industry_id', 'location_id', 'user_nik', 'user_name'], 'required'],
            [['name', 'user_nik', 'user_name'], 'string'],
            [['industry_id', 'location_id', 'is_deleted'], 'integer'],
            [['date_updated', 'date_created'], 'safe'],
            [['industry_id', 'location_id', 'name'], 'unique', 'targetAttribute' => ['industry_id', 'location_id', 'name'], 'message' => 'The combination of Name, Industry ID and Location ID has already been taken.'],
            [['industry_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditIndustry::className(), 'targetAttribute' => ['industry_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'industry_id' => 'Industry ID',
            'location_id' => 'Location ID',
            'user_nik' => 'User Nik',
            'user_name' => 'User Name',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditBusinessUnits()
    {
        return $this->hasMany(TbAuditBusinessUnit::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditPicaCorrectives()
    {
        return $this->hasMany(TbAuditPicaCorrective::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditPicaFindings()
    {
        return $this->hasMany(TbAuditPicaFinding::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditPicaHeaders()
    {
        return $this->hasMany(TbAuditPicaHeader::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditPicaProgresses()
    {
        return $this->hasMany(TbAuditPicaProgress::className(), ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustry()
    {
        return $this->hasOne(TbAuditIndustry::className(), ['id' => 'industry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(TbAuditLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditWorkflows()
    {
        return $this->hasMany(TbAuditWorkflow::className(), ['region_id' => 'id']);
    }
}
