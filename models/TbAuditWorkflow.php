<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_workflow".
 *
 * @property integer $id
 * @property integer $order_number
 * @property string $actor_nik
 * @property string $actor_name
 * @property string $actor_position
 * @property string $approval_name
 * @property string $approval_status
 * @property integer $industry_id
 * @property integer $location_id
 * @property integer $region_id
 * @property integer $business_unit_id
 * @property integer $division_id
 * @property string $user_nik
 * @property string $user_name
 * @property integer $is_approved
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 *
 * @property TbAuditBusinessUnit $businessUnit
 * @property TbAuditDivision $division
 * @property TbAuditIndustry $industry
 * @property TbAuditLocation $location
 * @property TbAuditRegion $region
 */
class TbAuditWorkflow extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_workflow';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_number', 'industry_id', 'location_id', 'region_id', 'business_unit_id', 'division_id', 'is_approved', 'is_deleted'], 'integer'],
            [['actor_nik', 'actor_name', 'actor_position', 'approval_name', 'approval_status', 'industry_id', 'location_id', 'region_id', 'business_unit_id', 'division_id', 'user_nik', 'user_name'], 'required'],
            [['actor_nik', 'actor_name', 'actor_position', 'approval_name', 'approval_status', 'user_nik', 'user_name'], 'string'],
            [['date_updated', 'date_created'], 'safe'],
            [['business_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditBusinessUnit::className(), 'targetAttribute' => ['business_unit_id' => 'id']],
            [['division_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditDivision::className(), 'targetAttribute' => ['division_id' => 'id']],
            [['industry_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditIndustry::className(), 'targetAttribute' => ['industry_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditLocation::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => TbAuditRegion::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_number' => 'Order Number',
            'actor_nik' => 'Actor Nik',
            'actor_name' => 'Actor Name',
            'actor_position' => 'Actor Position',
            'approval_name' => 'Approval Name',
            'approval_status' => 'Approval Status',
            'industry_id' => 'Industry ID',
            'location_id' => 'Location ID',
            'region_id' => 'Region ID',
            'business_unit_id' => 'Business Unit ID',
            'division_id' => 'Division ID',
            'user_nik' => 'User Nik',
            'user_name' => 'User Name',
            'is_approved' => 'Is Approved',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessUnit()
    {
        return $this->hasOne(TbAuditBusinessUnit::className(), ['id' => 'business_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDivision()
    {
        return $this->hasOne(TbAuditDivision::className(), ['id' => 'division_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustry()
    {
        return $this->hasOne(TbAuditIndustry::className(), ['id' => 'industry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(TbAuditLocation::className(), ['id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(TbAuditRegion::className(), ['id' => 'region_id']);
    }
}
