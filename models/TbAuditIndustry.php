<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tb_audit_industry".
 *
 * @property integer $id
 * @property string $name
 * @property string $user_nik
 * @property string $user_name
 * @property string $date_updated
 * @property string $date_created
 * @property integer $is_deleted
 *
 * @property TbAuditBusinessUnit[] $tbAuditBusinessUnits
 * @property TbAuditDivision[] $tbAuditDivisions
 * @property TbAuditPicaCorrective[] $tbAuditPicaCorrectives
 * @property TbAuditPicaFinding[] $tbAuditPicaFindings
 * @property TbAuditPicaHeader[] $tbAuditPicaHeaders
 * @property TbAuditPicaProgress[] $tbAuditPicaProgresses
 * @property TbAuditRegion[] $tbAuditRegions
 * @property TbAuditWorkflow[] $tbAuditWorkflows
 */
class TbAuditIndustry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tb_audit_industry';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user_nik', 'user_name'], 'required'],
            [['name', 'user_nik', 'user_name'], 'string'],
            [['date_updated', 'date_created'], 'safe'],
            [['is_deleted'], 'integer'],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'user_nik' => 'User Nik',
            'user_name' => 'User Name',
            'date_updated' => 'Date Updated',
            'date_created' => 'Date Created',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditBusinessUnits()
    {
        return $this->hasMany(TbAuditBusinessUnit::className(), ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditDivisions()
    {
        return $this->hasMany(TbAuditDivision::className(), ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditPicaCorrectives()
    {
        return $this->hasMany(TbAuditPicaCorrective::className(), ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditPicaFindings()
    {
        return $this->hasMany(TbAuditPicaFinding::className(), ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditPicaHeaders()
    {
        return $this->hasMany(TbAuditPicaHeader::className(), ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditPicaProgresses()
    {
        return $this->hasMany(TbAuditPicaProgress::className(), ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditRegions()
    {
        return $this->hasMany(TbAuditRegion::className(), ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTbAuditWorkflows()
    {
        return $this->hasMany(TbAuditWorkflow::className(), ['industry_id' => 'id']);
    }
}
